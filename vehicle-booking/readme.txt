=== Simontaxi - Vehicle Booking  ===
Author URI: https://digisamaritan.com/
Plugin URI: http://simontaxi.conquerorstech.com/
Tags: airport booking, cab wordpress, hourly rental, paypal booking, payu booking, stripe booking, rental theme, responsive theme, taxi booking, vehicle booking
Requires at least: 4.4
Tested up to: 4.9
Stable Tag: 2.0.7
License: GNU Version 2 or Any Later Version

The easiest way to book a vehicle with WordPress.

== Description ==

Simontaxi - Vehicle Booking is a complete booking solution for booking digital products on WordPress. Learn more at [simontaxi.conquerorstech.com] (https://simontaxi.conquerorstech.com/).

= Get Booked effortlessly =

Whether it is Car, Ambulance, Bicycle, Motor Car, Tractor, Sports Car, Motor Scooter, Motor Cycle, Mountain Bike, Lorry, Crane, Buggy, Bulldozer, Van, Taxi, Bus, Auto, Auto Rikshaw, Truck, Diesel Truck, Delivery Truck, Boat, Sailboat, Fishing Boat, Cargo Ship, Ship, Hot-air Balloon etc. Simontaxi - Vehicle Booking provides a complete system for effortlessly book your vehicle.

= Localized for your language =

Simontaxi - Vehicle Booking is ready to translate plugin.

= Payment gateways for everyone =

The internet has made it possible for anyone to see their products to a world-wide audience. No matter where you live, Simontaxi - Vehicle Booking can work for you. We offer integrations for the most common merchant processors and, through 3rd party extensions, support for many, many more as well.

Payment gateways supported in the core, free plugin:

* PayPal Standard
* Payu Payments
* Byhand (Offline)

Payment gateways supported through free or premium extension:

* Stripe

We are coming with many more extensions on demand.

= Built with developers in mind =

Extensible, adaptable -- Simontaxi - Vehicle Booking is created with developers in mind, which is extendable with their own actions.

== Installation ==

1. Activate the plugin
2. Go to Vehicles > Settings and configure the options
3. Create Vehicles from the Vehicles > Add New page
4. For detailed setup instructions, visit the official [Documentation](https://simontaxi.conquerorstech.com/documentation/) page.

== Frequently Asked Questions ==

= Where can I find complete documentation? =

Full docs with screen shots can be found at [https://simontaxi.conquerorstech.com/documentation/)

= Where can I ask for help? =

You can submit a support ticket or pre-sale question from our [support page](https://themeforest.net/item/simontaxi-taxi-booking-wordpress-theme/19978212/comments) at anytime.

If you already buy the plugin you can rise support ticket on https://support.conquerorstech.com/

= Is an SSL certificate required? =

Simontaxi - Vehicle Booking can function without one just fine, making it easy to set up in a testing or development environment.  We still strongly recommend you have an SSL certificate for your production web site, both for security and for the peace of mind of your customers. 

= What themes work with Simontaxi - Vehicle Booking? =

Any properly written theme will work with Simontaxi - Vehicle Booking with minor CSS modifications.


= Is there a sample import file I can use to setup my site? =

Yes! Simply 'One Click Demo' Plugin, then navigate to 'Appearance > Import Demo Data' and click on 'Simontaxi Demo Data + Vehicles'. This will create several sample vehicles and plugin pages for you.


= My bookings are being marked as "new" =

There are several reasons this happens. Please see the settings (Booking Status when payment success)  [here](https://simontaxi.conquerorstech.com/settings-payment-gateways/).

= Getting a 404 error? =

To get rid of the 404 error when viewing a vehicle details, you need to resave your permalink structure. Go to Settings > Permalinks and click "Save Changes".

= How do I show the user’s booking history? =

Users can login to the system and can find their booking history, profile settings, support tickets.

= How do I display my vehicle? =

Simply Create your vehicle https://simontaxi.conquerorstech.com/vehicle-add/ and it will show front end for booking.


= Can customers book a vehicle without using PayPal? =

Yes. Simontaxi - Vehicle Booking also includes default support for OFFLINE if admin enable it.


== Changelog ==
= 2.0.7 - January 23, 2018 =
* Feature: To send user registration email
* Feature: Send additional information in booking success email
* Feature: Option for admin to alert the user for each change in admin
= 2.0.6 - December 30, 2017 =
* Feature: Option to add additional Luggage (ex: 2 Large+1 Small)
* Feature: Minimum distance applicable
* Feature: Quick Settings Link on Plugin Page
* Feature: Option to get Flight Arrival time from customer
* Feature: Bank Transfer Payment
* Feature: Option to work the system based on number of persons
* Feature: Option to edit payment details in admin
* Feature: Option to edit booking details in admin
* Tweak: Predefined Charges issue fixed. If customer travelled more than what admin specifies then we are calculating the additional price based on unit price
* Tweak: Option to enter distance in float numbers
* Tweak: No. of vehicles in a category in featured vehicles
* Tweak: mobile field required validations not showing (*)
* Fix: Sidebar options Error
* Fix: Wrong calculation if the system is in 'miles'
* Fix: Luggage Symbol Issue(Symbol Not Showing on Front End, Showing Text (Small, Large, Kilogramme))
* Fix: If disable the P2P transfer, it is Still showing on the front end. It is Not going away.
* Fix: String Translation issues in Booking Form (Tabs & breadcrumbs)
* Fix: Fixed float amount issue
* Fix: Few minor issues

= 2.0.5 - October 26, 2017 =
* Feature: Sidebar options Enable OR Disable
* Fix: Local variable issue fixed

= 2.0.4 - October 21, 2017 =
* Feature: Option to change admin logo
* Feature: Option not to display fare to user in step2
* Feature: Option Enable OR disable Default Login Menu Item 
* Feature: Option To change Payment Success OR Failed Messages from admin
* Feature: Bank Transfer Payment
* Tweak: Added missing page for vehicle booking
* Tweak: Visual Composer components organized display
* Fix: Warning in admin settings Page

= 2.0.3 - September 26, 2017 =
* Fix: Warning in admin settings Page

= 2.0.2 - September 22, 2017 =
* Fix: Vehicles -> Settings -> Currency -> The settings are not saved correctly...always jump back to left currency position...
* Tweak: Vehicles -> Settings -> Currency -> add an option for how many decimal places...now there are 4 decimal places here in Europe standard is 2 decimal places and also an option if there is comma or dot as separator...
* Tweak: Improved additional charges
* Tweak: Change the way of displaying currency in admin
* Feature: Vehicles -> Settings -> Optional Fields -> Booking Step3 -> add an option for field "company name" (no display;required;optional)
* Feature: Vehicles -> Settings -> Billing -> add an option to hide or show "invoice" column at the "Booking History" on frontend
* Feature: Google maps to select Region
* Tweak: Change the system to use for different vehicle transport like Airport, Railway Station, Bus Station etc.
* Feature: Improved Drivers module
* Feature: Option to change main loader and Ajax loader

= 2.0.1 - September 08, 2017 =
* Tweak: Terms and Conditions Check Box issue(Its showing as Question Mark(?).
* Tweak: Simontaxi Loading and Optimization.
* Fix: When you select two way option in the 2nd tab which is selecting the vehicle in which the cost shows only one-way option, where as it should show two-way option which is bug.
* Fix: In Manage Bookings Message to customer (Text is not going with mail).
* Fix: PayU Amount issues fixed (When user trying to pay amount greater than 50000 it is not taking so put restriction that if amount greater than 50000 we are not displaying PayU in Payment gateways!).

= 2.0.0 - August 08, 2017 =

* Feature: Support for external payment gateways - Stripe Payment Gateway,PayPal and PayuIndia.
* Feature: Catch the powerful Sessions.
* Feature: Added the support for pickup and drop-off locations Google or predefined.
* Feature: Added new feature to restrict the region
* Feature: Extensions for the Vehicle Booking Plugin (Vehicles->Manage Extensions) (Stripe gateway as an extension to Plugin, Drivers Plugin, Auto updater Plugin).
* Feature: Region Restriction.
* Feature: Number of vehicles restriction.
* Feature: Pickup and Drop-off Restriction.
* Feature: Permissions extended for each role.
* Tweak: Implementation of actions for developers in mind.
* Tweak: Provision for future expansion

= 1.0.0 - July 24, 2017 =

* Simontaxi - Vehicle Booking is born. This release was only available via download at https://themeforest.net/item/simontaxi-taxi-booking-wordpress-theme/19978212. We launched on themeforest.