<?php
/**
 * Plugin Name: Simontaxi - Vehicle Booking System
 * Plugin URI:http://logicsbuffer.com
 * Description: This plugin create vehicle custom post type, some meta option and widgets. Implements the vehicle booking system. Vehicle Booking System is developed and customized for commercial fleet owners and organizations. Its modules support most type of vehicles (passenger, Truck, construction and other commercial vehicles). Keep accurate records for any type of vehicle. Help you plan annual vehicle budgets faster.
 * Version: 2.0.7
 * Text Domain: LogicsBuffer
 * Author: Logics Buffer
 * Author URI: http://logicsbuffer.com
 * Requires at least: 4.4
 * Tested up to: 4.9
 *
 * @package STVB
 * @author Digisamaritan
 * @version 2.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Plugin URL.
define( 'SIMONTAXI_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Plugin Path.
define( 'SIMONTAXI_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

// Plugin Slug.
if ( ! defined( 'SIMONTAXI_SLUG' ) ) {
	define( 'SIMONTAXI_SLUG', 'vehicle' );
}

// Plugin Slug.
if ( ! defined( 'SIMONTAXI_PLUGIN_ID' ) ) {
	define( 'SIMONTAXI_PLUGIN_ID', 'vehicle-booking/index.php' );
}

// Plugin Version.
if ( ! defined( 'SIMONTAXI_VERSION' ) ) {
	define( 'SIMONTAXI_VERSION', '2.0.7' );
}

// Plugin Mode of running.
if ( ! defined( 'SIMONTAXI_SCRIPT_DEBUG' ) ) {
	define( 'SIMONTAXI_SCRIPT_DEBUG', false );
}

// Plugin Root File.
if ( ! defined( 'SIMONTAXI_PLUGIN_FILE' ) ) {
	define( 'SIMONTAXI_PLUGIN_FILE', __FILE__ );
}

// Plugin Root File.
if ( ! defined( 'SIMONTAXI_TEMPLATE_DEBUG_MODE' ) ) {
	define( 'SIMONTAXI_TEMPLATE_DEBUG_MODE', false );
}

require_once 'booking.php';

function simontaxi_plugin_add_settings_link( $links ) {
    $settings_link = '<a href="edit.php?post_type=vehicle&page=vehicle_settings">' . __( 'Settings' ) . '</a>';
    array_push( $links, $settings_link );
  	return $links;
}
$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'simontaxi_plugin_add_settings_link' );
