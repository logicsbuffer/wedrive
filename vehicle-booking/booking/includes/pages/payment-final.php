<?php
/**
 * This template is used to display the 'payment-final'
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  payment-final
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php $payment_success_message_offline = simontaxi_get_option( 'payment_success_message_offline', '' );
?>
<?php $payment_success_message_online_success = simontaxi_get_option( 'payment_success_message_online_success', '' ); ?>
<?php $payment_success_message_online_failed = simontaxi_get_option( 'payment_success_message_online_failed', '' ); ?>
<?php
$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
if ( 'success' === $payment_status ) : ?>
<div class="st-section">
    <div class="">
        <div class="">
        <div class="st-invoice">
            <?php if ( $success ) { ?>
            <div class="alert alert-success" >
				<?php if ( ( 'byhand' === $selected_payment_method || 'banktransfer' === $selected_payment_method ) && '' !== $payment_success_message_offline ) : ?>
					<?php echo sprintf( $payment_success_message_offline, $booking_step1['reference'] ); ?>
				<?php elseif ( '' !== $payment_success_message_online_success ) : ?>
					<?php echo sprintf( $payment_success_message_online_success, $booking_step1['reference'] ); ?>
				<?php else : ?>
				<i class="fa fa-check-circle" aria-hidden="true"></i><p><?php esc_html_e( 'Hurra ! We have received your booking !', 'simontaxi' );?><br><br><?php echo sprintf( __( 'Let‘s Go ! <br> Your booking reference is <b>%s</b>', 'simontaxi' ), $booking_step1['reference'] );?></p>
				<?php endif; ?>
			</div>

            <?php
            if ( is_user_logged_in() ) {
                if( simontaxi_is_user( 'administrator' ) || simontaxi_is_user( 'executive' ) ) {
                    echo '<meta http-equiv="refresh" content="0;URL=\''.simontaxi_get_bookingsteps_urls( 'manage_bookings' ).'\'" />   ';
                } else {
                    echo '<p style="text-align:center">' . sprintf ( __( 'Go to <a href="%s">Booking History </a> on your dashboard ! ', 'simontaxi' ), simontaxi_get_bookingsteps_urls( 'user_bookings' ) ) . '</p>';
                }
            }
            ?>
            <?php } else { ?>
            <div class="alert alert-danger"><i class="fa fa-times-circle" aria-hidden="true"></i><p><?php echo sprintf( __( 'Some thing went wrong. Click <a href="%s">here</a> to try again', 'simontaxi' ), simontaxi_get_bookingsteps_urls( 'step1' ) );?></p></div>
            <?php
            }?>
        </div>
        </div>
    </div>
</div>
<?php else : ?>
<div class="st-section">
    <div class="">
        <div class="">

<div class="ppayement st-invoice">
    
	<?php if ( 'byhand' === $selected_payment_method || 'banktransfer' === $selected_payment_method ) { ?>
	<div class="alert alert-info">
	<p style="color:#31708f"><?php esc_html__( 'Payment Pending!', 'simontaxi' );?></p>
	<?php } else { ?>
	<div class="alert alert-danger">
	<p style="color:red"><?php esc_html__( 'Payment Failed!', 'simontaxi' );?></p>
	<?php
	}
	if ( 'payu' === $selected_payment_method ) {
		if ( '' !== $payment_success_message_online_failed ) {
			echo $payment_success_message_online_failed;
		} else {
		echo '<h2>' . esc_html__( 'PayU Money payment status is pending', 'simontaxi' ) . '<br/>' . sprintf(esc_html__( 'PayU Money ID: %s(%s)', 'simontaxi' ), $_REQUEST['mihpayid'], $_REQUEST['txnid']) . '<br/>PG: ' . $_REQUEST['PG_TYPE'] . '( ' . $_REQUEST['unmappedstatus'] . ' )<br/>Bank Ref: ' . $_REQUEST['bank_ref_num'] . '( ' .$_REQUEST['mode'] .' )</h2>';
		}
	}
	elseif ( 'byhand' === $selected_payment_method || 'banktransfer' === $selected_payment_method ) {
		if ( '' !== $payment_success_message_offline ) {
			echo sprintf( $payment_success_message_offline, $payment_reference );
		} else {
			echo '<h2><i class="fa fa-hand-pointer-o" aria-hidden="true"></i>' . sprintf( __( 'Your payment in pending. Please contact administrator with payment reference number : <b>%s</b>', 'simontaxi' ), $payment_reference ) . '</h2>';
		}
	} elseif ( 'paypal' === $selected_payment_method ) {
		if ( '' !== $payment_success_message_online_failed ) {
			echo $payment_success_message_online_failed;
		} else {
			echo '<h2><i class="fa fa-hand-pointer-o" aria-hidden="true"></i>' . sprintf( __( 'Your payment in pending. Please contact administrator with payment reference number : <b>%s</b>', 'simontaxi' ), $payment_reference ) . '</h2>';
		}
	} else {		
		echo '<h2><i class="fa fa-times-circle" aria-hidden="true"></i>' . sprintf( __( 'Some thing went wrong. Click <a href="%s">here</a> to try again', 'simontaxi' ), simontaxi_get_bookingsteps_urls( 'step1' ) ) . '</h2>';
	}
	if ( is_user_logged_in() ) {
		echo '<h4>'.sprintf( __( 'Go to <a href="%s"> Booking History </a> on your dashboard !', 'simontaxi' ), simontaxi_get_bookingsteps_urls( 'user_bookings' ) ) . ' </h4>';
	}
	?>
</div>
</div>

</div>
    </div>
</div>
<?php endif; ?>
