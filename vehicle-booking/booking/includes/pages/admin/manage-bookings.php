<?php
/**
 * This template is used to display the 'bookings' for admin / executive
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  manage_bookings
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'admin_menu', 'simontaxi_theme_admin_menu_manage_bookings' );
function simontaxi_theme_admin_menu_manage_bookings() {
   		add_submenu_page( 'edit.php?post_type=vehicle', esc_html__( 'Manage Bookings', 'simontaxi' ),esc_html__( 'Manage Bookings', 'simontaxi' ),'manage_bookings','manage_bookings','manage_bookings' );
}

function manage_bookings() {
?>
<script>
function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}
</script>
<style type="text/css">
	table{font-family: arial; width: 100%; }
	table.booking-status-update {background: white;border:1px solid #e6e6e6;padding:10px;}
	table.booking-status-update th{text-align: left;padding:10px;padding-left: 0px; vertical-align: text-top;}
	table.booking-status-update td{ vertical-align: text-top;}

	.small-gray{color:gray;font-size:11px;}
	.status-count{font-size: 11px;background:transparent;color:white;font-weight:bold;padding:0px 7px;border-radius:100%;display: inline-block;border:1px solid white;}
	.bg-happygreen{background:#5cc550;}
	.bg-cancel{background:gray;}
	.bg-success{background:#44923b;}
	.bg-warning{background:#eab107;}
	.bg-danger{background:#ce4141;}
	.bg-sky{background:#00a0d2;}
	.bg-purple{background:#b35e89;}
	.font-white{color:white !important;min-width:100px;display: inline-block;text-indent:3px;border-radius: 2px;font-family: arial;font-size:13px;}

</style>
<?php

	$displaying_status = (isset( $_REQUEST['status']) ? $_REQUEST['status'] : 'new' );



	if(isset( $_GET['view_status']))
	{
		global $wpdb;
		$bookings = $wpdb->prefix . 'st_bookings';
		$payments = $wpdb->prefix . 'st_payments';

		$booking_id = $_GET['view_status'];

		$sql = "SELECT *, `" . $bookings . "`.`ID` AS booking_id, `" . $bookings . "`.`reference` AS booking_ref FROM `" . $bookings . "` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID` WHERE `" . $bookings . "`.booking_contacts!='' AND `" . $bookings . "`.ID=" . $booking_id;

		$result = $wpdb->get_results( $sql );
		if ( ! empty( $result ) ) {
			$booking =( array ) $result[0]; ?>
			<div class="wrap">
				<div id="icon-users" class="icon32"></div>
				<a style="float:right;" href="<?php echo admin_url( 'admin.php?page=manage_bookings' ); ?>"><?php esc_html_e( 'Back to bookings', 'simontaxi' ); ?></a>
					<h3><?php esc_html_e( 'Booking Details', 'simontaxi' ); ?> <button onclick="printContent( 'booking-div' )"><?php esc_html_e( 'Print', 'simontaxi' ); ?></button></h3>
					<div id="booking-div">

						<table class="booking-status-update">
							<tbody>
							<tr>
								<th><?php esc_html_e( 'Reference : ', 'simontaxi' ); ?></th><td><?php echo $booking['booking_ref']; ?></td>
							</tr>
							<tr>
								<th ><?php esc_html_e( 'Journey Type : ', 'simontaxi' ); ?></th><td><?php echo strtoupper( str_replace( '_', ' ', $booking['journey_type'])) ; ?></td>
							</tr>
							<tr>
								<th ><?php esc_html_e( 'Booking Type : ', 'simontaxi' ); ?></th><td><?php echo strtoupper( str_replace( '_', ' ', $booking['booking_type'])) ; ?></td></tr>


							<?php if( $booking['booking_type'] == 'hourly' ) {?>
							<tr><th><?php esc_html_e( 'Booking', 'simontaxi' ); ?></th><td>
								<table>
								<tr><td width='20%'><?php esc_html_e( 'Package Type : ', 'simontaxi' ); ?></td><td><?php echo $booking['hourly_package']; ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'Pickup Location : ', 'simontaxi' ); ?></td><td><?php echo $booking['pickup_location']; ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'Pickup Date : ', 'simontaxi' ); ?> </td><td><?php echo simontaxi_date_format( $booking['pickup_date'] ) . ' ' . $booking['pickup_time']; ?></td></tr>

								</table>
								</td></tr>
							<?php } else { ?>

							<tr>
								<th><?php esc_html_e( 'Booking', 'simontaxi' ); ?></th><td>
								<table>
								<tr><td width='20%'><?php esc_html_e( 'From : ', 'simontaxi' ); ?></td><td><?php echo $booking['pickup_location']; ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'To : ', 'simontaxi' ); ?></td><td><?php echo $booking['drop_location']; ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'Pickup Date : ', 'simontaxi' ); ?> </td><td><?php echo simontaxi_date_format( $booking['pickup_date'] ) . ' ' . $booking['pickup_time']; ?></td></tr>

								</table>
								<?php if( $booking['journey_type'] == 'two_way' ) {?>
								<?php esc_html_e( 'Return journey', 'simontaxi' ); ?>
								<table>
								<tr><td width='20%'><?php esc_html_e( 'From : ', 'simontaxi' ); ?></td><td><?php echo $booking['drop_location']; ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'To : ', 'simontaxi' ); ?></td><td><?php echo $booking['pickup_location']; ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'Pickup Date : ', 'simontaxi' ); ?> </td><td><?php echo simontaxi_date_format( $booking['return_pickup_date'] ) . ' ' . $booking['return_pickup_time']; ?></td></tr>

								</table>
								<?php } ?>
								</td>
							</tr>
							<?php } ?>
							
														
							<?php $contact = (array)json_decode( $booking['booking_contacts']); ?>
							<tr>
								<th><?php esc_html_e( 'Contacts', 'simontaxi' ); ?></th><td>
								<table>
								<?php if ( isset( $contact['full_name'] ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'Name : ', 'simontaxi' ); ?> </td><td><?php echo $contact['full_name']; ?></td></tr>
								<?php } elseif ( isset( $contact['first_name'] ) ) {
									echo $contact['first_name'];
									if ( isset( $contact['last_name'] ) && $contact['last_name'] != '' ) {
										echo ' ' . $contact['last_name'];
									}
								} ?>
								
								<?php if ( isset( $contact['mobile'] ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'Mobile : ', 'simontaxi' ); ?></td><td><?php echo $contact['mobile']; ?></td></tr>
								<?php } ?>
								<tr><td width='20%'><?php esc_html_e( 'Email : ', 'simontaxi' ); ?></td><td><?php echo $contact['email']; ?></td></tr>
								<?php
								if ( ! empty( $contact['company_name'] ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'Company : ', 'simontaxi' ); ?></td><td><?php echo $contact['company_name']; ?></td></tr>
								<?php
								}
								if ( ! empty( $contact['land_mark_pickupaddress'] ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'Landmark / Pickup address : ', 'simontaxi' ); ?></td><td><?php echo $contact['land_mark_pickupaddress']; ?></td></tr>
								<?php
								}
								if ( ! empty( $contact['special_instructions'] ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'Special Instructions : ', 'simontaxi' ); ?></td><td><?php echo $contact['special_instructions']; ?></td></tr>
								<?php
								}
								$persons = '';
								$session_details = json_decode( $booking['session_details'] );								
								if ( ! empty( $session_details ) ) {
									if ( ! empty( $session_details[0]->number_of_persons ) ) { ?>
									<tr><td width='20%'><?php esc_html_e( 'No. of passengers : ', 'simontaxi' ); ?></td><td><?php echo $session_details[0]->number_of_persons; ?></td></tr>
									<?php
									}
								}					
								if ( $persons == '' && ! empty( $contact['no_of_passengers'] ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'No. of passengers : ', 'simontaxi' ); ?></td><td><?php echo $det['no_of_passengers']; ?></td></tr>
								<?php
								}								
								if ( ! empty( $session_details ) ) {
									if ( ! empty( $session_details[0]->flight_no ) ) { ?>
									<tr><td width='20%'><?php esc_html_e( 'Flight No. : ', 'simontaxi' ); ?></td><td><?php echo $session_details[0]->flight_no; ?></td></tr>
									<?php
									}
									if ( ! empty( $session_details[0]->flight_arrival_time ) ) { ?>
									<tr><td width='20%'><?php esc_html_e( 'Arrival Time : ', 'simontaxi' ); ?></td><td><?php echo $session_details[0]->flight_arrival_time; ?></td></tr>
									<?php
									}
								}
								?>
								</table>
								</td>
							</tr>
							
							<?php
							/**
							 * @since 2.0.0
							*/
							?>
							<tr>
								<th><?php esc_html_e( 'Payment Details', 'simontaxi' ); ?></th><td>
								<table>
								
								<tr><td width='20%'><?php esc_html_e( 'Amount Payable : ', 'simontaxi' ); ?> </td><td><?php echo simontaxi_get_currency( $booking['amount_payable'] ); ?></td></tr>
								
								<tr><td width='20%'><?php esc_html_e( 'Amount Paid : ', 'simontaxi' ); ?></td><td><?php echo simontaxi_get_currency( $booking['amount_paid'] ); ?></td></tr>
								
								<tr><td width='20%'><?php esc_html_e( 'Gateway : ', 'simontaxi' ); ?></td><td><?php echo ucfirst( $booking['payment_method'] ); ?></td></tr>
								
								<tr><td width='20%'><?php esc_html_e( 'Reference : ', 'simontaxi' ); ?></td><td><?php echo ( $booking['transaction_reference'] ) ? $booking['transaction_reference'] : $booking['reference']; ?></td></tr>
								
								<tr><td width='20%'><?php esc_html_e( 'Status : ', 'simontaxi' ); ?></td><td><?php echo ucfirst( $booking['payment_status'] ); ?></td></tr>
								
								</table>
								</td>
							</tr>
							
							<?php
							/**
							 * @since 2.0.0
							*/
							$vehicle_details = array();
							$session_details = json_decode( $booking['session_details'] );
							
							if ( ! empty( $session_details ) ) {
								
								foreach ( $session_details as $session ) {
									
									if ( isset( $session->vehicle_details ) ) {
										
										$vehicle_details = $session->vehicle_details;
									}
								}
							}

							if ( ! empty( $vehicle_details ) ) {
							?>
							<tr>
								<th><?php esc_html_e( 'Vehicle Details', 'simontaxi' ); ?></th><td>
								<table>
								
								<?php if( ! empty( $vehicle_details->post_title ) ) { ?>
								<tr><td width='20%'><?php esc_html_e( 'Vehicle : ', 'simontaxi' ); ?> </td><td> 
								<?php if ( ! simontaxi_is_user( 'administrator' ) && ! simontaxi_is_user( 'executive' ) ) { ?>
								<?php echo esc_attr( $vehicle_details->post_title ); ?>
								<?php									
								} else { ?>
								<a href="<?php echo esc_url( $vehicle_details->guid );?>" target="_blank"><?php echo esc_attr( $vehicle_details->post_title ); ?></a>
								<?php } ?>
								</td></tr>
								<?php } ?>
								
								</table>
								</td>
							</tr>
							<?php } ?>
							
							
							<?php do_action( 'simontaxi_vehicle_other_details', $vehicle_details ); ?>
							
							<tr>
								<th><?php esc_html_e( 'Current Status', 'simontaxi' ); ?></th><td>
								<table>
								<tr><td width='20%'><?php esc_html_e( 'Status : ', 'simontaxi' ); ?></td><td><?php echo strtoupper( $booking['status']); ?></td></tr>
								<tr><td width='20%'><?php esc_html_e( 'Time : ', 'simontaxi' ); ?></td><td><?php echo strtoupper( $booking['status_updated']); ?></td></tr>
								</table>
								</td>
							</tr>
							</tbody>
						</table>
						<h3><?php esc_html_e( 'Change Status', 'simontaxi' ); ?></h3>
						<?php
						$status_change = '';
						if( $booking['status']=='new' )
			    		$status_change = '<a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=confirmed&booking_id=' . $booking['booking_id']) . '">' .  esc_html__( 'Confirm', 'simontaxi' ) . '</a> | <a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=cancelled&booking_id=' . $booking['booking_id']) . '">' .  esc_html__( 'Cancel', 'simontaxi' ) . '</a>';
				    	else if( $booking['status']=='confirmed' )
				    		$status_change = '<a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=onride&booking_id=' . $booking['booking_id']) . '">' .  esc_html__( 'Start Ride', 'simontaxi' ) . '</a> | <a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=cancelled&booking_id=' . $booking['booking_id']) . '">' .  esc_html__( 'Cancel', 'simontaxi' ) . '</a>';
				    	elseif( $booking['status']=='onride' )
				    		$status_change = '<a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=success&booking_id=' . $booking['booking_id']) . '">' .  esc_html__( 'Completed', 'simontaxi' ) . '</a> | <a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=cancelled&booking_id=' . $booking['booking_id']) . '">' .  esc_html__( 'Cancel', 'simontaxi' ) . '</a>';
				    	else $status_change = '<span class="small-gray">' .  esc_html__( 'NO ACTIONS', 'simontaxi' ) . '</span>';

				    	echo $status_change ;



						?>
						<a style="float:right;" href="<?php echo admin_url( 'admin.php?page=manage_bookings' ); ?>"><?php esc_html_e( 'Back to bookings', 'simontaxi' ); ?></a>
					</div>

				</div>
			</div>

<?php
		}
	}


	elseif( isset( $_GET['change_status'] ) ) {
		global $wpdb;
		$bookings = $wpdb->prefix . 'st_bookings';
		$payments = $wpdb->prefix . 'st_payments';

		$new_status = $_GET['change_status'];
		$booking_id = $_GET['booking_id'];

		$sql = "SELECT *, `" . $bookings . "`.`ID` AS booking_id, `" . $bookings . "`.`reference` AS booking_ref FROM `" . $bookings . "` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID` WHERE `" . $bookings . "`.booking_contacts!='' AND `" . $bookings . "`.ID=" . $booking_id;

		$result = $wpdb->get_results( $sql);
		if ( !empty( $result) ) {
			$booking=(array)$result[0];
		?>
			<div class="wrap">
				<div id="icon-users" class="icon32"></div>
					<h3><?php esc_html_e( 'Updating Booking Status As :', 'simontaxi' ); ?><b><?php esc_html_e(strtoupper( $new_status), 'simontaxi' ); ?></b></h3>
					<div class="">
						<h4><?php esc_html_e( 'Booking Details : ', 'simontaxi' ); ?></h4>
						<table class="booking-status-update">
							<thead>
								<th><?php esc_html_e( 'Reference', 'simontaxi' ); ?></th>
								<th><?php esc_html_e( 'Booking', 'simontaxi' ); ?></th>
								<th><?php esc_html_e( 'Contacts', 'simontaxi' ); ?></th>
								<th><?php esc_html_e( 'Current Status', 'simontaxi' ); ?></th>
							</thead>
							<tbody>
							<tr>
								<td><?php echo $booking['booking_id'] . '#' . $booking['booking_ref']?></td>
								<td>
								<?php 
								echo $booking['pickup_location'] . '-' . $booking['drop_location'] . '<br>' . simontaxi_date_format( $booking['pickup_date'] ) . ' ' . $booking['pickup_time'];
								
								$session_details = json_decode( $booking['session_details'] );
								  if ( ! empty( $session_details ) ) {
										if ( ! empty( $session_details[0]->distance_text ) ) {
											echo '<br><span class="small-gray">Distance: ' . $session_details[0]->distance_text . '</span>';
										}
									}
								?></td>
								<td><?php $contact = (array)json_decode( $booking['booking_contacts']);
											if ( isset( $contact['full_name']) ) {
												echo esc_attr( $contact['full_name']) . '<br>';
											}elseif ( isset( $contact['first_name']) ) {
												echo $contact['first_name'];
												if ( isset( $contact['last_name']) && $contact['last_name'] != '' ) {
													echo ' ' . $contact['last_name'];
												}
											}

											if ( isset( $contact['mobile']) ) {
												echo esc_attr( $contact['mobile']) . '<br>';
											}
											echo $contact['email'];
											
											/**
											 * @since 2.0.6
											 */
											$str = '';
											if ( ! empty( $contact['company_name'] ) ) {
												$str .= '<br><span class="small-gray">Company: ' . $contact['company_name'] . '</span>';
											}
											if ( ! empty( $contact['land_mark_pickupaddress'] ) ) {
												$str .= '<br><span class="small-gray">Landmark / Pickup address: ' . $contact['land_mark_pickupaddress'] . '</span>';
											}
											if ( ! empty( $contact['special_instructions'] ) ) {
												$str .= '<br><span class="small-gray">Special Instructions: ' . $contact['special_instructions'] . '</span>';
											}
											$persons = '';
											$session_details = json_decode( $booking['session_details'] );
											
											if ( ! empty( $session_details ) ) {
												if ( ! empty( $session_details[0]->number_of_persons ) ) {
													$persons .= '<br><span class="small-gray">No. of passengers: ' . $session_details[0]->number_of_persons . '</span>';
												}
											}					
											if ( $persons == '' && ! empty( $contact['no_of_passengers'] ) ) {
												$persons .= '<br><span class="small-gray">No. of passengers: ' . $contact['no_of_passengers'] . '</span>';
											}
											
											if ( ! empty( $session_details ) ) {
												if ( ! empty( $session_details[0]->flight_no ) ) {
													$persons .= '<br><span class="small-gray">Flight No.: ' . $session_details[0]->flight_no . '</span>';
												}
												if ( ! empty( $session_details[0]->flight_arrival_time ) ) {
													$persons .= '<br><span class="small-gray">Arrival Time: ' . $session_details[0]->flight_arrival_time . '</span>';
												}
											}
											echo $str .= $persons;
											unset( $booking['session_details']); /* No need this information so kill it!!*/
											?></td>
								<td><?php echo esc_html__(strtoupper( $booking['status']), 'simontaxi' ) . '<br>' . $booking['status_updated']; ?></td>
							</tr>
							</tbody>
						</table>
					</div>
					<form action="<?php echo admin_url( 'admin.php?page=manage_bookings' )?>" method="POST">
						<input type="hidden" name="change_status" value="<?php echo $new_status; ?>">
						<input type="hidden" name="booking_id" value="<?php echo $booking_id; ?>">
						<input type="hidden" name="email" value="<?php echo $contact['email']; ?>">
						<input type="hidden" name="fulldet" value="<?php echo json_encode( $booking); ?>">
						<?php
						$selected_vehicle = 0;

						if ( isset( $booking['selected_vehicle'] ) && ( $booking['selected_vehicle']!=0) ) {
							echo '<input type="hidden" name="selected_vehicle" value="' . $booking['selected_vehicle'] . '">';
							$selected_vehicle = $booking['selected_vehicle'];
						}
						?>
						<?php
						if( $new_status=='onride' ) {
							$cabs = get_posts(array( 'post_type'=>'vehicle','post_status'=>'published','include' => array( 'post__in' => $selected_vehicle)));
							
							echo '<h4>' .esc_html__( 'Choose among the available', 'simontaxi' ) . strtoupper( $booking['vehicle_name']) . '!</h4>';

							if ( empty( $cabs ) ) {
								echo esc_html__( 'Sorry , no ', 'simontaxi' ) . simontaxi_get_default_title() . esc_html__( ' is available ! Still you can start ride !', 'simontaxi' );
							} else {
								foreach( $cabs as $cab ) {
									$cab = ( array ) $cab;
									echo '<label for="radio-' . $cab['ID'] . '">
											 <input type="radio" id="radio-' . $cab['ID'] . '" name="selected_vehicle" value="' . $cab['ID'] . '">' .strtoupper( $cab['post_title']) . '
										  </label>';
								}
							}
						}
						?>
						<h4><?php esc_html_e( 'Booking Details:', 'simontaxi' )?></h4>
						<table>
						<tr>
							<td>
							<h5><?php echo simontaxi_get_pickuppoint_title(); ?></h5>
							<textarea name="pickup_location" id="pickup_location" ><?php echo esc_html( $booking['pickup_location'] ); ?></textarea>
							</td>
							<td>
							<h5><?php echo simontaxi_get_dropoffpoint_title(); ?></h5>
							<textarea name="drop_location" id="drop_location" ><?php echo esc_html( $booking['drop_location'] ); ?></textarea>
							</td>
						</tr>
						
						<tr>
							<td>
							<h5><?php echo simontaxi_get_pickupdate_title(); ?></h5>
							<input type="text" class="st_datepicker_limit" name="pickup_date" id="pickup_date" value="<?php echo esc_html( date('d-m-Y', strtotime( $booking['pickup_date']) ) ); ?>" readonly>
							</td>
							<td>
							<h5><?php echo simontaxi_get_pickuptime_title(); ?></h5>
							<?php
							$parts =  explode( ':', $booking['pickup_time'] );
							$hours = ! empty( $parts[0] ) ? trim( $parts[0] ) : 0;
							$minutes = ! empty( $parts[1] ) ? trim( $parts[1] ) : 0;
							?>
							<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_hours" id="pickup_time_hours">
								<option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
								<?php for ( $h = 0; $h <= 23; $h++ ) {
									$val = str_pad( $h,2,0, STR_PAD_LEFT);
									$sel = '';
									if ( $val == $hours)
										$sel = ' selected="selected"';
									echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
								}?>
							</select>
							&nbsp;
							<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_minutes" id="pickup_time_minutes">
								<option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
								<?php for ( $m = 0; $m < 60; $m+=5 ) {
									$val = str_pad( $m,2,0, STR_PAD_LEFT);
									$sel = '';
									if ( $val == $minutes)
										$sel = ' selected="selected"';
									echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
								}?>
							</select>
							</td>
						</tr>
						</table>
						
						<h4><?php esc_html_e( 'Payment Details:', 'simontaxi' )?></h4>
						<table>
						<tr><td>
						<h5><?php esc_html_e( 'Amount Payable:', 'simontaxi' )?></h5>
						<input type="number" name="amount_payable" id="amount_payable" value="<?php echo esc_html( $booking['amount_payable'] ); ?>" step="0.01">
						</td>
						<td>
						<h5><?php esc_html_e( 'Amount Paid:', 'simontaxi' )?></h5>
						<input type="number" name="amount_paid" id="amount_paid" value="<?php echo esc_html( $booking['amount_paid'] ); ?>" step="0.01">
						&nbsp;
						<?php
						$payment_status = $booking['payment_status']
						?>
						<input type="hidden" name="payment_status_current" id="payment_status_current" value="<?php echo esc_html( $payment_status ); ?>">
						<select name="payment_status" id="payment_status">
							<option value=""><?php esc_html_e( 'Change payment status to', 'simontaxi' ); ?></option>
							<option value="success"><?php esc_html_e( 'Paid', 'simontaxi' ); ?></option>
							<option value="cancelled"><?php esc_html_e( 'Cancelled', 'simontaxi' ); ?></option>
							<option value="refunded"><?php esc_html_e( 'Refunded', 'simontaxi' ); ?></option>
							<option value="pending"><?php esc_html_e( 'Pending', 'simontaxi' ); ?></option>
						</select>
						<br><small><?php esc_html_e( 'Current Payment Status: '); echo '<b>' . $payment_status . '</b>'; ?></small>
						</td>
						</tr>
						
						<tr><td>
						<h5><?php esc_html_e( 'Gateway:', 'simontaxi' )?></h5>
						<input type="text" name="payment_method" id="payment_method" value="<?php echo esc_html( $booking['payment_method'] ); ?>" disabled>
						</td>
						<td>
						<h5><?php esc_html_e( 'Current Status:', 'simontaxi' )?></h5>
						<input type="text" value="<?php echo esc_html( $booking['payment_status'] ); ?>" disabled>
						</td>
						</tr>
						</table>
						
						
						<h4><?php esc_html_e( 'Message to customer', 'simontaxi' )?></h4>
						<?php esc_html_e( 'Alert customer about change?', 'simontaxi' ); ?> : <select name="alert_customer" id="alert_customer" required>
							<option value="yes"><?php esc_html_e( 'Yes', 'simontaxi' ); ?></option>
							<option value="no"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
						</select>
						
						<h5><?php esc_html_e( 'Write a message to send an email to customer ! Ignore if no message is need to be sent . ', 'simontaxi' )?></h5>
						<textarea class="wp-editor-area" style="height: 100px;width:100%;" autocomplete="off" cols="40" name="reason_message" id="content"></textarea>
						<input type="submit" class="button button-primary button-large" value="Update" onclick="return validate();"/>
					</form>
				</div>
			</div>
			
			<script type="text/javascript">
				function validate()
				{
					var payment_status = document.getElementById('payment_status').value;
					var content = document.getElementById('content').value;
					<?php if ( 'new' === $booking['status'] && 'pending' == $booking['payment_status'] ) { ?>
					if ( payment_status == '' ) {
						alert( '<?php esc_html_e( 'Please select payment status' ); ?>' );
						return false;
					}
					/*
					if ( content == '' ) {
						alert( '<?php esc_html_e( 'Please enter message' ); ?>' );
						return false;
					}
					*/
					<?php } ?>
				}
				jQuery(document).ready(function ( $ ) {
					$( '.st_datepicker_limit' ).datepicker({
						dateFormat: 'dd-mm-yy'
					});
				});
			</script>
<?php
		}
	}
	else
	{
		if ( isset( $_POST['change_status'] ) ) {
			$new_status = $_POST['change_status'];
			$booking_id = $_POST['booking_id'];
			$reason_message = $_POST['reason_message'];
			$to = $_POST['email'];
			$booking = $_POST['fulldet'];

			global $wpdb;
			$data['status'] = $new_status;
			$data['status_updated'] = date( 'Y-m-d h:i:s' );
			$data['reason_message'] = $reason_message;
			if ( $new_status=='onride' ) {
				$data['selected_vehicle'] = ( isset( $_POST['selected_vehicle'] ) ? $_POST['selected_vehicle'] : 0 );
			}
			
			/**
			 * @since 2.0.6
			 */
			if ( $new_status=='confirmed' ) {
				if ( empty( $_POST['payment_status'] ) ) {
					$payment_status = $_POST['payment_status_current'];
				} else {
					$payment_status = $_POST['payment_status'];
				}
				$payments_data = array(
					'amount_payable' => $_POST['amount_payable'],
					'amount_paid' => $_POST['amount_paid'],
					'payment_status' => $payment_status,
					'payment_status_updated' => date('Y-m-d H:i:s')
				);
				$wpdb->update( $wpdb->prefix  . 'st_payments', $payments_data , array( 'booking_id' => $booking_id ) );
				
				$pickup_time = $_POST['pickup_time_hours'] . ':' . $_POST['pickup_time_minutes'];
				$booking_data = array(
					'pickup_location' => $_POST['pickup_location'],
					'drop_location' => $_POST['drop_location'],
					'pickup_date' => date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) ),
					'pickup_time' => $pickup_time,
				);
				$wpdb->update( $wpdb->prefix  . 'st_bookings', $booking_data , array( 'ID' => $booking_id ) );
			}
			$updated = $wpdb->update( $wpdb->prefix  . 'st_bookings', $data , array( 'ID'=>$booking_id));
			$updated = true;
			$sent = false;
			if ( $updated ) {
				/**
				 * Let us send Email OR SMS based on admin settings
				 * This function will send the Email OR SMS based on admin settings for admin AND|OR user
				*/
				if ( 'yes' === $_POST['alert_customer'] ) {
					$sent = do_action( 'simontaxi_send_email_sms_adminside', $booking_id, $new_status );
				}

				echo '<div id="lc-plugin-activated" class="notice updated lc-plugin-activated is-dismissible">
	                <p>
	                    <b>' . esc_html__( 'Booking Status Updated!', 'simontaxi' ) . '</b>
	                </p>
	            </div>';
			}

			if ( $sent == true ) {
				echo '<div id="lc-plugin-activated" class="notice updated lc-plugin-activated is-dismissible">
	                <p>
	                    <b>' .esc_html__( 'Email set to the customer!', 'simontaxi' ) . '</b>
	                </p>
	            </div>';
			}
	}
	if ( ! class_exists( 'WP_List_Table' ) ) {
	   require_once( ABSPATH . 'age_paymentswp-admin/includes/class-wp-list-table.php' );
	}
	class Bookings_List extends WP_List_Table
	{

		/** Class constructor */
		public function __construct() {

			parent::__construct( array(
				'singular' => esc_html__( 'Booking', 'simontaxi' ), //singular name of the listed records
				'plural'   => esc_html__( 'Bookings', 'simontaxi' ), //plural name of the listed records
				'ajax'     => false //should this table support ajax?

			) );

		}

		/**
		*   get_views] : Frontend Filters
		*
		*
		*/

		protected function get_views() {
		    $status_links = array(
		        "new" => "<a class='' href='".admin_url( 'admin.php?page=manage_bookings&status=new' ) . "'>" . esc_html__( 'New', 'simontaxi' ) . " <span class='status-count bg-danger'>" . $this->record_count( 'new' ) . "</span></a>",
				"confirmed"   => "<a class='' href='".admin_url( 'admin.php?page=manage_bookings&status=confirmed' ) . "'>" . esc_html__( 'Confirmed', 'simontaxi' ) . " <span class='status-count bg-purple'>" . $this->record_count( 'confirmed' ) . "</span></a>",
		        "onride"   => "<a class='' href='".admin_url( 'admin.php?page=manage_bookings&status=onride' ) . "'>" . esc_html__( 'On Ride', 'simontaxi' ) . " <span class='status-count bg-warning'>" . $this->record_count( 'onride' ) . "</span></a>",		        
				"success"   => "<a class='' href='".admin_url( 'admin.php?page=manage_bookings&status=success' ) . "'>" . esc_html__( 'Completed', 'simontaxi' ) . "<span class='status-count bg-happygreen'>" . $this->record_count( 'success' ) . " </span></a>",
				"cancelled"   => "<a class='' href='".admin_url( 'admin.php?page=manage_bookings&status=cancelled' ) . "'>" . esc_html__( 'Cancelled', 'simontaxi' ) . " <span class='status-count bg-cancel'>" . $this->record_count( 'cancelled' ) . "</span></a>",
				"all"       => "<a class='' href='".admin_url( 'admin.php?page=manage_bookings&status=all' ) . "'>" . esc_html__( 'All', 'simontaxi' ) . " <span class='status-count bg-sky'>" . $this->record_count( 'all' ) . "</span></a>"
		    );
		    return $status_links;
		}

		/**
		 * Retrieve Booking data from the database
		 *
		 * @param int $per_page
		 * @param int $page_number
		 *
		 * @return mixed
		 */
		public static function get_bookings( $per_page = 20, $page_number = 1 ) {
			global $wpdb;
			$bookings = $wpdb->prefix . 'st_bookings';
			$payments = $wpdb->prefix . 'st_payments';
			$sql = "SELECT *, `" . $bookings . "`.`ID` AS booking_id, `" . $bookings . "`.`reference` AS booking_ref FROM `" . $bookings . "` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID`";
			if ( ! simontaxi_is_user( 'administrator' ) && ! simontaxi_is_user( 'executive' ) ) {
				$sql .= apply_filters( 'simontaxi_bookings_join_condition', '' );
			}
			$sql .= " WHERE `" . $bookings . "`.booking_contacts!='' ";
			
			if ( isset( $_REQUEST['status']) && ( $_REQUEST['status']!='all' ) ){
				$sql .=" AND `" . $bookings . "`.`status`='" . $_REQUEST['status'] . "'";
			}
			if(! isset( $_REQUEST['status'])) {
					$sql .=" AND `" . $bookings . "`.`status`='new'";
			}

			$search = ( isset( $_REQUEST['s'] ) ) ? $_REQUEST['s'] : false;
			if( $search ) {
				$sql .= " AND (`" . $bookings . "`.`reference` LIKE '%" . $search . "%' OR `" . $bookings . "`.booking_contacts LIKE '%" . $search . "%' OR `" . $bookings . "`.vehicle_name LIKE '%" . $search . "%' ) ";
			}
			if ( ! simontaxi_is_user( 'administrator' ) && ! simontaxi_is_user( 'executive' ) ) {
				$sql .= apply_filters( 'simontaxi_bookings_where_condition', '' );
			}

			if ( ! empty( $_REQUEST['orderby'] ) ) {
				$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
				$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
			} else {
				$sql .= ' ORDER BY ' . $bookings . ' .ID DESC';
			}
			$sql .= " LIMIT $per_page";
			
			$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
			// echo $sql;	
			$result = $wpdb->get_results( $sql, 'ARRAY_A' );

			return $result;
		}

		/**
		 * Delete a Booking record.
		 *
		 * @param int $id booking ID
		 */
		public static function delete_booking( $id ) {
			global $wpdb;
			$wpdb->delete("{$wpdb->prefix}st_payments", array( 'booking_id' => $id ), array( '%d' ) );

			$wpdb->delete("{$wpdb->prefix}st_bookings", array( 'ID' => $id ), array(  '%d' ) );
		}

		/**
		 * Returns the count of records in the database.
		 *
		 * @return null|string
		 */
		public static function record_count( $status='' ) {
			global $wpdb;

			$bookings = $wpdb->prefix . 'st_bookings';
			$payments = $wpdb->prefix . 'st_payments';
						
			$sql = "SELECT COUNT(*) FROM `" . $bookings . "` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID`";
			if ( ! simontaxi_is_user( 'administrator' ) && ! simontaxi_is_user( 'executive' ) ) {
				$sql .= apply_filters( 'simontaxi_bookings_join_condition', '' );
			}
			$sql .= " WHERE `" . $bookings . "`.booking_contacts!='' ";
			
		  if ( $status == '' ) {
		  	if ( isset( $_REQUEST['status']) && ( $_REQUEST['status']!='all' ) ) {
				$sql .= " AND status='" . $_REQUEST['status'] . "'";
			}
		  } elseif( $status != 'all' ) {
		  		$sql .= " AND status='" . $status . "'";
		  }
		  if ( ! simontaxi_is_user( 'administrator' ) && ! simontaxi_is_user( 'executive' ) ) {
			$sql .= apply_filters( 'simontaxi_bookings_where_condition', '' );
		  }
		  return $wpdb->get_var( $sql );
		}
		/** Text displayed when no booking data is available */
		public function no_items() {
		  esc_html_e( 'No Bookings avaliable . ', 'simontaxi' );
		}



		/**
		 * Method for name column
		 *
		 * @param array $item an array of DB data
		 *
		 * @return string
		 */
		function column_name( $item ) {

		  // create a nonce
		  $delete_nonce = wp_create_nonce( 'sp_delete_booking' );

		  $title = '<strong>' . $item['reference'] . '</strong>';

		  $actions = array(
		    'delete' => sprintf( '<a href="?page=%s&action=%s&booking=%s&_wpnonce=%s">' .  esc_html__( 'Delete', 'simontaxi' ) . '</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
		  );

		  return $title . $this->row_actions( $actions );
		}

		/**
		 *  Associative array of columns
		 *
		 * @return array
		 */
		function get_columns() {
		  $columns = array(
		    'cb'      => '<input type="checkbox" />',
		    'ID'    => esc_html__( 'ID #Reference', 'simontaxi' ),
		    'pickup_location'    => esc_html__( 'From - To', 'simontaxi' ),
		    'booking_contacts'    => esc_html__( 'Customer', 'simontaxi' ),
		    'status_updated'    => esc_html__( 'Status', 'simontaxi' ),
		    'payment_id'    => esc_html__( 'Payment', 'simontaxi' ),
		    'change_status'    => esc_html__( 'Change Status', 'simontaxi' )
		  );

		  return $columns;
		}

		/**
		 * Render a column when no column specific method exists.
		 *
		 * @param array $item
		 * @param string $column_name
		 *
		 * @return mixed
		 */
		public function column_default( $item, $column_name ) {

			 switch ( $column_name ) {
			    case 'cb':
			      return $this->column_cb( $item );
			    case 'ID':
			      return ' <span class="small-gray"> ' . $item['booking_id'] . '-' . $item['booking_ref'] . '</span><br><span class="small-gray">' . esc_html__( 'Booking Date : ', 'simontaxi' ) . $item['date'] . '</span><br> <a href="' .admin_url( 'admin.php?page=manage_bookings&view_status=' . $item['booking_id']) . '">' . esc_html__( 'View Details', 'simontaxi' ) . '</a>';
			    case 'pickup_location':
			      $str = $item['pickup_location'] . ' - ' . $item['drop_location'] . '<br><span class="small-gray">' . simontaxi_date_format( $item['pickup_date'] ) . ' ' . $item['pickup_time'] . '</span>';
				  if ( $item['journey_type'] == 'two_way' ) {
					  $return_pickup_date = (isset( $item['return_pickup_date'])) ? simontaxi_date_format( $item['return_pickup_date'] ) : '';
					  $return_pickup_date .= (isset( $item['return_pickup_time'])) ? $item['return_pickup_time'] : '';
					  $str .= '<br>' . $item['drop_location'] . ' - ' . $item['pickup_location'] . '<br><span class="small-gray">' . esc_html__( 'Return : ', 'simontaxi' ) . $return_pickup_date . '</span>';
				  }
				  $str .= '&nbsp<span class="small-gray">' .strtoupper( $item['booking_type'] . ' - ' . $item['journey_type'] . ' - ' . $item['vehicle_name']) . '</span>';
				  
				  $session_details = json_decode( $item['session_details'] );
				  if ( ! empty( $session_details ) ) {
						if ( ! empty( $session_details[0]->distance_text ) ) {
							$str .= '<br><span class="small-gray">Distance: ' . $session_details[0]->distance_text . '</span>';
						}
					}
				  
				  return $str;
			    case 'booking_contacts':
			    	$det = (array)json_decode( $item['booking_contacts']);
					$str = '';
					if ( isset( $det['full_name']) ) {
						$str = ucfirst( $det['full_name']) . '<br><span class="small-gray">';
						if ( isset( $det['mobile']) ) {
							$str .= $det['mobile'] . ' | ';
						}
						$str .= $det['email'] . '</span>';
					} elseif ( isset( $det['first_name']) ) {
						$str = $det['first_name'];
						if ( isset( $det['last_name']) && $det['last_name'] != '' ) {
							$str .= ' ' . $det['last_name'];
						}
						if ( isset( $det['mobile']) ) {
							$str .= '<br><span class="small-gray">' . $det['mobile'] . '</span>';
						}
					}else {
						$str = ucfirst( $det['email']);
						if ( isset( $det['mobile']) ) {
							$str .= '<br><span class="small-gray">' . $det['mobile'] . '</span>';
						}
					}
					
					/**
					 * @since 2.0.6
					 */
					
					if ( ! empty( $det['company_name'] ) ) {
						$str .= '<br><span class="small-gray">Company: ' . $det['company_name'] . '</span>';
					}
					if ( ! empty( $det['land_mark_pickupaddress'] ) ) {
						$str .= '<br><span class="small-gray">Landmark / Pickup address: ' . $det['land_mark_pickupaddress'] . '</span>';
					}
					if ( ! empty( $det['special_instructions'] ) ) {
						$str .= '<br><span class="small-gray">Special Instructions: ' . $det['special_instructions'] . '</span>';
					}
					$persons = '';
					$session_details = json_decode( $item['session_details'] );
					
					if ( ! empty( $session_details ) ) {
						if ( ! empty( $session_details[0]->number_of_persons ) ) {
							$persons .= '<br><span class="small-gray">No. of passengers: ' . $session_details[0]->number_of_persons . '</span>';
						}
					}					
					if ( $persons == '' && ! empty( $det['no_of_passengers'] ) ) {
						$persons .= '<br><span class="small-gray">No. of passengers: ' . $det['no_of_passengers'] . '</span>';
					}
					
					if ( ! empty( $session_details ) ) {
						if ( ! empty( $session_details[0]->flight_no ) ) {
							$persons .= '<br><span class="small-gray">Flight No.: ' . $session_details[0]->flight_no . '</span>';
						}
						if ( ! empty( $session_details[0]->flight_arrival_time ) ) {
							$persons .= '<br><span class="small-gray">Arrival Time: ' . $session_details[0]->flight_arrival_time . '</span>';
						}
					}
					
					$str .= $persons;					
			    	return $str;
			    case 'change_status':
			    	$delete_link = ' | <a href="' .admin_url( 'admin.php?page=manage_bookings&action=delete&booking_id=' . $item['booking_id']) . '" onclick="return confirm(\'' . esc_html__( 'Are you sure?', 'simontaxi' ) . '\' )">' . esc_html__( 'Delete', 'simontaxi' ) . '</a>';
					if ( $item['status'] == 'new' ) {
						return '<a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=confirmed&booking_id=' . $item['booking_id']) . '">' . esc_html__( 'Confirm', 'simontaxi' ) . '</a> | <a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=cancelled&booking_id=' . $item['booking_id']) . '">' . esc_html__( 'Cancel', 'simontaxi' ) . '</a>' . $delete_link;
					} elseif ( $item['status'] == 'confirmed' ) {
						return '<a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=onride&booking_id=' . $item['booking_id']) . '">' . esc_html__( 'Start Ride', 'simontaxi' ) . '</a> | <a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=cancelled&booking_id=' . $item['booking_id']) . '">' . esc_html__( 'Cancel', 'simontaxi' ) . '</a>' . $delete_link;
					} elseif ( $item['status'] == 'onride' ) {
						return '<a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=success&booking_id=' . $item['booking_id']) . '">' . esc_html__( 'Completed', 'simontaxi' ) . '</a> | <a href="' .admin_url( 'admin.php?page=manage_bookings&change_status=cancelled&booking_id=' . $item['booking_id']) . '">' . esc_html__( 'Cancel', 'simontaxi' ) . '</a>' . $delete_link;
					} else {
						return '<span class="small-gray">' . esc_html__( 'NO ACTIONS', 'simontaxi' ) . '</span>';
					}
			    case 'payment_id':
			    	return simontaxi_get_currency( $item['amount_paid'] ) . ' / ' . simontaxi_get_currency( $item['amount_payable'] ) . ' <span class="small-gray">' .ucfirst( $item['payment_method'] ) . '</span><br>' . strtoupper( $item['payment_status'] ) . '<br><span class="small-gray">' . $item['datetime'] . '</span>';
			    case 'status_updated':
			    	return strtoupper( $item['status']) . '<br><span class="small-gray">' . $item['status_updated'] . '</span>';
			    default:
			      return ucfirst( $item[ $column_name ]); //Show the whole array for troubleshooting purposes
			  }

		}

		/**
		 * Render the bulk edit checkbox
		 *
		 * @param array $item
		 *
		 * @return string
		 */
		function column_cb( $item ) {
		 // return sprintf( '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID'] );
		}



		/**
		 * Columns to make sortable.
		 *
		 * @return array
		 */
		public function get_sortable_columns() {
		  $sortable_columns = array(
		    //'ID' => array( 'ID', true ),
		    'status_updated' => array( 'status_updated', false ),
		  );

		  return $sortable_columns;
		}

		/**
		 * Returns an associative array containing the bulk action
		 *
		 * @return array
		 */
		public function get_bulk_actions() {
		  $actions = array(
		    //'bulk-delete' => 'Delete'
		  );

		  return $actions;
		}

		/**
		 * Handles data query and filter, sorting, and pagination.
		 */
		public function prepare_items() {

			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array( $columns, $hidden, $sortable);

		  /** Process bulk action */
		  	$this->process_bulk_action();

		  $per_page     = simontaxi_get_option( 'records_per_page', 20);
		  $current_page = $this->get_pagenum();
		  $total_items  = self::record_count();
		  
		  $this->set_pagination_args( array(
		    'total_items' => $total_items, //WE have to calculate the total number of items
		    'per_page'    => $per_page //WE have to determine how many items to show on a page
		  ) );



		  $this->items = self::get_bookings( $per_page, $current_page );

		 // var_dump( $this->items );
		}

		public function process_bulk_action() {

		  //Detect when a bulk action is being triggered...
		  if ( 'delete' === $this->current_action() ) {
			
			self::delete_booking( absint( $_GET['booking_id'] ) );
			
			wp_redirect( esc_url( add_query_arg() ) );
			
			exit;
		  }

		  // If the delete bulk action is triggered
		  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		       || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		  ) {

		    $delete_ids = esc_sql( $_POST['bulk-delete'] );

		    // loop over the array of record IDs and delete them
		    foreach ( $delete_ids as $id ) {
		      self::delete_booking( $id );

		    }

		    wp_redirect( esc_url( add_query_arg() ) );
		    exit;
		  }
		}

		public function search_box( $text, $input_id ) {

	        $input_id = $input_id . '-search-input';

	        if ( ! empty( $_REQUEST['orderby'] ) ) {
	            echo '<input type="hidden" name="orderby" value="' . esc_attr( $_REQUEST['orderby'] ) . '" />';
			}
	        if ( ! empty( $_REQUEST['order'] ) ) {
	            echo '<input type="hidden" name="order" value="' . esc_attr( $_REQUEST['order'] ) . '" />';
			}
	        if ( ! empty( $_REQUEST['post_mime_type'] ) ) {
	            echo '<input type="hidden" name="post_mime_type" value="' . esc_attr( $_REQUEST['post_mime_type'] ) . '" />';
			}
	        if ( ! empty( $_REQUEST['detached'] ) ) {
	            echo '<input type="hidden" name="detached" value="' . esc_attr( $_REQUEST['detached'] ) . '" />';
			}
			echo '<p class="search-box">
					    <input type="search" id="' . $input_id . '" name="s" value="' .(isset( $_REQUEST['s']) ? $_REQUEST['s'] : '' ) . '" placeholder="' .esc_html__( 'Ex: Booking Ref, Vehicle, Customer Name, Mobile', 'simontaxi' ) . '"/>
					    ' .submit_button( $text, 'button', '', false, array( 'id' => 'search-submit' ,'style'=>'float:right;', 'onClick'=>'location.search+=\'&s=\'+document.getElementById(\'' . $input_id . '\' ).value;' ) ) . '
					</p>';

	    }

	}

	echo '<div class="wrap">
			<div id="icon-users" class="icon32"></div>
			<h2>' . esc_html__( 'Bookings', 'simontaxi' ) . ' <span style="color:gray">' . strtoupper( $displaying_status ) . '</span></h2>';
				$bookings = new Bookings_List();
				$bookings->views();
				$bookings->prepare_items();
				$bookings->search_box( esc_html__( 'Search Booking Reference', 'simontaxi' ), 'simontaxi' );
				$bookings->display();
	echo '	</div>
		  </div>';
}
}
?>