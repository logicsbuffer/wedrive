<?php
/**
 * This template is used to display the 'bookings' for admin / executive
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  manage_extensions
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'admin_menu', 'simontaxi_admin_menu_manage_extensions' );
function simontaxi_admin_menu_manage_extensions() {
    add_submenu_page( 'edit.php?post_type=vehicle', esc_html__( 'Manage Extensions', 'simontaxi' ),esc_html__( 'Manage Extensions', 'simontaxi' ),'manage_extensions','manage_extensions','manage_extensions' );
}

/**
 * Extensions Page
 *
 * Renders the Extensions page content.
 *
 * @since 2.0.0
 * @return void
 */
function manage_extensions() {

    $extensions_tabs = apply_filters( 'simontaxi_extensions_tabs', array( 'all' => esc_html__( 'All', 'simontaxi' ), 'free' => esc_html__( 'Free', 'simontaxi' ), 'paid' => esc_html__( 'Paid', 'simontaxi' ) ) );
    $active_tab = isset( $_GET['tab'] ) && array_key_exists( $_GET['tab'], $extensions_tabs ) ? $_GET['tab'] : 'free';
    $activate = isset( $_GET['activate'] ) ?  $_GET['activate'] : '';
    if( '' !== $activate ) {
        simontaxi_activate_plugin( $activate . '/' . $activate . '.php' );
    }
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    ob_start(); ?>
<div class="wrap" id="simontaxi-extensions">
    <h1>
        <?php esc_html__( 'Extensions for Simontaxi - Vehicle Booking', 'simontaxi' ); ?>
        <span>
            &nbsp;&nbsp;<a href="https://simontaxi.conquerorstech.com/downloads/" class="button-primary" target="_blank"><?php esc_html_e( 'Browse All Extensions', 'simontaxi' ); ?></a>
        </span>
    </h1>
    <table width="100%" class="simontaxi-extensions-layout">
		<tr>
			<td width="33%" align="center">
			<a href="https://simontaxi.conquerorstech.com/faq-2/" target="_blank" title="<?php esc_html_e( 'FAQs', 'simontaxi' ); ?>">
			<img src="<?php echo esc_url( SIMONTAXI_PLUGIN_URL . 'images/faq.png' );?>" class="attachment-showcase size-showcase wp-post-image" alt="<?php esc_html_e( 'FAQs', 'simontaxi' ); ?>" title="<?php esc_html_e( 'FAQs', 'simontaxi' ); ?>">
			</a>
			
			<a href="https://simontaxi.conquerorstech.com/faq-2/" target="_blank" title="<?php esc_html_e( 'FAQs', 'simontaxi' ); ?>"><h2><?php esc_html_e( 'FAQs', 'simontaxi' ); ?></h2></a></td>
			
			<td width="33%" align="center">
			<a href="https://simontaxi.conquerorstech.com/forums/" target="_blank" title="<?php esc_html_e( 'Forum', 'simontaxi' ); ?>">
			<img src="<?php echo esc_url( SIMONTAXI_PLUGIN_URL . 'images/forum.png' );?>" class="attachment-showcase size-showcase wp-post-image" alt="<?php esc_html_e( 'Forum', 'simontaxi' ); ?>" title="<?php esc_html_e( 'Forum', 'simontaxi' ); ?>">
			</a>
			
			<a href="https://simontaxi.conquerorstech.com/forums/" target="_blank" title="<?php esc_html_e( 'Forum', 'simontaxi' ); ?>"><h2><?php esc_html_e( 'Forum', 'simontaxi' ); ?></h2></a></td>
			
			<td align="center">
			<a href="https://simontaxi.conquerorstech.com/documentation/" target="_blank" title="<?php esc_html_e( 'Documentation', 'simontaxi' ); ?>">
			<img src="<?php echo esc_url( SIMONTAXI_PLUGIN_URL . 'images/file.png' );?>" class="attachment-showcase size-showcase wp-post-image" alt="<?php esc_html_e( 'Documentation', 'simontaxi' ); ?>" title="<?php esc_html_e( 'Documentation', 'simontaxi' ); ?>">
			</a>
			
			<a href="https://simontaxi.conquerorstech.com/documentation/" target="_blank" title="<?php esc_html_e( 'Documentation', 'simontaxi' ); ?>"><h2><?php esc_html_e( 'Documentation', 'simontaxi' ); ?></h2></a></td>
		</tr>
	</table>
	<p><?php echo __( 'These extensions <em><strong>add functionality</strong></em> to your Simontaxi - Vehicle Booking System.', 'simontaxi' ); ?></p>
    <h2 class="nav-tab-wrapper">
        <?php
    foreach( $extensions_tabs as $tab_id => $tab_name ) {

        $tab_url = add_query_arg( array(
            'tab' => $tab_id
        ) );

        $active = $active_tab == $tab_id ? ' nav-tab-active' : '';

        echo '<a href="' . esc_url( $tab_url ) . '" class="nav-tab' . $active . '">';
        echo esc_html( $tab_name );
        echo '</a>';
    }
        ?>
    </h2>

        <div class="simontaxi-extensions-layout">


                <?php $extensions = json_decode( simontaxi_get_extensions( $active_tab ) );
    $count = 0;
    if ( ! empty( $extensions->products ) ) {
        foreach( $extensions->products as $product ) {
            if ( 'free' === $active_tab ) {
                if ( $product->pricing->amount > 0 ) {
                    continue;
                }
            }
            if ( 'paid' === $active_tab ) {
                if ( $product->pricing->amount == 0 ) {
                    continue;
                }
            }
            $count++;
                ?>

                <div class="simontaxi-extension">
                    <h3 class="simontaxi-extension-title"><?php echo $product->info->title;?></h3>
                    <a href="<?php echo $product->info->link;?>" title="<?php echo $product->info->title;?>" target="_blank">
                        <?php
            $thumbnail = SIMONTAXI_PLUGIN_URL . 'images/logo.png';
            if( ! empty( $product->info->thumbnail ) ) {
                $thumbnail = $product->info->thumbnail;
            } ?>
                        <img src="<?php echo esc_url( $thumbnail );?>" class="attachment-showcase size-showcase wp-post-image" alt="" title="<?php echo $product->info->title;?>">
                    </a>

                    <p><?php echo ( $product->info->excerpt ) ? $product->info->excerpt : $product->info->content;?></p>
                    <?php if ( is_plugin_active( $product->info->slug . '/'.$product->info->slug.'.php' ) ) {
                echo __( '<span class="btn-extension-installed">Installed</span>', 'simontaxi' );
            } elseif( file_exists( ABSPATH . 'wp-content/plugins/' . $product->info->slug . '/' . $product->info->slug.'.php' ) ) { ?>
				<a href="<?php echo admin_url( 'edit.php?post_type=vehicle&page=manage_extensions&tab=' . $active_tab . '&activate=' . $product->info->slug );?>" class="button activate-now button-primary"><?php esc_html_e( 'Activate', 'simontaxi' );?></a>
				<?php } else { ?>                    
				<a href="<?php echo esc_url( $product->info->link );?>" target="_blank" class="btn-extension"><?php esc_html_e( 'Get this Extension', 'simontaxi' );?></a>
				<?php } ?>
                </div>

                <?php
        }
    }

    if ( 0 === $count ) {
        esc_html_e( 'No Plugins found', 'simontaxi' );
    }
                ?>
                <div class="clear"></div>
                <div class="simontaxi-extensions-footer">
                    <a href="https://simontaxi.conquerorstech.com/downloads/" class="button-primary" target="_blank"><?php esc_html_e( 'Browse All Extensions', 'simontaxi' ); ?></a>
                </div>

        </div><!-- #tab_container-->

</div>
<?php
    echo ob_get_clean();
}

add_action( 'simontaxi_activate_plugin', 'simontaxi_activate_plugin', 10, 1 );
function simontaxi_activate_plugin( $plugin ) {
    activate_plugin( $plugin );

    wp_redirect( admin_url('edit.php?post_type=vehicle&page=manage_extensions'), 301);
    exit;
}

/**
 * Get Extensions
 *
 * Gets the Extensions content.
 *
 * @since 2.0.0
 * @return void
 */
function simontaxi_get_extensions( $tab = 'free' ) {
    $cache = get_transient( 'simontaxi_extensions_' . $tab );
    $cache = false;
    if ( false === $cache ) {
        $url = 'https://simontaxi.conquerorstech.com/edd-api/products/';

        if ( 'free' !== $tab ) {
            $url = add_query_arg( array( 'display' => $tab ), $url );
        }

        $feed = wp_remote_get( esc_url_raw( $url ), array( 'sslverify' => false ) );

        if ( ! is_wp_error( $feed ) ) {
            if ( isset( $feed['body'] ) && strlen( $feed['body'] ) > 0 ) {
                $cache = wp_remote_retrieve_body( $feed );
                set_transient( 'simontaxi_extensions_' . $tab, $cache, 3600 );
            }
        } else {
            $cache = '<div class="error"><p>' . esc_html__( 'There was an error retrieving the extensions list from the server. Please try again later.', 'simontaxi' ) . '</div>';
        }
    }

    return $cache;
}
?>
