<?php
/**
 * This template is used to display the 'user_bookings' with [simontaxi_user_bookings]
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  simontaxi_user_bookings
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;	
}
$current_user = wp_get_current_user();
global $wpdb;
$bookings = $wpdb->prefix . 'st_bookings';
$payments = $wpdb->prefix . 'st_payments';
if ( isset( $_REQUEST['invoice_id'] ) ) {
	$ref = explode( '-', $_REQUEST['invoice_id']);
	$booking_id = $ref[0];
	$booking_ref = $ref[1];
	$invoice = $wpdb->get_results( 'SELECT *, ' . $bookings. '.reference as booking_ref, ' . $payments . '.reference as payment_ref  FROM ' . $bookings . ' INNER JOIN ' . $payments. ' ON ' . $bookings . '.ID = ' . $payments . '.booking_id WHERE ' . $bookings . '.user_id='.get_current_user_id() . ' AND ' . $bookings . '.ID=' . $booking_id );
	if ( empty( $invoice ) ) {
		echo esc_html__( 'NO INVOICE FOUND', 'simontaxi' );
	} else {
		$invoice = $invoice[0];
		$user_det = simontaxi_filter_gk( ( array ) get_user_meta( $invoice->user_id ) );
		$fail_message='';
		include_once(SIMONTAXI_PLUGIN_PATH  . '/booking/includes/pages/purchase_invoice.php' );
	}
} else {
	$per_page = 5;
	$page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
	if ( $page > 1) {
		$offset = $page * $per_page - $per_page;
	} else {
		$offset = 0;
	}
	$query_all = 'SELECT b.*, p.amount_paid, p.amount_payable FROM ' . $bookings. ' b INNER JOIN ' . $payments . ' p ON b.ID = p.booking_id WHERE b.user_id = ' . get_current_user_id() . ' AND booking_contacts != "" ORDER BY b.ID DESC';
	$query = $query_all . ' LIMIT ' . $per_page. ' OFFSET ' . $offset;
$results = $wpdb->get_results( $query );
?>

<!-- Booking Form -->

<?php if ( ! empty( $wp_error->errors ) ) { ?>
<div class="alert alert-danger">
<ul><?php echo implode( '</li><li>', $wp_error->get_error_messages() ); ?></ul>
</div>
<?php }
include_once(SIMONTAXI_PLUGIN_PATH .  '/booking/includes/pages/user_left.php' ); ?>
<div class="st-booking-block1">

<div class="tab-content">
<?php $show_invoice_to_user = simontaxi_get_option( 'show_invoice_to_user', 'yes' ); ?>
<!-- TAB-1 -->
<div id="st-booktab1" class="tab-pane fade in active">
	<div class="table-responsive">
		<table class="table table-hover st-table st-table-sm st-table-user-bookings">
			<thead>
			<tr>
			<th><?php esc_html_e( 'Order ID', 'simontaxi' ); ?></th>
			<th><?php echo simontaxi_get_pickuppoint_title(); ?></th>
			<th><?php echo simontaxi_get_dropoffpoint_title(); ?></th>
			<th><?php echo simontaxi_get_default_title(); ?></th>
			<th><?php esc_html_e( 'Paid / Payable', 'simontaxi' ); ?></th>
			<th><?php esc_html_e( 'Status', 'simontaxi' ); ?></th>
			<?php if( 'yes' === $show_invoice_to_user ) { ?>
			<th><?php esc_html_e( 'Invoice', 'simontaxi' ); ?></th>
			<?php } ?>
			</tr>
			</thead>
			<tbody>
			<?php foreach ( $results as $row ) { ?>
			<tr>
			<td><?php echo esc_attr( $row->reference ); ?><br><?php echo esc_attr( simontaxi_date_format( $row->date ) ); ?></td>
			<td><?php echo esc_attr( $row->pickup_location ); ?><br> <?php echo esc_attr( simontaxi_date_format( $row->pickup_date ) ) . ' ' . esc_attr( $row->pickup_time ); ?></td>
			<td><?php echo esc_attr( $row->drop_location ); ?></td>
			<td><?php echo esc_attr( $row->vehicle_name ); ?></td>
			<td><?php echo esc_attr( $row->amount_paid ) . ' / ' . esc_attr( $row->amount_payable ); ?></td>
			<td>
			<?php
			if ( $row->status == 'new' ) {
				echo esc_html__( 'Pending', 'simontaxi' );
			} else {
				echo esc_html__( strtoupper( $row->status ), 'simontaxi' );
			}
			?></td>
			<?php if( 'yes' === $show_invoice_to_user ) { ?>
			<td><a target="_blank" href="<?php echo add_query_arg(array( 'invoice_id' => $row->ID . '-' . $row->reference, 'download_invoice' => 'bookings' ),simontaxi_get_bookingsteps_urls( 'user_bookings' ) ); ?>" class="btn btn-dark btn-sm"><?php esc_html_e( 'View', 'simontaxi' ); ?></a></td>
			<?php } ?>
			</tr>
			<?php }

			$total = count( $wpdb->get_results( $query_all ) );
			if ( $total == 0 ) {
				?>
				<tr>
			<td colspan="7" class="st-center"><?php esc_html_e( 'No Records found', 'simontaxi' ); ?></td></tr>
				<?php
			}
			?>
			<?php if ( $total > $per_page ) { ?>
			<tr>
			<td colspan="7" class="st-center">
			<?php
			echo paginate_links(array(
				'base' => add_query_arg( 'cpage', '%#%' ),
				'format' => '',
				'prev_text' => esc_html__( '&laquo;', 'simontaxi' ),
				'next_text' => esc_html__( '&raquo;', 'simontaxi' ),
				'total' => ceil( $total / $per_page ),
				'current' => $page
			) );
			?>
			</td>
			</tr>
			<?php } ?>
			</tbody>

		</table>
	</div>
</div>

</div>
				</div>

<!-- /Booking Form -->
<?php } ?>