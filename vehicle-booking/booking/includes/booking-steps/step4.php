<?php
/**
 * Display the page to select vehicle (page is for the slug 'select-payment-method' )
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  Booking step4 page
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @global wpdb  $wpdb  WordPress database abstraction object.
 */
global $wpdb;

$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
if ( empty( $booking_step1 ) ) {
    $redirect_to = simontaxi_get_bookingsteps_urls( 'step1' );
	simontaxi_set_error( 'session_expired', esc_html__( 'Sorry, session is expired ! Now you will be redirected ...', 'simontaxi' ) );
    wp_safe_redirect( $redirect_to );
}

if ( isset( $_POST['validtestep4'] ) && wp_verify_nonce( $_POST['simontaxi_step4_nonce'], 'simontaxi-step4-nonce' ) ) {
	
	/**
     * Let us validate whether the user selects vehicle or not
     */
    if ( ! isset( $_POST['selected_payment_method'] ) ) {
        simontaxi_set_error( 'selected_payment_method', esc_html__( 'Please select payment gateway', 'simontaxi' ) );
    }
    if ( simontaxi_terms_page() == 'step4' && ! isset( $_POST['terms'] ) ) {
        simontaxi_set_error( 'terms', esc_html__( 'You should accept Terms of Service to proceed', 'simontaxi' ) );
    }
	// $errors = simontaxi_get_errors();
	$errors = apply_filters( 'simontaxi_flt_step4_errors', simontaxi_get_errors() );
    if ( empty( $errors ) ) {

        $db_ref = simontaxi_get_session( 'booking_step1', 0, 'db_ref' );
		if ( isset( $_POST ) && $db_ref > 0 && ! empty( $_POST ) ) {
            $amount_details = simontaxi_get_fare_details();

            $payment_id = simontaxi_get_session( 'booking_step4', 0, 'payment_id' );
			$payment_reference = simontaxi_get_session( 'booking_step4', '', 'payment_reference' );
			/**
			 * Let us unset nonce field
			 *
			 8 @since 2.0.0
			*/
			unset( $_POST['simontaxi_step4_nonce'] );
			
			simontaxi_set_session( 'booking_step4', $_POST );

			simontaxi_set_session( 'booking_step4', 
					array( 
					'amount_payable' => $amount_details['amount_payable'],
					/**
					 * Let us maintain payment status in session so that we can access from any where!.
					 *
					 * @since 2.0.0
					*/
					'payment_status' => 'pending',
					)
				);
			
            $data = array( 'payment_method' => simontaxi_get_session( 'booking_step4', 0, 'selected_payment_method' ) );

            $data['user_id'] = get_current_user_id();
            $data['booking_id'] = simontaxi_get_session( 'booking_step1', 0, 'db_ref' );

            $data['basic_amount'] = $amount_details['basic_amount'];
            $data['amount_payable'] = simontaxi_get_session( 'booking_step4', 0, 'amount_payable' );
            /**
             * This will update later if payment is success.
             */
            $data['amount_paid'] = 0;
            $data['discount_amount'] = $amount_details['discount_amount'];
            $data['tax_amount'] = $amount_details['tax_amount'];
            $data['surcharges_amount'] = $amount_details['surcharges_amount'];
            $data['amount_details'] = json_encode( $amount_details );
            $data['payment_status'] = 'pending';
			
            if (  $payment_id > 0 ) {
                $check = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}st_payments WHERE ID = $payment_id" );
                if ( empty( $check ) ) { //For any reason if it not already inserted, Let us insert
                    $data['reference'] = simontaxi_get_token( simontaxi_get_option( 'booking_ref_length', 6 ) );
                    $data['datetime'] = date( 'Y-m-d h:i:s' );
                    $wpdb->insert( $wpdb->prefix .'st_payments', $data );
                    simontaxi_set_session( 'booking_step4', array( 'payment_id' => $wpdb->insert_id, 'payment_reference' => $data['reference'] ) );
                } else {
                    $data['payment_status_updated'] = date( 'Y-m-d h:i:s' );
					if ( '' === $payment_reference ) {
						$data['reference'] = simontaxi_get_token( simontaxi_get_option( 'booking_ref_length', 6 ) );
					} else {
						$data['reference'] = $payment_reference;
					}
                    $wpdb->update( $wpdb->prefix .'st_payments', $data, array( 'ID' => $payment_id) );
					simontaxi_set_session( 'booking_step4', array( 'payment_id' => $payment_id, 'payment_reference' => $data['reference'] ) );
                }
            } else {
                $data['reference'] = simontaxi_get_token( simontaxi_get_option( 'booking_ref_length', 6) );
                $data['datetime'] = date( 'Y-m-d h:i:s' );
                $wpdb->insert( $wpdb->prefix .'st_payments', $data);
				simontaxi_set_session( 'booking_step4', array( 'payment_id' => $wpdb->insert_id, 'payment_reference' => $data['reference'] ) );
            }

            /**
             * Let us update sesssion details
            */
            $data = array();
            $data['session_details'] = json_encode( array( simontaxi_get_session( 'booking_step1' ), simontaxi_get_session( 'booking_step2' ), simontaxi_get_session( 'booking_step3' ), simontaxi_get_session( 'booking_step4' ) ) );
            $wpdb->update( $wpdb->prefix .'st_bookings',  $data, array( 'ID'=> simontaxi_get_session( 'booking_step1', 0, 'db_ref' ) ) );           
			
			/**
			 * Let us use powerfull sessions which is introduced from 2.0.0
			 *
			 & @since 2.0.0
			*/
			simontaxi_set_session( 'payment_gateway', $_POST );
			
			do_action( 'simontaxi_action_proceed_to_pay' );
			
        } else {
            /**
             * Means something went wrong and booking details not inserted into database, let us return back to step1, so that let us give an option for user to choose his location again
             */
            $redirect_to = simontaxi_get_bookingsteps_urls( 'step1' );
            wp_safe_redirect( $redirect_to );
        }
    }
}

$paymodes = simontaxi_get_option( 'payment_methods', array() );
if ( empty( $paymodes ) ) {
	$available_pay_methods = array( 
	'paypal' => esc_html__( 'Paypal', 'simontaxi' ),
	'payu' => esc_html__( 'PayU', 'simontaxi' ),
	'byhand' => esc_html__( 'By Hand', 'simontaxi' ),
	'banktransfer' => esc_html__( 'Bank Transfer', 'simontaxi' ),
	);
	$paymodes = apply_filters( 'simontaxi_payment_gateways', $available_pay_methods );	
}


$booking_summany_step4 = simontaxi_get_option( 'booking_summany_step4', 'yes' );
$cols = 8;
if ( 'no' === $booking_summany_step4 ) {
    $cols = 12;
}
?>
<!-- Booking Form -->
<div class="st-section-sm st-grey-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-<?php echo esc_attr( $cols ); ?> col-md-8 col-sm-12">
                <div class="st-booking-block">
                    <?php echo simontaxi_print_errors(); ?>
                    <!-- Booking Progress -->
                    <ol class="st-breadcrumb">
                        <li class="done"><a href="<?php echo simontaxi_get_bookingsteps_urls( 'step1' ); ?>"><?php echo simontaxi_get_step1_title(); ?></a></li>
                        <li class="done"><a href="<?php echo simontaxi_get_bookingsteps_urls( 'step2' ); ?>"><?php echo simontaxi_get_step2_title(); ?></a></li>
                        <li class="done"><a href="<?php echo simontaxi_get_bookingsteps_urls( 'step3' ); ?>"><?php echo simontaxi_get_step3_title(); ?></a></li>
                        <li class="active"><a><?php echo simontaxi_get_step4_title(); ?></a></li>
                    </ol>
                    <!-- end Booking Progress -->

                    <div id="info-div"></div>
                    <div class="tab-content">
                        <form id="select-payment" action="" method="POST">
                        <!-- TAB-1 -->
                        <div id="st-booktab1" class="tab-pane fade in active">
                            <div class="table-responsive">
                                <table class="table table-hover st-table st-table-payment">
                                    <?php									
                                    $default_payment_method = simontaxi_get_option( 'default_payment_method' );
									
                                    if ( !empty( $paymodes ) ) :
                                    foreach ( $paymodes as $paymode ) :
                                    if ( in_array( $paymode, array( 'paypal', 'payu', 'byhand', 'banktransfer' ), true ) ) :
									$can_display = true;
                                    if ( $paymode == 'paypal' ) {
                                        /**
                                         * If the paymod is 'paypal' we need to check whether the paypal is accepting currency!
                                        */
                                        $can_display = simontaxi_is_paypal_accept( simontaxi_get_currency_code() );
                                    } elseif( $paymode == 'payu' ) {
										/**
                                         * If the paymod is 'payu' we need to check whether the admin credentials are set!
                                        */
										$payu_live 	= "https://secure.payu.in/_payment";
										$payu_test 	= "https://test.payu.in/_payment";
										$payu = simontaxi_get_option( 'payu' );
                                        $payu_mode = (isset( $payu['mode']) ) ? $payu['mode'] : 'sandbox';
										$url = ( $payu_mode == 'sandbox' ) ? $payu_test : $payu_live;
										if ( 'sandbox' === $payu_mode ) {
											/**
											 * @since 2.0.1
											 */
											// @see https://documentation.payubiz.in/hosted-page-copy/
											$key = 'gtKFFx';
											$salt = 'eCwWELxi';
											$payu_service_provider = (isset( $payu['payu_service_provider'] ) ) ? $payu['payu_service_provider'] : 'money';
											if ( 'money' === $payu_service_provider ) {
												$key = 'rjQUPktU';
												$salt = 'e5iIg1jwi8';
											}
											$merchant_key =  (isset( $payu['merchant_key_sandbox']) ) ? $payu['merchant_key_sandbox'] : $key;
											$salt =  (isset( $payu['salt_sandbox']) ) ? $payu['salt_sandbox'] : $salt;
										} else {
											/**
											 * @since 2.0.1
											 */
											// @see https://www.payumoney.com/dev-guide/development/general.html
											$key = 'gtKFFx';
											$salt = 'eCwWELxi';
											$payu_service_provider = (isset( $payu['payu_service_provider'] ) ) ? $payu['payu_service_provider'] : 'money';
											if ( 'money' === $payu_service_provider ) {
												$key = 'rjQUPktU';
												$salt = 'e5iIg1jwi8';
											}
											$merchant_key =  (isset( $payu['merchant_key_live']) ) ? $payu['merchant_key_live'] : $key;
											$salt =  (isset( $payu['salt_live']) ) ? $payu['salt_live'] : $salt;
										}
										$amount_details = simontaxi_get_fare_details();
										$total = $amount_details['amount_payable'];
										
										/**
										 * Since PayU is accepting upto 50000 Online we need to check this
										 */
										if( $merchant_key == '' || $merchant_key == '' || $total > 50000 )
											$can_display = false;
									}
                                    if( ! $can_display ) {
										continue;
									}
                                    $options = simontaxi_get_option( $paymode);
                                    $title = isset( $options['title'] ) ? $options['title'] : ucfirst( $paymode );
                                    $logo = isset( $options['logo'] ) ? $options['logo'] : '';
                                    $description = isset( $options['description'] ) ? $options['description'] : '';
                                    $instructions = isset( $options['instructions'] ) ? $options['instructions'] : '';
                                    ?>
                                    <tr>
                                        <td>
                                            <input id="paymode<?php echo esc_attr( $paymode ); ?>" type="radio" name="selected_payment_method" class="paymentgateway" value="<?php echo esc_attr( $paymode ); ?>" <?php if ( $default_payment_method == $paymode ) { echo ' checked';}?>>
                                            <label for="paymode<?php echo esc_attr( $paymode ); ?>"><span><span></span></span>
                                            </label>
                                        </td>
                                        <td><?php
                                        if ( $title == '' ) {
                                            switch ( $paymode ) {
                                                case 'paypal':
                                                    $title = 'Paypal';
                                                    break;
                                                case 'payu':
                                                    $title = 'PayU';
                                                    break;
                                                case 'byhand':
                                                    $title = 'By Hand';
                                                    break;
												case 'banktransfer':
                                                    $title = 'Bank Transfer';
                                                    break;
                                            }
                                        }
                                        echo esc_attr( $title );
                                        ?></td>
                                        <td>
                                        <?php
                                        if ( $logo ==  '' ) {
                                            switch ( $paymode ) {
                                                case 'paypal':
                                                    $logo = SIMONTAXI_PLUGIN_URL . '/images/paypal-logo.png';
                                                    break;
                                                case 'payu':
                                                    $logo = SIMONTAXI_PLUGIN_URL . '/images/payu-logo.jpg';
                                                    break;
                                                case 'byhand':
												case 'banktransfer':
                                                    $logo = SIMONTAXI_PLUGIN_URL . '/images/byhand.jpg';
                                                    break;
                                            }
                                        }
                                        ?>
                                        <img src="<?php echo esc_url( $logo ); ?>" width="80" height="40" title="<?php echo esc_attr( $title ); ?>" alt="<?php echo esc_attr( $title ); ?>">
                                        </td>
                                        <td><p><?php echo esc_attr( $description ); ?></p></td>
                                    </tr>
                                    <?php
									else : // External Payment Gateway handling
										$external_gateway = '';
										
										echo apply_filters( 'simontaxi_external_gateway', $external_gateway );
									endif;
									endforeach;
                                    else :
                                        esc_html_e( 'No Payment methods available. Please contact administrator.', 'simontaxi' );
                                    endif; ?>

                                </table>
                            </div>
							
							<?php do_action( 'simontaxi_step4_additional_fields' ); ?>
							
                            <?php if ( 'step4' === simontaxi_terms_page() ) : ?>
                            <div class="col-sm-12">
                                <div class="input-group st-top40">
                                    <div>
                                        <input id="terms" type="checkbox" name="terms" value="option">
                                        <label for="terms"><span><span></span></span><i class="st-terms-accept"><?php echo simontaxi_terms_text(); ?></i></label>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
							
							<div id="submitbutton">
							<?php echo do_action( 'simontaxi_submit_button_step4' ); ?>
							</div>
                        </div>
						<input type="hidden" name="simontaxi_step4_nonce" value="<?php echo wp_create_nonce( 'simontaxi-step4-nonce' ); ?>"/>
                        </form>



                    </div>
                </div>
            </div>
            <?php if ( 'yes' === $booking_summany_step4 && isset( $booking_step1) && (!empty( $booking_step1) ) ) {
                require SIMONTAXI_PLUGIN_PATH . '/booking/includes/booking-steps/right-side.php';
                } ?>
        </div>
    </div>
</div>
<!-- /Booking Form -->

<script type="text/javascript">
jQuery( '#select-payment' ).on( 'submit', function (event) {
    var errors = 0;
    var message = '';
    if (jQuery( 'input[name="selected_payment_method"]:checked' ).val() === undefined) {
        message += '<?php echo esc_html__( 'Please choose a payment method', 'simontaxi' ); ?>';
        errors++;
    }
    <?php if ( 'step4' === simontaxi_terms_page() ) : ?>
    if ( ! document.getElementById( 'terms' ).checked ) {
        jQuery( '#terms' ).closest( '.input-group' ).after( '<span class="error"> <?php esc_html_e( 'You should accept Terms of Service to proceed', 'simontaxi' ); ?></span>' );
        if ( errors > 0 ) message += '<br>';
        message += '<?php echo esc_html__( 'You should accept Terms of Service to proceed', 'simontaxi' ); ?>';
        errors++;
    }
    <?php endif; ?>
    if( errors == 0 ) {
        jQuery( '#info-div' ).html( '' );
    } else {
        jQuery( '#info-div' ).html( '<div class="alert alert-danger"><p>'+message+'</p></div>' );
        event.preventDefault();
    }
});

function total_fare( amount ) {
    jQuery( '#selected_amount' ).val(amount);
}
</script>
