<?php
/**
 * Display the page to view the booking details
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  Booking page right side part
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$amount_details = $personal_details = array();
if ( isset( $booking_step2 ) && isset( $booking_step2['selected_amount'] ) ) {
    $amount_details = simontaxi_get_fare_details();
}

if ( isset( $booking_step3 ) ) {
    $personal_details = $booking_step3;
}
$vehicle_distance = simontaxi_get_option( 'vehicle_distance', 'km' );
$date_format = simontaxi_get_option( 'st_date_format', 'd-m-Y' );
$booking_type = $booking_step1['booking_type'];
?>
<div class="col-md-4 col-sm-12">
    <div class="st-booking-summary-panel">
        <?php if ( 'yes' === simontaxi_get_option( 'sidebar_fare_details_top', 'yes' ) ) : ?>
		<div class="st-booking-summary-title">
        <?php esc_html_e( 'Booking Summary', 'simontaxi' ); ?>
        <?php if ( ! empty( $amount_details ) ) {
        echo '<br>' . simontaxi_get_currency( $amount_details['amount_payable'] );
        esc_html_e( '( Inclusive of All Taxes )', 'simontaxi' );
        } ?>
        </div>
		<?php endif; ?>

        <ul class="st-booking-summary-content">
            <?php if ( isset( $booking_step1['pickup_date'] ) ) { ?>
            <?php if ( 'yes' === simontaxi_get_option( 'sidebar_start_over', 'yes' ) ) : ?>
			<li>
            <a href="<?php echo simontaxi_get_bookingsteps_urls( 'start_over' ); ?>" class="btn-active"><?php esc_html_e( 'Start Over', 'simontaxi' ); ?></a>
            </li>
			<?php endif; ?>
            <?php } ?>
            
			<?php if ( 'yes' === simontaxi_get_option( 'sidebar_booking_reference', 'yes' ) ) : ?>
			<li><span><?php esc_html_e( 'Booking Reference :', 'simontaxi' ); ?></span><span><?php echo esc_attr( $booking_step1['reference'] ); ?></span></li>
			<?php endif; ?>
			
            <?php if ( 'hourly' === $booking_type ) { ?>
            <li><span><?php esc_html_e( 'Journey Type :', 'simontaxi' ); ?></span><span><?php echo esc_attr( ucfirst( str_replace( '_', ' ', $booking_step1['journey_type'] ) ) ); ?></span></li>
            <?php } ?>

            <?php if ( 'yes' === simontaxi_get_option( 'sidebar_booking_type', 'yes' ) ) : ?>
			<li><span><?php esc_html_e( 'Booking Type :', 'simontaxi' ); ?></span><span><?php echo esc_attr( ucfirst(str_replace( '_', ' ', $booking_step1['booking_type'] ) ) ); ?></span></li>
			<?php endif; ?>
			
			<?php
			if ( 'yes' === simontaxi_get_option( 'sidebar_number_of_persons', 'no' ) && ! empty( $booking_step1['number_of_persons'] ) ) : ?>
			<li><span><?php esc_html_e( 'No. of persons :', 'simontaxi' ); ?></span><span><?php echo esc_html( $booking_step1['number_of_persons'] ); ?></span></li>
			<?php endif; ?>

            <?php if ( 'yes' === simontaxi_get_option( 'display_distance', 'no' ) && 'hourly' !== $booking_type ) { ?>
            <li><span><?php esc_html_e( 'Distance &amp; Time :', 'simontaxi' ); ?></span>
            <span>
            <?php
            if ( isset( $booking_step1['distance_text'] ) ) {
				echo esc_html( $booking_step1['distance_text'] );
			} else {
				echo esc_html__( 'NA', 'simontaxi' );
			}
			?>&nbsp; &amp; &nbsp;
			<?php
			if ( isset( $booking_step1['duration_text'] ) ) {
				echo esc_html( $booking_step1['duration_text'] );
			} else {
				echo esc_html__( 'NA', 'simontaxi' );
			}
			?>
            </span>
            </li>
            <?php } ?>

            <?php
			if ( ! empty( $amount_details) ) { ?>
            <?php if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details', 'yes' ) ) : ?>
			<li><span><?php echo sprintf( esc_html__( '%s Details :', 'simontaxi' ), simontaxi_get_label_singular() ); ?></span>
            <span>
			<?php if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_vehicle', 'yes' ) ) : ?>
				<?php echo simontaxi_get_label_singular(); ?> : <?php echo esc_attr( $booking_step2['vehicle_details']->post_title); ?>
			<?php endif; ?>
			<br>

            <?php
            if ( 'basicfare' === simontaxi_get_option( 'farecalculation_basedon', 'basicfare' ) ) {
                $vehicle_details = $booking_step2['vehicle_details'];
                if ( 'p2p' === $booking_step1['booking_type'] ) {
                    if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_distance', 'yes' ) ) {
						echo esc_html__( 'Basic Distance : ', 'simontaxi' ) . $vehicle_details->p2p_basic_distance . ' ' . $vehicle_distance . '<br>';
					}
                    if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_price', 'yes' ) ) {
						echo esc_html__( 'Basic Price : ', 'simontaxi' ) . simontaxi_get_currency( $vehicle_details->p2p_basic_price) . '<br>';
					}
                    if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_unit_price', 'yes' ) ) {
						echo esc_html__( 'Basic Unit Price : ', 'simontaxi' ) . simontaxi_get_currency( $vehicle_details->p2p_unit_price);
					}
                } elseif ( 'airport' === $booking_step1['booking_type'] ) {
                    
					if ( 'pickup_location' === $booking_step1['airport'] ) {
                        if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_distance', 'yes' ) ) {
							echo esc_html__( 'Basic Distance : ', 'simontaxi' ) . $vehicle_details->from_airport_basic_distance . '<br>';
						}
                        if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_price', 'yes' ) ) {
							echo esc_html__( 'Basic Price : ', 'simontaxi' ) . simontaxi_get_currency( $vehicle_details->from_airport_basic_price) . '<br>';
						}
                        if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_unit_price', 'yes' ) ) {
							echo esc_html__( 'Basic Unit Price : ', 'simontaxi' ) . simontaxi_get_currency( $vehicle_details->from_airport_unit_price);
						}
                    } else {
                        
						if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_distance', 'yes' ) ) {
							echo esc_html__( 'Basic Distance : ', 'simontaxi' ) . $vehicle_details->to_airport_basic_distance . '<br>';
						}
                        if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_price', 'yes' ) ) {
							echo esc_html__( 'Basic Price : ', 'simontaxi' ) . simontaxi_get_currency( $vehicle_details->to_airport_basic_price) . '<br>';
						}
                        if ( 'yes' === simontaxi_get_option( 'sidebar_vehicle_details_basic_unit_price', 'yes' ) ) {
							echo esc_html__( 'Basic Unit Price : ', 'simontaxi' ) . simontaxi_get_currency( $vehicle_details->to_airport_unit_price);
						}
                    }
                }
            ?>
            <?php } ?></span>
        </li>
		<?php endif; ?>
        <?php } ?>

            <li><span><?php
            if ( 'two_way' === $booking_step1['journey_type'] ) {
                esc_html_e( 'Onward Journey : ', 'simontaxi' );
            } else {
                if ( 'hourly' === $booking_type ) {
                    esc_html_e( 'Journey Details', 'simontaxi' );
                } else {
                esc_html_e( 'One Way Journey', 'simontaxi' );
                }
            } ?></span><a href="<?php echo simontaxi_get_bookingsteps_urls( 'step1' ); ?>"><span class="st-edit-btn fa fa-pencil-square-o"></span></a></li>

            <?php if ( 'hourly' === $booking_type ) { ?>
                <li><span><?php esc_html_e( 'Package :', 'simontaxi' ); ?></span><span><?php echo esc_attr( $booking_step1['hourly_package'] ); ?></span></li>
                <?php } ?>
            <li><span><?php
            if ( 'hourly' === $booking_type ) {
                echo simontaxi_get_pickuppoint_title() . esc_html__( ' :', 'simontaxi' );
            } else {
            esc_html_e( 'From :', 'simontaxi' );
            }
            ?></span><span>
			<?php			
			if ( ( 'airport' === $booking_type ) && ! empty ( $booking_step1['airport'] ) && ( 'pickup_location' === $booking_step1['airport'] ) ) {
				// $pickup_location = $booking_step1['pickup_location'];
				/**
				 * @since 2.0.2
				 */
				$pickup_location = $booking_step1['pickup_location_new'];
				$details = get_term( $pickup_location, 'vehicle_locations' );
				$name = $details->name;
				$term_meta = get_term_meta( $pickup_location );
				$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
				$name_value = ( '' !== $location_address ) ? $location_address : $name;
				echo esc_attr( $name_value );
			} else {
				echo esc_attr( $booking_step1['pickup_location'] );
			}
			?></span></li>
            <?php if ( 'hourly' !== $booking_type ) { ?>
            <li><span><?php esc_html_e( 'To :', 'simontaxi' ); ?></span><span>
			<?php
			if ( ( 'airport' === $booking_type ) && ! empty ( $booking_step1['airport'] ) && ( 'drop_location' === $booking_step1['airport'] ) ) {
				// $drop_location = $booking_step1['drop_location'];
				/**
				 * @since 2.0.2
				 */
				$drop_location = $booking_step1['drop_location_new'];
				$details = get_term( $drop_location, 'vehicle_locations' );
				$name = $details->name;
				$term_meta = get_term_meta( $drop_location );
				$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
				$name_value = ( '' !== $location_address ) ? $location_address : $name;
				echo esc_attr( $name_value );
			} else {
				echo esc_attr( $booking_step1['drop_location'] );
			}
			?></span></li>
            <?php } ?>

            <li><span><?php esc_html_e( 'Pickup Date :', 'simontaxi' ); ?></span><span><?php echo esc_attr( date( $date_format, strtotime( $booking_step1['pickup_date'] ) ) ); ?></span></li>
            <li><span><?php esc_html_e( 'Pickup Time :', 'simontaxi' ); ?></span><span><?php echo esc_attr( $booking_step1['pickup_time'] ); ?></span></li>


            <?php if ( ! empty( $amount_details ) ) {    ?>
            <?php if ( 'yes' === simontaxi_get_option( 'sidebar_fare_details', 'yes' ) ) : ?>
			<li><span><?php esc_html_e( 'Fare Details :', 'simontaxi' ); ?></span>
                <?php
				$sidebar_fare_details_basic_amount = simontaxi_get_option( 'sidebar_fare_details_basic_amount', 'yes' );
				$sidebar_fare_details_total_amount = simontaxi_get_option( 'sidebar_fare_details_total_amount', 'yes' );
				$sidebar_fare_details_surcharges = simontaxi_get_option( 'sidebar_fare_details_surcharges', 'yes' );
				$sidebar_fare_details_tax_amount = simontaxi_get_option( 'sidebar_fare_details_tax_amount', 'yes' );
				$sidebar_fare_details_discount = simontaxi_get_option( 'sidebar_fare_details_discount', 'yes' );
				if ( 'yes' === $sidebar_fare_details_basic_amount 
				|| 'yes' === $sidebar_fare_details_total_amount 
				|| 'yes' === $sidebar_fare_details_surcharges 
				|| 'yes' === $sidebar_fare_details_tax_amount 
				|| 'yes' === $sidebar_fare_details_discount ) :
				?>
				<span>
				<?php
				if ( 'yes' === $sidebar_fare_details_basic_amount ) {
					esc_html_e( 'Basic amount : ', 'simontaxi' ); ?><?php echo esc_attr( simontaxi_get_currency( $amount_details['basic_amount'] ) );
				}
				?>
                <?php
                if ( $amount_details['tax_amount_onward'] > 0 && 'yes' === $sidebar_fare_details_tax_amount ) {
                    echo '<br>';
                    esc_html_e( 'Tax amount : ', 'simontaxi' );
                    echo esc_attr( simontaxi_get_currency( $amount_details['tax_amount_onward'] ) );
                }
                ?>
                <?php
                if ( $amount_details['discount_amount'] > 0 && 'yes' === $sidebar_fare_details_discount ) {
                    echo '<br>';
                    esc_html_e( 'Discount : ', 'simontaxi' );
                    echo '-'.esc_attr( simontaxi_get_currency( $amount_details['discount_amount'] ) );
                }
                ?>
                <?php
                if ( $amount_details['surcharges_amount_onward'] > 0 && 'yes' === $sidebar_fare_details_surcharges ) {
                    echo '<br>';
                    esc_html_e( 'Surcharges : ', 'simontaxi' );
                    echo esc_attr( simontaxi_get_currency( $amount_details['surcharges_amount_onward'] ) );
                    echo '<br>(<i>';
                    if ( $amount_details['surcharges']['waitingtime_surcharge_onward'] > 0) {
                        echo esc_html__( 'Waiting time : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['waitingtime_surcharge_onward'] ) . ', ';
                    }
                    if ( $amount_details['surcharges']['additionalpoints_surcharge_onward'] > 0 ) {
                        echo esc_html__( 'Additional points : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['additionalpoints_surcharge_onward'] ) . ', ';
                    }
                    if ( $amount_details['surcharges']['airport_surcharge'] > 0) {
                        echo esc_html__( 'Airport : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['airport_surcharge'] ) . ', ';
                    }
                    if ( $amount_details['surcharges']['peak_time_surcharge_onward'] > 0) {
                        echo esc_html__( 'Peak time : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['peak_time_surcharge_onward'] ) . ', ';
                    }
					if ( $amount_details['surcharges']['peak_season_surcharge_onward'] > 0 ) {
                        echo esc_html__( 'Peak Season : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['peak_season_surcharge_onward'] ) . ', ';
                    }
					apply_filters( 'simontaxi_additional_surcharges_display', $amount_details );
                    echo '</i>)';
                }
                ?>
                <?php
				if ( 'yes' === $sidebar_fare_details_total_amount ) {
					echo '<br>';
					esc_html_e( 'Total amount : ', 'simontaxi' ); ?>
					<?php echo esc_attr( simontaxi_get_currency( $amount_details['amount_payable_onward'] ) );
				}
				?>
                </span>
				<?php endif; ?>
            </li>
			<?php endif; ?>
            <?php } ?>

            <?php if ( $booking_step1['journey_type'] == 'two_way' ) { ?>
            <li><span><?php esc_html_e( 'Return Journey', 'simontaxi' ); ?></span><a href="<?php echo simontaxi_get_bookingsteps_urls( 'step1' ); ?>"><span class="st-edit-btn fa fa-pencil-square-o"></span></a></li>
            <li><span><?php esc_html_e( 'From :', 'simontaxi' ); ?></span><span><?php echo esc_attr( $booking_step1['drop_location'] ); ?></span></li>
            <li><span><?php esc_html_e( 'To :', 'simontaxi' ); ?></span><span><?php echo esc_attr( $booking_step1['pickup_location'] ); ?></span></li>
            <li><span><?php esc_html_e( 'Return Pickup Date :', 'simontaxi' ); ?></span><span><?php echo esc_attr( date( $date_format, strtotime( $booking_step1['pickup_date_return'] ) ) ); ?></span></li>
            <li><span><?php esc_html_e( 'Return Pickup Time :', 'simontaxi' ); ?></span><span><?php echo esc_attr( $booking_step1['pickup_time_return'] ); ?></span></li>
            <?php if ( ! empty( $amount_details) ) {
            ?>
            <?php if ( 'yes' === simontaxi_get_option( 'sidebar_fare_details', 'yes' ) ) : ?>
			<li><span><?php esc_html_e( 'Fare Details :', 'simontaxi' ); ?></span>
                <?php
				if ( 'yes' === $sidebar_fare_details_basic_amount 
				|| 'yes' === $sidebar_fare_details_total_amount 
				|| 'yes' === $sidebar_fare_details_surcharges 
				|| 'yes' === $sidebar_fare_details_tax_amount 
				|| 'yes' === $sidebar_fare_details_discount ) :
				?>
				<span>
				<?php echo '<br>';
                if ( 'yes' === $sidebar_fare_details_basic_amount ) {
					esc_html_e( 'Basic amount : ', 'simontaxi' ); ?><?php echo esc_attr( simontaxi_get_currency( $amount_details['basic_amount'] ) );
				}
                if ( $amount_details['tax_amount'] > 0 && 'yes' === $sidebar_fare_details_tax_amount ) {
                    echo '<br>';
                    esc_html_e( 'Tax amount : ', 'simontaxi' );
                    echo esc_attr( simontaxi_get_currency( $amount_details['tax_amount'] ) );
                }
                ?>
                <?php
				
                if ( $amount_details['surcharges_amount_return'] > 0 && 'yes' === $sidebar_fare_details_discount ) {
                    echo '<br>';
                    esc_html_e( 'Surcharges : ', 'simontaxi' );
                    echo esc_attr( simontaxi_get_currency( $amount_details['surcharges_amount_return'] ) );
                    echo '<br>(<i>';
                    if ( $amount_details['surcharges']['waitingtime_surcharge_return'] > 0) {
                        echo esc_html__( 'Waiting time : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['waitingtime_surcharge_return'] ) . ', ';
                    }
                    if ( $amount_details['surcharges']['additionalpoints_surcharge_return'] > 0) {
                        echo esc_html__( 'Additional points : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['additionalpoints_surcharge_return'] ) . ', ';
                    }
                    if ( $amount_details['surcharges']['airport_surcharge'] > 0 ) {
                        echo  esc_html__( 'Airport : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['airport_surcharge'] ) . ', ';
                    }
                    if ( $amount_details['surcharges']['peak_time_surcharge_return'] > 0 ) {
                        echo esc_html__( 'Peak time : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['peak_time_surcharge_return'] ) . ', ';
                    }
					if ( $amount_details['surcharges']['peak_season_surcharge_return'] > 0 ) {
                        echo esc_html__( 'Peak Season : ', 'simontaxi' ) . simontaxi_get_currency( $amount_details['surcharges']['peak_season_surcharge_return'] ) . ', ';
                    }
					apply_filters( 'simontaxi_additional_surcharges_display', $amount_details );
                    echo '</i>)';
                }
                ?>
                <?php
				if ( 'yes' === $sidebar_fare_details_total_amount ) {
					echo '<br>';
					esc_html_e( 'Total amount : ', 'simontaxi' ); ?><?php echo esc_attr( simontaxi_get_currency( $amount_details['amount_payable_return'] ) );
				}
				?>
                </span>
				<?php endif; ?>
            </li>
			<?php endif; ?>
            <?php } ?>
            <?php } ?>


            <?php if ( count( $personal_details) > 0 && 'yes' === simontaxi_get_option( 'sidebar_personal_details', 'yes' ) ) { ?>
            <li><span><?php esc_html_e( 'Personal Details :', 'simontaxi' )?></span>
            <span>
            <?php echo esc_attr( $personal_details['email'] ); ?>
            <?php
            if ( isset( $personal_details['full_name'] ) )
                echo '<br>'.esc_attr( $personal_details['full_name'] );
            ?>
            <?php
            if ( isset( $personal_details['mobile_countrycode'] ) && isset( $personal_details['mobile'] ) ) {
				$parts = explode( '_', $personal_details['mobile_countrycode'] );
				$mobile = isset( $parts[0] ) ? $parts[0] : '';
				if ( $mobile != '' ) {
					echo '<br>' . esc_attr( $mobile ) . ' - ';
				}
				echo esc_attr( $personal_details['mobile'] );
            }
            ?>
            <?php
            if ( isset( $personal_details['no_of_passengers'] ) )
                echo '<br>'.esc_html__( 'Passengers : ', 'simontaxi' ) . esc_attr( $personal_details['no_of_passengers'] );
            ?>

            </span>
            </li>
            <?php } ?>
            <?php if ( ! empty( $amount_details ) && 'yes' === simontaxi_get_option( 'sidebar_fare_details_bottom', 'yes' ) ) { ?>
            <li class="st-booking-summary"><span><?php esc_html_e( 'Total Fare : ', 'simontaxi' ); ?></span> <span> <i class="st-booking-price"><?php echo simontaxi_get_currency( $amount_details['amount_payable'] ); ?> </i> <br><span  class="st-small-text"><?php esc_html_e( '( Inclusive of All Taxes )', 'simontaxi' )?></span></span></li>
            <?php } ?>
        </ul>
    </div>
</div>
