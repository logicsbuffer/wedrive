<?php
/**
 * Display the page to select vehicle (page is for the slug 'select-cab-type' )
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  Booking step2 page
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @global wpdb  $wpdb  WordPress database abstraction object.
 */
global $wpdb;

$booking_step1 = simontaxi_get_session( 'booking_step1', array() );

/**
 * User may change vehicle so we are unsetting selected vehicle and amount. so that we can caluclate amount once he selects vehicle in next step.
*/
simontaxi_unset_session( 'booking_step2', 'selected_amount' );

$booking_step2 = simontaxi_get_session( 'booking_step2', array() );
if ( empty( $booking_step1 ) ) {
	$redirect = simontaxi_get_bookingsteps_urls( 'step1' );
	echo '<section class="inner-page-content">
	<div class="">
	<div class="row">';
	echo '<meta http-equiv="refresh" content="5;' . $redirect . '">';
	echo '<div style="margin-top:50px;" class="alert alert-danger"><b>' . esc_html__( 'Sorry, session is expired ! Now you will be redirected ...' ) . '</b></div>';
	echo '			</div>
	</div>
	</section>';
	die();
}

if ( isset( $_POST['validtestep2'] ) ) {
	/**
	 * Let us validate whether the user selects vehicle or not
	 */
	if ( ! isset( $_POST['selected_vehicle'] ) ) {
		simontaxi_set_error( 'selected_vehicle', sprintf( esc_html__( 'Please select %s', 'simontaxi' ) , simontaxi_get_label_singular() ) );
	}
	if ( simontaxi_terms_page() == 'step2' && !isset( $_POST['terms'] ) ) {
		simontaxi_set_error( 'terms', esc_html__( 'You should accept Terms of Service to proceed', 'simontaxi' ) );
	}	

	if ( isset( $_POST['selected_vehicle'] ) ) {
		/**
		 * If user changes amount with browser tools! We are here to validate!!
		 */
		$vehicle_id = $_POST['selected_vehicle'];
		$vehicle = simontaxi_get_vehiclle_details( $vehicle_id );

		if ( ! empty( $vehicle ) ) {
			$fare = simontaxi_get_fare( $vehicle, $booking_step1 );
		} else {
			$fare = 0;
		}
		if ( round( $fare ) !=  round( $_POST['selected_amount'] ) ) {
			simontaxi_set_error( 'selected_amount', esc_html__( 'Something went wrong.', 'simontaxi' ) );
		}
	}
	
	/**
	 * Let us restrict number of vehicles
	 *
	 * @since 2.0.2
	*/
	if ( 'yes' === simontaxi_get_option( 'restrict_vehicles_count', 'no' ) ) {
		$bookings = $wpdb->prefix. 'st_bookings';
		$payments = $wpdb->prefix. 'st_payments';
		$confirmed_vehicle_status = simontaxi_get_option( 'confirmed_vehicle_status', 'confirmed' );
		
		$date = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date' );
		$pickup_date_return = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date_return' );
		
		$selected_vehicle = $_POST['selected_vehicle'];
		$sql = "SELECT COUNT(*) FROM `" . $bookings  ."` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID` WHERE `" . $bookings."`.booking_contacts!='' AND `" . $bookings . "`.status='" . $confirmed_vehicle_status . "' AND `" . $bookings . "`.`selected_vehicle` = '" . $selected_vehicle . "' AND `".$bookings."`.pickup_date = '" . $date . "'";
		$bookings_for_the_vehicle = $wpdb->get_var( $sql );

		$number_of_vehicles_available = get_post_meta( $selected_vehicle, 'number_of_vehicles', true );
		if ( $number_of_vehicles_available <= $bookings_for_the_vehicle  ) {
			simontaxi_set_error( 'selected_vehicle', esc_html__( 'Sorry the selected vehicle not available for booking', 'simontaxi' ) );
		}
		
		/*
		We are checking vehicles availability on return date also, if user trying to book for return journey
		*/
		$journey_type = simontaxi_get_session( 'booking_step1', 'one_way', 'journey_type' );
		if ( 'two_way' === $journey_type ) {
			$sql = "SELECT COUNT(*) FROM `" . $bookings  ."` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID` WHERE `" . $bookings."`.booking_contacts!='' AND `" . $bookings . "`.status='" . $confirmed_vehicle_status . "' AND `" . $bookings . "`.`selected_vehicle` = '" . $selected_vehicle . "' AND `".$bookings."`.pickup_date = '" . $pickup_date_return . "'";
			$bookings_for_the_vehicle = $wpdb->get_var( $sql );

			$number_of_vehicles_available = get_post_meta( $selected_vehicle, 'number_of_vehicles', true );
			if ( $number_of_vehicles_available <= $bookings_for_the_vehicle  ) {
				simontaxi_set_error( 'selected_vehicle_return', esc_html__( 'Sorry the selected vehicle not available for return booking', 'simontaxi' ) );
			}
		}
	}
	// $errors = simontaxi_get_errors();
	$errors = apply_filters( 'simontaxi_flt_step2_errors', simontaxi_get_errors() );
	if ( empty( $errors ) ) {
		$db_ref = simontaxi_get_session( 'booking_step1', '0', 'db_ref' );
		if ( isset( $_POST ) && $db_ref > 0 && ! empty( $_POST ) ) {
			$_POST['vehicle_details'] = $vehicle;
			simontaxi_set_session( 'booking_step2', $_POST );
			$data = array(
				'selected_vehicle' => $_POST['selected_vehicle'],
				'vehicle_name' => $vehicle->post_title,
				'session_details' => json_encode( array( simontaxi_get_session( 'booking_step1' ), simontaxi_get_session( 'booking_step2' ) ) ),
			);
			
			$wpdb->update( $wpdb->prefix . 'st_bookings',  $data, array( 'ID' => $db_ref ) );
			$redirect_to = simontaxi_get_bookingsteps_urls( 'step3' );
			wp_safe_redirect( $redirect_to );
		} else {
			/**
			 * Means something went wrong and booking details not inserted into database, let us return back to step1, so that let us give an option to user to choose his / her location again
			 */
			$redirect_to = simontaxi_get_bookingsteps_urls( 'step1' );
			wp_safe_redirect( $redirect_to );
		}
		die();
	}
}
$currency = simontaxi_get_currency();

$page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
$perpage = simontaxi_get_option( 'records_per_page', 5 );
if ( $page > 1 ) {
	$offset = $page * $perpage - $perpage;
} else {
	$offset = 0;
}
$args = array( 
	'perpage' => $perpage,
	'offset' => $offset,
	'pagination' => true,
);
if ( isset( $_GET['vname'] ) && 'desc' === $_GET['vname'] ) {
	$args['orderby'] = 'title';
	$args['order'] = 'DESC';
} else {
	$args['orderby'] = 'title';
	$args['order'] = 'ASC';
}
$vehicles_arr = simontaxi_get_vehicles( $args );

$vehicles = isset( $vehicles_arr['vehicles'] ) ? $vehicles_arr['vehicles'] : array();
$discount_details = '';

/**
 * User may select different vahicle and user may get more or less discount based on vehicle selecting!!. So we need to unset discount_details if already applied so user may enter coupon.
*/
simontaxi_set_session( 'discount_details', null );

$is_hourly = ( 'hourly' === $booking_step1['booking_type'] ? TRUE : FALSE);
$journey_type = $booking_step1['journey_type'];

$distance_taken_from = simontaxi_get_option( 'distance_taken_from', 'google' );
$vehicle_places = simontaxi_get_option( 'vehicle_places', 'googleall' );
if ( $is_hourly ) {
	$distance=0;
} else {
	$from = $booking_step1['pickup_location'];
	$to = $booking_step1['drop_location'];
	if ( 'predefined' === $distance_taken_from ) {
		/**
		* Which means admin wants to enter places and distances and user only pick those locations for pickup or dropup. So we need to know the distance between pickup and drop-off locaitons from predefined list
		*/
		$distance = simontaxi_get_distance_time( $from, $to, 'distance' );
	} else {
		/**
		* If the "distance_taken_from" is 'google' means there is no restriction on places hence we are using google given distance and same will be used to calculate fare
		*/
		$distance = $booking_step1['distance'];
	}
}

/**
 * Let us display layout according admin settings
*/
$booking_summany_step2 = simontaxi_get_option( 'booking_summany_step2', 'yes' );
$cols = 8;
if ( 'no' === $booking_summany_step2 ) {
	$cols = 12;
}

?>
<!-- Booking Form -->
<div class="st-section-sm st-grey-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-<?php echo esc_attr( $cols ); ?> col-md-8 col-sm-12">
				<div class="st-booking-block">
					<?php echo simontaxi_print_errors() ?>
					<!-- Booking Progress -->
					<ol class="st-breadcrumb">
						<li class="done"><a href="<?php echo simontaxi_get_bookingsteps_urls( 'step1' ); ?>"><?php echo simontaxi_get_step1_title(); ?></a></li>
						<li class="active"><a><?php echo simontaxi_get_step2_title(); ?></a></li>
						<li><a><?php echo simontaxi_get_step3_title(); ?></a></li>
						<li><a><?php echo simontaxi_get_step4_title(); ?></a></li>
					</ol>
					<!-- end Booking Progress -->

					<div id="info-div"></div>
					<div class="tab-content">
						<form id="select-vehicle" action="" method="POST">
						<!-- TAB-1 -->
						<div id="st-booktab1" class="tab-pane fade in active">
							<div class="table-responsive">
								<table class="table table-hover st-table st-table-pagination st-table-select-cab">
									<tr class="st-table-head">
									<td><?php esc_html_e( 'Select', 'simontaxi' ); ?></td>
									<td>&nbsp;</td>
									<td>
									<?php
									/**
									 * @since 2.0.1
									 */									
									$step2_url = simontaxi_get_bookingsteps_urls( 'step2' ) . '?vname=desc';
									if ( isset( $_GET['vname'] ) && 'desc' === $_GET['vname'] ) {
										$step2_url = simontaxi_get_bookingsteps_urls( 'step2' ) . '?vname=asc';
									}
									?>
									<a href="<?php echo esc_url( $step2_url ); ?>" title="<?php esc_html_e( 'Vehicle', 'simontaxi' ); ?>"><?php esc_html_e( 'Vehicle', 'simontaxi' ); ?></a>
									<?php $cols = 3; ?>
									</td>
									<?php if ( simontaxi_get_option( 'show_luggage_information', 'yes' ) == 'yes' ) {
									$cols++;
									?>
									<td><?php esc_html_e( 'Luggage', 'simontaxi' ); ?></td>
									<?php } ?>
									<?php
									/**
									 * @since 2.0.1
									 */									
									$step2_url = simontaxi_get_bookingsteps_urls( 'step2' ) . '?price=desc';
									if ( isset( $_GET['price'] ) && 'desc' === $_GET['price'] ) {
										$step2_url = simontaxi_get_bookingsteps_urls( 'step2' ) . '?price=asc';
									}								
									
									$show_fare = simontaxi_get_option( 'show_fare', 'totalbasic' );
									/**
									 * Let us display the fare for the user based on admin settings.
									 */
									if ( in_array( $show_fare, array( 'totalbasic', 'totalonly' ) ) ) {
									$cols++;
									?>
									<td><a href="<?php echo esc_url( $step2_url ); ?>" title="<?php esc_html_e( 'Total Fare', 'simontaxi' ); ?>"><?php esc_html_e( 'Total Fare', 'simontaxi' ); ?></a></td>
									<?php } ?>
									<?php
									/**
									 * Let us display the fare for the user based on admin settings.
									 */

									if ( in_array( $show_fare, array( 'totalbasic', 'basicdetailsonly' ) ) && ! $is_hourly ) {
									$cols++;
									?>
									<td><?php esc_html_e( 'Basic Fare', 'simontaxi' ); ?></td>
									<?php } ?>
									</tr>
									<?php if ( ! empty( $vehicles) ) :
									/**
									 * @since 2.0.1
									 */									
									if ( ! isset( $_GET['vname'] ) ) {
										$vehicles_old = $vehicles;
										$vehicles = array();
										foreach ( $vehicles_old as $vehicle ) :
											$fare = simontaxi_get_fare( $vehicle, $booking_step1 );
											$vehicle->calculated_amount = $fare;
											$vehicles[] = $vehicle;
										endforeach;									
										
										$vehicles_sort = array();
										foreach ($vehicles as $key => $row ) {
											$vehicles_sort[ $row->ID ] = $row->calculated_amount;
										}
										if ( isset( $_GET['price'] ) && 'desc' === $_GET['price'] ) {
											array_multisort( $vehicles_sort, SORT_DESC, $vehicles );
										} else {
											array_multisort( $vehicles_sort, SORT_ASC, $vehicles );
										}
									}
									
									$thumb_w = '50';
									$thumb_h = '50';
									
									foreach ( $vehicles as $vehicle ) :
									$vehicle_id = $vehicle->ID;
									/**
									 * Let us find calculate the basic fare for the user selection.
									 */
									$fare = simontaxi_get_fare( $vehicle, $booking_step1 );
									
									?>
									<tr>
										<td>
											<input id="vehicle<?php echo esc_attr( $vehicle_id ); ?>" type="radio" name="selected_vehicle" value="<?php echo esc_attr( $vehicle_id); ?>" onClick="total_fare( '<?php echo esc_attr( $fare ); ?>' )">
											<label for="vehicle<?php echo esc_attr( $vehicle_id ); ?>"><span><span></span></span>
											</label>
										</td>
										<td>
											<?php
											if ( has_post_thumbnail( $vehicle_id ) ) {
												$thumb = get_post_thumbnail_id( $vehicle_id );
												$attachment_url = wp_get_attachment_url( $thumb, 'full' );
												$image = simontaxi_resize( $attachment_url, 50, 50, true );
												?>
												<img src="<?php echo esc_url( $image ); ?>" class="car-images" alt="<?php echo esc_attr( $vehicle->post_title ); ?>" title="<?php echo esc_attr( $vehicle->post_title ); ?>">
												<?php
											} else {
											?>
												<div class="st-cab"></div>
											<?php
											}
											?>

										</td>
										<td>
											<a href="<?php echo esc_url(get_permalink( $vehicle_id ) ); ?>" target="_blank"><h4><?php echo esc_attr( $vehicle->post_title ); ?></h4></a>
											<?php if ( simontaxi_get_option( 'show_seating_capacity', 'yes' ) == 'yes' ) { ?>
											<p><?php echo ( isset( $vehicle->post_meta['seating_capacity'] ) ) ? esc_attr( $vehicle->post_meta['seating_capacity'] ) : esc_html__( 'NA', 'simontaxi' ); ?> <?php esc_html_e( 'seats', 'simontaxi' ); ?></p>
											<?php } ?>
										</td>

										<?php
										if ( simontaxi_get_option( 'show_luggage_information', 'yes' ) == 'yes' ) { ?>
										<td>
										<?php
										if ( ! empty( $vehicle->post_meta['luggage'] ) && ! empty( $vehicle->post_meta['luggage2'] ) ) {
											
											echo '<p>';
											if ( ! empty( $vehicle->post_meta['luggage_type_symbol'] ) ) {
												echo $vehicle->post_meta['luggage'] . ' ' . $vehicle->post_meta['luggage_type_symbol'];
											} elseif ( ! empty( $vehicle->post_meta['luggage_type'] ) ) {
												echo $vehicle->post_meta['luggage'] . ' ' . $vehicle->post_meta['luggage_type'];
											} else {
												echo esc_html__( 'NA', 'simontaxi' );
											}
											echo ' + ';
											
											if ( ! empty( $vehicle->post_meta['luggage2_type_symbol'] ) ) {
												echo $vehicle->post_meta['luggage2'] . ' ' . $vehicle->post_meta['luggage2_type_symbol'];
											} elseif ( ! empty( $vehicle->post_meta['luggage2_type'] ) ) {
												echo $vehicle->post_meta['luggage2'] . ' ' . $vehicle->post_meta['luggage2_type'];
											} else {
												echo esc_html__( 'NA', 'simontaxi' );
											}
											echo '</p>';
											/*
											echo '<p>' . $vehicle->post_meta['luggage'] . ' ' . $vehicle->post_meta['luggage_type'] . ' + ' . $vehicle->post_meta['luggage2'] .' '. $vehicle->post_meta['luggage2_type'] . '</p>';
										*/
										} elseif ( ! empty( $vehicle->post_meta['luggage'] ) ) {
										echo $vehicle->post_meta['luggage'];
										?>										
										<i class="fa fa-suitcase st-fa-icon"><span>&nbsp;<?php
										if ( ! empty( $vehicle->post_meta['luggage_type_symbol'] ) ) {
											echo $vehicle->post_meta['luggage_type_symbol'];
										} elseif ( ! empty( $vehicle->post_meta['luggage_type'] ) ) {
											echo $vehicle->post_meta['luggage_type'];
										} else {
											echo esc_html__( 'NA', 'simontaxi' );
										}
										?></span></i>
										<?php } elseif ( ! empty( $vehicle->post_meta['luggage2'] ) ) {
										echo $vehicle->post_meta['luggage2'];
										?>										
										<i class="fa fa-suitcase st-fa-icon"><span>&nbsp;<?php
										if ( ! empty( $vehicle->post_meta['luggage2_type_symbol'] ) ) {
											echo $vehicle->post_meta['luggage2_type_symbol'];
										} elseif ( ! empty( $vehicle->post_meta['luggage2_type'] ) ) {
											echo $vehicle->post_meta['luggage2_type'];
										} else {
											echo esc_html__( 'NA', 'simontaxi' );
										}
										?></span></i>
										<?php } else {
											echo esc_html__( 'NA', 'simontaxi' );
										}?>
										</td>
										<?php } ?>

										<?php
										/**
										 * Let us display the fare for the user based on admin settings.
										 */
										if ( in_array( $show_fare, array( 'totalbasic', 'totalonly' ) ) ) { ?>
										<td>
										<h4><?php
										$display_tax_rate = simontaxi_get_option( 'display_tax_rate', 'yes' );
										/**
										 * Bug Fixed.
										 * @since 2.0.1
										 */
										if ( $booking_step1['journey_type'] == 'two_way' ) {
											$fare = $fare * 2; //To avoid the confusion let us display whole basic fare for the user
										}
										if ( $display_tax_rate == 'yes' ) {
											$tax = simontaxi_get_tax( $fare );
											echo esc_html( simontaxi_get_currency( $fare + $tax ) );
										} else {
											echo ( $fare > 0) ? esc_html( simontaxi_get_currency( $fare ) ) : simontaxi_get_currency( '0' );
										}?></h4>
										<p><?php
										/**
										* Display basic price for the user based on selection
										*/
										if (  ! $is_hourly ) {
										if ( $booking_step1['booking_type'] == 'p2p' ) {
											echo ( isset( $vehicle->post_meta['p2p_basic_price'] ) ) ? esc_attr( simontaxi_get_currency( $vehicle->post_meta['p2p_basic_price'] ) ) : esc_html__( 'NA', 'simontaxi' );
										} elseif ( $booking_step1['booking_type'] == 'airport' ) {
											if ( $booking_step1['airport'] == 'pickup_location' ) {
												echo ( isset( $vehicle->post_meta['from_airport_basic_price'] ) ) ? esc_attr( simontaxi_get_currency( $vehicle->post_meta['from_airport_basic_price'] ) ) : esc_html__( 'NA', 'simontaxi' );
											} else {
												echo ( isset( $vehicle->post_meta['to_airport_basic_price'] ) ) ? esc_attr( simontaxi_get_currency( $vehicle->post_meta['to_airport_basic_price'] ) ) : esc_html__( 'NA', 'simontaxi' );
											}
										}
										echo esc_html_e( ' (min charge)', 'simontaxi' );
										}
										?></p>
										</td>
										<?php } ?>

										<?php
										/**
										 * Let us display the fare for the user based on admin settings.
										 */

										if ( in_array( $show_fare, array( 'totalbasic', 'basicdetailsonly' ) ) && ! $is_hourly ) { ?>
										<td>
										<?php
										/**
										* Display unit price for the user based on selection
										*/
										if ( $booking_step1['booking_type'] == 'p2p' ) {
											echo ( isset( $vehicle->post_meta['p2p_unit_price'] ) ) ? simontaxi_get_currency( $vehicle->post_meta['p2p_unit_price'] ) : esc_html__( 'NA', 'simontaxi' );
										} elseif ( $booking_step1['booking_type'] == 'airport' ) {
											if ( $booking_step1['airport'] == 'pickup_location' ) {
												echo ( isset( $vehicle->post_meta['from_airport_unit_price'] ) ) ? esc_attr( simontaxi_get_currency( $vehicle->post_meta['from_airport_unit_price'] ) ) : esc_html__( 'NA', 'simontaxi' );
											} else {
												echo ( isset( $vehicle->post_meta['to_airport_unit_price'] ) ) ? esc_attr( simontaxi_get_currency( $vehicle->post_meta['to_airport_unit_price'] ) ) : esc_html__( 'NA', 'simontaxi' );
											}
										}
										 ?> / <?php
										 /**
										* Display Price per Standard Unit Distance
										*/
										 echo esc_html__( 'after', 'simontaxi' ) . ' ';
										 if ( $booking_step1['booking_type'] == 'p2p' ) {
											 echo ( isset( $vehicle->post_meta['p2p_basic_distance'] ) ) ? esc_html( $vehicle->post_meta['p2p_basic_distance'] ) : esc_html__( 'NA', 'simontaxi' );
										 } elseif ( $booking_step1['booking_type'] == 'airport' ) {
											 if ( $booking_step1['airport'] == 'pickup_location' ) {
												 echo ( isset( $vehicle->post_meta['from_airport_basic_distance'] ) ) ? esc_html( $vehicle->post_meta['from_airport_basic_distance'] ) : esc_html__( 'NA', 'simontaxi' );
											 } else {
												 echo ( isset( $vehicle->post_meta['to_airport_basic_distance'] ) ) ? esc_html( $vehicle->post_meta['to_airport_basic_distance'] ) : esc_html__( 'NA', 'simontaxi' );
											 }
										 }
										 echo  ' ' . simontaxi_get_option( 'vehicle_distance', 'km' );

										 ?>
										</td>
										<?php } ?>

									</tr>
									<?php endforeach; ?>
									<tr><td colspan="<?php echo esc_attr( $cols ); ?>" style="text-align:right">
									<?php
									$total = $vehicles_arr['total'];
									echo paginate_links(array(
									'base' => add_query_arg( 'cpage', '%#%' ),
									'format' => '',
									'prev_text' => esc_html__( '&laquo;', 'simontaxi' ),
									'next_text' => esc_html__( '&raquo;', 'simontaxi' ),
									'total' => ceil( $total / $args['perpage'] ),
									'current' => $page
									) );
									?>
									</td></tr>
									<?php else : ?>
									<tr><td colspan="<?php echo esc_attr( $cols ); ?>" style="text-align:center">
									<?php esc_html_e( 'No records found', 'simontaxi' ); ?>
									</td></tr>
									<?php endif; ?>

								</table>
							</div>
							<?php if ( simontaxi_terms_page() == 'step2' ) : ?>
							<div class="col-sm-12">
								<div class="input-group st-top40">
									<div>
										<input id="terms" type="checkbox" name="terms" value="option">
										<label for="terms"><span><span></span></span><i class="st-terms-accept"><?php echo simontaxi_terms_text(); ?></i></label>
									</div>
								</div>
							</div>
							<?php endif; ?>

							<?php
							if ( simontaxi_get_option( 'coupon_code_form', 'yes' ) == 'yes' ) :
								if ( isset( $discount_details) && $discount_details == '' ) { ?>
								<div class="row">
									<div id="coupon_div_msg"></div>
									<div class="coupon_div  col-md-8 lc-coupon_div" id="coupon_div">
										<p><strong><?php echo apply_filters( 'simontaxi_filter_coupon_title', esc_html__( 'Coupon', 'simontaxi' ) ); ?></strong></p>
										<div class="input-group">
										<input type="text" name="coupon_code" class="form-control input-lg" id="coupon_code" value="" placeholder="<?php esc_html_e( 'Enter', 'simontaxi' ); ?> <?php echo apply_filters( 'simontaxi_filter_coupon_title', esc_html__( 'Coupon', 'simontaxi' ) ); ?>" autocomplete="off">
										<span class="input-group-addon" name="apply_coupon" id="apply_coupon">
										<?php esc_html_e( 'Apply', 'simontaxi' ); ?> <?php echo apply_filters( 'st_filter_coupon_title', __( 'Coupon', 'simontaxi' ) ); ?>
										</span>
										</div>
									</div>
								</div>
								<?php }
							endif;
							?>
							
							<?php do_action( 'simontaxi_step2_additional_fields' ); ?>

							<div class="st-terms-block">
								<a href="<?php echo simontaxi_get_bookingsteps_urls( 'step1' ); ?>" class="btn-dull"><i class="fa fa-angle-double-left"></i> <?php esc_html_e( 'Back', 'simontaxi' ); ?> </a>
								<button type="submit" class="btn btn-primary btn-mobile" name="validtestep2"><?php echo apply_filters( 'simontaxi_filter_booknow_title', esc_html__( 'Book Now', 'simontaxi' ) ); ?></button>
								<input type="hidden" name="selected_amount" id="selected_amount" value="0">
								<input type="hidden" name="selected_amount_onward" id="selected_amount_onward" value="0">
								<input type="hidden" name="selected_amount_return" id="selected_amount_return" value="0">
							</div>
						</div>
						</form>



					</div>
				</div>
			</div>
			<?php if ( $booking_summany_step2 == 'yes' && isset( $booking_step1 ) && ( ! empty( $booking_step1 ) ) ) {
				require SIMONTAXI_PLUGIN_PATH . '/booking/includes/booking-steps/right-side.php';
				} ?>
		</div>
	</div>
</div>
<!-- /Booking Form -->

<script type="text/javascript">
jQuery( '#apply_coupon' ).on( 'click', function (e) {

	var coupon_code = jQuery( '#coupon_code' ).val();
	var selected_amount = jQuery( '#selected_amount' ).val();
	if (jQuery( 'input[name="selected_vehicle"]:checked' ).val() === undefined) {
		e.preventDefault();
		jQuery( '#coupon_div_msg' ).html( '<div class="alert alert-danger"><p><?php echo sprintf( esc_html__( 'Please choose a %s', 'simontaxi' ) , simontaxi_get_label_singular() ); ?></p></div>' );
		return false
	}
	if (coupon_code == '' ) {
		jQuery( '#coupon_div_msg' ).html( '<div class="alert alert-danger"><p><?php esc_html_e( 'Please enter coupon code', 'simontaxi' ); ?></p></div>' );
		return false;
	}
	var data = {
		'action': 'get_coupon_amount',
		'coupon_code': coupon_code,
		'selected_amount': selected_amount
		};
	// We can also pass the url value separately from ajaxurl for front end AJAX implementations
	jQuery.post( '<?php echo admin_url( 'admin-ajax.php ' ); ?>', data
		, function (response) {
			var result = jQuery.parseJSON(response);
			if(result['status'] == 'success' ) {
			jQuery( '#coupon_code' ).val( '' );
			jQuery( '#apply_coupon' ).prop( 'disabled', true);
			jQuery( '#coupon_div' ).html( '<div class="alert alert-success">'+result['msg']+'</div>' );
			jQuery( '#coupon_div_msg' ).html( '' );
			} else {
				jQuery( '#coupon_div_msg' ).html( '<div class="alert alert-danger">'+result['msg']+'</div>' );
			}
		});
});
jQuery( '#select-vehicle' ).on( 'submit', function (event) {
	var errors = 0;
	var message = '';
	if (jQuery( 'input[name="selected_vehicle"]:checked' ).val() === undefined) {
		message += '<?php echo sprintf( esc_html__( 'Please choose a %s', 'simontaxi' ) ,simontaxi_get_label_singular() ); ?>';
		errors++;
	}
	<?php if ( simontaxi_terms_page() == 'step2' ) : ?>
	if ( !document.getElementById( 'terms' ).checked ) {
		if ( errors > 0 ) message += '<br>';
		message += '<?php echo esc_html__( 'You should accept Terms of Service to proceed', 'simontaxi' ); ?>';
		errors++;
	}
	<?php endif; ?>
	if( errors == 0 ) {
		jQuery( '#info-div' ).html( '' );
	} else {
		jQuery( '#info-div' ).html( '<div class="alert alert-danger"><p>'+message+'</p></div>' );
		event.preventDefault();
	}

});

function total_fare( amount ) {
	jQuery( '#selected_amount' ).val(amount);
}
</script>