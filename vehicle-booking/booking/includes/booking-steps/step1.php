<?php
/**
 * Display the page to select journey information (page is for the slug 'pick-locations' )
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  Booking step1 page
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @global wpdb  $wpdb  WordPress database abstraction object.
 */
global $wpdb;

$vehicle_country = simontaxi_get_option( 'vehicle_country', 'US' );
/**
 * @since 2.0.0
*/
$vehicle_country_dropoff = simontaxi_get_option( 'vehicle_country_dropoff', 'US' );
$vehicle_places = simontaxi_get_option( 'vehicle_places', 'googleall' );

$vehicle_places_airport = simontaxi_get_option( 'vehicle_places_airport', 'googleall' );
$vehicle_places_airport_display = simontaxi_get_option( 'vehicle_places_airport_display', 'auto' );

$vehicle_places_hourly = simontaxi_get_option( 'vehicle_places_hourly', 'googleall' );
$vehicle_places_hourly_display = simontaxi_get_option( 'vehicle_places_hourly_display', 'auto' );

$vehicle_places_dropoff = simontaxi_get_option( 'vehicle_places_dropoff', 'googleall' );

$google_api = simontaxi_get_option( 'google_api', 'AIzaSyCqRV6HQ_BSw3MMjPen2bT2IwDnZgfjwu4' );
$vehicle_distance = simontaxi_get_option( 'vehicle_distance', 'km' );
$outofservice = simontaxi_get_option( 'outofservice', 0);
$minimum_distance = simontaxi_get_option( 'minimum_distance', 0);
$distance_taken_from = simontaxi_get_option( 'distance_taken_from', 'google' );

$predefined_place = '';
if ( 'predefined' === $vehicle_places ) {
	$predefined_place = 'predefined_place';
}

$predefined_place_airport = '';
if ( 'predefined' === $vehicle_places_airport ) {
	$predefined_place_airport = 'predefined_place';
}

$predefined_place_hourly = '';
if ( 'predefined' === $vehicle_places_hourly ) {
	$predefined_place_hourly = 'predefined_place';
}

/**
 * As we have many requests to display the predefined places as Drop down, we have given an opiton in admin 'Settings->General->Pickup Places' so that admin can decide the way of displaying places in front end
 *
 * @since 2.0.0
 */
$vehicle_places_display = simontaxi_get_option( 'vehicle_places_display', 'auto' );
$vehicle_places_dropoff_display = simontaxi_get_option( 'vehicle_places_dropoff_display', 'auto' );

$fixed_point_vehicle_name = simontaxi_get_option( 'fixed_point_vehicle_name', 'Flight' );

/**
 * @since 2.0.0
*/
$predefined_place_dropoff = '';
if ( 'predefined' === $vehicle_places_dropoff ) {
	$predefined_place_dropoff = 'predefined_place';
}

if ( 
	( in_array( $vehicle_places, array( 'googleall', 'googleregions' ) ) && $google_api == '' ) 
	|| ( in_array( $vehicle_places_dropoff, array( 'googleall', 'googleregions' ) ) && $google_api == '' )
	|| ( in_array( $vehicle_places_airport, array( 'googleall', 'googleregions' ) ) && $google_api == '' )
	|| ( in_array( $vehicle_places_hourly, array( 'googleall', 'googleregions' ) ) && $google_api == '' )
	) {
    simontaxi_set_error( 'pickup_location', esc_html__( 'Google API key is not set. Please contact administrator', 'simontaxi' ) );
}
//Get the session data if it exists.
$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
/**
 * User may change vehicle so we are unsetting selected vehicle and amount. so that we can caluclate amount once he selects vehicle in next step.
*/
$booking_step2 = simontaxi_get_session( 'booking_step2', 'selected_amount' );
if ( ! empty( $booking_step2 ) ) {
	simontaxi_unset_session( 'booking_step2', 'selected_amount' );
}

$booking_step2 = simontaxi_get_session( 'booking_step2', array() );
$modify = ! empty( $booking_step1 );
if ( ! empty( $_POST ) ) {
	$modify = true;
}

$fixed_point_title = simontaxi_get_fixed_point_title();


if ( isset( $_POST['validtestep1'] ) ) {
    $booking_type = $_POST['booking_type'];
    /**
     * If some one tries to hack with out selecting "journey_type", here we are validating and take it as "one_way" journey
    */
    if ( ! isset( $_POST['journey_type'] ) ) {
        $_POST['journey_type'] = 'one_way';
    }
    if ( $_POST['pickup_location'] == '' ) {
        simontaxi_set_error( 'pickup_location', sprintf( esc_html__( 'Please select %s', 'simontaxi' ), simontaxi_get_pickuppoint_title() ) );
    }
    if ( in_array( $booking_type, array( 'p2p', 'airport' ) ) ) {
        if ( $_POST['drop_location'] == '' ) {
            simontaxi_set_error( 'drop_location', sprintf( esc_html__( 'Please select %s', 'simontaxi' ), simontaxi_get_dropoffpoint_title() ) );
        }
        if ( $_POST['pickup_location'] != '' && $_POST['drop_location'] != '' ) {
            if ( $_POST['pickup_location'] == $_POST['drop_location'] ) {
                simontaxi_set_error( 'pickup_location', sprintf( esc_html__( '%s and %s should be different', 'simontaxi' ), simontaxi_get_pickuppoint_title(), simontaxi_get_dropoffpoint_title() ) );
            }
        }
    }

	$date_time = $_POST['pickup_date'] . ' ' . $_POST['pickup_time_hours'] . ':' . $_POST['pickup_time_minutes'];
    if ( $_POST['pickup_date'] == '' ) {
        simontaxi_set_error( 'pickup_date', sprintf( esc_html__( 'Please select %s', 'simontaxi' ), simontaxi_get_pickupdate_title() ) );
    } elseif ( date( 'Y-m-d H:i', strtotime ( $date_time ) ) < date( 'Y-m-d H:i' ) ) {
		/* Validating the pickup date and time it should be in future date. Thanks to our testers!!!*/
		simontaxi_set_error( 'pickup_date', sprintf( esc_html__( '%s and time should be future date', 'simontaxi' ), simontaxi_get_pickupdate_title() ) );
	} elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) ) < date( 'Y-m-d', strtotime(date( 'Y-m-d' ) ) ) ) {
        /* Validating the pickup date it should be in future date. If any one tries to hack the system by changing system date!. We are here to validate!!. Thanks to our testers!!!*/
        simontaxi_set_error( 'pickup_date', sprintf( esc_html__( '%s should be future date', 'simontaxi' ), simontaxi_get_pickupdate_title() ) );
    } elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) ) < date( 'Y-m-d', strtotime("+".simontaxi_get_option( 'minimum_notice' )." days") ) ) {
        /* Validating the minimum notice */
        simontaxi_set_error( 'pickup_date', sprintf( esc_html__( '%s should be %s day(s) before', 'simontaxi' ), simontaxi_get_pickupdate_title(), simontaxi_get_option( 'minimum_notice' ) ) );
    } elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) ) > date( 'Y-m-d', strtotime("+".simontaxi_get_maximum_notice()." days") ) ) {
        /* Validating the maximum notice */
        simontaxi_set_error( 'pickup_date', sprintf( esc_html__( '%s should be %s day(s) before', 'simontaxi' ),  simontaxi_get_pickupdate_title(), simontaxi_get_maximum_notice() ) );
    }
    if ( $_POST['pickup_time_hours'] == '' ) {
        simontaxi_set_error( 'pickup_time_hours', sprintf( esc_html__( 'Please select %s hours', 'simontaxi' ), simontaxi_get_pickuptime_title() ) );
    }
    if ( $_POST['pickup_time_minutes'] == '' ) {
        simontaxi_set_error( 'pickup_time_minutes', sprintf( esc_html__( 'Please select %s minutes', 'simontaxi' ), simontaxi_get_pickuptime_title() ) );
    }
	/**
	 * @since 2.0.6
	 */
	if ( 'yesrequired' === simontaxi_get_option( 'allow_number_of_persons', 'no' ) ) {
		if ( empty( $_POST['number_of_persons'] ) ) {
			simontaxi_set_error( 'number_of_persons', esc_html__( 'Please enter number of persons', 'simontaxi' ) );
		} elseif ( ! ctype_digit( $_POST['number_of_persons'] ) ) {
			simontaxi_set_error( 'number_of_persons', esc_html__( 'Please enter valid number', 'simontaxi' ) );
		}
	}
	if ( 'yesoptional' === simontaxi_get_option( 'allow_number_of_persons', 'no' ) ) {
		if ( ! empty( $_POST['number_of_persons'] ) && ! ctype_digit( $_POST['number_of_persons'] ) ) {
			simontaxi_set_error( 'number_of_persons', esc_html__( 'Please enter valid number', 'simontaxi' ) );
		}
	}
	
    if ( 'airport' === $booking_type ) {
        if ( 'yesrequired' === simontaxi_get_option( 'allow_flight_number', 'no' ) && $_POST['flight_no'] == '' ) {
            simontaxi_set_error( 'flight_no', esc_html__( 'Please enter ' . $fixed_point_vehicle_name . ' number', 'simontaxi' ) );
        }
		if ( simontaxi_get_option( 'allow_flight_arrival_time', 'no' ) == 'yesrequired' && $_POST['flight_arrival_time'] == '' ) {
            simontaxi_set_error( 'flight_arrival_time', esc_html__( 'Please enter ' . $fixed_point_vehicle_name . ' arrival', 'simontaxi' ) );
        }
    }

    if ( $booking_type == 'hourly' ) {
        if ( simontaxi_get_option( 'allow_itinerary', 'no' ) == 'yesrequired' && $_POST['itineraries'] == '' ) {
            simontaxi_set_error( 'itineraries', esc_html__( 'Please enter itinerary', 'simontaxi' ) );
        }
    }
	
	$google_errors = array();

    if ( 'hourly' === $booking_type ) {
        $distance = 0;
    } else {
		if ( 'google' === $distance_taken_from ) {
			if ( 'airport' === $booking_type ) {
				
				if ( ! empty( $_POST['airport'] ) && ( 'pickup_location' === $_POST['airport'] ) ) {
					$pickup_location = $_POST['pickup_location'];
					$details = get_term( $pickup_location, 'vehicle_locations' );
					$name = $details->name;
					$term_meta = get_term_meta( $pickup_location );

					$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
					$name_value = ( '' !== $location_address ) ? $location_address : $name;
					$_POST['pickup_location'] = $name_value;
					$_POST['pickup_location_new'] = $pickup_location;				
					
					if ( 'predefined' === $vehicle_places_airport && 'dropdown' === $vehicle_places_airport_display ) {						
						$drop_location = $_POST['drop_location'];
						$details = get_term( $drop_location, 'vehicle_locations' );
						$name = $details->name;
						$term_meta = get_term_meta( $drop_location );
						$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
						$name_value = ( '' !== $location_address ) ? $location_address : $name;
						$_POST['drop_location'] = $name_value;
						$_POST['drop_location_new'] = $drop_location;
					}
				} else {
					$drop_location = $_POST['drop_location'];
					if ( ! empty( $drop_location ) ) {
						$details = get_term( $drop_location, 'vehicle_locations' );
						$name = $details->name;
						$term_meta = get_term_meta( $drop_location );
						$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
						$name_value = ( '' !== $location_address ) ? $location_address : $name;
						$_POST['drop_location'] = $name_value;
						$_POST['drop_location_new'] = $drop_location;
					}
					
					if ( 'predefined' === $vehicle_places_airport && 'dropdown' === $vehicle_places_airport_display ) {						
						$pickup_location = $_POST['pickup_location'];
						if ( ! empty( $pickup_location ) ) {
							$details = get_term( $pickup_location, 'vehicle_locations' );
							$name = $details->name;
							$term_meta = get_term_meta( $pickup_location );
							$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
							$name_value = ( '' !== $location_address ) ? $location_address : $name;
							$_POST['pickup_location'] = $name_value;
							$_POST['pickup_location_new'] = $pickup_location;
						}
					}
				}
				
			} else {
				if (  ( 'predefined' === $vehicle_places && 'dropdown' === $vehicle_places_display ) ) {
					$pickup_location = $_POST['pickup_location'];
					$details = get_term( $pickup_location, 'vehicle_locations' );
					$name = $details->name;
					$term_meta = get_term_meta( $pickup_location );

					$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
					$name_value = ( '' !== $location_address ) ? $location_address : $name;
					$_POST['pickup_location'] = $name_value;
					$_POST['pickup_location_new'] = $pickup_location;
				}
				if ( ( 'predefined' === $vehicle_places_dropoff && 'dropdown' === $vehicle_places_dropoff_display ) ) {
					$drop_location = $_POST['drop_location'];
					if ( ! empty( $drop_location ) ) {
						$details = get_term( $drop_location, 'vehicle_locations' );
						
						$name = '';
						if ( $details ) {
							$name = $details->name;
						}
						$term_meta = get_term_meta( $drop_location );
						$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
						$name_value = ( '' !== $location_address ) ? $location_address : $name;
						$_POST['drop_location'] = $name_value;
						$_POST['drop_location_new'] = $drop_location;
					}
				}
			}
			
			$pickup_location = $_POST['pickup_location'];
			$drop_location = $_POST['drop_location'];			
			if ( ! empty( $pickup_location ) && ! empty( $drop_location ) ) {
				$distance_details = get_google_distance( $pickup_location, $drop_location, simontaxi_get_distance_units() );
				
				if ( ! empty( $distance_details['status'] ) ) {
					$google_errors['code'] = $distance_details['status'];
				} else {
					$distance = $distance_details['distance'];
					$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
							   
					$_POST['distance'] = $distance;
					$_POST['distance_text'] = $distance_details['distance_text'];
					$_POST['distance_units'] = $distance_details['distance_units'];
					$_POST['duration_text'] = $distance_details['duration_text'];
				}
			}			
			
		} else {
			$pickup_location = $_POST['pickup_location'];
			$drop_location = $_POST['drop_location'];
			
			if (  ( 'predefined' === $vehicle_places && 'dropdown' === $vehicle_places_display && 'p2p' === $booking_type )
			|| ( 'predefined' === $vehicle_places_airport && 'dropdown' === $vehicle_places_airport_display && 'pickup_location' === $_POST['airport'] && 'airport' === $booking_type )
			) {
				$details = get_term( $pickup_location, 'vehicle_locations' );
				$name = $details->name;
				$term_meta = get_term_meta( $pickup_location );

				$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
				$name_value = ( '' !== $location_address ) ? $location_address : $name;
				$_POST['pickup_location'] = $name_value;
				$_POST['pickup_location_new'] = $pickup_location;
				
				$pickup_location = $_POST['pickup_location'];
				
			}
			if ( ( 'predefined' === $vehicle_places_dropoff && 'dropdown' === $vehicle_places_dropoff_display && 'p2p' === $booking_type )
			|| ( 'predefined' === $vehicle_places_dropoff && 'dropdown' === $vehicle_places_dropoff_display && 'airport' === $booking_type )
			) {
				$details = get_term( $drop_location, 'vehicle_locations' );
				$name = $details->name;
				$term_meta = get_term_meta( $drop_location );
				$location_address = ( ! empty( $term_meta['location_address'] ) ) ? $term_meta['location_address'][0] : '';
				$name_value = ( '' !== $location_address ) ? $location_address : $name;
				$_POST['drop_location'] = $name_value;
				$_POST['drop_location_new'] = $drop_location;
				
				$drop_location = $_POST['drop_location'];
			}
			
			$pick = $pickup_location;
			$drop = $drop_location;
			if ( ! empty( $_POST['pickup_location_new'] ) ) {
				$pick = $_POST['pickup_location_new'];
			}
			if ( ! empty( $_POST['drop_location_new'] ) ) {
				$drop = $_POST['drop_location_new'];
			}
			$distance_details = simontaxi_get_distance_time( $pick, $drop );

            $distance = $distance_details['distance'];
			$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
			          
			$_POST['distance'] = $distance;
			$_POST['distance_text'] = $distance_details['distance_text'];
			$_POST['distance_units'] = $distance_details['distance_units'];
			$_POST['duration_text'] = $distance_details['duration_text'];     
		}
	}

    if ( in_array( $booking_type, array( 'p2p', 'airport' ) ) ) {
        $distance_number = filter_var( $_POST['distance'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
		if ( $distance_number == '' ) {
            simontaxi_set_error( 'distance', esc_html__( 'Please select locations', 'simontaxi' ) );
        } elseif ( $minimum_distance > 0 ) {
			/**
			 * If admin enables any restriction on distance, we need to validate
			 */
			if ( $distance_number < $minimum_distance ) {
                simontaxi_set_error( 'distance', sprintf( esc_html__( 'Online bookings are only available above %s %s. Distance between selected locations is %s %s', 'simontaxi' ), $minimum_distance, simontaxi_get_distance_units(), $distance_number, simontaxi_get_distance_units() ) );
            }
		} elseif ( $outofservice > 0 ) {
            if ( $distance_number > $outofservice ) {
                simontaxi_set_error( 'distance', sprintf( esc_html__( 'Service not available for above %s %s', 'simontaxi' ), $outofservice, simontaxi_get_distance_units() ) );
            }
        }
        if ( isset( $_POST['journey_type'] ) && $_POST['journey_type'] == 'two_way' ) {
            if ( $_POST['pickup_date_return'] == '' ) {
                simontaxi_set_error( 'pickup_date_return', sprintf( esc_html__( 'Please select return %s', 'simontaxi' ), simontaxi_get_pickupdate_title() ) );
            } elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date_return'] ) ) < date( 'Y-m-d' ) ) {
                /* Validating the return pickup date it should be in future date and after the pickup date. If any one tries to hack the system by changing system date!. We are here to validate!!. Thanks to our testers!!!*/
                simontaxi_set_error( 'pickup_date_return', sprintf( esc_html__( 'Return %s should be future date', 'simontaxi' ), simontaxi_get_pickupdate_title() ) );
            } elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date_return'] ) ) < date( 'Y-m-d', strtotime("+".simontaxi_get_option( 'minimum_notice' )." days") ) ) {
                /* Validating the minimum notice */
                simontaxi_set_error( 'pickup_date_return', sprintf( esc_html__( '%s should be %s days(s) before', 'simontaxi' ), simontaxi_get_pickupdate_title(), simontaxi_get_option( 'minimum_notice' ) ) );
            } elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date_return'] ) ) > date( 'Y-m-d', strtotime("+".simontaxi_get_maximum_notice()." days") ) ) {
                /* Validating the maximum notice */
                simontaxi_set_error( 'pickup_date_return', sprintf( esc_html__( '%s should be %s day(s) before', 'simontaxi' ), simontaxi_get_pickupdate_title(), simontaxi_get_maximum_notice() ) );
            } elseif ( date( 'Y-m-d', strtotime( $_POST['pickup_date_return'] ) ) < date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) ) ) {
				/**
				 * @since 2.0.0
				 */
				simontaxi_set_error( 'pickup_date_return', sprintf( esc_html__( 'Return %s should be after %s', 'simontaxi' ), simontaxi_get_pickupdate_title(), simontaxi_get_pickupdate_title() ) );
			} else {
				/**
				 * @since 2.0.0
				 */
				$pickup_date_time = date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) ) . $_POST['pickup_time_hours'] . ':' . $_POST['pickup_time_minutes'];

				// Let us assume a journey is minimum 1 hour, so there will be no return booking upto 1 hour
				$pickup_date_time = date( 'Y-m-d H:i', strtotime( "+1 hour", strtotime( $pickup_date_time ) ) );

				$pickup_date_time_return = date( 'Y-m-d', strtotime( $_POST['pickup_date_return'] ) ) . ' ' . $_POST['pickup_time_hours_return'] . ':' . $_POST['pickup_time_minutes_return'];

				if ( $pickup_date_time_return < $pickup_date_time ) {
					simontaxi_set_error( 'pickup_date_return', sprintf( esc_html__( 'Return %s should be after one hour', 'simontaxi' ), simontaxi_get_pickupdate_title(), simontaxi_get_pickupdate_title() ) );
				}
			}

            if ( $_POST['pickup_time_hours_return'] == '' ) {
                simontaxi_set_error( 'pickup_time_hours_return', sprintf( esc_html__( 'Please select return %s hours', 'simontaxi' ), simontaxi_get_pickuptime_title() ) );
            }
            if ( $_POST['pickup_time_minutes_return'] == '' ) {
                simontaxi_set_error( 'pickup_time_minutes_return', sprintf( esc_html__( 'Please select return %s minutes', 'simontaxi' ), simontaxi_get_pickuptime_title() ) );
            }
        }
    }

	if ( ( ( '' === $_POST['distance'] ) || ( '0.00' === $_POST['distance'] ) ) && ( $booking_type != 'hourly' ) ) {
        $pickup_location = $_POST['pickup_location'];
		$drop_location = $_POST['drop_location'];
		
		if ( ! empty( $google_errors ) ) {
			/**
			 * @link https://developers.google.com/maps/documentation/directions/intro
			 */
			$message = esc_html__( 'At least one of the locations specified in the ' . $pickup_location . ', ' . $drop_location . ', could not be geocoded', 'simontaxi' );
			switch( $google_errors['code'] ) {
				case 'ZERO_RESULTS':
					$message = esc_html__( 'No route could be found between the ' . $pickup_location . ' and ' . $drop_location, 'simontaxi' );
					break;
				
				case 'MAX_WAYPOINTS_EXCEEDED':
					$message = esc_html__( 'Too many waypoints were provided in the request', 'simontaxi' );
					break;
				case 'MAX_ROUTE_LENGTH_EXCEEDED':
					$message = esc_html__( 'Requested route is too long and cannot be processed', 'simontaxi' );
					break;
				case 'INVALID_REQUEST':
					$message = esc_html__( 'Provided request was invalid', 'simontaxi' );
					break;
				case 'OVER_QUERY_LIMIT':
					$message = esc_html__( 'Service has received too many requests from your application within the allowed time period.', 'simontaxi' );
					break;
				case 'REQUEST_DENIED':
					$message = esc_html__( 'Service denied use of the directions service by your application', 'simontaxi' );
					break;
				case 'UNKNOWN_ERROR':
					$message = esc_html__( 'Request could not be processed due to a server error. The request may succeed if you try again.', 'simontaxi' );
					break;
			}
			simontaxi_set_error( 'distance', $message );
		} else {
			simontaxi_set_error( 'distance', sprintf( esc_html__( 'Sorry, no services from %s to %s', 'simontaxi' ), $pickup_location, $drop_location ) );
		}
    } elseif ( $outofservice > 0 && $distance > $outofservice) {
        simontaxi_set_error( 'distance', sprintf( esc_html__( 'Service not available for above %s%s', 'simontaxi' ), $outofservice, simontaxi_get_distance_units() ) );
    }

    if ( simontaxi_terms_page() == 'step1' && ! isset( $_POST['terms'] ) ) {
        simontaxi_set_error( 'terms', esc_html__( 'You should accept Terms of Service to proceed', 'simontaxi' ) );
    }
	$errors = apply_filters( 'simontaxi_flt_step1_errors', simontaxi_get_errors() );

    if ( empty( $errors ) ) {
        $_POST['pickup_date'] = date( 'Y-m-d', strtotime( $_POST['pickup_date'] ) );
        $_POST['pickup_time'] = $_POST['pickup_time_hours'] . ':' . $_POST['pickup_time_minutes'];
        unset( $_POST['pickup_time_hours'] );
        unset( $_POST['pickup_time_minutes'] );

        if ( isset( $_POST['waiting_time_hours'] ) && isset( $_POST['waiting_time_minutes'] ) && $_POST['waiting_time_hours'] != '' &&  $_POST['waiting_time_minutes'] != '' ) {
            $_POST['waiting_time'] = $_POST['waiting_time_hours'] . ':' . $_POST['waiting_time_minutes'];
        }

        if ( isset( $_POST['additional_pickups'] ) ) {
            $_POST['additional_pickups'] = $_POST['additional_pickups'];
        }

        if ( isset( $_POST['additional_dropoff'] ) ) {
            $_POST['additional_dropoff'] = $_POST['additional_dropoff'];
        }

        if ( 'two_way' === $_POST['journey_type'] ) {
            $_POST['pickup_date_return'] = date( 'Y-m-d', strtotime( $_POST['pickup_date_return'] ) );
            $_POST['pickup_time_return'] = $_POST['pickup_time_hours_return'] . ':' . $_POST['pickup_time_minutes_return'];
            if ( isset( $_POST['waiting_time_hours_return'] ) && isset( $_POST['waiting_time_minutes_return'] ) && $_POST['waiting_time_hours_return'] != '' &&  $_POST['waiting_time_minutes_return'] != '' ) {
                $_POST['waiting_time_return'] = $_POST['waiting_time_hours_return'] . ':' . $_POST['waiting_time_minutes_return'];
            }
            if ( isset( $_POST['additional_pickups_return'] ) ) {
                $_POST['additional_pickups_return'] = $_POST['additional_pickups_return'];
            }

            if ( isset( $_POST['additional_dropoff_return'] ) ) {
                $_POST['additional_dropoff_return'] = $_POST['additional_dropoff_return'];
            }
        }

        if ( isset( $_POST['waiting_time_hours'] ) ) {
			unset( $_POST['waiting_time_hours'] );
		}
        if ( isset( $_POST['waiting_time_minutes'] ) ) {
			unset( $_POST['waiting_time_minutes'] );
		}
        if ( isset( $_POST['terms'] ) ) {
			unset( $_POST['terms'] );
		}
        if ( isset( $_POST['validtestep1'] ) ) {
			unset( $_POST['validtestep1'] );
		}

        if ( isset( $_POST['pickup_time_hours_return'] ) ) {
			unset( $_POST['pickup_time_hours_return'] );
		}
        if ( isset( $_POST['pickup_time_minutes_return'] ) ) {
			unset( $_POST['pickup_time_minutes_return'] );
		}
        if ( isset( $_POST['waiting_time_hours_return'] ) ) {
			unset( $_POST['waiting_time_hours_return'] );
		}
        if ( isset( $_POST['waiting_time_minutes_return'] ) ) {
			unset( $_POST['waiting_time_minutes_return'] );
		}

        $db_ref = simontaxi_get_session( 'booking_step1', 0, 'db_ref' );
		// $data_get = simontaxi_get_session( 'booking_step1' );
		$data_get = $_POST;
		if ( isset( $data_get['number_of_persons'] ) ) {
			unset( $data_get['number_of_persons'] );
		}
		if ( isset( $data_get['flight_arrival_time'] ) ) {
			unset( $data_get['flight_arrival_time'] );
		}


		$pickup_location_new = isset( $_POST['pickup_location_new'] ) ? $_POST['pickup_location_new'] : '';
		$drop_location_new = isset( $_POST['drop_location_new'] ) ? $_POST['drop_location_new'] : '';

		if ( ! $modify ) {
            simontaxi_set_session( 'booking_step1', null );
			simontaxi_set_session( 'booking_step2', null );
			simontaxi_set_session( 'booking_step3', null );
			simontaxi_set_session( 'booking_step4', null );
			simontaxi_set_session( 'discount_details', null );

            simontaxi_set_session( 'booking_step1', $_POST );

			$additional_data = array(
				'reference' => simontaxi_get_token(simontaxi_get_option( 'booking_ref_length', 6) ),
				'user_id' => get_current_user_id(),
				'date' => date( 'Y-m-d h:i:s' ),
				'status_updated' => date( 'Y-m-d h:i:s' ),
				/**
				 * @since 2.0.0
				 */
				 'pickup_location_new' => $pickup_location_new,
				 'drop_location_new' => $drop_location_new,
				);
			simontaxi_set_session( 'booking_step1', $additional_data );
			foreach( $additional_data as $key => $val ) {
				$data_get[ $key ] = $val;
			}
            if ( $_POST['booking_type']=='hourly' ) {
                $additional_data = array(
					'drop_location' => '-' ,
					'journey_type' => '-',
					);
				simontaxi_set_session( 'booking_step1', $additional_data );
				foreach( $additional_data as $key => $val ) {
					$data_get[ $key ] = $val;
				}
            }

			if ( isset( $data_get['pickup_location_new'] ) ) {
				unset( $data_get['pickup_location_new'] );
			}
			if ( isset( $data_get['drop_location_new'] ) ) {
				unset( $data_get['drop_location_new'] );
			}
            $wpdb->insert( $wpdb->prefix . 'st_bookings', $data_get );
			$additional_data = array(
					'db_ref' => $wpdb->insert_id,
					);
			simontaxi_set_session( 'booking_step1', $additional_data );
			foreach( $additional_data as $key => $val ) {
				$data_get[ $key ] = $val;
			}
        } elseif ( $db_ref > 0 ) {
            $db_ref = simontaxi_get_session( 'booking_step1', 0, 'db_ref' );
            $ref = simontaxi_get_session( 'booking_step1', '', 'reference' );
            if ( '' === $ref ) {
                $ref = simontaxi_get_token(simontaxi_get_option( 'booking_ref_length', 6) );
            }
            simontaxi_set_session( 'booking_step1', $_POST );
			$additional_data = array(
				'reference' => $ref,
				/**
				 * @since 2.0.0
				 */
				 'pickup_location_new' => $pickup_location_new,
				 'drop_location_new' => $drop_location_new,
				);
			simontaxi_set_session( 'booking_step1', $additional_data );
			foreach( $additional_data as $key => $val ) {
				$data_get[ $key ] = $val;
			}

			$additional_data = array(
					'reference' => $ref,
					'user_id' => get_current_user_id(),
					);
			simontaxi_set_session( 'booking_step1', $additional_data );
			foreach( $additional_data as $key => $val ) {
				$data_get[ $key ] = $val;
			}

            if ( $_POST['booking_type']=='hourly' ) {
                $additional_data = array(
					'drop_location' => '-' ,
					'journey_type' => '-',
					);
				simontaxi_set_session( 'booking_step1', $additional_data );
				foreach( $additional_data as $key => $val ) {
					$data_get[ $key ] = $val;
				}
            }

            $check = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}st_bookings WHERE ID = $db_ref" );

            if ( $db_ref == 0 || empty( $check ) ) { //For any reason if it not already inserted, Let us insert
                $additional_data = array(
					'reference' => simontaxi_get_token(simontaxi_get_option( 'booking_ref_length', 6) ),
					'user_id' => get_current_user_id(),
					'date' => date( 'Y-m-d h:i:s' ),
					'status_updated' => date( 'Y-m-d h:i:s' ),
					);
				simontaxi_set_session( 'booking_step1', $additional_data );
				foreach( $additional_data as $key => $val ) {
					$data_get[ $key ] = $val;
				}
                if ( isset( $data_get['pickup_location_new'] ) ) {
					unset( $data_get['pickup_location_new'] );
				}
				if ( isset( $data_get['drop_location_new'] ) ) {
					unset( $data_get['drop_location_new'] );
				}
				$wpdb->insert( $wpdb->prefix . 'st_bookings', $data_get );

                $db_ref = $wpdb->insert_id;
            } else {
				$additional_data = array(
					'status_updated' => date( 'Y-m-d h:i:s' ),
					);
				simontaxi_set_session( 'booking_step1', $additional_data );
				foreach( $additional_data as $key => $val ) {
					$data_get[ $key ] = $val;
				}
				if ( isset( $data_get['db_ref'] ) ) {
					unset( $data_get['db_ref'] );
				}
				if ( isset( $data_get['pickup_location_new'] ) ) {
					unset( $data_get['pickup_location_new'] );
				}
				if ( isset( $data_get['drop_location_new'] ) ) {
					unset( $data_get['drop_location_new'] );
				}
                $wpdb->update( $wpdb->prefix . 'st_bookings',  $data_get, array( 'ID' => $db_ref) );
            }

            simontaxi_set_session( 'booking_step1', array( 'db_ref' => $db_ref ) );
			simontaxi_set_session( 'booking_step2', null );
			simontaxi_set_session( 'booking_step3', null );
			simontaxi_set_session( 'booking_step4', null );
        } else {

			simontaxi_set_session( 'booking_step1', $_POST );

			$additional_data = array(
					'reference' => simontaxi_get_token(simontaxi_get_option( 'booking_ref_length', 6) ),
					'user_id' => get_current_user_id(),
					'date' => date( 'Y-m-d h:i:s' ),
					'status_updated' => date( 'Y-m-d h:i:s' ),
					/**
					 * @since 2.0.0
					 */
					 'pickup_location_new' => $pickup_location_new,
					 'drop_location_new' => $drop_location_new,
					);
			simontaxi_set_session( 'booking_step1', $additional_data );
			foreach( $additional_data as $key => $val ) {
				$data_get[ $key ] = $val;
			}
			if ( isset( $data_get['db_ref'] ) ) {
				unset( $data_get['db_ref'] );
			}
			if ( isset( $data_get['pickup_location_new'] ) ) {
				unset( $data_get['pickup_location_new'] );
			}
			if ( isset( $data_get['drop_location_new'] ) ) {
				unset( $data_get['drop_location_new'] );
			}

			// $wpdb->insert( $wpdb->prefix . 'st_bookings', $data_get );
			$sql = 'insert into ' . $wpdb->prefix . 'st_bookings (' . implode( ',', array_keys( $data_get ) ) . ') values("' . implode( '","', array_values( $data_get ) ) . '")';
			$wpdb->query( $sql );
			$db_ref = $wpdb->insert_id;
            simontaxi_set_session( 'booking_step1', array( 'db_ref' => $db_ref ) );
			simontaxi_set_session( 'booking_step2', null );
			simontaxi_set_session( 'booking_step3', null );
			simontaxi_set_session( 'booking_step4', null );			
        }		
        $redirect_to = simontaxi_get_bookingsteps_urls( 'step2' );
        wp_safe_redirect( $redirect_to );
    }
}

//Getting action tab user selects
$tabs = simontaxi_get_active_tab();

//Get all airports entered in admin
$airports = simontaxi_get_airports();

//Get all hourly packages
$hourly_packs = simontaxi_get_hourly_packages();

if ( ! $modify )
{
    simontaxi_set_session( 'booking_step1', null );
	simontaxi_set_session( 'booking_step2', null );
	simontaxi_set_session( 'booking_step3', null );
	simontaxi_set_session( 'booking_step4', null );
}
else
{
    if ( ! empty( $_POST ) ) {
        if ( isset( $_POST['pickup_location'] ) ) {
			$from = $_POST['pickup_location'];
		}
    } else {
        $from = (isset( $booking_step1['pickup_location'] ) ) ? $booking_step1['pickup_location'] : '';
    }
    $to = $from = '';
    if ( ! empty( $_POST ) ) {
        if ( isset( $_POST['drop_location'] ) ) {
			$to = $_POST['drop_location'];
		}
    } else {
        $from = ( isset( $booking_step1['drop_location'] ) ) ? $booking_step1['drop_location'] : '';
    }
    $is_hourly = FALSE;
    if ( ! empty( $_POST) ) {
        if ( isset( $_POST['booking_type'] ) && $_POST['booking_type'] == 'hourly' ) {
            $is_hourly = TRUE;
		}
    } else {
        $is_hourly = ( isset( $booking_step1['booking_type'] ) && $booking_step1['booking_type'] == 'hourly' ) ? TRUE : FALSE;
    }

    if ( $is_hourly)
    {
        $pack = simontaxi_get_value( $booking_step1, 'hourly_package' );
        $dt=simontaxi_get_hourly_packages( $pack );
        $distance=0;
    }
    else
    {
        $dt=simontaxi_get_distance_time( $from, $to, '' );
        $distance = $dt[0];
    }

    $du = simontaxi_get_distance_units();
    $currency_code = simontaxi_get_currency_code();
    if ( ! $distance )
    $distance=0;
}
$booking_summany_step1 = simontaxi_get_option( 'booking_summany_step1', 'yes' );
$cols = $columns;
if ( $booking_summany_step1 == 'no' ) {
    $cols = 12;
}

/**
 * Let us apply the classes based on placemet. We are using this page in different places. Based on place we need to change layout ie. class to fit the content in particular place.
 * Defaults to 'homepageleft'
*/
if ( $pre_class != '' ) {
		$class = $pre_class;
	} else {
$class = 'col-lg-8 st-right-col st-equal-col col-md-8';
	}
$class_div = 'st-section st-right-section-form';
$tabfills = 'nav nav-tabs st-nav-tabs st-color-tabs st-home-booking';
$tab_content = 'tab-content st-home-tab-content';

$breadcrumb = '<ol class="st-breadcrumb">
                        <li class="active"><a>' . simontaxi_get_step1_title() . '</a></li>
                        <li><a>' . simontaxi_get_step2_title() . '</a></li>
                        <li><a>' . simontaxi_get_step3_title() . '</a></li>
                        <li><a>' . simontaxi_get_step4_title() . '</a></li>
                    </ol>';
if ( $placement == 'hometop' ) {
    if ( $pre_class != '' ) {
		$class = $pre_class;
	} else {
		$class = 'col-lg-8 col-lg-offset-2 col-md-12';
	}
    $class_div = '';
    $tabfills = 'nav nav-tabs st-nav-tabs st-home-booking nav-justified';
    $tab_content = 'tab-content st-home-tab-content';
}
?>
<!-- Booking Form -->
<?php if ( $placement == 'fullpage' ) {
    if ( $pre_class != '' ) {
		$class = $pre_class;
	} else {
		$class = 'col-lg-' . $cols . ' col-md-8 col-sm-12';
	}
    $class_div = 'st-booking-block st-mtabs';
    $tabfills = 'nav nav-pills st-booking-pills nav-justified';
    $tab_content = 'tab-content';
    ?>
<div class="st-section-sm st-grey-bg">
    <div class="container">
        <div id="main_row_booking" class="row">
<?php } ?>
            <div class="<?php echo esc_attr( $class); ?>">

				<?php if ( $placement == 'hometop' ) { ?>
				<h3 class="st-gheading">
				<?php
				echo simontaxi_get_option( 'booking_step1_title_home', esc_html__( 'Book a taxi now', 'simontaxi' ) ); ?></h3>
				<?php } ?>

				<?php if ( $placement != 'hometop' ) { ?>
                <div class="<?php echo esc_attr( $class_div); ?>">
                <?php } ?>
				<!-- TabPills Navigation -->
                    <ul class="<?php echo esc_attr( $tabfills); ?>">
                         <?php
                         $p2p_active = 'active';
                         $airport_active = '';
                         $hourly_active = '';
						 
						 if ( in_array( 'Point to Point Transfer', $tabs ) ) {
							$p2p_active = 'active';
							$airport_active = '';
							$hourly_active = '';
						 } elseif ( in_array( 'Airport Transfer', $tabs ) ) {
							 $p2p_active = '';
							$airport_active = 'active';
							$hourly_active = '';
						 } elseif (in_array( 'Hourly Rental', $tabs) ) {
							 $p2p_active = '';
							$airport_active = '';
							$hourly_active = 'active';
						 }
                         if ( $modify ) {
                             $tab = simontaxi_get_value( $booking_step1, 'booking_type' );
                             if ( $tab == 'p2p' ) {
                                 $p2p_active = 'active';
                                 $airport_active = '';
                                 $hourly_active = '';
                             } elseif ( $tab == 'airport' ) {
                                 $p2p_active = '';
                                 $airport_active = 'active';
                                 $hourly_active = '';
                             } elseif ( $tab == 'hourly' ) {
                                 $p2p_active = '';
                                 $airport_active = '';
                                 $hourly_active = 'active';
                             }
                         }
                         if ( in_array( 'Point to Point Transfer', $tabs ) ) { ?>
                        <li class="<?php echo esc_attr( $p2p_active); ?>"><a data-toggle="pill" href="#st-p2p"><?php echo simontaxi_get_p2ptab_title(); ?></a></li>
                        <?php } ?>
                        <?php if ( in_array( 'Airport Transfer', $tabs ) ) { ?>
                        <li class="<?php echo esc_attr( $airport_active); ?>"><a data-toggle="pill" href="#st-airport"><?php echo simontaxi_get_airporttab_title(); ?></a></li>
                        <?php } ?>
                        <?php if (in_array( 'Hourly Rental', $tabs) ) { ?>
                        <li class="<?php echo esc_attr( $hourly_active); ?>"><a data-toggle="pill" href="#st-hourly"><?php echo simontaxi_get_hourlytab_title(); ?></a></li>
                        <?php } ?>
                    </ul>
                    <!-- end TabPills Navigation -->

				<?php /* if ( $placement != 'hometop' ) { ?>
                <div class="<?php echo esc_attr( $class_div); ?>">
                <?php } */ ?>

					<?php echo simontaxi_print_errors() ?>

                    <!-- Booking Progress -->
                    <?php if ( $placement == 'fullpage' ) {
                        echo $breadcrumb;
                    }?>
                    <!-- end Booking Progress -->
					
                    <div class="<?php echo esc_attr( $tab_content); ?>">
                    <div id="main_title">
						<h3>LET US DRIVE YOU TO YOUR DESTINATION</h3>
						<p>Explore more places that you wish to travel</p>					
					</div>
						<!-- TAB-1 -->
                        <div id="st-p2p" class="tab-pane fade <?php if ( $p2p_active == 'active' ) { echo 'in active'; }?>">
                            <!-- Booking Progress -->
                            <?php if ( in_array( $placement, array( 'hometop', 'homeleft' ) ) ) {
                                echo $breadcrumb;
                            }?>
                            <!-- end Booking Progress -->
                            <form id="booking-p2p" action="" method="POST" class="st-booking-form row">
                            <input type="hidden" name="booking_type" value="p2p">
                                <?php if ( simontaxi_is_allow_twoway_booking() == 'yes' ) :?>
                                <div class="col-sm-12">
                                    <div class="input-group st-radio">
                                        <input id="radio11" type="radio" name="journey_type" value="one_way" onclick="toggle_show(this.value)" <?php if ( $modify ) { if ( simontaxi_get_value( $booking_step1, 'journey_type' ) == 'one_way' ) echo 'checked'; } else echo 'checked'?>>
                                        <label for="radio11"><span><span></span></span><?php echo simontaxi_get_oneway_title(); ?></label>
                                        <input id="radio22" type="radio" name="journey_type" value="two_way" onclick="toggle_show(this.value)" <?php if ( $modify ) { if ( simontaxi_get_value( $booking_step1, 'journey_type' ) == 'two_way' ) echo 'checked'; }?>>
                                        <label id="label_2" for="radio22"><span><span></span></span><?php echo simontaxi_get_twoway_title(); ?></label>
                                    </div>
                                </div>
                                <?php else:
                                ?>
                                <input type="hidden" name="journey_type" value="one_way">
                                <?php
                                endif; ?>

                                <div class="form-group col-sm-6">
                                    <label> <?php echo simontaxi_get_pickuppoint_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <?php
										if ( 'predefined_place' === $predefined_place && 'dropdown' === $vehicle_places_display ) {
										$pickup_locations = simontaxi_get_locations( 'pickup_location' );
										$pickup_location = simontaxi_get_value( $booking_step1, 'pickup_location_new' );

										?>
										<select name="pickup_location" id="pickup_location" class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5">
										<option value=""><?php esc_html_e( 'Please select ' .simontaxi_get_pickuppoint_title() ) ?></option>
										<?php
										if ( ! empty( $pickup_locations ) ) {
											foreach( $pickup_locations as $key => $val ) { ?>
											<option value="<?php echo $key ?>" <?php if( $pickup_location == $key ) { echo 'selected';} ?>><?php echo $val; ?></option>
											<?php
											}
										}
										?>
										</select>
										<?php } else { 
										$plocation = simontaxi_get_value( $booking_step1, 'pickup_location' );
										if ( empty( $plocation ) ) {
											$plocation = simontaxi_get_value( $booking_step1, 'pickup_location_new' );
										}
										?>
										<i class="st-icon icon-location-pin"></i>
										<input type="text" class="form-control required <?php echo $predefined_place ?>" placeholder="<?php echo simontaxi_get_pickuppoint_title(); ?>" name="pickup_location" id="pickup_location" <?php echo (in_array( $vehicle_places, array( 'googleall', 'googleregions', 'googlecities' ) ) ) ? 'onClick="initialize(this.id);" onFocus="initialize(this.id);"' : ''; ?> value="<?php if ( $modify ){ echo $plocation; }?>" autocomplete="off" tabIndex="0"/>
										<?php } ?>

                                        <input type="hidden" id="pickup_location_country" name="pickup_location_country" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'pickup_location_country' ); }?>">
                                        <input type="hidden" id="pickup_location_lat" name="pickup_location_lat" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'pickup_location_lat' ); }?>">
                                        <input type="hidden" id="pickup_location_lng" name="pickup_location_lng" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'pickup_location_lng' ); }?>">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label> <?php echo simontaxi_get_dropoffpoint_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <?php if ( '' !== $predefined_place_dropoff && 'dropdown' === $vehicle_places_dropoff_display ) {
										$drop_locations = simontaxi_get_locations( 'drop_location' );
										$drop_location = simontaxi_get_value( $booking_step1, 'drop_location_new' );
										?>
										<select name="drop_location" id="drop_location" class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5">
										<option value=""><?php esc_html_e( 'Please select ' . simontaxi_get_dropoffpoint_title() ) ?></option>
										<?php
										if ( ! empty( $drop_locations ) ) {
											foreach( $drop_locations as $key => $val ) { ?>
											<option value="<?php echo $key ?>" <?php if ( $drop_location == $key ) { echo 'selected';}?>><?php echo $val; ?></option>
											<?php
											}
										}
										?>
										</select>
										<?php
										} else {
											$dlocation = simontaxi_get_value( $booking_step1, 'drop_location' );
											if ( empty( $dlocation ) ) {
												$dlocation = simontaxi_get_value( $booking_step1, 'drop_location_new' );
											}
										?>
										<i class="st-icon icon-location-pin"></i>
                                        <input type="text" class="form-control <?php echo $predefined_place_dropoff?>" placeholder="<?php echo simontaxi_get_dropoffpoint_title(); ?>" <?php echo (in_array( $vehicle_places_dropoff, array( 'googleall', 'googleregions', 'googlecities' ) ) ? 'onClick="initialize(this.id);" onFocus="initialize(this.id);"' : '' ) ?> value="<?php if ( $modify ){ echo $dlocation; }?>" name="drop_location" id="drop_location" autocomplete="off" tabIndex="1"/>
										<?php } ?>

                                        <input type="hidden" id="drop_location_country" name="drop_location_country" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'drop_location_country' ); }?>">
                                        <input type="hidden" id="drop_location_lat" name="drop_location_lat" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'drop_location_lat' ); }?>">
                                        <input type="hidden" id="drop_location_lng" name="drop_location_lng" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'drop_location_lng' ); }?>">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label><?php echo simontaxi_get_pickupdate_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <i class="st-icon icon-calendar"></i>
                                        <input type="text" class="form-control st_datepicker_limit" data-language='en' data-timepicker="false" placeholder="<?php echo simontaxi_get_pickupdate_title(); ?>" name="pickup_date" id="p2p_pickup_date" value="<?php if ( $modify ) {
                                        $pickup_date = simontaxi_get_value( $booking_step1, 'pickup_date' );
                                        if ( $pickup_date != '' )
                                            echo date(simontaxi_get_option( 'st_date_format', 'd-m-Y' ), strtotime( $pickup_date) ); } ?>">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 pickup_time">
                                    <div class="row">
                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $_POST['pickup_time_hours'] ) || isset( $_POST['pickup_time_minutes'] ) ) {
                                        $minutes = $_POST['pickup_time_minutes'];
                                        $hours = $_POST['pickup_time_hours'];
                                    } elseif ( isset( $booking_step1['pickup_time'] ) ) {
                                        $parts =  explode( ':', $booking_step1['pickup_time'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
                                    <label id="pickup_time"><?php echo simontaxi_get_pickuptime_title(); ?><?php echo simontaxi_required_field(); ?></label>
									<div class="col-xs-6 cs-pad-right">
									
									<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_hours" id="pickup_time_hours">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
                                    
									
									<div class="col-xs-6 cs-pad-left">								
									<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_minutes" id="pickup_time_minutes">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									</div>
                                </div>
								
								<?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) != 'no' ) { ?>
                                <div class="form-group col-sm-12">
                                    <label> <?php esc_html_e( 'No. of persons', 'simontaxi' ); ?><?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) == 'yesrequired' ) { echo simontaxi_required_field(); }?></label>
                                    <div class="inner-addon right-addon">
                                        <input type="number" name="number_of_persons" id="number_of_persons" class="form-control number_of_persons_p2p" placeholder="<?php esc_html_e( 'Enter No. persons', 'simontaxi' )?>" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'number_of_persons' ); } ?>">
                                    </div>
                                </div>
                                <?php } ?>
								
                                <?php if ( simontaxi_is_allow_additional_pickups() == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Additional Pickup Points', 'simontaxi' ); ?></label>
                                    <?php
                                    $additional_pickups = 0;
                                    if ( $modify ) {
                                        $additional_pickups = simontaxi_get_value( $booking_step1, 'additional_pickups', $additional_pickups);
                                    }
                                    ?>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="additional_pickups" id="additional_pickups">
                                        <option value="0"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
                                        <?php for( $i = 1; $i <= simontaxi_get_option( 'max_additional_pickups', 5); $i++) {
                                            $sel = '';
                                            if ( $i == $additional_pickups)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i.esc_html__( ' Points', 'simontaxi' ) . '</option>';
                                        }?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <?php if ( simontaxi_is_allow_additional_dropoff() == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Additional Dropoff Points', 'simontaxi' ); ?></label>
                                    <?php
                                    $additional_dropoff = 0;
                                    if ( $modify ) {
                                        $additional_dropoff = simontaxi_get_value( $booking_step1, 'additional_dropoff', $additional_dropoff);
                                    }
                                    ?>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="additional_dropoff" id="additional_dropoff">
                                        <option value="0"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
                                        <?php for( $i = 1; $i <= simontaxi_get_max_additional_dropoff(); $i++) {
                                            $sel = '';
                                            if ( $i == $additional_dropoff)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i . ' Points</option>';
                                        }?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <?php if ( simontaxi_get_option( 'allow_waiting_time', 'no' ) == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <div class="row">

                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $_POST['waiting_time_hours'] ) || isset( $_POST['waiting_time_minutes'] ) ) {
                                        $hours = $_POST['waiting_time_hours'];
                                        $minutes = $_POST['waiting_time_minutes'];
                                    } elseif ( isset( $booking_step1['waiting_time'] ) ) {
                                        $parts =  explode( ':', $booking_step1['waiting_time'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
									<div class="col-xs-6 cs-pad-right">
									<label><?php esc_html_e( 'Waiting time', 'simontaxi' ); ?></label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="waiting_time_hours" id="waiting_time_hours">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>

									<label>&nbsp;</label>
									<div class="col-xs-6 cs-pad-left">
									
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="waiting_time_minutes" id="waiting_time_minutes">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									</div>
                                </div>
                                <?php endif; ?>

                                <div <?php if ( isset( $booking_step1[ 'journey_type'] )&& $booking_step1[ 'journey_type']=='two_way' ) echo 'style="display:block;"'; else echo 'style="display:none;"'; ?> id="showvalue">
                                <div class="form-group col-sm-6">
                                    <label><?php echo esc_html__( 'Return ', 'simontaxi' ) . simontaxi_get_pickupdate_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <i class="st-icon icon-calendar"></i>
                                        <input type="text" class="form-control st_datepicker_limit_return" data-language='en' data-timepicker="false" placeholder="<?php echo simontaxi_get_pickupdate_title(); ?>" name="pickup_date_return" id="pickup_date_return" value="<?php

            if ( simontaxi_get_value( $booking_step1, 'pickup_date_return' ) != '' )
                echo date(simontaxi_get_option( 'st_date_format', 'd-m-Y' ), strtotime(simontaxi_get_value( $booking_step1, 'pickup_date_return' ) ) );

        ?>">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                  <div class="row">
                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $booking_step1['pickup_time_return'] ) ) {
                                        $parts =  explode( ':', $booking_step1['pickup_time_return'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
                                    
									<label id="return_pickup_time"><?php esc_html_e( 'Return ', 'simontaxi' );
									echo simontaxi_get_pickuptime_title(); ?>
									<?php echo simontaxi_required_field(); ?></label>
									<div class="col-xs-6 cs-pad-right">
									<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_hours_return" id="pickup_time_hours_return">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									<label>&nbsp;</label>
									<div class="col-xs-6 cs-pad-left">
									
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_minutes_return" id="pickup_time_minutes_return">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
                                </div>
								</div>

                                <?php if ( simontaxi_get_option( 'allow_additional_pickups_return', 'no' ) == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Additional Pickup Points (Return)', 'simontaxi' ); ?></label>
                                    <?php
                                    $additional_pickups_return = 0;
                                    if ( $modify ) {
                                        $additional_pickups = simontaxi_get_value( $booking_step1, 'additional_pickups_return', $additional_pickups_return);
                                    }
                                    ?>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="additional_pickups_return" id="additional_pickups_return">
                                        <option value="0"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
                                        <?php for( $i = 1; $i <= simontaxi_get_option( 'max_additional_dropoff', '5' ); $i++ ) {
                                            $sel = '';
                                            if ( $i == $additional_pickups_return )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i.esc_html__( ' Points', 'simontaxi' ) . ' </option>';
                                        }?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <?php if ( simontaxi_get_option( 'allow_additional_pickups_return', 'no' ) == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Additional Dropoff Points (Return)', 'simontaxi' ); ?></label>
                                    <?php
                                    $additional_dropoff_return = 0;
                                    if ( $modify ) {
                                        $additional_dropoff_return = simontaxi_get_value( $booking_step1, 'additional_dropoff_return', $additional_dropoff_return);
                                    }
                                    ?>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="additional_dropoff_return" id="additional_dropoff_return">
                                        <option value="0"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
                                        <?php for ( $i = 1; $i <= simontaxi_get_option( 'max_additional_dropoff_return', '5' ); $i++ ) {
                                            $sel = '';
                                            if ( $i == $additional_dropoff_return )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i.esc_html__( ' Points', 'simontaxi' ) . '</option>';
                                        }?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <?php if ( simontaxi_get_option( 'allow_waiting_time_return', 'no' ) == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <div class="row">

                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $_POST['waiting_time_hours_return'] ) || isset( $_POST['waiting_time_minutes_return'] ) ) {
                                        $hours = $_POST['waiting_time_hours_return'];
                                        $minutes = $_POST['waiting_time_minutes_return'];
                                    } elseif ( isset( $booking_step1['waiting_time_return'] ) ) {
                                        $parts =  explode( ':', $booking_step1['waiting_time_return'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
									<div class="col-xs-6 cs-pad-right">
									<label><?php esc_html_e( 'Waiting time (Return)', 'simontaxi' ); ?></label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="47%" data-size="5" name="waiting_time_hours_return" id="waiting_time_hours_return">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>

									<div class="col-xs-6 cs-pad-left">
									<label>&nbsp;</label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="47%" data-size="5" name="waiting_time_minutes_return" id="waiting_time_minutes_return">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									</div>
                                </div>
                                <?php endif; ?>
								<?php do_action( 'simontaxi_step1_p2p_additional_fields' ); ?>
                                </div>

                                <?php if ( simontaxi_terms_page() == 'step1' ) : ?>
                                <div class="col-sm-12">
                                    <div class="input-group st-top40">
                                        <div>
                                            <input id="terms" type="checkbox" name="terms" value="option">
                                            <label for="terms"><span><span></span></span><i class="st-terms-accept"><?php echo simontaxi_terms_text(); ?></i></label>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>


                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-mobile" name="validtestep1"><?php echo apply_filters( 'simontaxi_filter_nextstep_title', esc_html__( 'Next Step', 'simontaxi' ) ); ?></button>

                                    <input type="hidden" name="distance" id="distance" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'distance' );} ?>">
                                    <input type="hidden" name="distance_text" id="distance_text" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'distance_text' );} ?>">
                                    <input type="hidden" name="duration_text" id="duration_text" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'duration_text' );} ?>">
                                    <input type="hidden" name="distance_units" id="distance_units" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'distance_units' );} ?>">
                                </div>

                            </form>
                        </div>

                        <!-- AIR PORT Transfer-->
                        <div id="st-airport" class="tab-pane fade <?php if ( $airport_active == 'active' ) { echo 'in active'; }?>">

                            <!-- Booking Progress -->
                            <?php if ( in_array( $placement, array( 'hometop', 'homeleft' ) ) ) {
                                echo $breadcrumb;
                            }?>
                            <!-- end Booking Progress -->

                            <form class="st-booking-form row" id="booking-airport" method="POST" action="">
                                <input type="hidden" name="booking_type" value="airport">
                                <input type="hidden" name="journey_type" value="one_way">
                                <?php
                                /**
                                 * Let us display the airport tab options based on admin settings
                                */
                                $allow_twoway_airport = simontaxi_get_option( 'allow_twoway_airport', 'both' );
                                if ( $allow_twoway_airport == 'both' ) {
                                ?>
                                <div class="col-sm-12">
									<div class="input-group st-radio" id="airporttype">
										<input type="radio" id="drop_location_airport" name="airport" value="drop_location" onclick="toggle_pickupdrop( 'drop_location' )" <?php if ( $modify ) { if ( simontaxi_get_value( $booking_step1, 'airport' ) == 'drop_location' ) { echo 'checked'; } } else echo 'checked'?>>
										<label id="drop_location_airport" for="drop_location_airport"><span><span></span></span><?php esc_html_e( 'Going to ' . $fixed_point_title, 'simontaxi' ); ?></label>

										<input type="radio" id="pickup_location_airport" name="airport" value="pickup_location" onclick="toggle_pickupdrop( 'pickup_location')" <?php if ( $modify ) { if ( simontaxi_get_value( $booking_step1, 'airport' ) == 'pickup_location' ) echo 'checked'; }?>>
										<label id="pickup_location_airport" for="pickup_location_airport"><span><span></span></span><?php esc_html_e( 'Coming from ' . $fixed_point_title, 'simontaxi' )?></label>
									</div>
								</div>
								<?php } else {
									if ( $allow_twoway_airport == 'comingfrom' ) {
										$airport = 'pickup_location';
									} else {
										$airport = 'drop_location';
									}

									if ( $modify ) {
										if ( simontaxi_get_value( $booking_step1, 'airport' ) == 'drop_location' ) {
											$airport = 'drop_location';
										} elseif ( simontaxi_get_value( $booking_step1, 'airport' ) == 'pickup_location' ) {
											$airport = 'pickup_location';
										}
										$airport = simontaxi_get_value( $booking_step1, 'airport' );
									}
									?>
									<input type="hidden" name="airport" id="airport" value="<?php echo esc_attr( $airport ); ?>">
									<?php
								}?>

                                <div class="form-group col-sm-6">
									<label><?php esc_html_e( $fixed_point_title, 'simontaxi' ); ?><?php echo simontaxi_required_field(); ?></label>
									<div class="inner-addon right-addon">
										<i class="st-icon icon-location-pin"></i>
										<?php
										$airportname = 'drop_location';
										if ( $modify ) {
											if ( isset( $booking_step1['airport'] ) ) {
												$airportname = $booking_step1['airport'];
											} else {
												$airportname = 'pickup_location';
											}
										}									
										?>
										<select class="selectpicker show-tick show-menu-arrow"  data-size="5" id="airportname" name="<?php echo esc_attr( $airportname ); ?>">
												
												<option value=""><?php esc_html_e( 'Choose ' . $fixed_point_title, 'simontaxi' )?></option>
												
												<?php
											foreach ( $airports as $airport) {
												$selected = '';
												$value = $airport['id'];					

												if ( $modify ) {
													if ( isset( $booking_step1['airport'] ) ) {
														if ( $booking_step1[$booking_step1['airport']] == $airport['id'] )
															$selected = 'selected';
													} elseif ( isset( $_POST[ $airportname ] ) && $_POST[ $airportname ] == $airport['id'] ) {
														$selected = 'selected';
													} elseif ( isset( $_POST[ $airportname . '_new' ] ) && $_POST[ $airportname . '_new' ] == $airport['id'] ) {
														$selected = 'selected';
													}
												}
												echo '<option value="' . $value . '" ' . $selected . '>' . $airport['name'] . '</option>';
											}
											?>
										</select>
									</div>
								</div>
                                <div class="form-group col-sm-6" id="pickupfieldset">
									<label> <?php
											if ( $modify ) {
												if ( isset( $booking_step1['airport'] ) ) {
														if ( $booking_step1['airport'] == 'pickup_location' ) {
															esc_html_e( 'Drop off', 'simontaxi' );
														}
														else {
															if ( $allow_twoway_airport == 'comingfrom' ) {
																esc_html_e( 'Drop off', 'simontaxi' );
															} else {
																esc_html_e( 'Pickup', 'simontaxi' );
															}
														}
												} else {
													if ( $allow_twoway_airport == 'comingfrom' ) {
														esc_html_e( 'Drop off', 'simontaxi' );
													} else {
														esc_html_e( 'Pickup', 'simontaxi' );
													}
												}
											} else {
												if ( $allow_twoway_airport == 'comingfrom' ) {
													esc_html_e( 'Drop off', 'simontaxi' );
												} else {
													esc_html_e( 'Pickup', 'simontaxi' );
												}
											} ?><?php esc_html_e( ' point', 'simontaxi' ); ?><?php echo simontaxi_required_field(); ?></label>
									<div class="inner-addon right-addon">
										<i class="st-icon icon-location-pin"></i>
										<?php
										if ( $airportname == 'pickup_location' ) {
											$name = 'drop_location';
										} else {
											$name = 'pickup_location';
										}
										if ( $modify ) {
											if ( isset( $booking_step1['airport'] ) && $booking_step1['airport'] == 'pickup_location' ) {
												$name = 'drop_location';
											} else {
												if ( isset( $_POST['airport'] ) && $_POST['airport'] == 'pickup_location' ) {
													$name = 'drop_location';
												}
											}
										}

										$value = '';
										if ( $modify ) {
											if ( isset( $booking_step1['airport'] ) &&
												$booking_step1['airport'] == 'pickup_location' ) {
													$value = simontaxi_get_value( $booking_step1, 'drop_location' );
												} else {
													$value = simontaxi_get_value( $booking_step1, $name );
													if ( empty( $value ) ) {
														$value = simontaxi_get_value( $booking_step1, $name . '_new' );
													}
												}
										} elseif ( isset( $booking_step1['pickup_location'] ) ) {
											$value = simontaxi_get_value( $booking_step1, 'pickup_location' );
											if ( empty( $value ) ) {
												$value = simontaxi_get_value( $booking_step1, 'pickup_location_new' );
											}
										} else {
											if ( ! empty( $_POST[ $name ] ) ) {
													$value = $_POST[ $name ];
												} elseif ( ! empty( $_POST[ $name . '_new' ] ) ) {
													$value = $_POST[ $name . '_new' ];
												}
										}
										
										if ( 'predefined_place' === $predefined_place_airport && 'dropdown' === $vehicle_places_airport_display ) {
											$pickup_locations = simontaxi_get_locations( 'pickup_location' );
											$pickup_location = simontaxi_get_value( $booking_step1, 'pickup_location_new' );
										?>
										<select name="<?php echo esc_attr( $name ); ?>" id="pickinguplocation" class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5">
										<option value=""><?php esc_html_e( 'Please select ' .simontaxi_get_pickuppoint_title() ) ?></option>
										<?php
										if ( ! empty( $pickup_locations ) ) {
											foreach( $pickup_locations as $key => $val ) { ?>
											<option value="<?php echo $key ?>" <?php if( $pickup_location == $key ) { echo 'selected';} ?>><?php echo $val; ?></option>
											<?php
											}
										}
										?>
										</select>
										<?php } else {
										?>
										<input type="text" autocomplete="off" placeholder="<?php esc_html_e( 'Type and choose location', 'simontaxi' )?>" id="pickinguplocation" name="<?php echo esc_attr( $name ); ?>" class="form-control <?php echo $predefined_place_airport; ?>" value="<?php echo esc_attr( $value ); ?>" <?php echo (in_array( $vehicle_places_airport, array( 'googleall', 'googleregions', 'googlecities' ) ) ? 'onClick="initialize(this.id);" onFocus="initialize(this.id);"' : '' ) ?>>
										<?php } ?>
									</div>
								</div>
                                <div class="form-group col-sm-6">
                                    <label><?php echo simontaxi_get_pickupdate_title()?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <i class="st-icon icon-calendar"></i>
                                        <input type="text" class="form-control st_datepicker_limit" placeholder="<?php echo sprintf( esc_html__( 'Select %s', 'simontaxi' ), simontaxi_get_pickupdate_title() ); ?>" name="pickup_date" id="airport_pickup_date" value="<?php echo simontaxi_get_value( $booking_step1, 'pickup_date' ); ?>" />
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 pickup_time_airport">
                                    <div class="row">
                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $_POST['pickup_time_hours'] ) || isset( $_POST['pickup_time_minutes'] ) ) {
                                        $minutes = $_POST['pickup_time_minutes'];
                                        $hours = $_POST['pickup_time_hours'];
                                    } elseif ( isset( $booking_step1['pickup_time'] ) ) {
                                        $parts =  explode( ':', $booking_step1['pickup_time'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
									<div class="col-xs-6 cs-pad-right">
									<label><?php echo simontaxi_get_pickuptime_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_hours" id="airport_pickup_time_hours">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi'  ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>

									<div class="col-xs-6 cs-pad-left">
									<label>&nbsp;</label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_minutes" id="airport_pickup_time_minutes">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
                                </div>
								</div>
								
								<?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) != 'no' ) { ?>
                                <div class="form-group col-sm-12">
                                    <label> <?php esc_html_e( 'No. of persons', 'simontaxi' ); ?><?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) == 'yesrequired' ) { echo simontaxi_required_field(); }?></label>
                                    <div class="inner-addon right-addon">
                                        <input type="number" name="number_of_persons" id="number_of_persons" class="form-control number_of_persons_airport" placeholder="<?php esc_html_e( 'Enter No. persons', 'simontaxi' )?>" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'number_of_persons' ); } ?>">
                                    </div>
                                </div>
                                <?php } ?>
								
                                <?php if ( simontaxi_get_option( 'allow_flight_number', 'no' ) != 'no' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( $fixed_point_vehicle_name . ' Number', 'simontaxi' ); ?><?php if ( simontaxi_get_option( 'allow_flight_number', 'no' ) == 'yesrequired' ) { echo simontaxi_required_field(); }?></label>
                                    <input type="text" class="form-control" placeholder="<?php esc_html_e( $fixed_point_vehicle_name . ' Number', 'simontaxi' ); ?>" name="flight_no" id="flight_no" value="<?php
                                    if ( $modify ) {
                                        echo simontaxi_get_value( $booking_step1, 'flight_no' );
                                        } ?>">
                                </div>
                                <?php endif; ?>
								
								<?php if ( simontaxi_get_option( 'allow_flight_arrival_time', 'no' ) != 'no' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( $fixed_point_vehicle_name . ' Arrival Time', 'simontaxi' ); ?><?php if ( simontaxi_get_option( 'allow_flight_arrival_time', 'no' ) == 'yesrequired' ) { echo simontaxi_required_field(); }?></label>
                                    <input type="text" class="form-control" placeholder="<?php esc_html_e( $fixed_point_vehicle_name . ' Arrival Time. Eg: 15:26', 'simontaxi' ); ?>" name="flight_arrival_time" id="flight_arrival_time" value="<?php
                                    if ( $modify ) {
                                        echo simontaxi_get_value( $booking_step1, 'flight_arrival_time' );
                                        } ?>">
                                </div>
                                <?php endif; ?>

                                <?php if ( simontaxi_is_allow_additional_pickups() == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Additional Pickup Points', 'simontaxi' ); ?></label>
                                    <?php
                                    $additional_pickups = 0;
                                    if ( $modify ) {
                                        $additional_pickups = simontaxi_get_value( $booking_step1, 'additional_pickups', $additional_pickups);
                                    }
                                    ?>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="additional_pickups" id="additional_pickups">
                                        <option value="0"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
                                        <?php for ( $i = 1; $i <= simontaxi_get_max_additional_pickups(); $i++ ) {
                                            $sel = '';
                                            if ( $i == $additional_pickups)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i . ' Points</option>';
                                        }?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <?php if ( simontaxi_is_allow_additional_dropoff() == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Additional Drop-off Points', 'simontaxi' ); ?></label>
                                    <?php
                                    $additional_dropoff = 0;
                                    if ( $modify ) {
                                        $additional_dropoff = simontaxi_get_value( $booking_step1, 'additional_dropoff', $additional_dropoff);
                                    }
                                    ?>
                                    <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="additional_dropoff" id="additional_dropoff">
                                        <option value="0"><?php esc_html_e( 'No', 'simontaxi' ); ?></option>
                                        <?php for ( $i = 1; $i <= simontaxi_get_max_additional_dropoff(); $i++ ) {
                                            $sel = '';
                                            if ( $i == $additional_dropoff )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $i . '" ' . $sel . '>' . $i.esc_html__( ' Points', 'simontaxi' ) . ' </option>';
                                        }?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <?php if ( simontaxi_get_option( 'allow_waiting_time', 'no' ) == 'yes' ) :?>
                                <div class="form-group col-sm-6">
                                    <div class="row">

                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $_POST['waiting_time_hours'] ) || isset( $_POST['waiting_time_minutes'] ) ) {
                                        $hours = $_POST['waiting_time_hours'];
                                        $minutes = $_POST['waiting_time_minutes'];
                                    } elseif ( isset( $booking_step1['waiting_time'] ) ) {
                                        $parts =  explode( ':', $booking_step1['waiting_time'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
                                    <div class="col-xs-6 cs-pad-right">
									<label><?php esc_html_e( 'Waiting time', 'simontaxi' ); ?></label>
									<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="waiting_time_hours" id="waiting_time_hours">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									<div class="col-xs-6 cs-pad-left">
									<label>&nbsp;</label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="waiting_time_minutes" id="waiting_time_minutes">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									</div>
                                </div>
                                <?php endif; ?>
								<?php do_action( 'simontaxi_step1_airport_additional_fields' ); ?>
                                <?php if ( simontaxi_terms_page() == 'step1' ) : ?>
                                <div class="col-sm-12">
                                    <div class="input-group st-top40">
                                        <div>
                                            <input id="terms_airport" type="checkbox" name="terms" value="option">
                                            <label for="terms_airport"><span><span></span></span><i class="st-terms-accept"><?php echo simontaxi_terms_text(); ?></i></label>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-mobile" name="validtestep1"><?php echo apply_filters( 'simontaxi_filter_nextstep_title', esc_html__( 'Next Step', 'simontaxi' ) ); ?></button>

                                    <input type="hidden" name="distance" id="distance_airport" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'distance' );} ?>">
                                    <input type="hidden" name="distance_text" id="distance_text_airport" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'distance_text' );} ?>">
                                    <input type="hidden" name="duration_text" id="duration_text_airport" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'duration_text' );} ?>">
                                    <input type="hidden" name="distance_units" id="distance_units_airport" value="<?php if ( $modify ) { echo simontaxi_get_value( $booking_step1, 'distance_units' );} ?>">
                                </div>
                            </form>
                        </div>
                        <!-- Hourly Rental -->
                        <div id="st-hourly" class="tab-pane fade <?php if ( $hourly_active == 'active' ) { echo 'in active'; }?>">

                            <!-- Booking Progress -->
                            <?php if ( in_array( $placement, array( 'hometop', 'homeleft' ) ) ) {
                                echo $breadcrumb;
                            }?>
                            <!-- end Booking Progress -->

                            <form class="st-booking-form row" id="booking-hourly" action="" method="POST">
                                <input type="hidden" name="booking_type" value="hourly">
                                <input type="hidden" name="journey_type" value="one_way">
                                <div class="form-group col-sm-6">
                                    <label><?php esc_html_e( 'Package', 'simontaxi' ); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <?php
                                        $hourly_package = '';
                                        if ( $modify ) {
                                            $hourly_package = simontaxi_get_value( $booking_step1, 'hourly_package' );
                                        }
                                        ?>
                                        <select class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5" name="hourly_package" id="hourly_package">
                                            <option value=""><?php esc_html_e( 'Please select', 'simontaxi' ); ?></option>
                                            <?php
                                            foreach ( $hourly_packs as $pack) {
                                                $selected = '';
                                                if ( $modify ) {
                                                    if ( $hourly_package == $pack['slug'] )
                                                            $selected = 'selected';
                                                }
                                                echo '<option value="' . $pack['slug'] . '" ' . $selected . '>' . $pack['name'] . ' (' . $pack['hourly_hours'] . ' hours' . ')</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label> <?php echo simontaxi_get_pickuppoint_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <?php
										if ( 'predefined_place' === $predefined_place_hourly && 'dropdown' === $vehicle_places_hourly_display ) {
										$pickup_locations = simontaxi_get_locations( 'pickup_location' );
										$pickup_location = simontaxi_get_value( $booking_step1, 'pickup_location_new' );
										?>
										<select name="pickup_location" id="hourly_pickup_location" class="selectpicker show-tick show-menu-arrow" data-width="100%" data-size="5">
										<option value=""><?php esc_html_e( 'Please select ' .simontaxi_get_pickuppoint_title() ) ?></option>
										<?php
										if ( ! empty( $pickup_locations ) ) {
											foreach( $pickup_locations as $key => $val ) { ?>
											<option value="<?php echo $key ?>" <?php if( $pickup_location == $key ) { echo 'selected';} ?>><?php echo $val; ?></option>
											<?php
											}
										}
										?>
										</select>
										<?php
										} else {
											$plocation = simontaxi_get_value( $booking_step1, 'pickup_location' );
											if ( empty( $plocation ) ) {
												$plocation = simontaxi_get_value( $booking_step1, 'pickup_location_new' );
											}
										?>
										<i class="st-icon icon-location-pin"></i>
                                        <input type="text" class="form-control required <?php echo $predefined_place_hourly; ?>" placeholder="<?php echo simontaxi_get_pickuppoint_title(); ?>" name="pickup_location" id="hourly_pickup_location" <?php echo (in_array( $vehicle_places_hourly, array( 'googleall', 'googleregions', 'googlecities' ) ) ) ? 'onClick="initialize(this.id);" onFocus="initialize(this.id);"' : ''; ?> value="<?php if ( $modify ){ echo $plocation; }?>" autocomplete="off"/>
										<?php } ?>
                                    </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label><?php echo simontaxi_get_pickupdate_title(); ?><?php echo simontaxi_required_field(); ?></label>
                                    <div class="inner-addon right-addon">
                                        <i class="st-icon icon-calendar"></i>
                                        <input type="text" class="form-control st_datepicker_limit" placeholder="<?php echo simontaxi_get_pickupdate_title(); ?>" name="pickup_date" id="hourly_pickup_date" value="<?php
                                        if ( $modify ) {
                                            $pickup_date = simontaxi_get_value( $booking_step1, 'pickup_date' );
                                            if ( $pickup_date != '' )
                                                echo date(simontaxi_get_option( 'st_date_format', 'd-m-Y' ), strtotime( $pickup_date) );
                                            }?>">
                                    </div>
                                </div>
                                <div class="form-group col-sm-6 pickup_time_hourly">
								<div class="row">

                                    <?php
                                    $hours = '';
                                    $minutes = '';
                                    if ( isset( $_POST['pickup_time_hours'] ) || isset( $_POST['pickup_time_minutes'] ) ) {
                                        $minutes = $_POST['pickup_time_minutes'];
                                        $hours = $_POST['pickup_time_hours'];
                                    } elseif ( isset( $booking_step1['pickup_time'] ) ) {
                                        $parts =  explode( ':', $booking_step1['pickup_time'] );
                                        $hours = trim( $parts[0] );
                                        $minutes = trim( $parts[1] );
                                    }
                                    ?>
                                    <div class="col-xs-6 cs-pad-right">
									<label><?php echo simontaxi_get_pickuptime_title(); ?><?php echo simontaxi_required_field(); ?></label>
									<select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_hours" id="hourly_pickup_time_hours">
                                        <option value=""><?php esc_html_e( 'Hour', 'simontaxi' ); ?></option>
                                        <?php for ( $h = 0; $h <= 23; $h++ ) {
                                            $val = str_pad( $h,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $hours)
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
									<div class="col-xs-6 cs-pad-left">
									<label>&nbsp;</label>
                                    <select class="selectpicker show-tick show-menu-arrow" data-size="5" name="pickup_time_minutes" id="hourly_pickup_time_minutes">
                                        <option value=""><?php esc_html_e( 'Min', 'simontaxi' ); ?></option>
                                        <?php for ( $m = 0; $m < 60; $m+=5 ) {
                                            $val = str_pad( $m,2,0, STR_PAD_LEFT);
                                            $sel = '';
                                            if ( $val == $minutes )
                                                $sel = ' selected="selected"';
                                            echo '<option value="' . $val . '" ' . $sel . '>' . $val . '</option>';
                                        }?>
                                    </select>
									</div>
                                </div>
								</div>
								
								<?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) != 'no' ) { ?>
                                <div class="form-group col-sm-12">
                                    <label> <?php esc_html_e( 'No. of persons', 'simontaxi' ); ?><?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) == 'yesrequired' ) { echo simontaxi_required_field(); }?></label>
                                    <div class="inner-addon right-addon">
                                        <input type="number" name="number_of_persons" id="number_of_persons" class="form-control number_of_persons_hourly" placeholder="<?php esc_html_e( 'Enter No. persons', 'simontaxi' )?>" value="<?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'number_of_persons' ); } ?>">
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if ( simontaxi_get_option( 'allow_itinerary', 'no' ) != 'no' ) { ?>
                                <div class="form-group col-sm-12">
                                    <label> <?php esc_html_e( 'Itinerary', 'simontaxi' ); ?><?php if ( simontaxi_get_option( 'allow_itinerary', 'no' ) == 'yesrequired' ) { echo simontaxi_required_field(); }?></label>
                                    <div class="inner-addon right-addon">
                                        <textarea name="itineraries" id="itineraries" rows="4" cols="60" class="form-control" placeholder="<?php esc_html_e( 'Itinerary example : 1). Legoland 2). AEON Bukit Indah 3). etc. 4. Return drop off address', 'simontaxi' )?>"><?php if ( $modify ){ echo simontaxi_get_value( $booking_step1, 'itineraries' );}?></textarea>
                                    </div>
                                </div>
                                <?php } ?>
								
								<?php do_action( 'simontaxi_step1_hourlyrental_additional_fields' ); ?>
								
								
                                <?php if ( simontaxi_terms_page() == 'step1' ) : ?>
                                <div class="col-sm-12">
                                    <div class="input-group st-top40">
                                        <div>
                                            <input id="hourly_terms" type="checkbox" name="terms" value="option">
                                            <label for="hourly_terms"><span><span></span></span><i class="st-terms-accept"><?php echo simontaxi_terms_text(); ?></i></label>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-mobile" name="validtestep1"><?php echo apply_filters( 'simontaxi_filter_nextstep_title', esc_html__( 'Next Step', 'simontaxi' ) ); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php if ( $placement != 'hometop' ) { ?>
                </div>
                <?php } ?>
            </div>
            <?php if ( $placement == 'fullpage' ) { ?>
            <?php if ( $booking_summany_step1 == 'yes' && isset( $booking_step1) && ( ! empty( $booking_step1 ) ) ) {
                require SIMONTAXI_PLUGIN_PATH . '/booking/includes/booking-steps/right-side.php';
                } ?>
        </div>
    </div>
</div>
            <?php } ?>
<!-- /Booking Form -->
<script type="text/javascript">
function toggle_show(type) {
    var type = jQuery( 'input[name="journey_type"]:checked' ).val();
    if (type == 'two_way' ) {
    jQuery( '#showvalue' ).show();
    }
    else {
    jQuery( '#showvalue' ).hide();
    }
}
toggle_show();
</script>

<script type="text/javascript">
jQuery(document).ready(function ( $ ) {
    <?php
    $minimum_notice = simontaxi_get_option( 'minimum_notice', 1);
    $maximum_notice = simontaxi_get_option( 'maximum_notice', 1);;
    $maximum_notice_type = simontaxi_get_option( 'maximum_notice_type', 'month' );

    $maximum_notice_days = 30;
    if ( $maximum_notice_type == 'day' )
    $maximum_notice_days = $maximum_notice;
    elseif ( $maximum_notice_type == 'month' )
    $maximum_notice_days = $maximum_notice * 30;
    elseif ( $maximum_notice_type == 'year' )
    $maximum_notice_days = $maximum_notice * 12 * 30;
    ?>
    var dateFormat = 'dd-mm-yy';
    /*reference docs - http://api.jqueryui.com/datepicker/ */
    var onward_date = $( '.st_datepicker_limit' ).datepicker({
    minDate: new Date(new Date().getTime()+(<?php echo $minimum_notice; ?>*24*60*60*1000) ),
    todayButton:true,
    clearButton:true,
    autoClose: true,
    timePicker: false,
    dateFormat: dateFormat,
    maxDate: new Date(new Date().getTime()+(<?php echo $maximum_notice_days; ?>*24*60*60*1000) ),
    showOtherYears: true
    }).on( 'change', function(){
        return_date.datepicker( "option", "minDate", getDate( this ) );
    });

    var return_date = $( '.st_datepicker_limit_return' ).datepicker({
        minDate: new Date(new Date().getTime()+(<?php echo $minimum_notice; ?>*24*60*60*1000) ),
        todayButton:true,
        clearButton:true,
        autoClose: true,
        timePicker: false,
        dateFormat: dateFormat,
        maxDate: new Date(new Date().getTime()+(<?php echo $maximum_notice_days; ?>*24*60*60*1000) ),
        showOtherYears: true
        });

      function getDate( element ) {
      var date;
      try {
        date = jQuery.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
      return date;
    }


$( '#booking-p2p' ).submit(function (event) {

    var pickup_location = $( '#pickup_location' ).val();
    var drop_location = jQuery( '#drop_location' ).val();
    var p2p_pickup_date = jQuery( '#p2p_pickup_date' ).val();
    var pickup_time = jQuery( '#pickup_time' ).val();
    var pickup_time_hours = jQuery( '#pickup_time_hours' ).val();
    var pickup_time_minutes = jQuery( '#pickup_time_minutes' ).val();
    var pickup_date_return = jQuery( '#pickup_date_return' ).val();
    var pickup_time_hours_return = jQuery( '#pickup_time_hours_return' ).val();
    var pickup_time_minutes_return = jQuery( '#pickup_time_minutes_return' ).val();
    var distance = jQuery( '#distance' ).val();

    var error = 0;
    jQuery( '.error' ).hide();

	if ( pickup_location == '') {
    jQuery( '#pickup_location' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please Enter your %s', 'simontaxi' ), simontaxi_get_pickuppoint_title() ); ?> </span>' );
    error++;
    }
    if (drop_location == '') {
    jQuery( '#drop_location' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please Enter your %s', 'simontaxi' ), simontaxi_get_dropoffpoint_title() ); ?> </span>' );
    error++;
    }

    if (pickup_location != '' && drop_location != '')
    {
    if ( pickup_location == drop_location)
    {
    jQuery( '#pickup_location' ).after( '<span class="error"> <?php echo sprintf( esc_html__( '%s and %s should not be same', 'simontaxi' ), simontaxi_get_pickuppoint_title(), simontaxi_get_dropoffpoint_title() ); ?></span>' );
    error++;
    }
    }

    <?php
    $outofservice = simontaxi_get_option( 'outofservice', 0);
    if ( $outofservice > 0) :
    ?>
    if ( distance > <?php echo $outofservice; ?>)
    {
    jQuery( '#pickup_location' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Service not available for above %s%s', 'simontaxi' ), $outofservice, simontaxi_get_distance_units() ); ?></span>' );
    error++;
    }
    <?php endif; ?>
    if (p2p_pickup_date == '') {
    jQuery( '#p2p_pickup_date' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please Enter your %s', 'simontaxi' ),  simontaxi_get_pickupdate_title() ); ?></span>' );
    error++;
    }

    if (pickup_time_minutes == '') {
    jQuery( '.pickup_time' ).after( '<span class="error"><?php echo sprintf( esc_html__( 'Please select your %s minutes', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
    error++;
    }
    if (pickup_time_hours == '') {
    jQuery( '.pickup_time' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please select your %s hours', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
    error++;
    }

    if (jQuery( 'input[name="journey_type"]:checked' ).val() == 'two_way' ) {
        if (pickup_date_return == '') {
        jQuery( '#pickup_date_return' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please select your return %s', 'simontaxi' ),simontaxi_get_pickupdate_title() ); ?></span>' );
        error++;
        }
        if ( p2p_pickup_date != '' && pickup_date_return != '' )
        {
            var parts = p2p_pickup_date.split( '-' );
			p2p_pickup_date_utc = Date.UTC( parts[0], parts[1], parts[2],0,0,0,0);
			var parts2 = pickup_date_return.split( '-' );
			pickup_date_utc_return = Date.UTC( parts2[0], parts2[1], parts2[2],0,0,0,0);
			
			if ( pickup_date_return < p2p_pickup_date ) {
                jQuery( '#pickup_date_return' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Return %s should be after %s', 'simontaxi' ), simontaxi_get_pickupdate_title(), simontaxi_get_pickupdate_title() ); ?></span>' );
                error++;
            } else {
                
                if ( pickup_date_utc_return < p2p_pickup_date_utc)
                {
                jQuery( '#pickup_date_return' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Date of return should be after %s', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
                error++;
                }
            }
        }

        if (pickup_time_minutes_return == '') {
        jQuery( '#pickup_time_minutes_return' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please select your return %s minutes', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
        error++;
        }
        if (pickup_time_hours_return == '') {
        jQuery( '#pickup_time_hours_return' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please select your return %s hours', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
        error++;
        }
    }
	
	<?php if ( simontaxi_get_option( 'allow_number_of_persons', 'no' ) === 'yesrequired' ) { ?>
	if ( jQuery( '.number_of_persons_p2p' ).val() == '' ) {
        jQuery( '.number_of_persons_p2p' ).after( '<span class="error"> <?php esc_html_e( 'Please enter number of persons', 'simontaxi' ); ?></span>' );
        error++;
    }
	<?php } ?>

    <?php if ( simontaxi_terms_page() == 'step1' ) : ?>
    if ( ! document.getElementById( 'terms' ).checked ) {
        jQuery( '#terms' ).closest( '.input-group' ).after( '<span class="error"> <?php esc_html_e( 'You should accept Terms of Service to proceed', 'simontaxi' )?></span>' );
        error++;
    }
    <?php endif; ?>
    if (error > 0) event.preventDefault();
});

});
</script>

<script src="//maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $google_api; ?>"></script>
<?php wp_enqueue_script( 'gmap3', SIMONTAXI_PLUGIN_URL . 'js/gmap3.min.js' );
$unitSystem = 'google.maps.UnitSystem.METRIC';
if ( $vehicle_distance == 'miles' ) {
    $unitSystem = 'google.maps.UnitSystem.IMPERIAL';
}

$vehicle_country_region_from = simontaxi_get_option( 'vehicle_country_region_from', '' );
$vehicle_country_region_to = simontaxi_get_option( 'vehicle_country_region_to', '' );

$vehicle_country_dropoff_region_from = simontaxi_get_option( 'vehicle_country_dropoff_region_from', '' );
$vehicle_country_dropoff_region_to = simontaxi_get_option( 'vehicle_country_dropoff_region_to', '' );

$jquery_version = '1.12.1';
wp_enqueue_style( 'jquery-ui-style', '//code.jquery.com/ui/' . $jquery_version . '/themes/base/jquery-ui.min.css', array(), $jquery_version );

// wp_enqueue_script( 'jquery-ui-new', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'), false, true );
?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
var ajaxUrl = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';
jQuery(".predefined_place").autocomplete({
    source: function (request, response) {
        var type = jQuery( '.predefined_place:focus' ).attr( 'id' );
        jQuery.getJSON(ajaxUrl + '?action=st_auto_places&type='+type+'&term=' + request.term, function (data) {
            response(data);
        });
    }
    , select: function (event, ui) {
        jQuery(this).val(ui.item.value);
    }
    , minLength: 2
});
function initialize(id) {

	/**
	12.864162,77.438610

	13.139807,77.711895
	*/
	
	if ( id == 'drop_location' ) {
		var selected_country = '<?php echo $vehicle_country_dropoff; ?>';
		<?php if ( '' !== $vehicle_country_dropoff_region_from && '' !== $vehicle_country_dropoff_region_to ) : ?>
		<?php
		$vehicle_country_dropoff_region_from_parts = explode( ',', $vehicle_country_dropoff_region_from );
		$vehicle_country_dropoff_region_to_parts = explode( ',', $vehicle_country_dropoff_region_to );
		?>
		<?php if ( ! empty( $vehicle_country_dropoff_region_from_parts[1] ) && ! empty( $vehicle_country_dropoff_region_to_parts[1] ) && ! empty( $vehicle_country_dropoff_region_from_parts[0] ) && ! empty( $vehicle_country_dropoff_region_to_parts[0] ) ) { ?>
		var regionBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(<?php echo $vehicle_country_dropoff_region_from_parts[1]; ?>,<?php echo $vehicle_country_dropoff_region_to_parts[1]; ?>),
		new google.maps.LatLng(<?php echo $vehicle_country_dropoff_region_from_parts[0]; ?>,<?php echo $vehicle_country_dropoff_region_to_parts[0]; ?>) );
		<?php } ?>
		<?php endif; ?>

	} else {
		var selected_country = '<?php echo $vehicle_country; ?>';
		<?php if ( '' !== $vehicle_country_region_from && '' !== $vehicle_country_region_to ) : ?>
		<?php
		$vehicle_country_region_from_parts = explode( ',', $vehicle_country_region_from );
		$vehicle_country_region_to_parts = explode( ',', $vehicle_country_region_to );
		?>
		
		<?php if ( ! empty( $vehicle_country_region_from_parts[1] ) && ! empty( $vehicle_country_region_to_parts[1] ) && ! empty( $vehicle_country_region_from_parts[0] ) && ! empty( $vehicle_country_region_to_parts[0] ) ) { ?>
		
		var regionBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(<?php echo (float) $vehicle_country_region_from_parts[1]; ?>,<?php echo (float) $vehicle_country_region_to_parts[1]; ?>),
		new google.maps.LatLng(<?php echo (float) $vehicle_country_region_from_parts[0]; ?>,<?php echo (float) $vehicle_country_region_to_parts[0]; ?>) );
		
		<?php } ?>
		
		<?php endif; ?>
	}


	if ( typeof(regionBounds) == 'undefined' ) {
		var options = {
                    language: 'en-GB',
                    <?php
                    /**
                    * If the admin impose restriction on places, then we are taking only regions (Important places). Reference : https://developers.google.com/places/supported_types
					* Regions: locality (Name)
						sublocality
						postal_code
						country
						administrative_area_level_1 (State)
						administrative_area_level_2 (District)
					* Cities: locality
						administrative_area_level_3
                    */
                    if ( $vehicle_places == 'googleregions' ) {
                    ?>
                    types: ['(regions)'],
                    <?php }
					if ( $vehicle_places == 'googlecities' ) {
                    ?>
                    types: ['(cities)'],
                    <?php } ?>
                    componentRestrictions: {
                        country: selected_country
                    }
                };
	} else {
    var options = {
                    language: 'en-GB',
                    <?php
                    /**
                    * If the admin impose restriction on places, then we are taking only regions (Important places). Reference : https://developers.google.com/places/supported_types
					* Regions: locality (Name)
						sublocality
						postal_code
						country
						administrative_area_level_1 (State)
						administrative_area_level_2 (District)
					* Cities: locality
						administrative_area_level_3
                    */
                    if ( $vehicle_places == 'googleregions' ) {
                    ?>
                    types: ['(regions)'],
                    <?php }
					if ( $vehicle_places == 'googlecities' ) {
                    ?>
                    types: ['(cities)'],
                    <?php }
					?>
					<?php
					/**
					 * We have received many requests to restrict the region to book, so here is solution!
					 */
					if ( 'predefined' !== $vehicle_places ) { ?>
					bounds: regionBounds,
					strictBounds: true,
					<?php } ?>
                    componentRestrictions: {
                        country: selected_country
                    }
                };
	}
    var input = jQuery( '#' + id);
    var autocomplete_my = new google.maps.places.Autocomplete(input[0], options);
	
	if ( typeof(regionBounds) != 'undefined' ) {
		<?php
		/**
		 * We have received many requests to restrict the region to book, so here is solution!
		 */
		if ( 'predefined' !== $vehicle_places ) { ?>
		autocomplete_my.setOptions({bounds:regionBounds, strictBounds: true});
		<?php } ?>
	}
	
    google.maps.event.addListener(autocomplete_my, 'place_changed', function () {
        place = autocomplete_my.getPlace();
		// console.log( place );
        jQuery( '#' + id + '_lat' ).val(place.geometry.location.lat() );
        jQuery( '#' + id + '_lng' ).val(place.geometry.location.lng() );

        if (place.address_components) {
            stateID = place.address_components[0] && place.address_components[0].long_name || '';
            countryID = place.address_components[3] && place.address_components[3].short_name || '';
            jQuery( '#' + id + '_country' ).val( countryID );
        }
        stateID = place.formatted_address;
        input.blur();
        input.val(stateID);
        calculate_distance( id );
    });
}

function calculate_distance( id )
{
    if ( id == 'pickinguplocation' ) { //Airport transfer
        var pickup_location = jQuery( '#airportname' ).val();
        var drop_location = jQuery( '#pickinguplocation' ).val();
    } else {
        var pickup_location = jQuery( '#pickup_location' ).val();
        var drop_location = jQuery( '#drop_location' ).val();
    }
    if ( pickup_location != '' && drop_location != '' ) {
    get_map(pickup_location, drop_location, id);
    }
}
function get_map(PickLocation,DropLocation,id) {

    jQuery("#pickup_location").gmap3({
    clear: {},

     getroute:{
       options:{
           origin:PickLocation,
           destination:DropLocation,
           travelMode: google.maps.TravelMode.DRIVING,
           provideRouteAlternatives: true,
           optimizeWaypoints: true,
           /*--- Set avoid Tolls and Highways by Zunnie@FreshDigital 18 Apr 2016 ---*/
           avoidHighways: false,
           avoidTolls: false,
        /*
        google.maps.UnitSystem.METRIC - specifies usage of the metric system. Distances are shown using kilometers.
        google.maps.UnitSystem.IMPERIAL - specifies usage of the Imperial (English) system. Distances are shown using miles.
        */
        unitSystem: <?php echo $unitSystem; ?>
       },
       callback: function(results){

    //console.log(results);
    if ( results)
    {
        var no_of_routes = results.routes.length;
        // var distance_temp = results.routes[0].legs[0].distance.text;
		var distance_temp_text = results.routes[0].legs[0].distance.text;
		var distance_temp = results.routes[0].legs[0].distance.value; // The distance in Meters || Feet.
        for (var i = 0; i < no_of_routes; i++) {
            /*
			route_wise_distance = results.routes[i].legs[0].distance.text;
			if ( parseFloat(route_wise_distance) < parseFloat(distance_temp) ) {
              distance_temp = route_wise_distance;
            }
			*/
			var route_wise_distance_text = results.routes[i].legs[0].distance.text;
			var route_wise_distance = results.routes[i].legs[0].distance.value; // The distance in meters.
			if (parseFloat(route_wise_distance) < parseFloat(distance_temp) ) {
			  distance_temp = route_wise_distance;
			  distance_temp_text = route_wise_distance_text;
			}

        }

        //var distance = distance_temp;
        //var dist0 = distance;
        //var dist  = distance.split(" ")[0];
        //dist = dist.replace( ',', '' );
		<?php if ( 'miles' === $vehicle_distance ) {
			?>
		var distance = parseFloat( distance_temp / 5280).toFixed(2); // Hence we are getting distance in feet we are converting that into Miles
			<?php
		} else { ?>
		var distance = parseFloat( distance_temp / 1000).toFixed(2); // Hence we are getting distance in meters we are converting that into KM
		<?php } ?>
		var dist = distance;

        var time  = results.routes[0].legs[0].duration.text+" (Approx)";
        if (id == 'pickinguplocation' ) {
            jQuery( '#distance_airport' ).val(dist);
            jQuery( '#distance_text_airport' ).val(distance_temp_text);
            jQuery( '#duration_text_airport' ).val(time);
            /**
             * To make sure if admin changes unit system in middle of the user booking process, we need to find out
            */
            jQuery( '#distance_units_airport' ).val( '<?php echo $vehicle_distance; ?>' );
        } else {
        jQuery( '#distance' ).val(dist);
        jQuery( '#distance_text' ).val(distance_temp_text);
        jQuery( '#duration_text' ).val(time);
        /**
         * To make sure if admin changes unit system in middle of the user booking process, we need to find out
        */
        jQuery( '#distance_units' ).val( '<?php echo $vehicle_distance; ?>' );
        }

          if ( ! results ) return;
       }
       }
     }

    });
}

/* Airport transfer functions */
function toggle_pickupdrop(type) {
    if (type != 'pickup_location' ) {
        jQuery( '#pickupfieldset' ).removeClass( 'hide' );
        jQuery( '#pickinguplocation' ).attr( 'name', 'pickup_location' );
        jQuery( '#pickupfieldset label' ).text( '<?php echo simontaxi_get_pickuppoint_title(); ?>' );
        jQuery( '#airportname' ).attr( 'name', 'drop_location' );
    }
    else {
        jQuery( '#airportname' ).attr( 'name', 'pickup_location' );
        jQuery( '#pickinguplocation' ).attr( 'name', 'drop_location' );
        jQuery( '#pickupfieldset label' ).text( '<?php echo simontaxi_get_dropoffpoint_title(); ?>' );
    }
}

 jQuery( '#booking-airport' ).submit(function (event) {
    var airport = jQuery( 'input[name="airport"]:checked' ).val();
    var pickinguplocation = jQuery( '#pickinguplocation' ).val();
    var airportname = jQuery( '#airportname' ).val();
    var select_date = jQuery( '#airport_pickup_date' ).val();
    var airport_pickup_time_hours = jQuery( '#airport_pickup_time_hours' ).val();
    var airport_pickup_time_minutes = jQuery( '#airport_pickup_time_minutes' ).val();
    var distance_airport = jQuery( '#distance_airport' ).val();

    var error = 0;
    jQuery( '.error' ).hide();
    <?php if ( $allow_twoway_airport == 'both' ) { ?>
	if ( typeof(airport) == 'undefined' ) {
			jQuery( '#airporttype' ).after( '<span class="error"><?php esc_html_e( 'Please select', 'simontaxi' ); ?></span>' );
            error++;
    }
	<?php } ?>
    if (airportname == '') {
        jQuery( '#airportname' ).after( '<span class="error"><?php esc_html_e( 'Please select ' . $fixed_point_vehicle_name . ' name', 'simontaxi' ); ?></span>' );
        error++;
    }
    if (pickinguplocation == '') {
        if ( jQuery( '#airport' ).val() == 'pickup_location' ) {
            jQuery( '#pickinguplocation' ).after( '<span class="error"> <?php echo esc_html__( 'Please Enter your ', 'simontaxi' ) . simontaxi_get_dropoffpoint_title(); ?> </span>' );
        } else {
        jQuery( '#pickinguplocation' ).after( '<span class="error"> <?php echo esc_html__( 'Please Enter your ', 'simontaxi' ) . simontaxi_get_pickuppoint_title(); ?> </span>' );
        }
        error++;
    }
    <?php
    $outofservice = simontaxi_get_option( 'outofservice', 0);
    if ( $outofservice > 0) :
    ?>
    if ( distance_airport > <?php echo $outofservice; ?>)
    {
    jQuery( '#pickinguplocation' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Service not available for above %s%s', 'simontaxi' ), $outofservice, simontaxi_get_distance_units() ); ?></span>' );
    error++;
    }
    <?php endif; ?>
    if (select_date == '') {
        jQuery( '#airport_pickup_date' ).after( '<span class="error"><?php echo sprintf( esc_html__( 'Please Enter your %s', 'simontaxi' ), simontaxi_get_pickupdate_title() ); ?></span>' );
        error++;
    }
    if (airport_pickup_time_minutes == '') {
    jQuery( '.pickup_time_airport' ).after( '<span class="error"><?php echo sprintf( esc_html__( 'Please select your %s minutes', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
    error++;
    }
    if (airport_pickup_time_hours == '') {
    jQuery( '.pickup_time_airport' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please select your %s hours', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
    error++;
    }
	
	<?php if ( 'yesrequired' === simontaxi_get_option( 'allow_number_of_persons', 'no' ) ) { ?>
    if ( jQuery( '.number_of_persons_airport' ).val() == '' ) {
        jQuery( '.number_of_persons_airport' ).after( '<span class="error"> <?php esc_html_e( 'Please enter number of persons', 'simontaxi' ); ?></span>' );
        error++;
    }
    <?php } ?>
	
    <?php if ( simontaxi_get_option( 'allow_flight_number', 'no' ) == 'yesrequired' ) { ?>
    if ( jQuery( '#flight_no' ).val() == '' ) {
        jQuery( '#flight_no' ).after( '<span class="error"> <?php esc_html_e( 'Please enter ' . $fixed_point_vehicle_name . ' number', 'simontaxi' ); ?></span>' );
        error++;
    }
    <?php } ?>
	<?php if ( simontaxi_get_option( 'allow_flight_arrival_time', 'no' ) == 'yesrequired' ) { ?>
    if ( jQuery( '#flight_arrival_time' ).val() == '' ) {
        jQuery( '#flight_arrival_time' ).after( '<span class="error"> <?php esc_html_e( 'Please enter ' . $fixed_point_vehicle_name . ' arrival time', 'simontaxi' ); ?></span>' );
        error++;
    }
    <?php } ?>
    <?php if ( simontaxi_terms_page() == 'step1' ) : ?>
    if ( ! document.getElementById( 'terms_airport' ).checked ) {
        jQuery( '#terms_airport' ).closest( '.input-group' ).after( '<span class="error"> <?php esc_html_e( 'You should accept Terms of Service to proceed', 'simontaxi' )?></span>' );
        error++;
    }
    <?php endif; ?>
    if (error > 0) event.preventDefault();
});

//Hourly Rental
jQuery( '#booking-hourly' ).submit(function (event) {
    var hourly_package = jQuery( '#hourly_package' ).val();
    var hourly_pickup_location = jQuery( '#hourly_pickup_location' ).val();
    var hourly_pickup_date = jQuery( '#hourly_pickup_date' ).val();
    var hourly_pickup_time_hours = jQuery( '#hourly_pickup_time_hours' ).val();
    var hourly_pickup_time_minutes = jQuery( '#hourly_pickup_time_minutes' ).val();

    var error = 0;
    jQuery( '.error' ).hide();
    if (hourly_package == '') {
        jQuery( '#hourly_package' ).after( '<span class="error"><?php esc_html_e( 'Please select hourly package', 'simontaxi' ); ?></span>' );
        error++;
    }
    if (hourly_pickup_location == '') {
        jQuery( '#hourly_pickup_location' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please Enter your %s', 'simontaxi' ), simontaxi_get_pickuppoint_title() ); ?> </span>' );
        error++;
    }
    if (hourly_pickup_date == '') {
        jQuery( '#hourly_pickup_date' ).after( '<span class="error"><?php echo sprintf( esc_html__( 'Please Enter your %s', 'simontaxi' ), simontaxi_get_pickupdate_title() ); ?></span>' );
        error++;
    }
    if (hourly_pickup_time_minutes == '') {
    jQuery( '.pickup_time_hourly' ).after( '<span class="error"><?php echo sprintf( esc_html__( 'Please select your %s minutes', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
    error++;
    }
    if (hourly_pickup_time_hours == '') {
    jQuery( '.pickup_time_hourly' ).after( '<span class="error"> <?php echo sprintf( esc_html__( 'Please select your %s hours', 'simontaxi' ), simontaxi_get_pickuptime_title() ); ?></span>' );
    error++;
    }
    <?php if ( simontaxi_get_option( 'allow_itinerary', 'no' ) == 'yesrequired' ) { ?>
    if ( jQuery( '#itineraries' ).val() == '' ) {
        jQuery( '#itineraries' ).after( '<span class="error"> <?php esc_html_e( 'Please enter itineraries', 'simontaxi' ); ?></span>' );
        error++;
    }
    <?php } ?>
	<?php if ( 'yesrequired' === simontaxi_get_option( 'allow_number_of_persons', 'no' ) ) { ?>
    if ( jQuery( '.number_of_persons_hourly' ).val() == '' ) {
        jQuery( '.number_of_persons_hourly' ).after( '<span class="error"> <?php esc_html_e( 'Please enter number of persons', 'simontaxi' ); ?></span>' );
        error++;
    }
    <?php } ?>
    <?php if ( simontaxi_terms_page() == 'step1' ) : ?>
    if ( ! document.getElementById( 'hourly_terms' ).checked ) {
        jQuery( '#hourly_terms' ).closest( '.input-group' ).after( '<span class="error"> <?php esc_html_e( 'You should accept Terms of Service to proceed', 'simontaxi' )?></span>' );
        error++;
    }
    <?php endif; ?>
    if (error > 0) event.preventDefault();
});

</script>
