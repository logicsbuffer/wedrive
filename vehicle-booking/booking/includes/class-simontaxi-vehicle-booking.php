<?php
/**
 * Simontaxi - Vehicle Booking Session
 *
 * This is a wrapper class for WP_Session / PHP $_SESSION and handles the storage of session data between pages, errors, etc
 *
 * @author   Digisamaritan
 * @package  Simontaxi - Vehicle Booking
 * @since    2.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Simontaxi_Vehicle_Booking' ) ) :

/**
 * Main Simontaxi_Vehicle_Booking Class.
 *
 * @since 2.0.0
 */
final class Simontaxi_Vehicle_Booking {
	/** Singleton *************************************************************/

	/**
	 * @var Simontaxi_Vehicle_Booking The one true Simontaxi_Vehicle_Booking
	 * @since 1.0.0
	 */
	private static $instance;

	/**
	 * Main Simontaxi_Vehicle_Booking Instance.
	 *
	 * Insures that only one instance of Simontaxi_Vehicle_Booking exists in memory at any one
	 * time. Also prevents needing to define globals all over the place.
	 *
	 * @since 2.0.0
	 * @static
	 * @staticvar array $instance
	 * @uses Simontaxi_Vehicle_Booking::setup_constants() Setup the constants needed.
	 * @uses Simontaxi_Vehicle_Booking::includes() Include the required files.
	 * @uses Simontaxi_Vehicle_Booking::load_textdomain() load the language files.
	 * @see EDD()
	 * @return object|Simontaxi_Vehicle_Booking The one true Simontaxi_Vehicle_Booking
	 */
	public static function instance() {
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Simontaxi_Vehicle_Booking ) ) {
			self::$instance = new Simontaxi_Vehicle_Booking;

			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );

			self::$instance->includes();
			self::$instance->session = new Simontaxi_Session();
		}

		return self::$instance;
	}

	/**
	 * File inclues.
	 *
	 * @access private
	 * @since 2.0.0
	 * @return void
	 */
	private function includes() {
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/vehicle-install.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/vehicle-settings.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/aq-resizer.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/admin/request-callbacks.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/admin/support-request.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/admin/manage-bookings.php';
		/**
		 * To enable the Manage Extensions Menu
		 *		 
		 * @since 2.0.0
		 */
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/admin/manage-extensions.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/admin/manage-countries.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/vehicle-shortcodes.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/booking-functions.php';
		require SIMONTAXI_PLUGIN_PATH . '/booking/includes/ajax-functions.php';

		/**
		 * Session handling introduced
		 *
		 * @since 2.0.0
		*/
		require_once SIMONTAXI_PLUGIN_PATH . '/booking/includes/class-simontaxi-session.php';
		require_once SIMONTAXI_PLUGIN_PATH . '/booking/includes/error-handling.php';
		$this->updater();
	}

	/**
	 * Loads the plugin language files.
	 *
	 * @access public
	 * @since 2.0.0
	 * @return void
	 */
	public function load_textdomain() {

		// Set filter for plugin's languages directory.
		$simontaxi_lang_dir  = dirname( plugin_basename( SIMONTAXI_PLUGIN_FILE ) ) . '/languages/';
		$simontaxi_lang_dir  = apply_filters( 'simontaxi_languages_directory', $simontaxi_lang_dir );

		// Load the default language files.
		load_plugin_textdomain( 'simontaxi', false, $simontaxi_lang_dir );
	}
	
	/**
	 * Gets updater instance.
	 * @since 2.0.0
	 *
	 * @return STVB UPdater
	 */
	public function updater() {
		
		require_once SIMONTAXI_PLUGIN_PATH .  'booking/includes/updaters/class-simontaxi-updater.php';
		$updater = new Simontaxi_Updater();
		$updater->init();
	}
	
	/**
	 * Get the template path.
	 *
	 * @return string
	 *
	 *@since 2.0.6
	 */
	public function template_path() {
		return apply_filters( 'simontaxi_template_path', 'vehicle-booking/' );
	}
	
	/**
	 * Get the plugin path.
	 *
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( SIMONTAXI_PLUGIN_FILE ) );
	}

}

endif;

/**
 * The main function for that returns Vehicle Booking
 *
 * The main function responsible for returning the one true Vehicle Booking
 * Instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 *
 * Example: <?php $simontaxi = STVB(); ?>
 *
 * @since 1.0.0
* @return object|Easy_Digital_Downloads The one true Easy_Digital_Downloads Instance.
 */
function STVB() {
	return Simontaxi_Vehicle_Booking::instance();
}
