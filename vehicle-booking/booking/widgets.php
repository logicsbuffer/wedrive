<?php
/**
 * Plugin widgets
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  widgets
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require SIMONTAXI_PLUGIN_PATH . '/booking/widgets/widget-simontaxi-request-callback.php';
require SIMONTAXI_PLUGIN_PATH . '/booking/widgets/widget-simontaxi-support-contact.php';
