<?php
/**
 * Plugin core functions
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  Functions
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! function_exists( 'simontaxi_get_default_title_plural' ) ) {
	/**
	 * Returns Default usage plural title for the items
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function simontaxi_get_default_title_plural() {
		return apply_filters( 'simontaxi__filter_default_title_plural', simontaxi_get_option( 'default_title_plural', esc_html__( 'Vehicles', 'simontaxi' ) ) );
	}
}


if ( ! function_exists( 'simontaxi_get_option' ) ) {
	/**
	 * Returns given option value
	 *
	 * @param string $option - option to get.
	 * @param string $default - default value.
	 * @since 1.0.0
	 * @return string
	 */
	function simontaxi_get_option( $option = '', $default = '' ) {
		$simontaxi_settings = get_option( 'simontaxi_settings' );
		if ( ! empty( $option ) ) {
			return ( isset( $simontaxi_settings[ $option ] ) ) ? $simontaxi_settings[ $option ] : $default;
		} else {
			return $simontaxi_settings;
		}
	}
}


if ( ! function_exists( 'simontaxi_get_default_title' ) ) {
	/**
	 * Returns Default usage title for the items
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function simontaxi_get_default_title() {
		return apply_filters( 'simontaxi_filter_default_title', simontaxi_get_option( 'default_title', esc_html__( 'Vehicle', 'simontaxi' ) ) );
	}
}

if ( ! function_exists( 'simontaxi_get_label_singular' ) ) :
	/**
	 * Get Singular Label
	 *
	 * @since 1.0.0
	 *
	 * @param bool $lowercase - Case of the string.
	 * @return string $defaults['singular'] Singular label
	 */
	function simontaxi_get_label_singular( $lowercase = false ) {
		$defaults = simontaxi_get_default_labels();
		return ( $lowercase) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
	}
endif;

/**
* Constants used throughout the booking applicaiton
*/
define( 'VARIABLE_PREFIX', 'v_' );

if ( ! function_exists( 'simontaxi_get_default_labels' ) ) :
	/**
	 * Get Default Labels
	 *
	 * @since 1.0.0
	 * @return array $defaults Default labels
	 */
	function simontaxi_get_default_labels() {
		$defaults = array(
			'singular' => simontaxi_get_default_title(),
			'plural'   => simontaxi_get_default_title_plural(),
		);
		return apply_filters( 'simontaxi_filter_default_vehicle_name', $defaults );
	}
endif;

if ( ! function_exists( 'simontaxi_get_label_plural' ) ) :
	/**
	 * Get Plural Label
	 *
	 * @since 1.0.0
	 * @param bool $lowercase - Case of the string.
	 * @return string $defaults['plural'] Plural label
	 */
	function simontaxi_get_label_plural( $lowercase = false ) {
		$defaults = simontaxi_get_default_labels();
		return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
	}
endif;

add_action( 'init', 'simontaxi_do_output_buffer' );
/**
 * Allow redirection even if my theme starts to send output to the browser
 */
function simontaxi_do_output_buffer() {
	ob_start();
}

/**
 * Enqueue scripts and styles on admin end.
 */
function simontaxi_enqueue_media_uploader() {
	// Use minified libraries if SIMONTAXI_SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SIMONTAXI_SCRIPT_DEBUG' ) && SIMONTAXI_SCRIPT_DEBUG ) ? '' : '.min';
	
	wp_enqueue_style( 'jquery-ui', SIMONTAXI_PLUGIN_URL . 'css/jquery-ui.min.css' );
	wp_enqueue_style( 'simontaxi-admin-style', SIMONTAXI_PLUGIN_URL . 'css/admin-style' . $suffix . '.css' );	
	 
	/**
	 * Simple Line icons
	 *
	 * @since 2.0.0
	*/
	wp_enqueue_style( 'simple-line-icons', 'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css' );
	
	/**
	 * Bootstrap
	 *
	 * @since 2.0.6
	 */
	wp_enqueue_script( 'bootstrap', SIMONTAXI_PLUGIN_URL . '/js/bootstrap.min.js' );
	
		
	wp_enqueue_script( 'simontaxi-admin-main', SIMONTAXI_PLUGIN_URL . 'js/admin-main' . $suffix . '.js' );
	wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery', 'jquery-ui-core' ), time() );
	wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'simontaxi_enqueue_media_uploader' );

add_action( 'admin_footer', 'simontaxi_custom_css_admin' );

/**
 * Enqueue scripts and styles on admin end to fix footer issue.
 *
 * @since 2.0.0
 */
function simontaxi_custom_css_admin() {
	echo '<style>#wpfooter { position: initial !important; }</style>';
}

/**
 * Enqueue scripts and styles on front end.
 */
function simontaxi_vehicle_scripts() {
	wp_enqueue_style( 'font-awesome', SIMONTAXI_PLUGIN_URL . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'bootstrap-select2', SIMONTAXI_PLUGIN_URL . '/css/bootstrap-select.min.css' );
	if ( ! wp_style_is( 'bootstrap-css' ) ) {
		wp_enqueue_style( 'bootstrap-css', SIMONTAXI_PLUGIN_URL . '/css/bootstrap.min.css' );
	}
	wp_enqueue_style( 'simple-line-icons', 'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css' );

	wp_enqueue_style( 'simontaxi-frontend', SIMONTAXI_PLUGIN_URL . '/css/booking-frontend.min.css' );

	if ( ! wp_script_is( 'jquery' ) ) {
		wp_enqueue_script( 'jquery' );
	}
	if ( ! wp_script_is( 'bootstrap-js' ) ) {
		wp_enqueue_script( 'bootstrap-js', SIMONTAXI_PLUGIN_URL . '/js/bootstrap.min.js' );
	}

	if ( ! wp_script_is( 'jquery-ui-autocomplete' ) ) {
		wp_enqueue_script( 'jquery-ui-autocomplete', array( 'jquery' ) );
	}
	if ( ! wp_script_is( 'jquery-ui-datepicker' ) ) {
		wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery' ) );
	}
	wp_enqueue_script( 'bootstrap-select2', SIMONTAXI_PLUGIN_URL . '/js/bootstrap-select.min.js', array( 'jquery' ) );

	// Use minified libraries if SIMONTAXI_SCRIPT_DEBUG is turned off
	$suffix = ( defined( 'SIMONTAXI_SCRIPT_DEBUG' ) && SIMONTAXI_SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'simontaxi-front-main-vars', SIMONTAXI_PLUGIN_URL . '/js/front-main' . $suffix . '.js', array( 'jquery' ) );

	/**
	 * We are receiving request from client to change loader image, so here is the provision.
	 *
	 * @since 2.0.0
	*/
	$loaders = simontaxi_get_option( 'loaders', array() );
	$main_loader = ( isset( $loaders['main_loader'] ) && '' !== $loaders['main_loader'] ) ? $loaders['main_loader'] : SIMONTAXI_PLUGIN_URL . '/images/preloader.gif';

	$ajax_loader = ( isset( $loaders['ajax_loader'] ) && '' !== $loaders['ajax_loader'] ) ? $loaders['ajax_loader'] : SIMONTAXI_PLUGIN_URL . '/images/preloader.gif';
	wp_localize_script( 'simontaxi-front-main-vars', 'simontaxi_vars', apply_filters( 'simontaxi_ajax_vars', array(
		'ajaxurl'                 => admin_url( 'admin-ajax.php' ),
		'base_url' => site_url(),
		'plugin_url' => SIMONTAXI_PLUGIN_URL,
		'main_loader' => $main_loader,
		'ajax_loader' => $ajax_loader,
	) ) );
	add_action( 'wp_head', 'simontaxi_main_loader' );
}
add_action( 'wp_enqueue_scripts', 'simontaxi_vehicle_scripts', 2 );

if ( ! function_exists( 'simontaxi_main_loader' ) ) :
	/**
	 * To change the loader image provided with theme.
	 *
	 * @since 2.0.2
	 */
	function simontaxi_main_loader() {
		$loaders = simontaxi_get_option( 'loaders', array() );
		$main_loader = ( isset( $loaders['main_loader'] ) && '' !== $loaders['main_loader'] ) ? $loaders['main_loader'] : SIMONTAXI_PLUGIN_URL . '/images/preloader.gif';
		?>
		<style type="text/css">
		#status{
				background-image: url(<?php echo esc_url( $main_loader ); ?>) !important;
		}
		</style>
		<?php
	}
endif;


/**
 * Append additional menu items.
 *
 * @param mixed $items - items.
 * @param array $args - items array.
 * @since 1.0.0
 */
function simontaxi_vehicle_menu( $items, $args ) {
	if ( 'primary' !== $args->theme_location ) {
		return $items;
	}
	$link = '';
	$login_menu_item = simontaxi_get_option( 'login_menu_item', 'yes' );

	/**
	 * Let us check whether the funtion is available or not. This function is availanle in booking plusin which is provided along with this theme.
	*/
	if ( function_exists( 'simontaxi_get_bookingsteps_urls' ) && 'yes' === $login_menu_item  ) {
		if ( is_user_logged_in() ) {
			$link .= sprintf( '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-235"><a href="%s">' . esc_html__( 'My Account', 'simontaxi' ) . '</a></li>', simontaxi_get_bookingsteps_urls( 'user_account' ) );
		} else {
			$link .= sprintf( '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-235"><a href="%s">' . esc_html__( 'Login', 'simontaxi' ) . '</a></li>', simontaxi_get_bookingsteps_urls( 'login' ) );
		}
	}
	return $items . $link;
}
add_filter( 'wp_nav_menu_items', 'simontaxi_vehicle_menu', 10, 2 );

if ( ! function_exists( 'vehicle_settings' ) ) :
	/**
	 * Returns the instance of vehicle settings class
	 *
	 * @since 1.0.0
	 * @return object
	 */
	function vehicle_settings() {
			return Simontaxi_Vehicle_settings::instance();
	}
endif;

if ( ! function_exists( 'simontaxi_currencies' ) ) :
	/**
	 * Returns the currency list select item
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function simontaxi_currencies() {
		global $wpdb;
		$currency_list = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}st_countries WHERE `name` != '' AND  `currency_code` != '' GROUP BY `name` ORDER BY `name`" );
		return $currency_list;
	}
endif;

if ( ! function_exists( 'simontaxi_countries' ) ) :
	/**
	 * Returns the countries list select item
	 *
	 * @since 1.0.0
	 * @return string
	 */
	function simontaxi_countries( $show_currency = 'yes' ) {
		global $wpdb;

		$country_list = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}st_countries WHERE `name` != '' GROUP BY `name` ORDER BY name" );
		$countries = array();
		if ( ! empty( $country_list ) ) {
			foreach ( $country_list as $country ) {
				if ( 'no' === $show_currency ) {
				$countries[ $country->iso_alpha2 ] = $country->name;				
				} else {
					$countries[ $country->iso_alpha2 ] = $country->name . ' ( ' . $country->currency_symbol . ' )';
				}
			}
		}
		return $countries;
	}
endif;

if ( ! function_exists( 'simontaxi_get_currency_symbol' ) ) :
	/**
	 * Return the currency symbol for the entire vehicle system
	 *
	 * @since 1.0.0
	 * @param string $value - Value to display.
	 * @return string
	 */
	function simontaxi_get_currency_symbol( $value = '' ) {
		global $wpdb;
		$currency_code = simontaxi_get_option( 'vehicle_currency' );
		if ( '' !== $currency_code ) {
			/**
			 * Since we are displaying all countries, some countries using same curreny so we need to take only ISO code 
			 *
			 * @since 2.0.0
			 */
			 $currency_code = substr( $currency_code, 0, 3 );
			$result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}st_countries WHERE currency_code = '%s'", $currency_code ) );

			if ( ! empty( $result ) ) {
				foreach ( $result as $row ) {
					$currency_symbol = $row->currency_symbol;
				}
			} else {
				/*
				"$" symbol is default
				*/
				$currency_symbol = '&#36;';
			}
		} else {
			/*
			"$" symbol is default
			*/
			$currency_symbol = '&#36;';
		}

		if ( '' !== $value ) {
			$currency_symbol = simontaxi_currency_placement( $currency_symbol, $value );
		}
		return $currency_symbol;
	}
endif;

if ( ! function_exists( 'simontaxi_get_currency_code' ) ) :
	/**
	 * Return the currency code for the entire vehicle system
	 *
	 * @param string $value - Value to display.
	 * @return string
	 * @since 1.0.0
	 */
	function simontaxi_get_currency_code( $value = '' ) {
		$currency_symbol = simontaxi_get_option( 'vehicle_currency' );
		if ( '' === $currency_symbol ) {
			$currency_symbol = 'USD';
		} else {
			/**
			 * Since we are displaying all countries, some countries using same curreny so we need to take only ISO code 
			 *
			 * @since 2.0.0
			 */
			 $currency_symbol = substr( $currency_symbol, 0, 3 );
		}
		if ( '' !== $value ) {
			$currency_symbol = simontaxi_currency_placement( $currency_symbol, $value );
		}
		return $currency_symbol;
	}
endif;

if ( ! function_exists( 'simontaxi_currency_placement' ) ) :
	/**
	 * This function returns the currency based on admin settings
	 *
	 * @param string $currency - currency.
	 * @param string $value - Value to display.
	 * @return string
	 * @since 1.0.0
	 */
	function simontaxi_currency_placement( $currency, $value ) {
		/**
		* Format the number according to the admin settings 'Currency' - (Number of Decimals, Decimal Separator, Thousand Separator)
		*/
		$number_of_decimals = simontaxi_get_option( 'number_of_decimals', 2 );
		$decimal_separator = simontaxi_get_option( 'decimal_separator', '.' );
		$thousand_separator = simontaxi_get_option( 'thousand_separator', ',' );
		if ( filter_var( $value, FILTER_VALIDATE_FLOAT ) === false ) {
			$number_of_decimals = 0;
		}
		
		/**
		 * @since 2.0.2
		 */
		$value = number_format( $value, $number_of_decimals, $decimal_separator, $thousand_separator );
		
		$currency_position = simontaxi_get_option( 'currency_position', 'left' );
		if ( 'left' === $currency_position ) {
			$currency_value = $currency . $value; /* Appending currency left side */
		} elseif ( 'right' === $currency_position ) {
			$currency_value = $value . $currency; /* Appending currency right side */
		} elseif ( 'left_with_space' === $currency_position ) {
			$currency_value = $currency . ' ' . $value; /* Appending currency left side with space */
		} elseif ( 'right_with_space' === $currency_position ) {
			$currency_value = $value . ' ' . $currency; /* Appending currency right side with space */
		}
		return $currency_value;
	}
endif;

if ( ! function_exists( 'simontaxi_get_currency' ) ) :
	/**
	 * Return the currency code or symbol depends on admin settings
	 *
	 * @param string $value - Value to display.
	 * @return string
	 * @since 1.0.0
	 */
	function simontaxi_get_currency( $value = '' ) {
		$display_currency = simontaxi_get_option( 'display_currency', 'symbol' );
		if ( 'code' === $display_currency ) {
			$currency = simontaxi_get_currency_code();
		} else {
			$currency = simontaxi_get_currency_symbol();
		}
		if ( '' !== $value ) {
			$currency = simontaxi_currency_placement( $currency, $value );
		}
		return $currency;
	}
endif;

if ( ! function_exists( 'simontaxi_get_country' ) ) :
	 /**
	  * Return the country code for the entire vehicle system
	  *
	  * @return string
	  * @since 1.0
	  */
	function simontaxi_get_country() {
		return simontaxi_get_option( 'vehicle_country', 'USA' );
	}
endif;

if ( ! function_exists( 'simontaxi_get_active_tab' ) ) :
	/**
	 * Return active tab. which means user selected tab
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_active_tab() {
		$tabs = simontaxi_get_option( 'active_tabs', 'Point to Point Transfer' );
		if ( 'Point to Point Transfer' === $tabs ) {
			$tabs = array( 'Point to Point Transfer', 'Airport Transfer', 'Hourly Rental' );
		}
		return $tabs;
	}
endif;

if ( ! function_exists( 'simontaxi_get_airports' ) ) :
	/**
	 * Return airports
	 *
	 * @since 1.0
	 * @return array
	 */
	function simontaxi_get_airports() {
		$args = array(
			'taxonomy' => 'vehicle_locations',
			'hide_empty' => false,
			'meta_key' => 'location_type',
			'meta_value' => 'airport',
		);
		$airports = get_terms( $args );
		$airports_return = array();
		if ( ! empty( $airports ) && ! is_wp_error( $airports ) ) {
			foreach ( $airports as $term ) {
				$airports_return[] = array(
					'id' => $term->term_id,
					'name' => $term->name,
					'slug' => $term->slug,
					'description' => $term->description,
					'location_type' => get_term_meta( $term->term_id, 'location_type', true ),
					'location_address' => get_term_meta( $term->term_id, 'location_address', true ),
					'distances' => get_term_meta( $term->term_id, 'distances', true ),
					'times' => get_term_meta( $term->term_id, 'location_type', true ),
				);
			}
		}
		return $airports_return;
	}
endif;

if ( ! function_exists( 'simontaxi_get_hourly_packages' ) ) {
	/**
	 * Return Hourly packages
	 *
	 * @since 1.0
	 * @param string $get - term to get.
	 * @return array
	 */
	function simontaxi_get_hourly_packages( $get = '' ) {
		$args = array(
			'taxonomy' => 'hourly_packages',
			'hide_empty' => false,
		);
		$packages = get_terms( $args );
		$packages_return = array();
		if ( ! empty( $packages ) && ! is_wp_error( $packages ) ) {
			foreach ( $packages as $term ) {
				if ( '' === $get ) {
					$packages_return[] = array(
						'name' => $term->name,
						'slug' => $term->slug,
						'description' => $term->slug,
						'hourly_hours' => get_term_meta( $term->term_id, 'hourly_hours', true ),
						'hourly_price' => get_term_meta( $term->term_id, 'hourly_price', true ),
					);
				} elseif ( is_array( $get ) ) {

				} else {
					if ( $get === $term->name ) {
						$packages_return[] = array(
							'name' => $term->name,
							'slug' => $term->slug,
							'description' => $term->slug,
							'hourly_hours' => get_term_meta( $term->term_id, 'hourly_hours', true ),
							'hourly_price' => get_term_meta( $term->term_id, 'hourly_price', true ),
					);
					}
				}
			}
		}
		return $packages_return;
	}
}

if ( ! function_exists( 'simontaxi_get_distance_time' ) ) {
	/**
	 * Return Distance and Time From one point to another point
	 *
	 * @since 1.0
	 * @return array
	 */
	function simontaxi_get_distance_time( $from, $to, $case='' ) {
        
		if ( $from != '' && $to != '' ) {
            $dist = $time = false;
			$term = ( is_numeric( $from ) ) ? get_term_by( 'id', $from, 'vehicle_locations' ) : get_term_by( 'name', $from, 'vehicle_locations' );
			$term2 = ( is_numeric( $to ) ) ? get_term_by( 'id', $to, 'vehicle_locations' ) : get_term_by( 'name', $to, 'vehicle_locations' );
			
            if ( ! ( $term == false || $term2 == false) ) {
                $from_term_id = $term->term_id;
                $to_term_id = $term2->term_id;

                $from_distances = ( array ) json_decode(get_term_meta( $from_term_id, 'distances', true ) );
				$from_times = ( array ) json_decode(get_term_meta( $from_term_id, 'times', true ) );

                $to_distances = ( array ) json_decode(get_term_meta( $to_term_id, 'distances', true ) );
				$to_times = ( array ) json_decode(get_term_meta( $to_term_id, 'times', true ) );
			
				$from_dt_index = is_numeric( $from ) ? 'dt_' . $from : $from;
				$to_dt_index = is_numeric( $to ) ? 'dt_' . $to : $to;
				
                if ( isset( $from_distances[ $to_dt_index ] ) && $from_distances[ $to_dt_index ] != '' ) {
					$dist = $from_distances[ $to_dt_index ];
				} elseif ( isset( $to_distances[ $from_dt_index ] ) && $to_distances[ $from_dt_index ] != '' ) {
					$dist = $to_distances[ $from_dt_index ] ;	
				} else {
					$dist = false;	
				}
                if ( $dist=='' ) {
					return false;	
				}

				$from_tm_index = is_numeric( $from ) ? 'tm_' . $from : $from;
				$to_tm_index = is_numeric( $to ) ? 'tm_' . $to : $to;
                if ( isset( $from_times[ $to_tm_index ] ) ) {
					$tm = $from_times[ $to_tm_index ] ;
				}
                elseif ( isset( $to_times[ $from_tm_index ] ) ) {
					$tm = $to_times[ $from_tm_index ] ;	
				} else {
					$tm = false;	
				}

                $u = simontaxi_get_distance_units();
				$output = array();
				$output['distance'] = $dist;

				$output['distance_units'] = $u;
				$output['distance_text'] = $dist . ' ' . $u;
				$output['duration_text'] = $tm;
				if ( $tm != false ) {
					$parts = explode( ':', $tm);
					$str = '';
					if ( isset( $parts[0] ) ) {
						$str .= $parts[0] . ' hours';
					}
					if ( isset( $parts[1] ) ) {
						$str .= ' ' . $parts[1] . ' mins';
					}
					$output['duration_text'] = $str;
				}

				if ( $case == '' ) {
					return $output;
				} elseif ( isset( $output[ $case ] ) ) {
					return $output[ $case ];
				} else {
					return false;
				}
            }
            else {
				return false;
			}
        } else {
			return false;
		}
    }
}

if ( ! function_exists( 'get_google_distance' ) ) :
	/**
	 * Helper function to get the distance from google between two places.
	 *
	 * @params string $from - Origin.
	 * @param string $to - Destination.
	 * @return array
	 * @since 2.0.0
	*/
	function get_google_distance( $from, $to, $units = 'km' ){
		$origin = str_replace( ' ', '+', $from);
		$destination = str_replace( ' ', '+', $to);
		/**
		 * It supports only two metrics.
		 *
		 * @link https://developers.google.com/maps/documentation/javascript/distancematrix
		 * google.maps.UnitSystem.METRIC - Kilometers & Meters
		 * google.maps.UnitSystem.IMPERIAL - Miles & Feet
		*/
		if ( 'miles' === $units ) {
			$google_units = 'imperial';
		} else {
			$google_units = 'metric';
		}
		$url = 'http://maps.googleapis.com/maps/api/directions/json?origin=' . $origin . '&destination=' . $destination . '&sensor=false&units=' . $google_units;
		// sendRequest
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		$body = curl_exec( $ch);
		curl_close( $ch);
		$json = json_decode( $body);
		
		if ( 'OK' === $json->status ) {
			$legs = $json->routes[0]->legs[0];
			$drivingSteps = $json->routes[0]->legs[0]->steps;

			$output = array();
			/**
			 * The total distance of this route, expressed in meters.
			 *
			 * @link https://developers.google.com/maps/documentation/javascript/distancematrix
			 *
			 * distance: The total distance of this route, expressed in meters (value) and as text. The textual value is formatted according to the unitSystem specified in the request (or in metric, if no preference was supplied).
			*/			
			
			/*
			$distance = $legs->distance->value;
			if ( 'miles' === $units ) {
				// $distance = number_format( ( $distance / 5280 ), 2); // Hence we are getting distance in feet we are converting that into Miles
				// @since 2.0.6
				 
				$distance = number_format( ( $distance / 1609.344 ), 2); // Hence we are getting distance in feet we are converting that into Miles. miles = meters/1,609.344
			} else {
				$distance = number_format( ( $distance / 1000 ), 2); // Hence we are getting distance in meters we are converting that into KM
			}
			*/
			/**
			 * @since 2.0.6
			 */
			 $distance = $legs->distance->text;
			 $distance = filter_var( $distance, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
			
			$output['distance'] = $distance; // Meters

			$u = simontaxi_get_distance_units();
			$output['distance_units'] = $u;
			$output['distance_text'] = $legs->distance->text;
			$output['duration_text'] = $legs->duration->text;
			
			return $output;
		} else {
			return array( 'status' => $json->status );
		}
	}
endif;

if ( ! function_exists( 'simontaxi_get_distance_units' ) ) {
	/**
	 * Return distance units set in admin
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_distance_units() {
		return simontaxi_get_option( 'vehicle_distance', 'km' );
	}
}

/**
 * Returns the p2p tab title. It is useful if user want to change tab names
 *
 * @since 1.0
 * @return string
 */
 if ( ! function_exists( 'simontaxi_get_p2ptab_title' ) ) {
	function simontaxi_get_p2ptab_title() {
		return apply_filters( 'simontaxi_filter_p2ptab_title', esc_html__( simontaxi_get_option( 'p2p_tab_title', 'Point To Point' ), 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_airporttab_title' ) ) {
	/**
	 * Returns the airport transfer tab title. It is useful if user want to change tab names
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_airporttab_title() {
		return apply_filters( 'simontaxi_filter_airporttab_title', esc_html__( simontaxi_get_option( 'airport_tab_title', 'Airport Transfer' ), 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_hourlytab_title' ) ) {
	/**
	 * Returns the hourly rental tab title. It is useful if user want to change tab names
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_hourlytab_title() {
		return apply_filters( 'simontaxi_filter_hourlytab_title', esc_html__( simontaxi_get_option( 'hourly_tab_title', 'Hourly Rental' ), 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_step1_title' ) ) {
	/**
	 * Returns the Booking Step 1 Title. It is useful if user want to change tab names
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_step1_title() {
		return apply_filters( 'simontaxi_filter_step1_title', esc_html__( simontaxi_get_option( 'booking_step1_title', 'Location' ), 'simontaxi' ) );
	}
}

if (! function_exists( 'simontaxi_get_step2_title' ) ) {
	/**
	 * Returns the Booking Step 2 Title. It is useful if user want to change tab names
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_step2_title() {
		return apply_filters( 'simontaxi_filter_step2_title', esc_html__( simontaxi_get_option( 'booking_step2_title', 'Select Cab' ), 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_step3_title' ) ) {
	/**
	 * Returns the Booking Step 3 Title. It is useful if user want to change tab names
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_step3_title() {
		return apply_filters( 'simontaxi_filter_step3_title', esc_html__( simontaxi_get_option( 'booking_step3_title', 'Confirm Booking' ), 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_step4_title' ) ) {
	/**
	 * Returns the Booking Step 4 Title. It is useful if user want to change tab names
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_step4_title() {
		return apply_filters( 'simontaxi_filter_step4_title', esc_html__( simontaxi_get_option( 'booking_step4_title', 'Payment' ), 'simontaxi' ) );
	}
}

if (! function_exists( 'simontaxi_get_help' ) ) {
	/**
	 * Returns the help text as tooltip
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_help( $help = 'This is help text' ) {
		$text = sprintf( '&nbsp;<span class="st_tooltip" title="%s" data-toggle="tooltip"><span class="dashicons dashicons-editor-help"></span></span>', esc_html__( $help ) );
		return apply_filters( 'simontaxi_filter_get_help', $text);
	}
}

if (! function_exists( 'simontaxi_get_oneway_title' ) ) {
	/**
	 * Returns 'One way' title to display in front end. It is useful if user wants to change this to other title
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_oneway_title() {
		return apply_filters( 'simontaxi_filter_oneway_title', esc_html__( 'One way', 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_twoway_title' ) ) {
	/**
	 * Returns 'Two way' title to display in front end. It is useful if user wants to change this to other title
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_twoway_title() {
		return apply_filters( 'simontaxi_filter_twoway_title', esc_html__( 'Two way', 'simontaxi' ) );
	}
}


 if ( ! function_exists( 'simontaxi_get_pickuppoint_title' ) ) {
	/**
	 * Returns 'Pick up point' title to display in front end. It is useful if user wants to change this to other title
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_pickuppoint_title() {
		return apply_filters( 'simontaxi_filter_pickuppoint_title', esc_html__( 'Pick-up point', 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_dropoffpoint_title' ) ) {
	/**
	 * Returns 'Drop off point' title to display in front end. It is useful if user wants to change this to other title
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_dropoffpoint_title() {
		return apply_filters( 'simontaxi_filter_dropoffpoint_title', esc_html__( 'Drop-off point', 'simontaxi' ) );
	}
}


 if ( ! function_exists( 'simontaxi_get_pickupdate_title' ) ) {
	/**
	 * Returns 'Pick Up Date' title to display in front end. It is useful if user wants to change this to other title
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_pickupdate_title() {
		return apply_filters( 'simontaxi_filter_pickupdate_title', esc_html__( 'Pick-up Date', 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_get_pickuptime_title' ) ) {
	/**
	 * Returns 'Pick Up Time' title to display in front end. It is useful if user wants to change this to other title
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_pickuptime_title() {
		return apply_filters( 'simontaxi_filter_pickuptime_title', esc_html__( 'Pick-up Time', 'simontaxi' ) );
	}
}

if ( ! function_exists( 'simontaxi_terms_page' ) ) {
	/**
	 * Returns the terms & conditions page.
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_terms_page() {
		return simontaxi_get_option( 'terms_page', 'step1' );
	}
}

if ( ! function_exists( 'simontaxi_is_allow_twoway_booking' ) ) {
	/**
	 * Returns whether the two way booking is allowed in admin. Default 'yes'
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_is_allow_twoway_booking() {
		return simontaxi_get_option( 'alloow_twoway_booking', 'yes' );
	}
}

if ( ! function_exists( 'simontaxi_required_field' ) ) {
	/**
	 * Returns the "*" mark
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_required_field() {
		return '<font color="red"> * </font>';
	}
}

if ( ! function_exists( 'simontaxi_get_value' ) ) {
	/**
	 * Returns the value from a given array
	 *
	 * @param array|stdClass $item.
	 * @param string $key.
	 * @param string $default - Optional.
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_value( $item, $key, $default = '' ) {
		$value = $default;
		if ( isset( $_POST[ $key ] ) ) {
			$value = $_POST[ $key ];
		}
		elseif ( is_array( $item ) ) {
			if ( isset( $item[ $key ] ) ) {
				$value = $item[ $key ];
			}
		} else {
			if ( isset( $item->$key ) ) {
				$value = $item->$key;
			}
		}
		return $value;
	}
}

if ( ! function_exists( 'simontaxi_date_format' ) ) {
	/**
	 * Returns the date format for the bookings applicaiton
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_date_format( $date = '' ) {
		$format = simontaxi_get_option( 'st_date_format', 'd-m-Y' );
		if ( '' !== $date ) {
			$format = date( $format, strtotime( $date ) );
		}
		return $format;
	}
}

if ( ! function_exists( 'simontaxi_is_allow_additional_pickups' ) ) {
	/**
	 * Returns whether the additional pickup points allowed in admin. Default 'no'
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_is_allow_additional_pickups() {
		return simontaxi_get_option( 'allow_additional_pickups', 'no' );
	}
}

 if ( ! function_exists( 'simontaxi_get_max_additional_pickups' ) ) {
	/**
	 * Returns Max. Additional Pickup Points
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_max_additional_pickups() {
		return simontaxi_get_option( 'max_additional_pickups', 5);
	}
}

if ( ! function_exists( 'simontaxi_is_allow_additional_dropoff' ) ) {
	/**
	 * Returns whether the additional dropoff points allowed in admin. Default 'no'
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_is_allow_additional_dropoff() {
		return simontaxi_get_option( 'allow_additional_dropoff', 'no' );
	}
}


if ( ! function_exists( 'simontaxi_get_max_additional_dropoff' ) ) {
	/**
	 * Returns Max. Additional Drop-off Points
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_max_additional_dropoff() {
		return simontaxi_get_option( 'max_additional_dropoff', 5);
	}
}

if ( ! function_exists( 'simontaxi_get_maximum_notice' ) ) {
	/**
	 * Returns maximum notice days
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_maximum_notice() {
		$maximum_notice = simontaxi_get_option( 'maximum_notice', 3);
		$maximum_notice_type = simontaxi_get_option( 'maximum_notice_type', 'month' );
		/*Convert maximum notice period into days so that we can manage easily*/
		$maximum_notice_days = 30;
		if ( $maximum_notice_type == 'day' )
		$maximum_notice_days = $maximum_notice;
		elseif ( $maximum_notice_type == 'month' )
		$maximum_notice_days = $maximum_notice * 30;
		elseif ( $maximum_notice_type == 'year' )
		$maximum_notice_days = $maximum_notice * 12 * 30;
		return $maximum_notice_days;
	}
}

if ( ! function_exists( 'simontaxi_crypto_rand_secure' ) ){
    /**
	 * Returns the random string which is used in 'simontaxi_get_token'
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_crypto_rand_secure( $min, $max ) {
        $range = $max - $min;
        if ( $range < 1 ) return $min; // not so random...
        $log = ceil(log( $range, 2) );
        $bytes = (int) ( $log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes( $bytes ) ) );
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ( $rnd >= $range );
        return $min + $rnd;
    }
}

if ( ! function_exists( 'simontaxi_get_token' ) ) {
	/**
	 * Returns random string
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_get_token( $length = 6 ) {
		$token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";
        $max = strlen( $codeAlphabet ) - 1;
        for ( $i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[simontaxi_crypto_rand_secure(0, $max)];
        }
        return $token;
	}
}

 if ( ! function_exists( 'simontaxi_neat_print_die' ) ) {
	 /**
	 * Print the neatly formated output
	 *
	 * @since 1.0
	 * @return string
	 */
	function simontaxi_neat_print_die( $item = '', $die = true ) {
		echo '<pre>';
		if ( '' === $item ) {
			print_r( $_POST );
		} else {
			print_r ( $item );
		}
		if ( $die ) {
			die( '<br>-----------------------DIE----------------' );
		}
	}
 }


 if ( ! function_exists( 'simontaxi_get_vehicles' ) ) {
	 /**
	 * Return the all available vehicles based on other settings.
	 *
	 * @since 1.0
	 * @param array $args - arguments.
	 * @return string
	 */
	 function simontaxi_get_vehicles( $args = array() ) {
		global $wpdb;
		$date = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date' );
		$pickup_date_return = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date_return' );

		/*
		We are finding vehicles which are blocked for the selected date
		*/
		$vehicles_array = $blockout_vehicles = array();
		$blockout_dates_objects = get_terms( 'blockout_date', array( 'hide_empty' => false ) );
		if ( ! empty ( $blockout_dates_objects ) ) {
			foreach ( $blockout_dates_objects as $blockout_date ) {
				$term_meta['block_date'] = get_term_meta( $blockout_date->term_id, 'block_date', true );
				$term_meta['block_date_end'] = get_term_meta( $blockout_date->term_id, 'block_date_end', true );
				$term_meta['vehicles'] = get_term_meta( $blockout_date->term_id, 'vehicles', true );

				$block_date = ( isset( $term_meta['block_date'] ) && $term_meta['block_date'] != '' ) ? date( 'Y-m-d', strtotime( $term_meta['block_date'] ) ) : '';
				$block_date_end = ( isset( $term_meta['block_date_end'] ) && $term_meta['block_date_end'] != '' ) ? date( 'Y-m-d', strtotime( $term_meta['block_date_end'] ) ) : '';

				/*
				If the selected date is between blocked date! then it is blockeddate!!
				*/
				if ( $date >= $block_date && $date <= $block_date_end ) {
					$blocked_cabs = (array)json_decode( $term_meta['vehicles'] );
					if ( ! empty( $blocked_cabs ) ) {
						foreach ( $blocked_cabs as $key => $val ) {
								$blockout_vehicles[] = $val;
						}
					}
				}
				/*
				We are checking vehicles availability on return date also, if user trying to book for return journey
				*/
				$journey_type = simontaxi_get_session( 'booking_step1', 'one_way', 'journey_type' );
				if ( 'two_way' === $journey_type ) {
					if ( $pickup_date_return >= $block_date && $pickup_date_return <= $block_date_end ) {
					$blocked_cabs = (array)json_decode( $term_meta['vehicles'] );
						if ( !empty( $blocked_cabs) ) {
							foreach ( $blocked_cabs as $key => $val ) {
									$blockout_vehicles[] = $val;
							}
						}
					}
				}
			}
		}

		/**
		 * Let us restrict number of vehicles
		 *
		 * @since 2.0.0
		*/
		if ( 'yes' === simontaxi_get_option( 'restrict_vehicles_count', 'no' ) ) {
			$bookings = $wpdb->prefix. 'st_bookings';
			$payments = $wpdb->prefix. 'st_payments';
			$confirmed_vehicle_status = simontaxi_get_option( 'confirmed_vehicle_status', 'confirmed' );
			$sql = "SELECT *, `" . $bookings . "`.`ID` AS booking_id, `" . $bookings . "`.`reference` AS booking_ref FROM `" . $bookings . "` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID` WHERE `" . $bookings . "`.booking_contacts!='' AND `" . $bookings . "`.status='" . $confirmed_vehicle_status . "' AND `" . $bookings . "`.`pickup_date` = '" . $date . "' GROUP BY selected_vehicle";
			
			$result = $wpdb->get_results( $sql, 'ARRAY_A' );
			
			if ( ! empty( $result ) ) {
				foreach ( $result as $row ) {
					$sql = "SELECT COUNT(*) FROM `" . $bookings."` INNER JOIN `" . $payments."` ON `" . $payments . "`.`booking_id`=`" . $bookings."`.`ID` WHERE `" . $bookings . "`.booking_contacts!='' AND `" . $bookings . "`.status='" . $confirmed_vehicle_status . "' AND `" . $bookings . "`.`selected_vehicle` = '" . $row['selected_vehicle'] . "' AND `".$bookings."`.pickup_date = '" . $date . "'";
					
					$bookings_for_the_vehicle = $wpdb->get_var( $sql );

					$number_of_vehicles_available = get_post_meta( $row['selected_vehicle'], 'number_of_vehicles', true );
					if ( $number_of_vehicles_available <= $bookings_for_the_vehicle  ) {
						$blockout_vehicles[] = $row['selected_vehicle'];
					}
				}
			}
			
			/*
			We are checking vehicles availability on return date also, if user trying to book for return journey
			*/
			$journey_type = simontaxi_get_session( 'booking_step1', 'one_way', 'journey_type' );
			if ( 'two_way' === $journey_type ) {
				$sql = "SELECT *, `" . $bookings . "`.`ID` AS booking_id, `" . $bookings . "`.`reference` AS booking_ref FROM `" . $bookings . "` INNER JOIN `" . $payments . "` ON `" . $payments . "`.`booking_id`=`" . $bookings . "`.`ID` WHERE `" . $bookings . "`.booking_contacts!='' AND `" . $bookings . "`.status='" . $confirmed_vehicle_status . "' AND `" . $bookings . "`.pickup_date = '" . $pickup_date_return . "' GROUP BY selected_vehicle";
				$result = $wpdb->get_results( $sql, 'ARRAY_A' );
				if ( ! empty( $result ) ) {
					foreach ( $result as $row ) {
						$sql = "SELECT COUNT(*) FROM `" . $bookings."` INNER JOIN `" . $payments."` ON `" . $payments . "`.`booking_id`=`" . $bookings."`.`ID` WHERE `" . $bookings . "`.booking_contacts!='' AND `" . $bookings . "`.status='" . $confirmed_vehicle_status . "' AND `" . $bookings . "`.`selected_vehicle` = '" . $row['selected_vehicle'] . "' AND `".$bookings."`.pickup_date = '" . $pickup_date_return . "'";
						$bookings_for_the_vehicle = $wpdb->get_var( $sql );

						$number_of_vehicles_available = get_post_meta( $row['selected_vehicle'], 'number_of_vehicles', true );
						if ( $number_of_vehicles_available <= $bookings_for_the_vehicle  ) {
							$blockout_vehicles[] = $row['selected_vehicle'];
						}
					}
				}
			}
		}

		/**
		 * Now we are getting vehicles which are availabile for display means which are not blocked
		*/
		if ( isset( $args['pagination'] ) && $args['pagination'] == true ) {
			$args_new = array(
				'post_status' => 'publish',
				'orderby' => 'name',
				'order' => 'ASC',
				'post_type' => 'vehicle',
				'exclude' => $blockout_vehicles,
				'posts_per_page' => -1,
			);

			$all_vehicles = get_posts( $args_new );
			$vehicles_array['total'] = count( $all_vehicles);

			$args_new = array(
				'post_status' => 'publish',
				'orderby' => $args['orderby'],
				'order' => $args['order'],
				'post_type' => 'vehicle',
				'exclude' => $blockout_vehicles,
				'posts_per_page' => $args['perpage'],
				'offset' => $args['offset'],
			);

			$vehicles = get_posts( $args_new );

		} else {
			$args_new = array(
				'post_status' => 'publish',
				'orderby' => $args['orderby'],
				'order' => $args['order'],
				'post_type' => 'vehicle',
				'exclude' => $blockout_vehicles,
				'posts_per_page' => -1,
			);

			$vehicles = get_posts( $args_new );
		}
		foreach ( $vehicles as $vehicle ) {
			$post_id = $vehicle->ID;
			$vehicle->post_meta = simontaxi_filter_gk( get_post_meta( $post_id) );
			$vehicle->thumbnail = get_the_post_thumbnail( $post_id);
			$vehicle->types = wp_get_post_terms( $post_id, 'vehicle_types' );
			$vehicle->features = wp_get_post_terms( $post_id, 'vehicle_features' );
			$vehicles_array['vehicles'][] = $vehicle;
		}
		return $vehicles_array;
	 }
 }
 
 if ( ! function_exists( 'simontaxi_get_vehicles_order' ) ) {
	 /**
	 * Return the all available vehicles based on other settings.
	 *
	 * @since 1.0
	 * @param array $args - arguments.
	 * @return string
	 */
	 function simontaxi_get_vehicles_order( $args = array() ) {
		global $wpdb;
		$date = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date' );
		$pickup_date_return = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date_return' );

		/*
		We are finding vehicles which are blocked for the selected date
		*/
		$vehicles_array = $blockout_vehicles = array();
		$blockout_dates_objects = get_terms( 'blockout_date', array( 'hide_empty' => false ) );
		if ( ! empty ( $blockout_dates_objects ) ) {
			foreach ( $blockout_dates_objects as $blockout_date ) {
				$term_meta['block_date'] = get_term_meta( $blockout_date->term_id, 'block_date', true );
				$term_meta['block_date_end'] = get_term_meta( $blockout_date->term_id, 'block_date_end', true );
				$term_meta['vehicles'] = get_term_meta( $blockout_date->term_id, 'vehicles', true );

				$block_date = ( isset( $term_meta['block_date'] ) && $term_meta['block_date'] != '' ) ? date( 'Y-m-d', strtotime( $term_meta['block_date'] ) ) : '';
				$block_date_end = ( isset( $term_meta['block_date_end'] ) && $term_meta['block_date_end'] != '' ) ? date( 'Y-m-d', strtotime( $term_meta['block_date_end'] ) ) : '';

				/*
				If the selected date is between blocked date! then it is blockeddate!!
				*/
				if ( $date >= $block_date && $date <= $block_date_end ) {
					$blocked_cabs = (array)json_decode( $term_meta['vehicles'] );
					if ( ! empty( $blocked_cabs ) ) {
						foreach ( $blocked_cabs as $key => $val ) {
								$blockout_vehicles[] = $val;
						}
					}
				}
				/*
				We are checking vehicles availability on return date also, if user trying to book for return journey
				*/
				$journey_type = simontaxi_get_session( 'booking_step1', 'one_way', 'journey_type' );
				if ( 'two_way' === $journey_type ) {
					if ( $pickup_date_return >= $block_date && $pickup_date_return <= $block_date_end ) {
					$blocked_cabs = (array)json_decode( $term_meta['vehicles'] );
						if ( !empty( $blocked_cabs) ) {
							foreach ( $blocked_cabs as $key => $val ) {
									$blockout_vehicles[] = $val;
							}
						}
					}
				}
			}
		}

		/**
		 * Let us restrict number of vehicles
		 *
		 * @since 2.0.0
		*/
		if ( 'yes' === simontaxi_get_option( 'restrict_vehicles_count', 'no' ) ) {
			$bookings = $wpdb->prefix. 'st_bookings';
			$payments = $wpdb->prefix. 'st_payments';
			$confirmed_vehicle_status = simontaxi_get_option( 'confirmed_vehicle_status', 'confirmed' );
			$sql = "SELECT *, `" . $bookings."`.`ID` AS booking_id, `" . $bookings."`.`reference` AS booking_ref FROM `" . $bookings."` INNER JOIN `" . $payments."` ON `" . $payments."`.`booking_id`=`" . $bookings."`.`ID` WHERE `" . $bookings."`.booking_contacts!='' AND `" . $bookings."`.status='" . $confirmed_vehicle_status."' GROUP BY selected_vehicle";
			$result = $wpdb->get_results( $sql, 'ARRAY_A' );
			if ( ! empty( $result ) ) {
				foreach ( $result as $row ) {
					$sql = "SELECT COUNT(*) FROM `" . $bookings."` INNER JOIN `" . $payments."` ON `" . $payments."`.`booking_id`=`" . $bookings."`.`ID` WHERE `" . $bookings."`.booking_contacts!='' AND `" . $bookings."`.status='" . $confirmed_vehicle_status."' AND `" . $bookings."`.`selected_vehicle` = '" . $row['selected_vehicle']."'";
					$bookings_for_the_vehicle = $wpdb->get_var( $sql );

					$number_of_vehicles_available = get_post_meta( $row['selected_vehicle'], 'number_of_vehicles', true );
					if ( $number_of_vehicles_available <= $bookings_for_the_vehicle  ) {
						$blockout_vehicles[] = $row['selected_vehicle'];
					}
				}
			}
		}

		/**
		 * Now we are getting vehicles which are availabile for display means which are not blocked
		*/
		if ( isset( $args['pagination'] ) && $args['pagination'] == true ) {
			$args_new = array(
				'post_status' => 'publish',
				'orderby' => 'name',
				'order' => 'ASC',
				'post_type' => 'vehicle',
				'exclude' => $blockout_vehicles,
				'posts_per_page' => -1,
			);

			$all_vehicles = get_posts( $args_new );
			$vehicles_array['total'] = count( $all_vehicles);

			$args_new = array(
				'post_status' => 'publish',
				'orderby' => 'name',
				'order' => 'ASC',
				'post_type' => 'vehicle',
				'exclude' => $blockout_vehicles,
				'posts_per_page' => $args['perpage'],
				'offset' => $args['offset'],
			);

			$vehicles = get_posts( $args_new );

		} else {
			$args_new = array(
				'post_status' => 'publish',
				'orderby' => 'name',
				'order' => 'ASC',
				'post_type' => 'vehicle',
				'exclude' => $blockout_vehicles,
				'posts_per_page' => -1,
			);

			$vehicles = get_posts( $args_new );
		}
		foreach ( $vehicles as $vehicle ) {
			$post_id = $vehicle->ID;
			$vehicle->post_meta = simontaxi_filter_gk( get_post_meta( $post_id) );
			$vehicle->thumbnail = get_the_post_thumbnail( $post_id);
			$vehicle->types = wp_get_post_terms( $post_id, 'vehicle_types' );
			$vehicle->features = wp_get_post_terms( $post_id, 'vehicle_features' );
			$vehicles_array['vehicles'][] = $vehicle;
		}
		return $vehicles_array;
	 }
 }


if ( ! function_exists( 'simontaxi_array_push_assoc' ) ) {
	/*
	 * instead of making several dozen calls to the get_post_meta function to grab the keys I wanted. So here is my logic
	 *
	 * @since 1.0
	 * @returns array
	 */
	function simontaxi_array_push_assoc( $array, $key, $value ){
		$array[ $key ] = $value;
		return $array;
	}
}

if ( ! function_exists( 'simontaxi_filter_gk' ) ) {
	/*
	* instead of making several dozen calls to the get_post_meta function to grab the keys I wanted. So here is my logic. It grab all the keys(filter grab keys - filter_gk) I want.
	*
	* @since 1.0
	* @param array - $array - array of keys.
	* @returns array
	*/
	function simontaxi_filter_gk( $array ) {
		$mk = array();
		foreach( $array as $k => $v ){
			if ( is_array( $v ) && count( $v ) == 1 ) {
				$mk = simontaxi_array_push_assoc( $mk, $k, $v[0] );
			} else {
				$mk = simontaxi_array_push_assoc( $mk, $k, $v );
			}
		}
		return $mk;
	}
}

if ( ! function_exists( 'simontaxi_get_bookingsteps_urls' ) ) {
	/**
	* Filter for Booking steps Page URLs. We are creating filter so that later any one can change URLs to their requirements
	*
	* @since 1.0
	* @param string $step - URL to get.
	* @return string
	*/
	function simontaxi_get_bookingsteps_urls( $step ) {
		$urls = apply_filters( 'simontaxi_filter_bookingsteps', array( 'step1' => get_permalink( get_page_by_path( 'pick-locations' ) ),
			'step2' => get_permalink( get_page_by_path( 'select-vehicle' ) ),
			'step3' => get_permalink( get_page_by_path( 'confirm-booking' ) ),
			'step4' => get_permalink( get_page_by_path( 'select-payment-method' ) ),
			'proceed_to_pay' => admin_url( 'admin-post.php?action=proceed_to_pay' ),
			'payment_success' => get_permalink( get_page_by_path( 'payment-success' ) ),
			'payment_final' => get_permalink( get_page_by_path( 'payment-final' ) ),
			'user_account' => get_permalink( get_page_by_path( 'user-account' ) ),
			'user_bookings' => get_permalink( get_page_by_path( 'user-bookings' ) ),
			'user_payments' => get_permalink( get_page_by_path( 'user-payments' ) ),
			'user_support' => get_permalink( get_page_by_path( 'user-support' ) ),
			'billing_address' => get_permalink( get_page_by_path( 'user-billing-address' ) ),
			'manage_bookings' => admin_url( 'edit.php?post_type=vehicle&page=manage_bookings' ),
			'start_over' => get_permalink( get_page_by_path( 'clear-selections' ) ),
			'login' => get_permalink( get_page_by_path( 'sign-in' ) ),
			'registration' => get_permalink( get_page_by_path( 'registration' ) ),
			'forgotpassword' => get_permalink( get_page_by_path( 'forgotpassword' ) ),
			'resetpassword' => get_permalink( get_page_by_path( 'resetpassword' ) ),
			'manage_support' => admin_url( 'edit.php?post_type=vehicle&page=view_support_request' ),
			'settings' => admin_url( 'edit.php?post_type=vehicle&page=vehicle_settings' ),
			) );
		if ( isset( $urls[ $step ] ) ) return $urls[ $step ];
		else return '';
	}
}

if ( ! function_exists( 'simontaxi_get_fare' ) ) :
	/**
	 * This function serves to get the fare based on distance and vehicle
	 *
	 * @since 1.0
	 * @param Object $vehicle - which contains all the vehicle information.
	 * @param array $booking_step1 - Booking details.
	 * @return decimal
	 */
	function simontaxi_get_fare( $vehicle, $booking_step1 ) {
		/**
		 * Let us calculate the fare based on admin settings so that admin will have flexibility to choose the way he want to calculate the fare!
		 */
		$farecalculation_basedon = simontaxi_get_option( 'farecalculation_basedon', 'basicfare' );
		$booking_type = $booking_step1['booking_type'];

		/**
		 * The fare what we return. Default it is '0'
		 */
		$fare = $basic_distance = $distance = 0;
// dd( $booking_step1 );
		if ( $farecalculation_basedon == 'basicfare' && $booking_type != 'hourly' ) {
			$distance = $booking_step1['distance'];
			/**
			 * @since 2.0.0
			 */
			$distance = filter_var( $distance, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
			
			if ( $booking_type == 'p2p' ) {
				$basic_distance  = ( isset( $vehicle->post_meta['p2p_basic_distance'] ) ) ? $vehicle->post_meta['p2p_basic_distance'] : 0;
				$basic_price  = ( isset( $vehicle->post_meta['p2p_basic_price'] ) ) ? $vehicle->post_meta['p2p_basic_price'] : 0;
				$unit_price = ( isset( $vehicle->post_meta['p2p_unit_price'] ) ) ? $vehicle->post_meta['p2p_unit_price'] : 0;
			} elseif ( $booking_type == 'airport' ) {
				if ( $booking_step1['airport'] == 'pickup_location' ) {
				$basic_distance = ( isset( $vehicle->post_meta['from_airport_basic_distance'] ) ) ? $vehicle->post_meta['from_airport_basic_distance'] : 0;
				$basic_price  = ( isset( $vehicle->post_meta['from_airport_basic_price'] ) ) ? $vehicle->post_meta['from_airport_basic_price'] : 0;
				$unit_price = ( isset( $vehicle->post_meta['from_airport_unit_price'] ) ) ? $vehicle->post_meta['from_airport_unit_price'] : 0;
				} else {
				$basic_distance = ( isset( $vehicle->post_meta['to_airport_basic_distance'] ) ) ? $vehicle->post_meta['to_airport_basic_distance'] : 0;
				$basic_price  = ( isset( $vehicle->post_meta['to_airport_basic_price'] ) ) ? $vehicle->post_meta['to_airport_basic_price'] : 0;
				$unit_price = ( isset( $vehicle->post_meta['to_airport_unit_price'] ) ) ? $vehicle->post_meta['to_airport_unit_price'] : 0;
				}
			}

			if ( $distance > $basic_distance ) {
				/**
				 * Let us find how much the additional distance user is travelling, so that we can caluclate the fare based on per unit charge
				 */
				$additional_distance = $distance - $basic_distance;

				$fare = $basic_price + ( $additional_distance * $unit_price);
				
			} else {
				$fare = $basic_price;
			}
		} else {
			$distance = 0;
			if ( $booking_type != 'hourly' ) {
				$distance = $booking_step1['distance'];
			}			
			
			$vehicle_id = VARIABLE_PREFIX . $vehicle->ID;
			$result = get_terms( array( 'taxonomy' => 'distance_price', 'hide_empty' => false) );
			if ( $booking_type == 'hourly' ) {
				$result = get_terms( array( 'taxonomy' => 'hourly_packages', 'hide_empty' => false, 'slug' => $booking_step1['hourly_package']  ) );
			}
			$fare_details = '';
			/**
			 * The maximum fare for the particular vehicle. we will return this if given distance is not in the list so that fare should not be '0' at any time!!
			 */
			$maximumfare = 0;
			
			/**
			 * @since 2.0.6
			 *
			 * If user travels more distance than maximum distance specified in the admin distance chart. We need to calculate additional price
			 */
			$maximum_distance_found = 0;
			
			if ( $booking_type == 'p2p' ) {
				$unit_price = ( isset( $vehicle->post_meta['p2p_unit_price'] ) ) ? $vehicle->post_meta['p2p_unit_price'] : 0;
			} elseif ( $booking_type == 'airport' ) {
				if ( $booking_step1['airport'] == 'pickup_location' ) {
					$unit_price = ( isset( $vehicle->post_meta['from_airport_unit_price'] ) ) ? $vehicle->post_meta['from_airport_unit_price'] : 0;
				} else {
					$unit_price = ( isset( $vehicle->post_meta['to_airport_unit_price'] ) ) ? $vehicle->post_meta['to_airport_unit_price'] : 0;
				}
			}

			if ( ! empty( $result ) && ! is_wp_error( $result ) ) {
				foreach ( $result as $row ) {
					$row = ( array ) $row;
					$term_id = $row['term_id'];
					if ( $booking_type == 'hourly' ) {
						$t_id = $row['term_id'];
						$fare_details = ( Object ) get_term_meta( $term_id, 'fare', true );
					} else {
						$minimum_distance = get_term_meta( $term_id, 'minimum_distance', true );
						$maximum_distance = get_term_meta( $term_id, 'maximum_distance', true );
						
						/**
						 * If the distance is between minimum and maximum distance we need take that price
						 */
						if ( ( $distance >= $minimum_distance && $distance <= $maximum_distance) ) {
							$fare_details = json_decode( get_term_meta( $term_id, 'fare', true ) );
						} else {
							$fare_details_notfound = json_decode( get_term_meta( $term_id, 'fare', true ) );
							/**
							 * Let us take maximum fare for the vehicle to return it if the given distance is not in the list.
							 */
							if ( ! empty( $fare_details_notfound ) ) {
								foreach ( $fare_details_notfound as $key => $val ) {
									if ( $key == $vehicle_id && $maximumfare < $val) {
										$maximumfare = $val;
										$maximum_distance_found = $maximum_distance;
									}
								}
							}
						}
					}
				}
				if ( is_object( $fare_details) ) {
					if ( ! empty( $fare_details ) ) {
						foreach ( $fare_details as $key => $val ) {
							if ( $key == $vehicle_id ) {
								$fare = $val;
							}
						}
					}
				}
				
				/**
				* Let us make sure the fare should not be the '0' at any time. If it is '0' means not in the list which admin add, then let us return the maximum fare for that vehicle.
				*/
				if ( $fare == 0) {
					if ( $farecalculation_basedon == 'predefined' && $booking_type != 'hourly' ) {
						/**
						 * @since 2.0.6
						 *
						 * If user travels more distance than maximum distance specified in the admin distance chart. We need to calculate additional price
						 */
						if ( $distance > $maximum_distance_found ) {
							/**
							 * Let us find how much the additional distance user is travelled, so that we can caluclate the fare based on per unit charge
							 */
							$additional_distance = $distance - $maximum_distance_found;

							$fare = $maximumfare + ( $additional_distance * $unit_price);
						} else {
							$fare = $maximumfare;
						}
					} else {
						$fare = $maximumfare;
					}
				}
			}
		}
		
		/**
		 * 2.0.6
		 *
		 * Let us calculate fare based on number of persons if admin set
		 */
		if ( simontaxi_get_option( 'pesons_calculation', 'no' ) === 'yes' ) {
			if ( ! empty( $booking_step1['number_of_persons'] ) ) {
				$number_of_persons = $booking_step1['number_of_persons'];
				if ( ctype_digit( $number_of_persons ) ) {
					$fare = $fare * $number_of_persons;
				}
			}
		}
		
		/**
		 * Let us format the number according to admin settings
		 */
		return $fare;
	}
endif;

if ( ! function_exists( 'simontaxi_get_tax' ) ) {
	/**
	 * This funciton returns the tax amount which admin sets for the entire applicaiton
	 * @param float $price - Price.
	 */
	function simontaxi_get_tax( $price ) {
		$tax_rate = simontaxi_get_option( 'tax_rate', 0 );
		$tax_amount = 0;
		if ( $tax_rate > 0 ) {
			$tax_rate_type = simontaxi_get_option( 'tax_rate_type', 'percent' );
			if ( $tax_rate_type == 'percent' ) {
				$tax_amount = ( $price * $tax_rate) / 100;
			} else {
				$tax_amount =  $tax_rate;
			}
		}
		return $tax_amount;
	}
}

if ( ! function_exists( 'simontaxi_get_surcharges' ) ) {
  /**
   * This funtion will return the all surcharges applied as per admin settings
   * @param string $key (Optional) - Desired array key.
   */
  function simontaxi_get_surcharges( $key = '' ) {
	$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
	$booking_step2 = simontaxi_get_session( 'booking_step2', array() );
	$basic_amount = ( isset( $booking_step2['selected_amount'] ) ) ? $booking_step2['selected_amount'] : 0;
	$surcharges = array();
	  /**
	  * Let me calculate surcharges if any
	  * Following surcharges we are considering for now
	  * Peak season, Peak time, Airport, Additional pick up / drop off points, Waiting time
	  */
	  $waitingtime_surcharge = $waitingtime_surcharge_onward = $waitingtime_surcharge_return = 0;
	  if ( isset( $booking_step1['waiting_time'] ) && $booking_step1['waiting_time'] != '' ) {
		  $parts = explode( ':', $booking_step1['waiting_time'] );
		  $hours = $parts[0];
		  if ( isset( $parts[1] ) && $parts[1] > 30 ) {
			  /* if the minutes is greater than 30 minutes we will consider it as 1 hour */
			  $hours = $hours + 1;
		  }
		  $waitingtime_surcharge_onward = simontaxi_get_option( 'waitingtime_surcharge', 0) * $hours;
	  }
	  if ( isset( $booking_step1['waiting_time_return'] ) && $booking_step1['waiting_time_return'] != '' ) {
		  $parts = explode( ':', $booking_step1['waiting_time_return'] );
		  $hours = $parts[0];
		  if ( isset( $parts[1] ) && $parts[1] > 30 ) {
			  /* if the minutes is greater than 30 minutes we will consider it as 1 hour */
			  $hours = $hours + 1;
		  }
		  $waitingtime_surcharge_return = simontaxi_get_option( 'waitingtime_surcharge', 0) * $hours;
	  }
	  $surcharges['waitingtime_surcharge'] = $waitingtime_surcharge_onward + $waitingtime_surcharge_return;
	  $surcharges['waitingtime_surcharge_onward'] = $waitingtime_surcharge_onward;
	  $surcharges['waitingtime_surcharge_return'] = $waitingtime_surcharge_return;

	  $additionalpoints_surcharge = simontaxi_get_option( 'additionalpoints_surcharge', 0);
	  $additionalpoints_surcharge_amount = $additionalpoints_surcharge_amount_onward = $additionalpoints_surcharge_amount_return = 0;
	  /**
	   * Which means admin sets additional points surcharge. Let us calculate.
	   */
	  if ( $additionalpoints_surcharge > 0 ) {
		  $additionalpoints = 0;
		  if ( isset( $booking_step1['additional_pickups'] ) && $booking_step1['additional_pickups'] > 0 ) {
			  $additionalpoints += $booking_step1['additional_pickups'];
		  }
		  if ( isset( $booking_step1['additional_dropoff'] ) && $booking_step1['additional_dropoff'] > 0 ) {
			  $additionalpoints += $booking_step1['additional_dropoff'];
		  }
		  if ( $additionalpoints > 0 ) {
			$additionalpoints_surcharge_amount_onward = $additionalpoints_surcharge * $additionalpoints;
		  }

		  $additionalpoints_return = 0;
		  if ( isset( $booking_step1['additional_pickups_return'] ) && $booking_step1['additional_pickups_return'] > 0 ) {
			  $additionalpoints_return += $booking_step1['additional_pickups_return'];
		  }
		  if ( isset( $booking_step1['additional_dropoff_return'] ) && $booking_step1['additional_dropoff_return'] > 0 ) {
			  $additionalpoints_return += $booking_step1['additional_dropoff_return'];
		  }
		  if ( $additionalpoints_return > 0 ) {
			$additionalpoints_surcharge_amount_return = $additionalpoints_surcharge * $additionalpoints_return;
		  }
	  }
	  $surcharges['additionalpoints_surcharge'] = $additionalpoints_surcharge_amount_onward + $additionalpoints_surcharge_amount_return;
	  $surcharges['additionalpoints_surcharge_onward'] = $additionalpoints_surcharge_amount_onward;
	  $surcharges['additionalpoints_surcharge_return'] = $additionalpoints_surcharge_amount_return;

	  /**
	   * If the 'booking_type' is airport then we need to take the airport surcharges if admin sets
	   */
	  $airport_surcharge = 0;
	  if ( simontaxi_get_option( 'airport_surcharge', 0) > 0 && $booking_step1['booking_type'] == 'airport' ) {
		  if ( simontaxi_get_option( 'airport_surcharge_type', 'value' ) == 'percent' ) {
			  $airport_surcharge = ( $basic_amount * simontaxi_get_option( 'airport_surcharge', 0) ) / 100;
		  } else {
			$airport_surcharge = simontaxi_get_option( 'airport_surcharge', 0);
		  }
	  }
	  $surcharges['airport_surcharge'] = $airport_surcharge;
	  /**
	   * Let us calculate Peak time surcharges
	   */
	  $peak_time_surcharge_onward = $peak_time_surcharge_return = 0;
	  
	  if ( simontaxi_get_option( 'peak_time_surcharge', 0 ) > 0 ) {

		  /** Let us take 'Peak time time' which admin specified */
		  $peak_time_from = simontaxi_get_option( 'peak_time_from', 0 );
		  $peak_time_to = simontaxi_get_option( 'peak_time_to', 6 );

		  /**
		   * Let us calculate peak-time surcharge for onward journey
		   */
		  $onward_pickup_time = $booking_step1['pickup_time'];
		  $hours = explode( ':', $onward_pickup_time );
		  /**
		   * @since 2.0.2
		   * Change Description: 
		   * PHP 5.3 doesn't support the [] array syntax. Only PHP 5.4 and later does. For older PHP, you need to use array() instead of [].
		   */
		  if ( ! empty( $hours ) ) {
			 $hours = $hours[0]; 
		  }
		  /** Which means user selects time in peak-time. Then we need to add surcharges */
		  if ( $hours >= $peak_time_from && $hours <= $peak_time_to ) {
			  if ( simontaxi_get_option( 'peak_time_surcharge_type', 'value' ) == 'percent' ) {
				  $peak_time_surcharge_onward = ( $basic_amount * simontaxi_get_option( 'peak_time_surcharge', 0) ) / 100;
			  } else {
				  $peak_time_surcharge_onward = simontaxi_get_option( 'peak_time_surcharge', 0);
			  }
		  }

		  /**
		   * Let us calculate peak-time surcharge for return journey if it exists
		   */
		  if ( $booking_step1['journey_type'] == 'two_way' ) {
			  $pickup_time_return = $booking_step1['pickup_time_return'];
			  $hours = explode( ':', $pickup_time_return );
			  /**
			   * @since 2.0.2
			   * Change Description: 
			   * PHP 5.3 doesn't support the [] array syntax. Only PHP 5.4 and later does. For older PHP, you need to use array() instead of [].
			   */
			  if ( ! empty( $hours ) ) {
				 $hours = $hours[0]; 
			  }
			  /** Which means user selects time in peak-time. Then we need to add surcharges */
			  if ( $hours >= $peak_time_from && $hours < $peak_time_to ) {
				  if ( simontaxi_get_option( 'peak_time_surcharge_type', 'value' ) == 'percent' ) {
					$peak_time_surcharge_return = ( $basic_amount * simontaxi_get_option( 'peak_time_surcharge', 0 ) ) / 100;
				  } else {
					$peak_time_surcharge_return = simontaxi_get_option( 'peak_time_surcharge', 0 );
				  }
			  }
		  }
	  }
	  $surcharges['peak_time_surcharge'] = $peak_time_surcharge_onward + $peak_time_surcharge_return;
	  $surcharges['peak_time_surcharge_onward'] = $peak_time_surcharge_onward;
	  $surcharges['peak_time_surcharge_return'] = $peak_time_surcharge_return;
	  

	/**
	 * Let us calculate Peack season surcharges if any
	 */
	$peak_season_surcharge_onward = $peak_season_surcharge_return = 0;
	$peak_seasons = get_terms( 'peak_season', array( 'hide_empty' => false ) );

	if ( ! empty( $peak_seasons ) ) {

		$pickup_date = isset( $booking_step1['pickup_date'] ) ? $booking_step1['pickup_date'] : '';
		$pickup_date_return = isset( $booking_step1['pickup_date_return'] ) ? $booking_step1['pickup_date_return'] : '';
		/**
		 * Let us convert date into a sinle format so that comparision is simple.
		*/
		if ( $pickup_date != '' ) {
			$pickup_date = date( 'Y-m-d', strtotime( $pickup_date ) );
		}
		if ( $pickup_date_return != '' ) {
			$pickup_date_return = date( 'Y-m-d', strtotime( $pickup_date_return ) );
		}
		foreach ( $peak_seasons as $peak_season ) {
			$peak_season_start = get_term_meta( $peak_season->term_id, 'peak_season', true );
			$peak_season_end = get_term_meta( $peak_season->term_id, 'peak_season_end', true );
			/**
			 * Let us convert date into a sinle format so that comparision is simple.
			*/
			if ( $peak_season_start != '' ) {
				$peak_season_start = date( 'Y-m-d', strtotime( $peak_season_start ) );
			}
			if ( $peak_season_end != '' ) {
				$peak_season_end = date( 'Y-m-d', strtotime( $peak_season_end ) );
			}
			/**
			 * Let us calculate peak season surcharge for onward journey
			 */
			/**
			 * If the selected date is between peack season dates, then we need apply peack season surcharges
			 */

			if ( $pickup_date >= $peak_season_start && $pickup_date <= $peak_season_end ) {
				$surcharge = get_term_meta( $peak_season->term_id, 'surcharge', true );
				$surcharge_type = get_term_meta( $peak_season->term_id, 'surcharge_type', true );
				/**
				 * We are adding all peack season charges, if more than one.
				 *
				 * @since 2.0.0
				 */
				if ( $surcharge_type == 'percent' ) {
					$peak_season_surcharge_onward += ( $basic_amount * $surcharge) / 100;
				} else {
					$peak_season_surcharge_onward += $surcharge;
				}
			}

			/**
			 * Let us calculate peak season surcharge for return journey
			 */
			 if ( $pickup_date_return != '' ) {
				 if ( $pickup_date_return >= $peak_season_start && $pickup_date_return <= $peak_season_end ) {
					$surcharge = get_term_meta( $peak_season->term_id, 'surcharge', true );
					$surcharge_type = get_term_meta( $peak_season->term_id, 'surcharge_type', true );
					if ( $surcharge_type == 'percent' ) {
						$peak_season_surcharge_return += ( $basic_amount * $surcharge) / 100;
					} else {
						$peak_season_surcharge_return += $surcharge;
					}
				}
			 }
		}
	}
	$surcharges['peak_season_surcharge'] = ( $peak_season_surcharge_onward + $peak_season_surcharge_return );
	$surcharges['peak_season_surcharge_onward'] = $peak_season_surcharge_onward;
	$surcharges['peak_season_surcharge_return'] = $peak_season_surcharge_return;

	$surcharges['surcharge_total'] = ( $surcharges['waitingtime_surcharge'] + $surcharges['additionalpoints_surcharge'] + $surcharges['airport_surcharge'] + $surcharges['peak_time_surcharge'] + $surcharges['peak_season_surcharge'] );
	
	/**
	 * We are 'airport_surcharge' surcharges to only onward journey
	 */
	$surcharges['surcharge_total_onward'] = ( $surcharges['waitingtime_surcharge_onward'] + $surcharges['additionalpoints_surcharge_onward'] + $surcharges['airport_surcharge'] + $peak_time_surcharge_onward + $peak_season_surcharge_onward);

	$surcharges['surcharge_total_return'] = ( $surcharges['waitingtime_surcharge_return'] + $surcharges['additionalpoints_surcharge_return'] + $peak_time_surcharge_return + $peak_season_surcharge_return);
	
	/**
	 * Let us give ability to modify external plugins if any other surcharges.
	 */
	$surcharges = apply_filters( 'simontaxi_additional_surcharges', $surcharges, $booking_step1, $basic_amount );

	if ( $key != '' ) {
		 return isset( $surcharges[ $key ] ) ? $surcharges[ $key ] : '';
	 } else {
		 return $surcharges;
	 }
  }
}


 if ( ! function_exists( 'simontaxi_get_fare_details' ) ) {
	 /**
	 * This function returns the fare details which includes basic fare, service tax if any, surcharges if any, onward fare, return fare separately, so that it helps to get the any amount at any point of time.
	 *
	 * @since 1.0
	 * @param string $key (Optional) - desired key.
	 * @return array|string
	 */
	 function simontaxi_get_fare_details( $key = '' ) {
		 $booking_step1 = simontaxi_get_session( 'booking_step1', array() );

		 $booking_step2 = simontaxi_get_session( 'booking_step2', array() );
		 $discount_details = simontaxi_get_session( 'discount_details', '' );
		 
		 $discount_amount = 0;
		 if ( $discount_details != '' ) {
			 $discount_amount = ( isset( $discount_details['discount_amount'] ) ) ? $discount_details['discount_amount'] : 0;
		 }
		 $basic_amount = ( isset( $booking_step2['selected_amount'] ) ) ? $booking_step2['selected_amount'] : 0;
		 /* Amount selected for a vehicle */
		 $details = array( 'basic_amount' => $basic_amount);

		 /**
		  * Let us calculate tax based on admin settings
		  */
		 $tax_calculation_based_on = simontaxi_get_option( 'tax_calculation_based_on', 'basicfare' );
		 if ( $tax_calculation_based_on == 'basicfaresurcharges' ) {
			$surcharges = simontaxi_get_surcharges();
			$details['tax_amount_onward'] = simontaxi_get_tax( $basic_amount + $surcharges['surcharge_total_onward'] );
			if ( $booking_step1['journey_type'] == 'two_way' ) {
				$details['tax_amount_return'] = simontaxi_get_tax( $basic_amount + $surcharges['surcharge_total_return'] );
			} else {
				$details['tax_amount_return'] = 0;
			}
		 } else {
			$tax_amount = simontaxi_get_tax( $basic_amount );
			$details['tax_amount_onward'] = $tax_amount;
			if ( $booking_step1['journey_type'] == 'two_way' ) {
				$details['tax_amount_return'] = $tax_amount;
			} else {
				$details['tax_amount_return'] = 0;
			}
		 }
		 $details['tax_amount'] = $details['tax_amount_onward'] + $details['tax_amount_return'];


		 /* Discount applied */
		 $details['discount_amount'] = $discount_amount;

		 /* Discount details */
		 $details['discount_details'] = $discount_details;

		 /**
		  * get all surcharges if any
		  */
		 $details['surcharges'] = simontaxi_get_surcharges();
		 /* Surcharges
		  * For now we are calculating
		  * - 'waitingtime_surcharge'
		  * - 'additionalpoints_surcharge',
		  * - 'airport_surcharge',
		  * - 'peak_time_surcharge',
		  * - 'peak_season_surcharge'
		  */
		 $details['surcharges_amount'] = $details['surcharges']['surcharge_total'];
		 $details['surcharges_amount_onward'] = $details['surcharges']['surcharge_total_onward'];
		 $details['surcharges_amount_return'] = $details['surcharges']['surcharge_total_return'];

		 /** Final amount For Onward
		  * We are assuming same additional pickups or drop-offs for onward and return if user choose 'Two way' Journey
		  * We are showing airport surcharges in onward journey only
		  * We are showing discount details in onward journey only
		  */
		 $amount_payable = ( $basic_amount + $details['tax_amount_onward'] + $details['surcharges_amount_onward'] ) - ( $details['discount_amount'] );
		 $details['amount_payable_onward'] = $amount_payable;

		 /** Final amount For Return
		  * We are assuming same additional pickups or drop-offs for onward and return if user choose 'Two way' Journey
		  * We are showing airport surcharges in onward journey only
		  * We are showing discount details in onward journey only
		  */
		 if ( $booking_step1['journey_type'] == 'two_way' ) {
		 $amount_payable = ( $basic_amount + $details['tax_amount_return'] + $details['surcharges_amount_return'] );
		 $details['amount_payable_return'] = $amount_payable;
		 } else {
			 $details['amount_payable_return'] = 0;
		 }

		 /** Final amount Both Onward and Return if it exists */
		 $details['amount_payable'] = $details['amount_payable_onward'] + $details['amount_payable_return'];
		 
		 $details = apply_filters( 'simontaxi_additional_charges', $details );
		
		 /**
		  * PHP number format without comma
		  *
		  * @since 2.0.1
		  */
		 $details['amount_payable'] = number_format( $details['amount_payable'], 2, '.', '' );
		 
		 

		 if ( $key != '' ) {
			 return isset( $details[ $key ] ) ? $details[ $key ] : '';
		 } else {
			 return $details;
		 }
	 }
 }

if ( ! function_exists( 'simontaxi_get_countries' ) ) :
	/**
	 * Let us get countries list which are available
	 *
	 * @global wpdb  $wpdb  WordPress database abstraction object.
	 * @since 1.0
	 */
	function simontaxi_get_countries() {
		global $wpdb;
		$query = "SELECT * FROM " . $wpdb->prefix."st_countries ORDER BY name ASC";
		return $wpdb->get_results( $query);
	}
endif;

if ( ! function_exists( 'simontaxi_get_countries_values' ) ) :
	/**
	 * Let us get specific value regarding countries
	 *
	 * @global wpdb  $wpdb  WordPress database abstraction object.
	 * @since 1.0
	 */
	function simontaxi_get_countries_values( $key, $value, $field ) {
		global $wpdb;
		$query = "SELECT * FROM " . $wpdb->prefix."st_countries WHERE $key = '$value' ORDER BY name ASC";
		$result = $wpdb->get_results( $query);
		$val = '';
		if ( !empty( $result) ) {
			foreach ( $result as $row ) {
				if ( isset( $row->$field) ) {
					$val = $row->$field;
					break;
				}
			}
		}
		return $val;
	}
endif;

if ( ! function_exists( 'simontaxi_get_vehiclle_details' ) ) :
	/**
	 * This function will get all data related to a vehicle
	 * @param integer $vehicle_id - ID.
	 */
	function simontaxi_get_vehiclle_details( $vehicle_id ) {
		if ( ! empty( $vehicle_id ) ) {
			$vehicle = get_post( $vehicle_id );
			if ( ! empty( $vehicle ) ) {
				$post_id = $vehicle->ID;
				$vehicle->post_meta = simontaxi_filter_gk( get_post_meta( $post_id) );
				$vehicle->thumbnail = get_the_post_thumbnail( $post_id);
				$vehicle->types = wp_get_post_terms( $post_id, 'vehicle_types' );
				$vehicle->features = wp_get_post_terms( $post_id, 'vehicle_features' );
				$vehicles_array[] = $vehicle;
			}
			return $vehicle;
		} else {
			return '';
		}
	}
endif;

if ( ! function_exists( 'simontaxi_is_paypal_accept' ) ) {
	/**
	 * This function checks whether the given currency is accepted by paypal or not
	 * @param string $currency_code - Code.
	 */
	function simontaxi_is_paypal_accept( $currency_code ) {
		/**
		 * Refenrence : https://developer.paypal.com/docs/classic/api/currency_codes/#paypal
		 */
		$accepted_currencies = array(
			'AUD' => 'Australian Dollar',
			'BRL' => 'Brazilian Real',
			'CAD' => 'Canadian Dollar',
			'CZK' => 'Czech Koruna',
			'DKK' => 'Danish Krone',
			'EUR' => 'Euro',
			'HKD' => 'Hong Kong Dollar',
			'HUF' => 'Hungarian Forint',
			'ILS' => 'Israeli New Sheqel',
			'JPY' => 'Japanese Yen',
			'MYR' => 'Malaysian Ringgit',
			'MXN' => 'Mexican Peso',
			'NOK' => 'Norwegian Krone',
			'NZD' => 'New Zealand Dollar',
			'PHP' => 'Philippine Peso',
			'PLN' => 'Polish Zloty',
			'GBP' => 'Pound Sterling',
			'RUB' => 'Russian Ruble',
			'SGD' => 'Singapore Dollar',
			'SEK' => 'Swedish Krona',
			'CHF' => 'Swiss Franc',
			'TWD' => 'Taiwan New Dollar',
			'THB' => 'Thai Baht',
			'USD' => 'U.S. Dollar',
		);
		if ( isset( $accepted_currencies[ $currency_code ] ) ) {
			return true;
		} else {
			return false;
		}
	}
}


if ( ! function_exists( 'simontaxi_send_email' ) ) {
	/**
	 * This function will send the email to the specified user
	 *
	 * @param string $email - Recepient email.
	 * @param string $template - Template to send.
	 * @param string $type - Type of Journey.
	 */
	function simontaxi_send_email( $email, $template, $type = 'onward' ) {
		if ( $template == 'booking-success' ) {
			$email_body = simontaxi_booking_success_mail( $type );
			$email_subject = simontaxi_get_option( 'vehicle_booking_success_email_subject', 'Booking Success' );
			$from_email = simontaxi_get_option( 'vehicle_booking_success_from_address', get_option( 'admin_email' ) );
			$from_name = simontaxi_get_option( 'vehicle_booking_success_from_name', get_bloginfo() );
			$headers = 'From: ' . $from_name. ' <' . $from_email. '>' . "\r\n";
            /**
			 * Let us change the email type based on admin settings
			*/
			if ( simontaxi_get_option( 'vehicle_booking_success_email_type', 'html' ) == 'html' ) {
				add_filter( 'wp_mail_content_type', 'simontaxi_mail_html_type' );
			} else {
				add_filter( 'wp_mail_content_type', 'simontaxi_mail_text_type' );
			}
			$attachments = array(WP_CONTENT_DIR. '/uploads/2018/05/admin_file1.pdf');

			wp_mail( $email, $email_subject, $email_body, $headers,$attachments);

			/**
			 * Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
			*/
			if ( simontaxi_get_option( 'vehicle_booking_success_email_type', 'html' ) == 'html' ) {
				remove_filter( 'wp_mail_content_type', 'simontaxi_mail_html_type' );
			} else {
				remove_filter( 'wp_mail_content_type', 'simontaxi_mail_text_type' );
			}
		}
	}
}

if ( ! function_exists( 'simontaxi_send_sms' ) ) :
	/**
	 * This function send the SMS based on admin settings
	 *
	 * @since 1.0
	 * @param string $mobile Mobile number.
	 * @param string $template Template to send.
	 * @param string $type onward|return.
	 * @return array|string
	 */
	function simontaxi_send_sms( $mobile, $template, $type = 'onward' ) {
		
		global $wpdb;		
		/**
		 * We are using 3rd party plugin to send SMS so we need to check whether the plugin is active or not
		*/
		if ( simontaxi_is_sms_gateway_active() && $mobile != '' && $template != '' ) {
			$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $template . "'" );
			$getpost = get_post( $postid);
			$content = $getpost->post_content;
			$pattern = array(
				'/\{BLOG_TITLE\}/',
				'/\{BOOKING_REF\}/',
				'/\{FROM\}/',
				'/\{TO\}/',
				'/\{AMOUNT\}/',
				'/\{PAYMENT_STATUS\}/',
				'/\{DATE\}/',
				'/\{PAID\}/',
			);
			$paid = simontaxi_get_session( 'booking_step4', 0, 'amount_paid' ); //This contains total amount (Onward+Return) paid at transaction, means paid through payment gateway.
			if ( 'two_way' === simontaxi_get_session( 'booking_step1', 'one_way', 'journey_type' ) ) {
				$amount_details = simontaxi_get_fare_details();
				if ( $type == 'onward' ) {
					$paid = $paid - $amount_details['amount_payable_return']; //Means not '$paid' variable has amount paid for 'onward' journey
				} else {
					$paid = $paid - $amount_details['amount_payable_onward'];
				}
			}
			$amount_details = simontaxi_get_fare_details();
			if ( 'return' === $type ) {
				$journey_date = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date_return' ) . ' ' .simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_time_return' );
				
				$paid_replace = simontaxi_get_currency( $paid );
				/**
				 * Let us find if the amount contains '$' at its starting. If it contains '$' it comes backreference and trying to find for PHP varaible!!
				 * So let us add a '\' before the '$'
				 * Specially this will be the case with currency Dollor ($) with left placement (Settings->currency->Currency Position)
				 * @see http://php.net/manual/en/function.preg-replace.php#106263
				 */				
				$first_character = substr( $paid_replace, 0, 1 );
				if ( '$' === $first_character ) {
					$paid_replace = '\\' . $paid_replace;
				}

				$replacement = array(
					get_bloginfo(),
					simontaxi_get_session( 'booking_step1', '', 'reference' ),
					simontaxi_get_session( 'booking_step1', '', 'drop_location' ),
					simontaxi_get_session( 'booking_step1', '', 'pickup_location' ),
					$amount_details['amount_payable_onward'],
					ucfirst(simontaxi_get_session( 'booking_step4', 'failed', 'payment_status' ) ),
					$journey_date,
					$paid_replace,
				);
			} else {
				$journey_date = simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_date' ) . ' ' .simontaxi_get_session( 'booking_step1', date( 'Y-m-d' ), 'pickup_time' );

				$paid_replace = simontaxi_get_currency( $paid );
				/**
				 * Let us find if the amount contains '$' at its starting. If it contains '$' it comes backreference and trying to find for PHP varaible!!
				 * So let us add a '\' before the '$'
				 * Specially this will be the case with currency Dollor ($) with left placement (Settings->currency->Currency Position)
				 * @see http://php.net/manual/en/function.preg-replace.php#106263
				 */				
				$first_character = substr( $paid_replace, 0, 1 );
				if ( '$' === $first_character ) {
					$paid_replace = '\\' . $paid_replace;
				}
				$replacement = array(
					get_bloginfo(),
					simontaxi_get_session( 'booking_step1', '', 'reference' ),
					simontaxi_get_session( 'booking_step1', '', 'pickup_location' ),
					simontaxi_get_session( 'booking_step1', '', 'drop_location' ),
					$amount_details['amount_payable_onward'],
					ucfirst(simontaxi_get_session( 'booking_step4', 'failed', 'payment_status' ) ),
					$journey_date,
					$paid_replace,
				);
			}
			$content = preg_replace( $pattern, $replacement, $content);
			global $sms;
			$sms->to = array( $mobile);
			$sms->msg =  $content;
			try{
			$sms->SendSMS();
			} catch(Exception $e) {}
		}
	}
endif;

if ( ! defined( 'simontaxi_booking_success_mail' ) ) :
	/**
	* This function generate the SMS message to send
	*
	* @since 1.0
	* @param string $type onward|return.
	* @return string
	*/
	function simontaxi_booking_success_mail( $type ) {
		global $wpdb;
		$blog_title = get_bloginfo();
		$posttitle = 'booking-success';
		$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "'" );
		$getpost= get_post( $postid);
		$template= $getpost->post_content;
		$pattern = array(
			'/\{BLOG_TITLE\}/',
			'/\{DATE\}/',
			'/\{INVOICE\}/',
			'/\{FROM\}/',
			'/\{TO\}/',
			'/\{JOURNY_DATE\}/',
			'/\{JOURNY_TIME\}/',
			'/\{JOURNY_TYPE\}/',
			'/\{AMOUNT\}/',
			'/\{PAYMENT_STATUS\}/',
			'/\{NAME\}/',
			'/\{MOBILE\}/',
			'/\{VEHICLE\}/',
			'/\{PAID\}/',
			'/\{BOOKING_STATUS\}/',
			
			'/\{PAYMENT_METHOD\}/',
			'/\{FLIGHT_NUMBER\}/',
			'/\{NO_OF_PASSENGERS\}/',
			'/\{SPECIAL_INSTRUCTIONS\}/',
			);
		if ( simontaxi_get_session( 'booking_step4', '', 'payment_status' ) == 'success' ) {
			$booking_status_payment_success = simontaxi_get_option( 'booking_status_payment_success', 'new' );
		} else {
			$booking_status_payment_success = 'new';
		}
		$paid = simontaxi_get_session( 'booking_step4', 0, 'amount_paid' ); //This contains total amount (Onward+Return) paid at transaction, means paid through payment gateway.
		
		$amount_details = simontaxi_get_fare_details();
		if ( 'two_way' === simontaxi_get_session( 'booking_step1', 'one_way', 'journey_type' ) ) {
			if ( 'onward' === $type ) {
				$paid = $paid - $amount_details['amount_payable_return']; //Means not '$paid' variable has amount paid for 'onward' journey
			} else {
				$paid = $paid - $amount_details['amount_payable_onward'];
			}
		}
		if ( 'onward' === $type ) {
			$amount_total = $amount_details['amount_payable_onward'];
		} else {
			$amount_total = $amount_details['amount_payable_return'];
		}

		$journey_type = 'ONE WAY';
		$mobile = '-';
		/**
		 * Mobile number field is optional in admin. Lets check whether it is enabled and user enter it.
		 */
		$mobile = simontaxi_get_session( 'booking_step3', '', 'mobile' );
		$mobile_countrycode = simontaxi_get_session( 'booking_step3', '', 'mobile_countrycode' );

		if ( '' !== $mobile && '' !== $mobile_countrycode ) {
			$mobile_countrycode = explode( '_', $mobile_countrycode);
			/**
			 * @since 2.0.2
			 * Change Description: 
			 * PHP 5.3 doesn't support the [] array syntax. Only PHP 5.4 and later does. For older PHP, you need to use array() instead of [].
			 */
			if ( ! empty( $mobile_countrycode ) ) {
				$mobile_countrycode = $mobile_countrycode[0]; 
			}
			$mobile = $mobile_countrycode . $mobile;
		}
		$vehicle_details = simontaxi_get_session( 'booking_step2', array(), 'vehicle_details' );
		$vehicle = ( isset( $vehicle_details->post_title ) ) ? $vehicle_details->post_title : '-';

		$full_name = simontaxi_get_session( 'booking_step3', '', 'email' );
		/**
		 * Full name field is optional in admin. Lets check whether it is enabled and user enter it.
		 */
		if ( simontaxi_get_session( 'booking_step3', '', 'full_name' ) != '' ) {
			$full_name = simontaxi_get_session( 'booking_step3', '', 'full_name' );
		}elseif ( simontaxi_get_session( 'booking_step3', '', 'first_name' ) != '' ) {
			$full_name = simontaxi_get_session( 'booking_step3', '', 'first_name' );
			if ( simontaxi_get_session( 'booking_step3', '', 'last_name' ) != '' ) {
				$full_name .= simontaxi_get_session( 'booking_step3', '', 'last_name' );
			}
		}
		if ( 'return' === $type ) {
			$amount_total_replace = simontaxi_get_currency( $amount_total );
			$paid_replace = simontaxi_get_currency( $paid );
			/**
			 * Let us find if the amount contains '$' at its starting. If it contains '$' it comes backreference and trying to find for PHP varaible!!
			 * So let us add a '\' before the '$'
			 * Specially this will be the case with currency Dollor ($) with left placement (Settings->currency->Currency Position)
			 * @see http://php.net/manual/en/function.preg-replace.php#106263
			 */
			$first_character = substr( $amount_total_replace, 0, 1 );
			if ( '$' === $first_character ) {
				$amount_total_replace = '\\' . $amount_total_replace;
			}
			$first_character = substr( $paid_replace, 0, 1 );
			if ( '$' === $first_character ) {
				$paid_replace = '\\' . $paid_replace;
			}
			
			$no_of_passengers = simontaxi_get_session( 'booking_step3', '', 'no_of_passengers' );
			if ( empty( $no_of_passengers ) ) {
				$no_of_passengers = simontaxi_get_session( 'booking_step1', '', 'number_of_persons' );
			}
			$selected_payment_method = simontaxi_get_session( 'booking_step4', '', 'selected_payment_method' );
			if ( ! empty( $selected_payment_method ) ) {
				$selected_payment_method_title = simontaxi_get_option( $selected_payment_method );
				if ( ! empty( $selected_payment_method_title ) ) {
					$selected_payment_method = ! empty( $selected_payment_method_title['title'] ) ? $selected_payment_method_title['title'] : $selected_payment_method;
				}
			}
			
			$replacement = array(
				$blog_title,
				date_i18n( 'Y-m-d',time() ),
				simontaxi_get_session( 'booking_step1', '', 'reference' ),
				simontaxi_get_session( 'booking_step1', '', 'drop_location' ),
				simontaxi_get_session( 'booking_step1', '', 'pickup_location' ),
				simontaxi_get_session( 'booking_step1', '', 'pickup_date_return' ),
				simontaxi_get_session( 'booking_step1', '', 'pickup_time_return' ),
				ucfirst( str_replace( '_', ' ', $journey_type ) ),
				$amount_total_replace,
				ucfirst(simontaxi_get_session( 'booking_step4', '', 'payment_status' ) ),
				$full_name,
				$mobile,
				$vehicle,
				$paid_replace,
				$booking_status_payment_success,
				
				$selected_payment_method,
				simontaxi_get_session( 'booking_step1', '', 'flight_no' ),
				$no_of_passengers,
				simontaxi_get_session( 'booking_step3', '', 'special_instructions' ),
			);
		} else {
			$drop_location = '-';
			/**
			* For hourly rental there will be no drop location, hence let us check whether it exists or not.
			*/
			if ( simontaxi_get_session( 'booking_step1', '', 'drop_location' ) !== '' ) {
				$drop_location = simontaxi_get_session( 'booking_step1', '', 'drop_location' );
			}
			
			$amount_total_replace = simontaxi_get_currency( $amount_total );
			$paid_replace = simontaxi_get_currency( $paid );
			/**
			 * Let us find if the amount contains '$' at its starting. If it contains '$' it comes backreference and trying to find for PHP varaible!!
			 * So let us add a '\' before the '$'
			 * Specially this will be the case with currency Dollor ($) with left placement (Settings->currency->Currency Position)
			 * @see http://php.net/manual/en/function.preg-replace.php#106263
			 */
			$first_character = substr( $amount_total_replace, 0, 1 );
			if ( '$' === $first_character ) {
				$amount_total_replace = '\\' . $amount_total_replace;
			}
			$first_character = substr( $paid_replace, 0, 1 );
			if ( '$' === $first_character ) {
				$paid_replace = '\\' . $paid_replace;
			}
			
			$no_of_passengers = simontaxi_get_session( 'booking_step3', '', 'no_of_passengers' );
			if ( empty( $no_of_passengers ) ) {
				$no_of_passengers = simontaxi_get_session( 'booking_step1', '', 'number_of_persons' );
			}			
			$selected_payment_method = simontaxi_get_session( 'booking_step4', '', 'selected_payment_method' );
			if ( ! empty( $selected_payment_method ) ) {
				$selected_payment_method_title = simontaxi_get_option( $selected_payment_method );
				if ( ! empty( $selected_payment_method_title ) ) {
					$selected_payment_method = ! empty( $selected_payment_method_title['title'] ) ? $selected_payment_method_title['title'] : $selected_payment_method;
				}
			}
			
			$replacement = array(
				$blog_title,
				date_i18n( 'Y-m-d',time() ),
				simontaxi_get_session( 'booking_step1', '', 'reference' ),
				simontaxi_get_session( 'booking_step1', '', 'pickup_location' ),
				$drop_location,
				simontaxi_get_session( 'booking_step1', '', 'pickup_date' ),
				simontaxi_get_session( 'booking_step1', '', 'pickup_time' ),
				ucfirst( str_replace( '_', ' ', $journey_type ) ),
				$amount_total_replace,
				ucfirst(simontaxi_get_session( 'booking_step4', '', 'payment_status' ) ),
				$full_name,
				$mobile,
				$vehicle,
				$paid_replace,
				$booking_status_payment_success,
				
				$selected_payment_method,
				simontaxi_get_session( 'booking_step1', '', 'flight_no' ),
				$no_of_passengers,
				simontaxi_get_session( 'booking_step3', '', 'special_instructions' ),
			);
		}

		$template =  preg_replace( $pattern, $replacement, $template );
		
		return $template;
	}
endif;

if ( ! defined( 'simontaxi_is_user' ) ) :
	/**
	 * This function check whenter the user is belongs to the particualte user role or not
	 * @param string $user_role - Role.
	 */
	function simontaxi_is_user( $user_role ) {
		$current_user = ( array ) wp_get_current_user();
		$roles = $status_links = array();
		foreach ( $current_user['roles'] as $role ) {
			$roles[] = trim( $role );
		}
		if ( in_array( $user_role, $roles ) )
			return true;
		else
			return false;
	}
endif;

add_action( 'show_user_profile', 'simontaxi_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'simontaxi_show_extra_profile_fields' );

if ( ! function_exists( 'simontaxi_show_extra_profile_fields' ) ) :
	/**
	 * This function to add additional profile fields
	 *
	 * @param string $user - User bject.
	 */
	function simontaxi_show_extra_profile_fields( $user ) {
	$countryList = simontaxi_get_countries();
	?>
	<h3><?php esc_html_e( 'Extra profile information', 'simontaxi' ); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="mobile_no"><?php esc_html_e( 'Mobile No.', 'simontaxi' ); ?></label></th>
			<td>
			<select id="mobile_countrycode" name="mobile_countrycode" title="<?php esc_html_e( 'Country code', 'simontaxi' ); ?>"class="selectpicker show-tick show-menu-arrow">
			<option value=""><?php esc_html_e( 'Country code', 'simontaxi' ); ?></option>
			<?php
			if ( $countryList) {
				$mobile_countrycode = get_the_author_meta( 'mobile_countrycode', $user->ID );
				if ( isset( $user_meta['mobile_countrycode'] ) )
					$mobile_countrycode = $user_meta['mobile_countrycode'];
				foreach ( $countryList as $result) {
					$code = $result->phonecode. '_' . $result->id_countries;
					?>
					<option value="<?php echo $code; ?>" <?php if ( $mobile_countrycode == $code) echo 'selected="selected"'; ?>><?php echo $result->name . ' ( ' . $result->phonecode. ' )'; ?> </option>
					<?php
				}
			}
			?>
			</select>
			&nbsp;<input type="text" name="mobile" id="mobile" value="<?php echo esc_attr( get_the_author_meta( 'mobile', $user->ID ) ); ?>" class="regular-text" />
			</td>
		</tr>
	</table>
	<?php }
endif;

add_action( 'personal_options_update', 'simontaxi_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'simontaxi_save_extra_profile_fields' );

if ( ! function_exists( 'simontaxi_save_extra_profile_fields' ) ) :
	/**
	 * This function to save additional profile fields
	 */
	function simontaxi_save_extra_profile_fields( $user_id ) {
		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}
		/* Copy and paste this line for additional fields. Make sure to change 'mobile' to the field ID. */
		update_user_meta( absint( $user_id ), 'mobile', wp_kses_post( $_POST['mobile'] ) );
		update_user_meta( absint( $user_id ), 'mobile_countrycode', wp_kses_post( $_POST['mobile_countrycode'] ) );
	}
endif;

if ( ! function_exists( 'simontaxi_send_email_sms' ) ) :
	/**
	 * @since 2.0.0
	 */
	add_action( 'simontaxi_send_email_sms', 'simontaxi_send_email_sms', 10, 1 );
	
	/**
	 * This function to send email OR SMS based on admin settings
	 *
	 * @param string $type - Booking type Onward|Return.
	 */
	function simontaxi_send_email_sms( $type = 'onward' ) {
		/**
		 * Let us send email to user based on admin settings
		 */
		if ( simontaxi_get_option( 'vehicle_booking_success_email_user', 'yes' ) == 'yes' ) {
				$email = simontaxi_get_session( 'booking_step3', '', 'email' );
				simontaxi_send_email( $email, 'booking-success', $type);
		}
		
		/**
		 * Let us send SMS to user based on admin settings
		 * Mobile number field is optional in admin. Lets check whether it is enabled and user enter it.
		 */
		$mobile = simontaxi_get_session( 'booking_step3', '', 'mobile' );
		
		$mobile_countrycode = simontaxi_get_session( 'booking_step3', '', 'mobile_countrycode' );
		
		if ( simontaxi_get_option( 'vehicle_booking_success_sms_user', 'no' ) == 'yes' && '' !== $mobile && '' !== $mobile_countrycode ) {
				$mobile_countrycode = explode( '_', $mobile_countrycode );
				/**
				   * @since 2.0.2
				   * Change Description: 
				   * PHP 5.3 doesn't support the [] array syntax. Only PHP 5.4 and later does. For older PHP, you need to use array() instead of [].
				   */
				  if ( ! empty( $mobile_countrycode ) ) {
					 $mobile_countrycode = $mobile_countrycode[0]; 
				  }
				$mobile = $mobile_countrycode . $mobile;
				simontaxi_send_sms( $mobile, 'sms-booking-success', $type );				
		}

		/**
		 * Let us send email to admin based on admin settings
		 */
		if ( simontaxi_get_option( 'vehicle_booking_success_email_admin', 'yes' ) == 'yes' ) {
				$email = get_option( 'admin_email' );
				simontaxi_send_email( $email, 'booking-success', $type);
		}

		/**
		 * Let us send SMS to admin based on admin settings
		 * Mobile number field is optional for admin users. So lets check whether admin user has updated mobile number or not.
		 * Here the admin refers to the mail admin only.
		 */
		$mobile_no = simontaxi_get_primary_admin_mobile(); //Assuming admin ID is '1'
		if ( simontaxi_get_option( 'vehicle_booking_success_sms_admin', 'no' ) == 'yes' && $mobile_no != '' ) {
				simontaxi_send_sms( $mobile_no, 'sms-booking-success', $type );
		}
		
	}
endif;

if ( ! function_exists( 'simontaxi_send_status_change_email' ) ) :
	/**
	 * @since 2.0.0
	 */
	add_action( 'simontaxi_send_status_change_email', 'simontaxi_send_status_change_email', 10, 3 );
	/**
	 * This function serves to send email when status is changed in admin area
	 *
	 * @param int $booking_id - ID.
	 * @param string $status - Booking Status.
	 * $param string $type - User Type - User|Admin.
	 */
	function simontaxi_send_status_change_email( $booking_id, $status, $type = 'user' ) {
		global $wpdb;
		$bookings = $wpdb->prefix. 'st_bookings';
		$payments = $wpdb->prefix. 'st_payments';
		$sql = "SELECT *, `" . $bookings."`.`ID` AS booking_id, `" . $bookings."`.`reference` AS booking_ref FROM `" . $bookings."` INNER JOIN `" . $payments."` ON `" . $payments."`.`booking_id`=`" . $bookings."`.`ID` WHERE `" . $bookings."`.booking_contacts!='' AND `" . $bookings."`.ID=" . $booking_id;
		$result = $wpdb->get_results( $sql);
		if ( ! empty( $result ) ) {
			$booking=( array ) $result[0];
			unset( $booking['session_details'] ); /* No need this information so kill it!!*/
			$contact = (array)json_decode( $booking['booking_contacts'] );
			if ( $type == 'user' ) {
				 $email = $contact['email'];
			 } elseif( $type == 'admin' ) {
				 $email = simontaxi_get_option( 'vehicle_booking_confirm_from_address', get_option( 'admin_email' ) );
			 } else {
				 $email = apply_filters( 'simontaxi_get_email', $booking_id );
			 }
			 
			if ( '' !== $email ) {
				$blog_title = get_bloginfo( 'name' );
				if ( $status == 'confirmed' ) {
					$posttitle = 'booking-confirmed';
					$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "'" );
					$getpost= get_post( $postid );

					if ( ! empty( $getpost ) ) {
						$template= $getpost->post_content;
						$pattern = array( 
							'/\{BLOG_TITLE\}/',
							'/\{DATE\}/',
							'/\{BOOKING_REF\}/' ,
							'/\{JOURNEY_TYPE\}/' ,
							'/\{PICKUP_LOCATION\}/',
							'/\{DROP_LOCATION\}/',
							'/\{PICKUP_DATE\}/',
							'/\{PICKUP_TIME\}/',
							'/\{CONTACT_NAME\}/',
							'/\{CONTACT_MOBILE\}/',
							'/\{CONTACT_EMAIL\}/',
							'/\{BOOKING_STATUS\}/',
							'/\{BOOKING_STATUS_UPDATED\}/',
							'/\{REASON\}/',
						);

						$full_name = '-';
						 if ( isset( $contact['full_name'] ) ) {
							 $full_name = $contact['full_name'];
						 }elseif ( isset( $contact['first_name'] ) ) {
							$full_name = $contact['first_name'];
							if ( isset( $contact['last_name'] ) && $contact['last_name'] != '' ) {
								$full_name .= ' ' . $contact['last_name'];
							}
						}
						 $mobile = '-';
						 if ( isset( $contact['mobile'] ) ) {
							 $mobile = $contact['mobile'];
						 }
						 $message = '';
						 if ( isset( $contact['reason_message'] ) ) {
							 $message = $contact['reason_message'];
						 }
						 $replacement = array(
							$blog_title,
							date_i18n( 'Y-m-d',time() ),
							$booking['booking_ref'],
							strtoupper( str_replace( '_', ' ', $booking['journey_type'] ) ),
							$booking['pickup_location'],
							$booking['drop_location'],
							$booking['pickup_date'],
							$booking['pickup_time'],
							$full_name,
							$mobile,
							$contact['email'],
							strtoupper( $booking['status'] ),
							strtoupper( $booking['status_updated'] ),
							$booking['reason_message'],
						);
						$template =  preg_replace( $pattern, $replacement, $template);

						$subject = simontaxi_get_option( 'vehicle_booking_confirm_email_subject', 'Booking Confirmed' );
						$from_email = simontaxi_get_option( 'vehicle_booking_confirm_from_address', get_option( 'admin_email' ) );
						$from_name = simontaxi_get_option( 'vehicle_booking_confirm_from_name', get_bloginfo() );
						$headers = 'From: ' . $from_name. ' <' . $from_email. '>' . "\r\n";
						/**
						 * Let us change the email type based on admin settings
						*/
						if ( simontaxi_get_option( 'vehicle_booking_confirm_email_type', 'html' ) == 'html' ) {
							add_filter( 'wp_mail_content_type', 'simontaxi_mail_html_type' );
						} else {
							add_filter( 'wp_mail_content_type', 'simontaxi_mail_text_type' );
						}

						wp_mail( $email, $subject, $template, $headers);

						/**
						 * Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
						*/
						if ( simontaxi_get_option( 'vehicle_booking_confirm_email_type', 'html' ) == 'html' ) {
							remove_filter( 'wp_mail_content_type', 'simontaxi_mail_html_type' );
						} else {
							remove_filter( 'wp_mail_content_type', 'simontaxi_mail_text_type' );
						}
					}
				} elseif ( $status == 'cancelled' ) {
					$posttitle = 'booking-cancel';
					$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "'" );
					$getpost= get_post( $postid);
					if ( !empty( $getpost) ) {
						$template= $getpost->post_content;
						$pattern = array( 
							'/\{BLOG_TITLE\}/',
							'/\{DATE\}/',
							'/\{BOOKING_REF\}/',
							'/\{JOURNEY_TYPE\}/',
							'/\{PICKUP_LOCATION\}/',
							'/\{DROP_LOCATION\}/',
							'/\{PICKUP_DATE\}/',
							'/\{PICKUP_TIME\}/',
							'/\{CONTACT_NAME\}/',
							'/\{CONTACT_MOBILE\}/',
							'/\{CONTACT_EMAIL\}/',
							'/\{BOOKING_STATUS\}/',
							'/\{BOOKING_STATUS_UPDATED\}/',
							'/\{REASON\}/',
						);
						$message = '';
						 if ( isset( $contact['reason_message'] ) ) {
							 $message = $contact['reason_message'];
						 }
						 $full_name = '';
						 if ( isset( $contact['full_name'] ) ) {
							 $full_name = $contact['full_name'];
						 }elseif ( isset( $contact['first_name'] ) ) {
							$full_name = $contact['first_name'];
							if ( isset( $contact['last_name'] ) && $contact['last_name'] != '' ) {
								$full_name .= ' ' . $contact['last_name'];
							}
						}
						$mobile = '-';
						 if ( isset( $contact['mobile'] ) ) {
							 $mobile = $contact['mobile'];
						 }
						$replacement = array(
							$blog_title,
							date_i18n( 'Y-m-d',time() ),
							$booking['booking_ref'],
							strtoupper( str_replace( '_', ' ', $booking['journey_type'] ) ),
							$booking['pickup_location'],
							$booking['drop_location'],
							$booking['pickup_date'],
							$booking['pickup_time'],
							$full_name,
							$mobile,
							$contact['email'],
							strtoupper( $booking['status'] ),
							strtoupper( $booking['status_updated'] ),
							$booking['reason_message'],
						);

						$template =  preg_replace( $pattern, $replacement, $template);

						$subject = simontaxi_get_option( 'vehicle_booking_cancel_email_subject', 'Booking Cancelled' );
						$from_email = simontaxi_get_option( 'vehicle_booking_cancel_from_address', get_option( 'admin_email' ) );
						$from_name = simontaxi_get_option( 'vehicle_booking_cancel_from_name', get_bloginfo() );
						$headers = 'From: ' . $from_name. ' <' . $from_email. '>' . "\r\n";
						/**
						 * Let us change the email type based on admin settings
						*/
						if ( simontaxi_get_option( 'vehicle_booking_cancel_email_type', 'html' ) == 'html' ) {
							add_filter( 'wp_mail_content_type', 'simontaxi_mail_html_type' );
						} else {
							add_filter( 'wp_mail_content_type', 'simontaxi_mail_text_type' );
						}
						
						wp_mail( $email, $subject, $template, $headers);

						/**
						 * Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
						*/
						if ( simontaxi_get_option( 'vehicle_booking_cancel_email_type', 'html' ) == 'html' ) {
							remove_filter( 'wp_mail_content_type', 'simontaxi_mail_html_type' );
						} else {
							remove_filter( 'wp_mail_content_type', 'simontaxi_mail_text_type' );
						}
					}
				}
			}
		}
	}
endif;

if ( ! function_exists( 'simontaxi_mail_html_type' ) ) :
	/**
	 * This function to set the mail type which is called through 'wp_mail_content_type' filter
	 */
	function simontaxi_mail_html_type() {
		return 'text/html';
	}
endif;

if ( ! function_exists( 'simontaxi_mail_text_type' ) ) :
	/**
	 * This function to set the mail type which is called through 'wp_mail_content_type' filter
	 */
	function simontaxi_mail_text_type() {
		return 'text/plain';
	}
endif;

if ( ! function_exists( 'simontaxi_replace_constants' ) ) :
	/**
	 * This function prepares the content to sent by replacing variables with values
	 *
	 * @param array $variables - variables.
	 * @param string $message - Message contains constants.
	 */
	function simontaxi_replace_constants( $variables, $message ) {
		if ( is_array( $variables) ) {
			foreach( $variables as $key => $val ) {
				$message = str_replace( $key, $val, $message);
			}
		}
		return $message;
	}
endif;

if ( ! function_exists( 'simontaxi_send_status_change_sms' ) ) :
	/**
	 * @since 2.0.0
	 */
	add_action( 'simontaxi_send_status_change_sms', 'simontaxi_send_status_change_sms', 10, 3 );
	/**
	 * This function serves to send SMS when status is changed in admin area
	 *
	 * @param int $booking_id - ID.
	 * @param string $status - Booking Status.
	 * @param string $type - User|Admin for now.
	 */
	function simontaxi_send_status_change_sms( $booking_id, $status, $type = 'user' ) {
		/**
		 * We are using 3rd party plugin to send SMS so we need to check whether the plugin is active or not
		 */
		if ( simontaxi_is_sms_gateway_active() && $booking_id != '' && $status != '' ) {
			global $wpdb;
			$bookings = $wpdb->prefix. 'st_bookings';
			$payments = $wpdb->prefix. 'st_payments';
			$sql = "SELECT *, `" . $bookings."`.`ID` AS booking_id, `" . $bookings."`.`reference` AS booking_ref FROM `" . $bookings."` INNER JOIN `" . $payments."` ON `" . $payments."`.`booking_id`=`" . $bookings."`.`ID` WHERE `" . $bookings."`.booking_contacts!='' AND `" . $bookings."`.ID=" . $booking_id;
			$result = $wpdb->get_results( $sql);
			$booking=(array)$result[0];
			unset( $booking['session_details'] ); /* No need this information so kill it!!*/
			$contact = (array)json_decode( $booking['booking_contacts'] );
			$mobile = '';
			if ( $type == 'user' ) {
				 $mobile_countrycode = '';
				 if ( isset( $contact['mobile_countrycode'] ) ) {
					$mobile_countrycode_parts = explode( '_', $contact['mobile_countrycode'] );
					/**
					   * @since 2.0.2
					   * Change Description: 
					   * PHP 5.3 doesn't support the [] array syntax. Only PHP 5.4 and later does. For older PHP, you need to use array() instead of [].
					   */
					  if ( ! empty( $mobile_countrycode_parts ) ) {
						 $mobile_countrycode = $mobile_countrycode_parts[0]; 
					  }
				 }
				 if ( isset( $contact['mobile'] ) ) {
					 $mobile = $mobile_countrycode . $contact['mobile'];
				 }
			 } elseif( 'admin' === $type ) {
				 $mobile = simontaxi_get_primary_admin_mobile(); //Assuming admin ID is '1'
			 } else {
				 $mobile = apply_filters( 'simontaxi_get_mobile', $booking_id );
			 }

			 if ( ! empty( $result) && $mobile != '' ) {
				 if ( $status == 'confirmed' ) {
					$posttitle = 'sms-booking-confirmed';
					$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "' AND post_status='publish'" );
					$getpost= get_post( $postid);
					$template= $getpost->post_content;
					$pattern = array( 
						'/\{BOOKING_REF\}/',
						'/\{PICKUP_DATE\}/',
						'/\{PICKUP_TIME\}/',
						'/\{CAR_PLATE\}/',
						'/\{FROM\}/',
						'/\{TO\}/',
					);
					$replacement = array(
						$booking['booking_ref'],
						$booking['pickup_date'],
						$booking['pickup_time'],
						$booking['vehicle_no'],
						$booking['pickup_location'],
						$booking['drop_location'],
					);
				 } elseif ( $status == 'cancelled' ) {
					$posttitle = 'sms-booking-cancel';
					$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "' AND post_status='publish'" );
					$getpost= get_post( $postid);
					$template= $getpost->post_content;
					$pattern = array(
						'/\{BOOKING_REF\}/',
						'/\{PICKUP_DATE\}/',
						'/\{PICKUP_TIME\}/',
						'/\{REASON\}/',
						'/\{FROM\}/',
						'/\{TO\}/',
					);
					$replacement = array(
						$booking['booking_ref'],
						$booking['pickup_date'],
						$booking['pickup_time'],
						$booking['reason_message'],
						$booking['pickup_location'],
						$booking['drop_location'],
					);
				}
				$template = preg_replace( $pattern, $replacement, $template);

				global $sms;
				$sms->to = array( $mobile);
				$sms->msg =  $template;
				try{
					$sms->SendSMS();
				} catch( Exception $e ) {}
			}
		}
	}
endif;

if ( ! function_exists( 'simontaxi_send_email_sms_adminside' ) ) :
	/**
	 * @since 2.0.0
	 */
	add_action( 'simontaxi_send_email_sms_adminside', 'simontaxi_send_email_sms_adminside', 10, 2 );
	/**
	 * This function send SMS and Email based on admin settings while changing booking status from admin end
	 *
	 * @param int $booking_id - ID.
	 * @param string $status - Booking Status.
	 */
	function simontaxi_send_email_sms_adminside( $booking_id, $status ) {
		$sent = false;
		if ( 'confirmed' === $status ) {
			/**
			 * Let us send email to user based on admin settings
			 */
			if ( simontaxi_get_option( 'vehicle_booking_confirm_email_user', 'yes' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_email', $booking_id, $status, 'user' );
					$sent = true;
			}

			/**
			 * Let us send SMS to user based on admin settings
			 * Mobile number field is optional in admin. Lets check whether it is enabled and user enter it.
			 */
			if ( simontaxi_get_option( 'vehicle_booking_confirm_sms_user', 'no' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_sms', $booking_id, $status, 'user' );
					$sent = true;
			}

			/**
			 * Let us send email to admin based on admin settings
			 */
			if ( simontaxi_get_option( 'vehicle_booking_confirm_email_admin', 'yes' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_email', $booking_id, $status, 'admin' );
					$sent = true;
			}

			/**
			 * Let us send SMS to admin based on admin settings
			 * Mobile number field is optional for admin users. So lets check whether admin user has updated mobile number or not.
			 * Here the admin refers to the mail admin only.
			 */
			if ( simontaxi_get_option( 'vehicle_booking_confirm_sms_admin', 'no' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_sms', $booking_id, $status, 'admin' );
					$sent = true;
			}
		} elseif ( 'cancelled' === $status ) {
			/**
			 * Let us send email to user based on admin settings
			 */
			if ( simontaxi_get_option( 'vehicle_booking_cancel_email_user', 'yes' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_email', $booking_id, $status, 'user' );
					$sent = true;
			}

			/**
			 * Let us send SMS to user based on admin settings
			 * Mobile number field is optional in admin. Lets check whether it is enabled and user enter it.
			 */
			if ( simontaxi_get_option( 'vehicle_booking_cancel_sms_user', 'no' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_sms', $booking_id, $status, 'user' );
					$sent = true;
			}

			/**
			 * Let us send email to admin based on admin settings
			 */
			if ( simontaxi_get_option( 'vehicle_booking_cancel_email_admin', 'yes' ) == 'yes' ) {					
					do_action( 'simontaxi_send_status_change_email', $booking_id, $status, 'admin' );
					$sent = true;
			}

			/**
			 * Let us send SMS to admin based on admin settings
			 * Mobile number field is optional for admin users. So lets check whether admin user has updated mobile number or not.
			 * Here the admin refers to the mail admin only.
			 */
			if ( simontaxi_get_option( 'vehicle_booking_cancel_sms_user', 'no' ) == 'yes' ) {
					do_action( 'simontaxi_send_status_change_sms', $booking_id, $status, 'admin' );
					$sent = true;
			}
		}
		return $sent;
	}
endif;

if ( ! function_exists( 'simontaxi_log_user_in' ) ) :
	/**
	 * Log User In
	 *
	 * @since 1.0
	 * @param int $user_id User ID.
	 * @param string $user_login Username.
	 * @param string $user_pass Password.
	 * @param boolean $remember Remember me.
	 * @return void
	 */
	function simontaxi_log_user_in( $user_id, $user_login, $user_pass, $remember = false ) {
		if ( $user_id < 1 )
			return;
		wp_set_auth_cookie( $user_id, $remember );
		wp_set_current_user( $user_id, $user_login );
		do_action( 'wp_login', $user_login, get_userdata( $user_id ) );
		do_action( 'simontaxi_log_user_in', $user_id, $user_login, $user_pass );
	}
endif;

if ( ! function_exists( 'simontaxi_terms_text' ) ) :
	/**
	 * This function reurns the terms and conditions text
	 */
	function simontaxi_terms_text() {
		$terms_page_id = simontaxi_get_option( 'terms_page_id', 0);
		$str = apply_filters( 'simontaxi_filter_termstitle', esc_html__( 'I understand and agree with the Terms of Service and Cancellation. ', 'simontaxi' ) );
		if ( $terms_page_id != 0) {
			$permalink = get_permalink( $terms_page_id );
			$link_title = apply_filters( 'simontaxi_filter_termslinktitle', esc_html__( ' Click here to view', 'simontaxi' ) );
			if ( $permalink ) {
				$str = $str . sprintf( '<a href="%s" target="_blank">%s</a>', $permalink, $link_title);
			}
		}
		return $str;
	}
endif;

if ( ! function_exists( 'simontaxi_redirect_login_page' ) ) :
	/**
	 * This function redirect the default wordpress login page
	 */
	function simontaxi_redirect_login_page(){

		// Store for checking if this page equals wp-login.php
		$page_viewed = basename( $_SERVER['REQUEST_URI'] );

		// permalink to the custom login page
		$login_page  = simontaxi_get_bookingsteps_urls( 'login' );

		if ( (( $page_viewed == "wp-login.php" && ! isset( $_POST ) )  || ( isset( $_REQUEST['loggedout'] ) ) )
		){
			wp_redirect( $login_page );
			exit();
		}
	}
endif;

add_action( 'init','simontaxi_redirect_login_page' );

add_action( 'login_form_lostpassword', 'simontaxi_redirect_to_custom_lostpassword' );
if ( ! function_exists( 'simontaxi_redirect_to_custom_lostpassword' ) ) :
	/**
	 * This function redirect the custom lost password page.
	 */
	function simontaxi_redirect_to_custom_lostpassword() {
		if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
			if ( is_user_logged_in() ) {
				wp_redirect( home_url() );
				exit;
			}

			wp_redirect( simontaxi_get_bookingsteps_urls( 'forgotpassword' ) );
			exit;
		}
	}
endif;
add_action( 'login_form_rp', 'simontaxi_redirect_to_custom_password_reset' );
add_action( 'login_form_resetpass', 'simontaxi_redirect_to_custom_password_reset' );

if ( ! function_exists( 'simontaxi_redirect_to_custom_password_reset' ) ) :
	/**
	 * This function redirect the custom reset password page.
	 */
	function simontaxi_redirect_to_custom_password_reset() {
		if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
			// Verify key / login combo
			$user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
			if ( ! $user || is_wp_error( $user ) ) {
				if ( $user && $user->get_error_code() === 'expired_key' ) {
					wp_redirect( home_url( 'member-login?login=expiredkey' ) );
				} else {
					wp_redirect( home_url( 'member-login?login=invalidkey' ) );
				}
				exit;
			}

			$redirect_url = simontaxi_get_bookingsteps_urls( 'resetpassword' );
			$redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
			$redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );

			wp_redirect( $redirect_url );
			exit;
		}
	}
endif;

if ( ! function_exists( 'simontaxi_login_logo' ) ) :
	/**
	 * This function changes the default wordpress logo with new logo.
	 */
	function simontaxi_login_logo(){
	
	$logo = SIMONTAXI_PLUGIN_URL . '/images/logo.png';
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	if ( ! empty( $custom_logo_id ) ) {
		$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
		if ( ! empty( $image ) ) {
			$logo = $image[0];
		}
	}
	?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(<?php echo esc_url( $logo ); ?>);
			padding-bottom: 30px;
			height: auto;
			width: auto;
			background-size: auto;
		}
	</style>
	<?php }
endif;

add_action( 'login_enqueue_scripts', 'simontaxi_login_logo',1 );

if ( ! function_exists( 'simontaxi_social_section_posts' ) ) :
	/**
	 * This function add social sharing buttons to the content.
	 */
	function simontaxi_social_section_posts( $content ) {
		/**
		 * We dont want to add social icons for the vehicle post type
		 */
		if ( is_single() ) {
			ob_start();
				$my_theme = wp_get_theme();
				if ( get_post_type() != 'vehicle' && in_array( $my_theme->get( 'Name' ), array( 'simontaxi' ) ) ) {
					include_once(SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/section-socialshare.php' );
				}
			$social =  ob_get_clean();
			$content = $content . $social;
		}
		return $content;
	}
endif;
add_filter( 'the_content', 'simontaxi_social_section_posts', 10 );

if ( ! function_exists( 'simontaxi_demo_import' ) ) :
	/**
	 * Import demo.
	 */
	function simontaxi_demo_import() {
		include_once(SIMONTAXI_PLUGIN_PATH . '/booking/includes/pages/admin/demo_import.php' );
	}
endif;

if ( ! function_exists( 'simontaxi_get_vehicle_categories' ) ) :
	/**
	 * This function returns vehicle types.
	 */
	function simontaxi_get_vehicle_categories() {
		$types = array();
		$vehicle_types = get_terms( array( 
			'taxonomy' => 'vehicle_types', 
			'hide_empty' => false,
		) );
		if ( ! empty( $vehicle_types ) ) {
			foreach ( $vehicle_types as $type ) {
				$types[ $type->name ] = $type->term_id;
			}
		}
		return $types;
	}
endif;

add_action( 'after_setup_theme', 'simontaxi_remove_admin_bar' );

if ( ! function_exists( 'simontaxi_remove_admin_bar' ) ) :
	/**
	 * Disable Admin Bar for All Users Except for Administrators OR Executives
	 */
	function simontaxi_remove_admin_bar() {
		if ( simontaxi_is_user( 'Customer' ) ) {
		  show_admin_bar( false );
		}
	}
endif;

if ( ! function_exists( 'simontaxi_is_proceed_to_payment' ) ) :
	/**
	 * Let us check the validity of the checkout!!
	 *
	 * @since 2.0.0
	 */
	function simontaxi_is_proceed_to_payment() {
		$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
		$booking_step2 = simontaxi_get_session( 'booking_step2', array() );
		$booking_step3 = simontaxi_get_session( 'booking_step3', array() );
		$booking_step4 = simontaxi_get_session( 'booking_step4', array() );

		return ( ! empty( $booking_step1 ) && ! empty( $booking_step2 ) && ! empty( $booking_step3 ) && ! empty( $booking_step4 ) );
	}

endif;

if ( ! function_exists( 'simontaxi_is_payment_page' ) ) :
	/**
	 * Let us check the validity of the checkout page!!
	 *
	 * @since 2.0.0
	 */
	function simontaxi_is_payment_page() {
		$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
		$booking_step2 = simontaxi_get_session( 'booking_step2', array() );
		$booking_step3 = simontaxi_get_session( 'booking_step3', array() );
		return ( ! empty( $booking_step1 ) && ! empty( $booking_step2 ) && ! empty( $booking_step3 ) );
	}
endif;

if ( ! function_exists( 'simontaxi_booking_details' ) ) :
	/**
	 * Let us check the validity of the checkout!!
	 *
	 * @param string $key - Key to get
	 * @since 2.0.0
	 */
	function simontaxi_booking_details( $key = '' ) {

		$booking_step1 = simontaxi_get_session( 'booking_step1', array() );
		$booking_step2 = simontaxi_get_session( 'booking_step2', array() );
		$booking_step3 = simontaxi_get_session( 'booking_step3', array() );
		$booking_step4 = simontaxi_get_session( 'booking_step4', array() );

		if ( '' === $key ) {
			return array_merge( $booking_step1, $booking_step2, $booking_step3, $booking_step4 );
		} else {
			$string = '-';
			switch ( $key ) {
				case 'journey':
					if ( ! empty( $booking_step1 ) ) {
						$string = $booking_step1['pickup_location'] . esc_html__( ' TO ' ) . $booking_step1['drop_location'];
					}
					break;
				case 'email':
					if ( ! empty( $booking_step3 ) ) {
						$string = isset( $booking_step3['email'] ) ? $booking_step3['email'] : '';
					}
					break;
				case 'payment_id':
					if ( ! empty( $booking_step4 ) ) {
						$string = isset( $booking_step4['payment_id'] ) ? $booking_step4['payment_id'] : 0;
					}
					break;
			}
			return $string;
		}
	}

endif;

add_action( 'wp_ajax_simontaxi_submit_button_step4', 'simontaxi_submit_button_step4' );
add_action( 'wp_ajax_nopriv_simontaxi_submit_button_step4', 'simontaxi_submit_button_step4' );
if ( ! function_exists( 'simontaxi_submit_button_step4' ) ) :

	add_action( 'simontaxi_submit_button_step4', 'simontaxi_submit_button_step4' );
	/**
	 * Filter for Step4 Submit Button
	 *
	 * @since 2.0.0
	 */
	function simontaxi_submit_button_step4() {
		ob_start();
		$paymentmethod = isset( $_POST['paymentmethod'] ) ? sanitize_text_field( wp_unslash( $_POST['paymentmethod'] ) ) : 'byhand';
		if ( in_array( $paymentmethod, array( 'paypal', 'payu', 'byhand', 'banktransfer' ), true ) ) :
			?>
			<div class="st-terms-block">
				<a href="<?php echo simontaxi_get_bookingsteps_urls( 'step3' ); ?>" class="btn-dull"><i class="fa fa-angle-double-left"></i> <?php esc_html_e( 'Back', 'simontaxi' ); ?> </a>
				<button type="submit" class="btn btn-primary btn-mobile" name="validtestep4"><?php echo apply_filters( 'simontaxi_filter_booknow_title', esc_html__( 'Book Now', 'simontaxi' ) ); ?></button>
			</div>
			<?php
			$content = ob_get_contents();
			ob_get_clean();
			echo apply_filters( 'simontaxi_filter_submit_button_step4', $content );
		else :
			do_action( 'simontaxi_submit_button_step4_' . $paymentmethod );
		endif;
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			die();
		}
	}
endif;

if ( ! function_exists( 'simontaxi_available_capabilities' ) ) :
	/**
	 * This function return the capabiities
	 *
	 * @since 2.0.0
	 */
	function simontaxi_available_capabilities() {
		$caps = array(
			'manage_bookings' => esc_html__( 'Manage Bookings', 'simontaxi' ),
			'manage_vehicles' => simontaxi_get_default_title_plural(),
			'manage_features' => esc_html__( 'Features', 'simontaxi' ),
			'manage_types' => esc_html__( 'Types', 'simontaxi' ),
			'manage_locations' => esc_html__( 'Locations', 'simontaxi' ),
			'manage_hourly_packages' => esc_html__( 'Hourly Packages', 'simontaxi' ),
			'manage_layoutdates' => esc_html__( 'Lay out Dates', 'simontaxi' ),
			'manage_special_fare' => esc_html__( 'Special Fare', 'simontaxi' ),
			'manage_distance_prices' => esc_html__( 'Distance Prices', 'simontaxi' ),
			'manage_coupon_codes' => esc_html__( 'Coupon Codes', 'simontaxi' ),
			'manage_faq' => esc_html__( 'All Vehicle FAQ', 'simontaxi' ),
			'manage_email_templates' => esc_html__( 'Email Templates', 'simontaxi' ),
			'manage_sms_templates' => esc_html__( 'SMS Templates', 'simontaxi' ),
			'manage_testimonials' => esc_html__( 'All Testimonials', 'simontaxi' ),
			'manage_settings' => esc_html__( 'Settings', 'simontaxi' ),
			'manage_callbacks' => esc_html__( 'Request Callbacks', 'simontaxi' ),
			'manage_support_request' => esc_html__( 'Support Request', 'simontaxi' ),
		);
		return apply_filters( 'simontaxi_available_capabilities', $caps );
	}
endif;

if ( ! function_exists( 'simontaxi_available_roles' ) ) :
	/**
	 * This function return the capabiities
	 *
	 * @since 2.0.0
	 */
	function simontaxi_available_roles() {
		$roles = array(
			'executive' => esc_html__( 'Executive', 'simontaxi' ),
		);
		return apply_filters( 'simontaxi_available_roles', $roles );
	}
endif;

if ( ! function_exists( 'get_capabilities' ) ) :
	/**
	 * Function to get Capabilities
	 *
	 * @param String $capability_type - Type.
	 * @since 2.0.0
	 */
	function get_capabilities( $capability_type ) {
		return array(
			// Meta capabilities.
			'edit_post' => "edit_{$capability_type}",
			'read_post' => "read_{$capability_type}",
			'delete_post' => "delete_{$capability_type}",

			// Primitive capabilities used outside of map_meta_cap().
			'edit_posts' => "edit_{$capability_type}s",
			'edit_others_posts' => "edit_other_{$capability_type}s",
			'publish_posts' => "publish_{$capability_type}s",
			'read_private_posts' => "read_private_{$capability_type}",

			// Primitive capabilities used within map_meta_cap().
			'read' => 'read',
			'delete_posts' => "delete_{$capability_type}s",
			'delete_private_posts' => "delete_private_{$capability_type}s",
			'delete_published_posts' => "delete_published_{$capability_type}s",
			'delete_others_posts' => "delete_others_{$capability_type}s",
			'edit_private_posts' => "edit_private_{$capability_type}s",
			'edit_published_posts' => "edit_published_{$capability_type}s",
		);
	}
endif;

if ( ! function_exists( 'simontaxi_is_sms_gateway_active' ) ) :
	/**
	 * Function to check the WP_SMS plugin activation
	 *
	 * @since 2.0.0
	 */
	function simontaxi_is_sms_gateway_active() {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		return ( is_plugin_active( 'wp-sms/wp-sms.php' ) || is_plugin_active( 'wp-sms-pro/wp-sms.php' ) ) ? true : false;
	}
endif;

if ( ! function_exists( 'simontaxi_special_capabilities' ) ) :
	/**
	 * This function return the capabiities
	 *
	 * @since 2.0.0
	 */
	function simontaxi_special_capabilities() {
		$caps = array(
			'manage_bookings' => esc_html__( 'Manage Bookings', 'simontaxi' ),
			'manage_callbacks' => esc_html__( 'Request Callbacks', 'simontaxi' ),
			'manage_support_request' => esc_html__( 'Support Request', 'simontaxi' ),
			'manage_settings' => esc_html__( 'Settings', 'simontaxi' ),
			'manage_extensions' => esc_html__( 'Manage Extensions', 'simontaxi' ),
			'get_extension' => esc_html__( 'Get Extensions', 'simontaxi' ),			
		);
		return apply_filters( 'simontaxi_special_capabilities', $caps );
	}
endif;

if ( ! function_exists( 'simontaxi_get_primary_admin_mobile' ) ) :
	/**
	 * This function return the mobile number of primary admin and we assume the primary admin ID as '1'
	 *
	 * @since 2.0.2
	 */
	function simontaxi_get_primary_admin_mobile() {
		$mobile = get_user_meta( 1, 'mobile', true ); //Assuming admin ID is '1'
		$mobile_countrycode = get_user_meta( 1, 'mobile_countrycode', true ); //Assuming admin ID is '1'

		if ( '' !== $mobile && '' !== $mobile_countrycode ) {
			$mobile_countrycode = explode( '_', $mobile_countrycode);
			/**
			   * @since 2.0.2
			   * Change Description: 
			   * PHP 5.3 doesn't support the [] array syntax. Only PHP 5.4 and later does. For older PHP, you need to use array() instead of [].
			   */
			  if ( ! empty( $mobile_countrycode ) ) {
				 $mobile_countrycode = $mobile_countrycode[0]; 
			  }
			$mobile = $mobile_countrycode. $mobile;
		} else {
			$mobile = '';
		}
		return $mobile;
	}
endif;

if ( ! function_exists( 'simontaxi_get_fixed_point_title' ) ) :
	/**
	 * This function return the mobile number of primary admin and we assume the primary admin ID as '1'
	 *
	 * @since 2.0.2
	 */
	function simontaxi_get_fixed_point_title() {
		return simontaxi_get_option( 'fixed_point_title', 'Airport' );
	}
endif;

if ( ! function_exists( 'simontaxi_validate_envato' ) ) :
	/**
	 * This function return the boolean based on envato result
	 *
	 * @since 2.0.0
	 */
	function simontaxi_validate_envato( $product_code ) {
		
		$url = 'https://api.envato.com/v3/market/author/sale?code=' . $product_code;
		$curl = curl_init( $url );
		$personal_token = '3GteFmA41PkDQEEaao8Bq78mLMzUPQMF';
		$header = array();
		$header[] = 'Authorization: Bearer ' . $personal_token;
		$header[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:41.0) Gecko/20100101 Firefox/41.0';
		$header[] = 'timeout: 20';
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header );
		$envatoRes = curl_exec( $curl );
		curl_close( $curl );
		$envatoRes = json_decode( $envatoRes );
		
		$valid = false;
		if ( isset( $envatoRes->item->name ) && 'SimonTaxi - Taxi Booking WordPress Theme' === $envatoRes->item->name ) {
			$valid = true;
		}
		return $valid;
	}
endif;

if ( ! function_exists( 'dd' ) ) :
	/**
	 * This function prints the mixed value
	 *
	 * @param $mixed Mixed - Value to print
	 * @since 2.0.6
	 */
	function dd( $mixed = '' )
	{
		echo '<pre>';
		if ( ! empty( $mixed ) ) {			
			print_r( $mixed );
		} else {
			print_r( $_POST );
		}
		die();
	}
endif;

/**
 * Retrieves a template part
 *
 * @since v1.2
 *
 * Taken from EDD
 *
 * @param string $slug
 * @param string $name Optional. Default null
 * @param bool   $load
 *
 * @return string
 *
 * @uses simontaxi_locate_template()
 * @uses load_template()
 * @uses get_template_part()
 */
function simontaxi_get_template_part( $slug, $name = null, $load = true ) {
	// Execute code for this part
	do_action( 'get_template_part_' . $slug, $slug, $name );

	$load_template = apply_filters( 'simontaxi_allow_template_part_' . $slug . '_' . $name, true );
	if ( false === $load_template ) {
		return '';
	}

	// Setup possible parts
	$templates = array();
	if ( isset( $name ) )
		$templates[] = $slug . '-' . $name . '.php';
	$templates[] = $slug . '.php';

	// Allow template parts to be filtered
	$templates = apply_filters( 'simontaxi_get_template_part', $templates, $slug, $name );

	// Return the part that is found
	return simontaxi_locate_template( $templates, $load, false );
}

/**
 * Retrieve the name of the highest priority template file that exists.
 *
 * Searches in the STYLESHEETPATH before TEMPLATEPATH so that themes which
 * inherit from a parent theme can just overload one file. If the template is
 * not found in either of those, it looks in the theme-compat folder last.
 *
 * Taken from bbPress
 *
 * @since 1.2
 *
 * @param string|array $template_names Template file(s) to search for, in order.
 * @param bool $load If true the template file will be loaded if it is found.
 * @param bool $require_once Whether to require_once or require. Default true.
 *   Has no effect if $load is false.
 * @return string The template filename if one is located.
 */
function simontaxi_locate_template( $template_names, $load = false, $require_once = true ) {
	// No file found yet
	$located = false;

	// Try to find a template file
	foreach ( (array) $template_names as $template_name ) {

		// Continue if template is empty
		if ( empty( $template_name ) )
			continue;

		// Trim off any slashes from the template name
		$template_name = ltrim( $template_name, '/' );

		// try locating this template file by looping through the template paths
		foreach( simontaxi_get_theme_template_paths() as $template_path ) {

			if( file_exists( $template_path . $template_name ) ) {
				$located = $template_path . $template_name;
				break;
			}
		}

		if( $located ) {
			break;
		}
	}

	if ( ( true == $load ) && ! empty( $located ) )
		load_template( $located, $require_once );

	return $located;
}

/**
 * Returns a list of paths to check for template locations
 *
 * @since 1.8.5
 * @return mixed|void
 */
function simontaxi_get_theme_template_paths() {

	$template_dir = simontaxi_get_theme_template_dir_name();

	$file_paths = array(
		1 => trailingslashit( get_stylesheet_directory() ) . $template_dir,
		10 => trailingslashit( get_template_directory() ) . $template_dir,
		100 => simontaxi_get_templates_dir()
	);

	$file_paths = apply_filters( 'simontaxi_template_paths', $file_paths );

	// sort the file paths based on priority
	ksort( $file_paths, SORT_NUMERIC );

	return array_map( 'trailingslashit', $file_paths );
}

/**
 * Returns the template directory name.
 *
 * Themes can filter this by using the simontaxi_templates_dir filter.
 *
 * @since 1.6.2
 * @return string
*/
function simontaxi_get_theme_template_dir_name() {
	return trailingslashit( apply_filters( 'simontaxi_templates_dir', 'simontaxi_templates' ) );
}

/**
 * Returns the path to the EDD templates directory
 *
 * @since 1.2
 * @return string
 */
function simontaxi_get_templates_dir() {
	return SIMONTAXI_PLUGIN_PATH . 'templates';
}

/**
 * Sends the email registration alert to admin and user
 *
 * @since 2.0.7
 * @return void
*/
function simontaxi_registration_email_alert( $user_id, $plaintext_pass ) {
    $user    = get_userdata( $user_id );
	$user_meta = simontaxi_filter_gk( ( array ) get_user_meta( $user_id ) );
	

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
    $message .= sprintf(__('E-mail: %s'), $user->user_email) . "\r\n";

    @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

    if ( empty($plaintext_pass) )
        return;

    $message  = sprintf(__('Username: %s'), $user->user_login) . "\r\n";
    $message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n";
    $message .= wp_login_url() . "\r\n";

    wp_mail($user->user_email, sprintf(__('[%s] Your username and password'), $blogname), $message);
}
