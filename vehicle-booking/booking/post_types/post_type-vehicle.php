<?php
/**
 * Register Custom post type (CPT) - vehicle
 *
 * @package     Simontaxi - Vehicle Booking
 * @subpackage  CPT
 * @copyright   Copyright (c) 2017, Digisamaritan
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function simontaxi_vehicle_post_type() {
    $vehicle_labels = apply_filters( 'simontaxi_vehicle_labels', array(
		'name'                  => _x( '%2$s', 'vehicle post type name', 'simontaxi' ),
		'singular_name'         => _x( '%1$s', 'singular vehicle post type name', 'simontaxi' ),
		'add_new'               => esc_html__( 'Add New', 'simontaxi' ),
		'add_new_item'          => esc_html__( 'Add New %1$s', 'simontaxi' ),
		'edit_item'             => esc_html__( 'Edit %1$s', 'simontaxi' ),
		'new_item'              => esc_html__( 'New %1$s', 'simontaxi' ),
		'all_items'             => esc_html__( 'All %2$s', 'simontaxi' ),
		'view_item'             => esc_html__( 'View %1$s', 'simontaxi' ),
		'search_items'          => esc_html__( 'Search %2$s', 'simontaxi' ),
		'not_found'             => esc_html__( 'No %2$s found', 'simontaxi' ),
		'not_found_in_trash'    => esc_html__( 'No %2$s found in Trash', 'simontaxi' ),
		'parent_item_colon'     => '',
		'menu_name'             => _x( '%2$s', 'vehicle post type menu name', 'simontaxi' ),
		'featured_image'        => esc_html__( '%1$s Image', 'simontaxi' ),
		'set_featured_image'    => esc_html__( 'Set %1$s Image', 'simontaxi' ),
		'remove_featured_image' => esc_html__( 'Remove %1$s Image', 'simontaxi' ),
		'use_featured_image'    => esc_html__( 'Use as %1$s Image', 'simontaxi' ),
		'filter_items_list'     => esc_html__( 'Filter %2$s list', 'simontaxi' ),
		'items_list_navigation' => esc_html__( '%2$s list navigation', 'simontaxi' ),
		'items_list'            => esc_html__( '%2$s list', 'simontaxi' ),
	) );
	foreach ( $vehicle_labels as $key => $value ) {
		$vehicle_labels[ $key ] = sprintf( $value, simontaxi_get_label_singular(), simontaxi_get_label_plural() );
	}
	$vehicle_args = array(
		'labels'             => $vehicle_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => SIMONTAXI_SLUG , 'with_front' => false),
		'capability_type'    => 'post',
		'capabilities' => get_capabilities( 'manage_vehicles' ),
		'map_meta_cap'       => true,
		'has_archive'        => false,
		'hierarchical'       => false,
		'supports'           => apply_filters( 'simontaxi_vehicle_supports', array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions' ) ),
		'menu_position' => 5,
        'menu_icon'           => SIMONTAXI_PLUGIN_URL . '/images/logo-16x16.png',
		'register_meta_box_cb' => 'add_vehicle_metaboxes',
		'taxonomies' => array( 'vehicle_features' ),
	);

	register_post_type( 'vehicle', apply_filters( 'simontaxi_vehicle_post_type_args', $vehicle_args ) );
}

function add_vehicle_metaboxes() {
	 $fixed_point_title = simontaxi_get_option( 'fixed_point_title', 'Airport' );
	 
	 add_meta_box( 'vehicle_p2p_settings', 'P2P Settings', 'p2p_settings', 'vehicle', 'normal', 'high' );
	 add_meta_box( 'vehicle_airport_settings', $fixed_point_title . ' Settings', 'airport_settings', 'vehicle', 'normal', 'high' );
	 add_meta_box( 'vehicle_feature_settings', 'Features', 'feature_settings', 'vehicle', 'normal', 'high' );
	 add_meta_box( 'vehicle_gallery', 'Gallery', 'vehicle_gallery', 'vehicle', 'normal', 'high' );
	 
	 /**
	  * To add metaboxes through extensions
	  *
	  * @since 2.0.0
	  */
	  do_action( 'simontaxi_vehicle_other_metaboxes' );
	  
}

function vehicle_gallery() {
	global $post;

	// noncename needed to verify where the data originated
    echo '<input type="hidden" name="vehiclemeta_noncename" id="vehiclemeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	?>
	<div id="vehicle_images_container">
		<ul class="vehicle_images">
			<?php
				if ( metadata_exists( 'post', $post->ID, 'vehicle_image_gallery' ) ) {
					$vehicle_image_gallery = get_post_meta( $post->ID, 'vehicle_image_gallery', true );
				} else {
					// Backwards compat
					$attachment_ids = get_posts( 'post_parent=' . $post->ID . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_vehicle_exclude_image&meta_value=0' );
					$attachment_ids = array_diff( $attachment_ids, array( get_post_thumbnail_id() ) );
					$vehicle_image_gallery = implode( ',', $attachment_ids );
				}

				$attachments         = array_filter( explode( ',', $vehicle_image_gallery ) );
				$update_meta         = false;
				$updated_gallery_ids = array();

				if ( ! empty( $attachments ) ) {
					foreach ( $attachments as $attachment_id ) {
						$attachment = wp_get_attachment_image( $attachment_id, 'thumbnail' );

						// if attachment is empty skip
						if ( empty( $attachment ) ) {
							$update_meta = true;
							continue;
						}

						echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
							' . $attachment . '
							<ul class="actions">
								<li><a href="#" class="delete tips" data-tip="' . esc_attr__( 'Delete image', 'simontaxi' ) . '">' . esc_html__( 'Delete', 'simontaxi' ) . '</a></li>
							</ul>
						</li>';

						// rebuild ids to be saved
						$updated_gallery_ids[] = $attachment_id;
					}

					// need to update product meta to set new gallery ids
					if ( $update_meta ) {
						update_post_meta( $post->ID, 'vehicle_image_gallery', implode( ',', $updated_gallery_ids ) );
					}
				}
			?>
		</ul>

		<input type="hidden" id="vehicle_image_gallery" name="vehicle_image_gallery" value="<?php echo esc_attr( $vehicle_image_gallery ); ?>" />

	</div>
	<p class="add_vehicle_images hide-if-no-js">
		<a href="#" data-choose="<?php esc_attr_e( 'Add Images to Vehicle Gallery', 'simontaxi' ); ?>" data-update="<?php esc_attr_e( 'Add to gallery', 'simontaxi' ); ?>" data-delete="<?php esc_attr_e( 'Delete image', 'simontaxi' ); ?>" data-text="<?php esc_attr_e( 'Delete', 'simontaxi' ); ?>"><?php esc_html_e( 'Add vehicle gallery images', 'simontaxi' ); ?></a>
	</p>
	<script type="text/javascript">

jQuery( function( $ ) {
		// Product gallery file uploads.
		var vehicle_gallery_frame;
		var $image_gallery_ids = $( '#vehicle_image_gallery' );
		var $vehicle_images    = $( '#vehicle_images_container' ).find( 'ul.vehicle_images' );
		$( '.add_vehicle_images' ).on( 'click', 'a', function( event ) {
			var $el = $( this );

			event.preventDefault();

			// If the media frame already exists, reopen it.
			if ( vehicle_gallery_frame ) {
				vehicle_gallery_frame.open();
				return;
			}

			// Create the media frame.
			vehicle_gallery_frame = wp.media.frames.product_gallery = wp.media({
				// Set the title of the modal.
				title: $el.data( 'choose' ),
				button: {
					text: $el.data( 'update' )
				},
				states: [
					new wp.media.controller.Library({
						title: $el.data( 'choose' ),
						filterable: 'all',
						multiple: true
					})
				]
			});

			// When an image is selected, run a callback.
			vehicle_gallery_frame.on( 'select', function() {
				var selection = vehicle_gallery_frame.state().get( 'selection' );
				var attachment_ids = $image_gallery_ids.val();

				selection.map( function( attachment ) {
					attachment = attachment.toJSON();

					if ( attachment.id ) {
						attachment_ids   = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
						var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

						$vehicle_images.append( '<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" /><ul class="actions"><li><a href="#" class="delete" title="' + $el.data( 'delete' ) + '">' + $el.data( 'text' ) + '</a></li></ul></li>' );
					}
				});

				$image_gallery_ids.val( attachment_ids );
			});

			// Finally, open the modal.
			vehicle_gallery_frame.open();
		});

		// Image ordering.
		$vehicle_images.sortable({
			items: 'li.image',
			cursor: 'move',
			scrollSensitivity: 40,
			forcePlaceholderSize: true,
			forceHelperSize: false,
			helper: 'clone',
			opacity: 0.65,
			placeholder: 'wc-metabox-sortable-placeholder',
			start: function( event, ui ) {
				ui.item.css( 'background-color', '#f6f6f6' );
			},
			stop: function( event, ui ) {
				ui.item.removeAttr( 'style' );
			},
			update: function() {
				var attachment_ids = '';

				$( '#vehicle_images_container' ).find( 'ul li.image' ).css( 'cursor', 'default' ).each( function() {
					var attachment_id = $( this ).attr( 'data-attachment_id' );
					attachment_ids = attachment_ids + attachment_id + ',';
				});

				$image_gallery_ids.val( attachment_ids );
			}
		});

		// Remove images.
		$( '#vehicle_images_container' ).on( 'click', 'a.delete', function() {
			$( this ).closest( 'li.image' ).remove();

			var attachment_ids = '';

			$( '#vehicle_images_container' ).find( 'ul li.image' ).css( 'cursor', 'default' ).each( function() {
				var attachment_id = $( this ).attr( 'data-attachment_id' );
				attachment_ids = attachment_ids + attachment_id + ',';
			});

			$image_gallery_ids.val( attachment_ids );

			// Remove any lingering tooltips.
			$( '#tiptip_holder' ).removeAttr( 'style' );
			$( '#tiptip_arrow' ).removeAttr( 'style' );

			return false;
		});
});
	</script>
	<?php
}
function p2p_settings() {
	global $post;
	$p2p_basic_distance = get_post_meta( $post->ID, 'p2p_basic_distance', true );
	$p2p_basic_price = get_post_meta( $post->ID, 'p2p_basic_price', true );
	$p2p_unit_price = get_post_meta( $post->ID, 'p2p_unit_price', true );
	// noncename needed to verify where the data originated

	echo '<input type="hidden" name="vehiclemeta_noncename" id="vehiclemeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
?>
<table class="st-table-vehicles">
	<tr>
		<td>
			<div class="form-field">
				 <label for="p2p_basic_distance"><?php esc_html_e( 'Basic Distance', 'simontaxi' ); ?></label>
				 <input type="text"  name="p2p_basic_distance" id="p2p_basic_distance" value="<?php echo esc_attr( $p2p_basic_distance );?>" placeholder="<?php esc_html_e( 'Basic Distance', 'simontaxi' ); ?>"/>
			</div>

		</td>
		<td>

			 <div class="form-field">
				 <label for="p2p_basic_price"><?php esc_html_e( 'Basic Price', 'simontaxi' ); ?></label>
				 <input type="text" name="p2p_basic_price" id="p2p_basic_price" value="<?php echo esc_attr( $p2p_basic_price );?>" placeholder="<?php esc_html_e( 'Basic Price', 'simontaxi' ); ?>"/>
			</div>

		</td>

		<td>
			<div class="form-field">
				 <label for="p2p_unit_price"><?php esc_html_e( 'Price per Standard Unit Distance', 'simontaxi' ); ?></label>
				 <input type="text" name="p2p_unit_price" id="p2p_unit_price" value="<?php echo esc_attr( $p2p_unit_price );?>" placeholder="<?php esc_html_e( 'Price per Standard Unit Distance', 'simontaxi' ); ?>"/>
			</div>

		</td>

	</tr>
</table>
<?php
}

function airport_settings()
{
	global $post;
	$to_airport_basic_distance = get_post_meta( $post->ID, 'to_airport_basic_distance', true );
	$to_airport_basic_price = get_post_meta( $post->ID, 'to_airport_basic_price', true );
	$to_airport_unit_price = get_post_meta( $post->ID, 'to_airport_unit_price', true );

	$from_airport_basic_distance = get_post_meta( $post->ID, 'from_airport_basic_distance', true );
	$from_airport_basic_price = get_post_meta( $post->ID, 'from_airport_basic_price', true );
	$from_airport_unit_price = get_post_meta( $post->ID, 'from_airport_unit_price', true );
	// noncename needed to verify where the data originated
    echo '<input type="hidden" name="vehiclemeta_noncename" id="vehiclemeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
		
	$fixed_point_title = simontaxi_get_option( 'fixed_point_title', 'Airport' );
?>
<table class="st-table-vehicles">
	<tr><td colspan="2"> <b><?php esc_html_e( 'Traveling To ' .  $fixed_point_title, 'simontaxi' );?></b> </td></tr>
	<tr>
		<td>
			<div class="form-field">
				 <label for="to_airport_basic_distance"><?php esc_html_e( 'Basic Distance', 'simontaxi' ); ?></label>
				 <input type="text" name="to_airport_basic_distance" id="to_airport_basic_distance" value="<?php echo esc_attr( $to_airport_basic_distance );?>" placeholder="<?php esc_html_e( 'Basic Distance', 'simontaxi' ); ?>"/>
			</div>

		</td>
		<td>

			 <div class="form-field">
				 <label for="to_airport_basic_price"><?php esc_html_e( 'Basic Price', 'simontaxi' ); ?></label>
				 <input type="text" name="to_airport_basic_price" id="to_airport_basic_price" value="<?php echo esc_attr( $to_airport_basic_price );?>" placeholder="<?php esc_html_e( 'Basic Price', 'simontaxi' ); ?>"/>
			</div>

		</td>

		<td>
			<div class="form-field">
				 <label for="to_airport_unit_price"><?php esc_html_e( 'Price per Standard Unit Distance', 'simontaxi' ); ?></label>
				 <input type="text" name="to_airport_unit_price" id="to_airport_unit_price" value="<?php echo esc_attr( $to_airport_unit_price );?>" placeholder="<?php esc_html_e( 'Price per Standard Unit Distance', 'simontaxi' ); ?>"/>
			</div>

		</td>

	</tr>

	<tr><td colspan="2"> <b><?php esc_html_e( 'Traveling From ' . $fixed_point_title, 'simontaxi' );?></b> </td></tr>
	<tr>
		<td>
			<div class="form-field">
				 <label for="from_airport_basic_distance"><?php esc_html_e( 'Basic Distance', 'simontaxi' ); ?></label>
				 <input type="text" name="from_airport_basic_distance" id="from_airport_basic_distance" value="<?php echo esc_attr( $from_airport_basic_distance );?>" placeholder="<?php esc_html_e( 'Basic Distance', 'simontaxi' ); ?>"/>
			</div>

		</td>
		<td>

			 <div class="form-field">
				 <label for="from_airport_basic_price"><?php esc_html_e( 'Basic Price', 'simontaxi' ); ?></label>
				 <input type="text" name="from_airport_basic_price" id="from_airport_basic_price" value="<?php echo esc_attr( $from_airport_basic_price );?>" placeholder="<?php esc_html_e( 'Basic Price', 'simontaxi' ); ?>"/>
			</div>

		</td>

		<td>
			<div class="form-field">
				 <label for="from_airport_unit_price"><?php esc_html_e( 'Price per Standard Unit Distance', 'simontaxi' ); ?></label>
				 <input type="text" name="from_airport_unit_price" id="from_airport_unit_price" value="<?php echo esc_attr( $from_airport_unit_price );?>" placeholder="<?php esc_html_e( 'Price per Standard Unit Distance', 'simontaxi' ); ?>"/>
			</div>

		</td>

	</tr>

</table>
<?php
}

function feature_settings()
{
	global $post;
	$number_of_vehicles = get_post_meta( $post->ID, 'number_of_vehicles', true );
	$seating_capacity = get_post_meta( $post->ID, 'seating_capacity', true );
	$luggage = get_post_meta( $post->ID, 'luggage', true );
	$luggage_type = get_post_meta( $post->ID, 'luggage_type', true );
	$luggage_type_symbol = get_post_meta( $post->ID, 'luggage_type_symbol', true );
	// noncename needed to verify where the data originated
    echo '<input type="hidden" name="vehiclemeta_noncename" id="vehiclemeta_noncename" value="' .
        wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
?>
<table class="st-table-vehicles">
	<tr>
		<td>
			<div class="form-field">
				 <label for="number_of_vehicles"><?php esc_html_e( 'Number of Vehicles', 'simontaxi' ); ?></label>
				 <input type="number" min="1" name="number_of_vehicles" id="number_of_vehicles" value="<?php echo esc_attr( $number_of_vehicles );?>" placeholder="<?php esc_html_e( 'Number of Vehicles', 'simontaxi' ); ?>"/>
			</div>

		</td>
		<td>
			<div class="form-field">
				 <label for="seating_capacity"><?php esc_html_e( 'Seating Capacity', 'simontaxi' ); ?></label>
				 <input type="number" min="1" name="seating_capacity" id="seating_capacity" value="<?php echo esc_attr( $seating_capacity );?>" placeholder="<?php esc_html_e( 'Seating Capacity', 'simontaxi' ); ?>"/>
			</div>
		</td>
	</tr>
	
	<tr>
		<td>

			 <div class="form-field">
				 <label for="luggage"><?php esc_html_e( 'Luggage', 'simontaxi' ); ?></label>
				 <input type="number" name="luggage" id="luggage" value="<?php echo esc_attr( $luggage );?>" placeholder="<?php esc_html_e( 'Luggage', 'simontaxi' ); ?>" class="st-input-number"/>&nbsp;
				 <select name="luggage_type" id="luggage_type">
					<optgroup label="General">
						<option value="Small" <?php if( $luggage_type == 'Small' ) echo 'selected'?>><?php esc_html_e( 'Small', 'simontaxi' );?></option>
						<option value="Medium" <?php if( $luggage_type == 'Medium' ) echo 'selected'?>><?php esc_html_e( 'Medium', 'simontaxi' );?></option>
						<option value="Large" <?php if( $luggage_type == 'Large' ) echo 'selected'?>><?php esc_html_e( 'Large', 'simontaxi' );?></option>
					</optgroup>

					<optgroup label="Weights">
						<option value="Kilogram" <?php if( $luggage_type == 'Kilogram' ) echo 'selected'?>><?php esc_html_e( 'Kilogram', 'simontaxi' );?></option>
						<option value="Gram" <?php if( $luggage_type == 'Gram' ) echo 'selected'?>><?php esc_html_e( 'Gram', 'simontaxi' );?></option>
						<option value="Milligram" <?php if( $luggage_type == 'Milligram' ) echo 'selected'?>><?php esc_html_e( 'Milligram', 'simontaxi' );?></option>
						<option value="Microgram" <?php if( $luggage_type == 'Microgram' ) echo 'selected'?>><?php esc_html_e( 'Microgram', 'simontaxi' );?></option>
						<option value="Imperial ton" <?php if( $luggage_type == 'Imperial ton' ) echo 'selected'?>><?php esc_html_e( 'Imperial ton', 'simontaxi' );?></option>
						<option value="US ton" <?php if( $luggage_type == 'US ton' ) echo 'selected'?>><?php esc_html_e( 'US ton', 'simontaxi' );?></option>
						<option value="Stone" <?php if( $luggage_type == 'Stone' ) echo 'selected'?>><?php esc_html_e( 'Stone', 'simontaxi' );?></option>
						<option value="Pound" <?php if( $luggage_type == 'Pound' ) echo 'selected'?>><?php esc_html_e( 'Pound', 'simontaxi' );?></option>
						<option value="Ounce" <?php if( $luggage_type == 'Ounce' ) echo 'selected'?>><?php esc_html_e( 'Ounce', 'simontaxi' );?></option>
						<option value="Tonne" <?php if( $luggage_type == 'Tonne' ) echo 'selected'?>><?php esc_html_e( 'Tonne', 'simontaxi' );?></option>
					</optgroup>
				 </select>&nbsp;
				 <input type="text" name="luggage_type_symbol" id="luggage_type_symbol" value="<?php echo esc_attr( $luggage_type_symbol );?>" placeholder="<?php esc_html_e( 'Luggage Type Symbol. Eg: KG', 'simontaxi' ); ?>" class="st-input-luggage_type_symbol"/>
			</div>
		</td>
		
		<td>
			<?php
			$luggage2 = get_post_meta( $post->ID, 'luggage2', true );
			$luggage2_type = get_post_meta( $post->ID, 'luggage2_type', true );
			$luggage2_type_symbol = get_post_meta( $post->ID, 'luggage2_type_symbol', true );
			?>
			 <div class="form-field">
				 <label for="luggage2"><?php esc_html_e( 'Luggage-2', 'simontaxi' ); ?></label>
				 <input type="number" name="luggage2" id="luggage2" value="<?php echo esc_attr( $luggage2 );?>" placeholder="<?php esc_html_e( 'Luggage-2', 'simontaxi' ); ?>" class="st-input-number"/>&nbsp;
				 <select name="luggage2_type" id="luggage2_type">
					<optgroup label="General">
						<option value="Small" <?php if( $luggage2_type == 'Small' ) echo 'selected'?>><?php esc_html_e( 'Small', 'simontaxi' );?></option>
						<option value="Medium" <?php if( $luggage2_type == 'Medium' ) echo 'selected'?>><?php esc_html_e( 'Medium', 'simontaxi' );?></option>
						<option value="Large" <?php if( $luggage2_type == 'Large' ) echo 'selected'?>><?php esc_html_e( 'Large', 'simontaxi' );?></option>
					</optgroup>

					<optgroup label="Weights">
						<option value="Kilogram" <?php if( $luggage2_type == 'Kilogram' ) echo 'selected'?>><?php esc_html_e( 'Kilogram', 'simontaxi' );?></option>
						<option value="Gram" <?php if( $luggage2_type == 'Gram' ) echo 'selected'?>><?php esc_html_e( 'Gram', 'simontaxi' );?></option>
						<option value="Milligram" <?php if( $luggage2_type == 'Milligram' ) echo 'selected'?>><?php esc_html_e( 'Milligram', 'simontaxi' );?></option>
						<option value="Microgram" <?php if( $luggage2_type == 'Microgram' ) echo 'selected'?>><?php esc_html_e( 'Microgram', 'simontaxi' );?></option>
						<option value="Imperial ton" <?php if( $luggage2_type == 'Imperial ton' ) echo 'selected'?>><?php esc_html_e( 'Imperial ton', 'simontaxi' );?></option>
						<option value="US ton" <?php if( $luggage2_type == 'US ton' ) echo 'selected'?>><?php esc_html_e( 'US ton', 'simontaxi' );?></option>
						<option value="Stone" <?php if( $luggage2_type == 'Stone' ) echo 'selected'?>><?php esc_html_e( 'Stone', 'simontaxi' );?></option>
						<option value="Pound" <?php if( $luggage2_type == 'Pound' ) echo 'selected'?>><?php esc_html_e( 'Pound', 'simontaxi' );?></option>
						<option value="Ounce" <?php if( $luggage2_type == 'Ounce' ) echo 'selected'?>><?php esc_html_e( 'Ounce', 'simontaxi' );?></option>
						<option value="Tonne" <?php if( $luggage2_type == 'Tonne' ) echo 'selected'?>><?php esc_html_e( 'Tonne', 'simontaxi' );?></option>
					</optgroup>
				 </select>&nbsp;
				 <input type="text" name="luggage2_type_symbol" id="luggage2_type_symbol" value="<?php echo esc_attr( $luggage2_type_symbol );?>" placeholder="<?php esc_html_e( 'Luggage-2 Type Symbol. Eg: KG', 'simontaxi' ); ?>" class="st-input-luggage_type_symbol"/>
			</div>
		</td>
	</tr>
</table>
<?php
}

// Save the Metabox Data
function simontaxi_save_vehicle_meta( $post_id, $post ) {
	// verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( isset( $_POST['vehiclemeta_noncename'] ) ) {

		if ( ! wp_verify_nonce( $_POST['vehiclemeta_noncename'], plugin_basename( __FILE__ ) ) ) {
			return $post->ID;
        }

        // Is the user allowed to edit the post or page?
        if ( ! current_user_can( 'edit_post', $post->ID ) ) {
			return $post->ID;
		}
        // OK, we're authenticated: we need to find and save the data
        // We'll put it into an array to make it easier to loop though.


        $vehicle_meta['p2p_basic_distance'] = $_POST['p2p_basic_distance'];
		$vehicle_meta['p2p_basic_price'] = $_POST['p2p_basic_price'];
		$vehicle_meta['p2p_unit_price'] = $_POST['p2p_unit_price'];

		$vehicle_meta['to_airport_basic_distance'] = $_POST['to_airport_basic_distance'];
		$vehicle_meta['to_airport_basic_price'] = $_POST['to_airport_basic_price'];
		$vehicle_meta['to_airport_unit_price'] = $_POST['to_airport_unit_price'];

		$vehicle_meta['from_airport_basic_distance'] = $_POST['from_airport_basic_distance'];
		$vehicle_meta['from_airport_basic_price'] = $_POST['from_airport_basic_price'];
		$vehicle_meta['from_airport_unit_price'] = $_POST['from_airport_unit_price'];

		/**
		 * @since 2.0.0
		*/
		$vehicle_meta['number_of_vehicles'] = $_POST['number_of_vehicles'];
		
		$vehicle_meta['seating_capacity'] = $_POST['seating_capacity'];
		$vehicle_meta['luggage'] = $_POST['luggage'];
		$vehicle_meta['luggage_type'] = $_POST['luggage_type'];
		$vehicle_meta['luggage_type_symbol'] = $_POST['luggage_type_symbol'];
		
		$vehicle_meta['luggage2'] = $_POST['luggage2'];
		$vehicle_meta['luggage2_type'] = $_POST['luggage2_type'];
		$vehicle_meta['luggage2_type_symbol'] = $_POST['luggage2_type_symbol'];

		$vehicle_meta['vehicle_image_gallery'] = $_POST['vehicle_image_gallery'];

        // Add values of $cabs_meta as custom fields
        foreach ( $vehicle_meta as $key => $value ) { // Cycle through the $vehicle_meta array!
            if ( $post->post_type == 'revision' ) {
				return; // Don't store custom data twice	
			}
            $value = implode( ',', ( array ) $value ); // If $value is an array, make it a CSV (unlikely)
            if ( get_post_meta( $post->ID, $key, FALSE ) ) { // If the custom field already has a value
                update_post_meta( $post->ID, $key, $value );
            } else { // If the custom field doesn't have a value
                add_post_meta( $post->ID, $key, $value );
            }
            if ( ! $value ) {
				delete_post_meta( $post->ID, $key ); // Delete if blank
			}
        }
    }

}
add_action( 'save_post', 'simontaxi_save_vehicle_meta', 1, 2 ); // save the custom fields